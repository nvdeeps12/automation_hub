from bs4 import BeautifulSoup
import requests
import re
import sys
import xlwt
import xlrd
from selenium import webdriver

# options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# options.add_argument('--headless')
# driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver',options=options)

rd = xlrd.open_workbook('.\Inputs\List Needed.xlsx')
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('Sheet1')
sheet1.write(0, 0, 'Website')
sheet1.write(0, 1, 'Emails')
sheet = rd.sheet_by_index(0)
sheet.cell_value(0, 0)



class get_mails():
    def write_website(self):
        self.row=0
        for i in range(sheet.nrows):
            if 'WEBSITE' in sheet.cell_value(i,9):
                continue
            else:
                web=sheet.cell_value(i,9)
                web='https://'+web
                self.get_soup(web)
                self.row = self.row + 1
    def get_soup(self,web):
        try:
            res = requests.get(web,headers =headers)
            print('Website-->>',web)
            print('Status_Code-->>', res.status_code)
            soup = BeautifulSoup(res.content, 'html.parser').text
            self.email = soup.split('info@')[1].split(' ')[0]
            self.email ='info@'+str(self.email)
            if '@' not in self.email:
                self.email = 'None'
        except:
            pass
            self.email='None'

        print('Email-->>>',self.email)
        if '@' in self.email:
            print('Done->>',self.row)

        if self.row ==0:
            self.row = 1

        print('--'*100)
        sheet1.write(self.row, 0, web)
        sheet1.write(self.row, 1, self.email)
        # wb.save('Emails_Bulk.xls')

obj = get_mails()
obj.write_website()