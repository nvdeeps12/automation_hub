-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2019 at 05:38 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `external_capture`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `address_line_1` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `country_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_address`
--

INSERT INTO `t_address` (`id`, `address_line_1`, `country_id`) VALUES
(-1, NULL, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_business_sector`
--

CREATE TABLE `t_business_sector` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_business_sector`
--

INSERT INTO `t_business_sector` (`id`, `name`) VALUES
(-1, 'unknown'),
(100001, 'Financial services'),
(100002, 'Medical');

-- --------------------------------------------------------

--
-- Table structure for table `t_business_service`
--

CREATE TABLE `t_business_service` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_business_service`
--

INSERT INTO `t_business_service` (`id`, `name`) VALUES
(-1, 'unknown');

-- --------------------------------------------------------

--
-- Table structure for table `t_business_size`
--

CREATE TABLE `t_business_size` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `business_valuation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_business_size`
--

INSERT INTO `t_business_size` (`id`, `name`, `business_valuation`) VALUES
(-1, 'unknown', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact`
--

CREATE TABLE `t_contact` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `clutch_url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `clutch_id` int(11) NOT NULL DEFAULT '-1',
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact`
--

INSERT INTO `t_contact` (`id`, `company_name`, `clutch_url`, `clutch_id`, `rec_added_on`, `rec_updated_on`) VALUES
(-1, 'division-labor', 'https://clutch.co/profile/division-labor', 4749228, '2019-11-16 12:43:49', '2019-11-17 06:55:37'),
(2, 'viewstream', 'https://clutch.co/profile/viewstream', 2178692, '2019-11-20 22:03:11', '2019-11-20 22:03:11'),
(100001, 'dev_test_1', NULL, -1, '2019-11-17 06:53:04', '2019-11-17 06:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_link_business_sector_supplier`
--

CREATE TABLE `t_contact_link_business_sector_supplier` (
  `id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `business_sector_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_link_business_sector_supplier`
--

INSERT INTO `t_contact_link_business_sector_supplier` (`id`, `contact_id`, `business_sector_id`) VALUES
(-1, -1, -1),
(2, 2, 100002),
(100001, 100001, 100001);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_link_business_sector_target_client`
--

CREATE TABLE `t_contact_link_business_sector_target_client` (
  `id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `business_sector_id` bigint(20) NOT NULL,
  `target_percentage` int(11) DEFAULT NULL COMMENT AS `% of sales targeted at this client's  business sector `
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_link_business_sector_target_client`
--

INSERT INTO `t_contact_link_business_sector_target_client` (`id`, `contact_id`, `business_sector_id`, `target_percentage`) VALUES
(-1, -1, -1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_link_business_service_supplier`
--

CREATE TABLE `t_contact_link_business_service_supplier` (
  `ìd` bigint(20) NOT NULL,
  `business_service_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_link_business_service_supplier`
--

INSERT INTO `t_contact_link_business_service_supplier` (`ìd`, `business_service_id`, `contact_id`) VALUES
(-1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_link_business_size_target_client`
--

CREATE TABLE `t_contact_link_business_size_target_client` (
  `id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `business_size_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_link_business_size_target_client`
--

INSERT INTO `t_contact_link_business_size_target_client` (`id`, `contact_id`, `business_size_id`) VALUES
(-1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_link_sales_project`
--

CREATE TABLE `t_contact_link_sales_project` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `contact_id` bigint(20) NOT NULL,
  `sales_project_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_link_sales_project`
--

INSERT INTO `t_contact_link_sales_project` (`id`, `contact_id`, `sales_project_id`) VALUES
(-1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_profile`
--

CREATE TABLE `t_contact_profile` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `contact_id` bigint(20) NOT NULL,
  `address_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_profile`
--

INSERT INTO `t_contact_profile` (`id`, `name`, `contact_id`, `address_id`) VALUES
(-1, 'unknown', -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_profile_link_email_address`
--

CREATE TABLE `t_contact_profile_link_email_address` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `email_address_id` bigint(20) NOT NULL DEFAULT '-1',
  `contact_profile_id` bigint(20) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_profile_link_email_address`
--

INSERT INTO `t_contact_profile_link_email_address` (`id`, `email_address_id`, `contact_profile_id`) VALUES
(-1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_profile_link_telephone`
--

CREATE TABLE `t_contact_profile_link_telephone` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `contact_profile_id` bigint(20) NOT NULL,
  `telephone_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_profile_link_telephone`
--

INSERT INTO `t_contact_profile_link_telephone` (`id`, `contact_profile_id`, `telephone_id`) VALUES
(-1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_profile_link_web_url`
--

CREATE TABLE `t_contact_profile_link_web_url` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `contact_profile_id` bigint(20) NOT NULL,
  `web_url_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_contact_profile_link_web_url`
--

INSERT INTO `t_contact_profile_link_web_url` (`id`, `contact_profile_id`, `web_url_id`) VALUES
(-1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `t_country`
--

CREATE TABLE `t_country` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_country`
--

INSERT INTO `t_country` (`id`, `name`) VALUES
(-1, 'unknown');

-- --------------------------------------------------------

--
-- Table structure for table `t_devtest_template_table`
--

CREATE TABLE `t_devtest_template_table` (
  `id` bigint(20) NOT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='template table. These fields shoud appear in every other table.';

-- --------------------------------------------------------

--
-- Table structure for table `t_email_address`
--

CREATE TABLE `t_email_address` (
  `id` bigint(20) NOT NULL,
  `email_address` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email_designation_id` bigint(20) DEFAULT NULL,
  `designation_custom` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_email_address`
--

INSERT INTO `t_email_address` (`id`, `email_address`, `email_designation_id`, `designation_custom`) VALUES
(-1, 'unknown', -1, 'unknown');

-- --------------------------------------------------------

--
-- Table structure for table `t_email_address_designation`
--

CREATE TABLE `t_email_address_designation` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_email_address_designation`
--

INSERT INTO `t_email_address_designation` (`id`, `name`, `rec_added_on`, `rec_updated_on`, `rec_custom_tag`) VALUES
(-1, 'unknown', '2019-11-16 12:54:01', '2019-11-16 12:54:01', NULL),
(100001, 'main enquiries', '2019-11-16 13:11:40', '2019-11-16 13:11:40', NULL),
(100002, 'support', '2019-11-16 13:11:40', '2019-11-16 13:11:40', NULL),
(100003, 'sales', '2019-11-16 13:11:40', '2019-11-16 13:11:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_sales_project`
--

CREATE TABLE `t_sales_project` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `client_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `started_on` datetime DEFAULT NULL,
  `finished_on` datetime DEFAULT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='case study for projects sold by a supplier to clients. Gives examples of work completed';

--
-- Dumping data for table `t_sales_project`
--

INSERT INTO `t_sales_project` (`id`, `name`, `description`, `client_name`, `started_on`, `finished_on`, `rec_added_on`, `rec_updated_on`, `rec_custom_tag`) VALUES
(-1, 'unknown', 'unknown', 'unknown', NULL, NULL, '2019-11-17 06:51:11', '2019-11-17 06:51:11', NULL),
(100001, 'dev_test_1', 'dev_test_1', 'dev_test_1', NULL, NULL, '2019-11-17 06:51:11', '2019-11-17 06:51:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_telephone`
--

CREATE TABLE `t_telephone` (
  `id` bigint(20) NOT NULL,
  `telephone_number_full` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `telephone_country_code_id` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `telephone_designation_id` bigint(20) DEFAULT NULL,
  `telephone_designation_custom` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_telephone`
--

INSERT INTO `t_telephone` (`id`, `telephone_number_full`, `telephone_country_code_id`, `telephone_designation_id`, `telephone_designation_custom`) VALUES
(-1, '0', '01', -1, 'unknown');

-- --------------------------------------------------------

--
-- Table structure for table `t_telephone_designation`
--

CREATE TABLE `t_telephone_designation` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_telephone_designation`
--

INSERT INTO `t_telephone_designation` (`id`, `name`, `rec_added_on`, `rec_updated_on`, `rec_custom_tag`) VALUES
(-1, 'unknown', '2019-11-16 12:56:03', '2019-11-16 12:56:03', NULL),
(100001, 'main enquries', '2019-11-16 13:10:47', '2019-11-16 13:10:47', NULL),
(100002, 'sales', '2019-11-16 13:10:47', '2019-11-16 13:10:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_web_url`
--

CREATE TABLE `t_web_url` (
  `id` int(11) NOT NULL,
  `web_url_number_full` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `web_url_designation_id` bigint(20) DEFAULT NULL,
  `web_url_designation_custom` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `t_web_url`
--

INSERT INTO `t_web_url` (`id`, `web_url_number_full`, `web_url_designation_id`, `web_url_designation_custom`) VALUES
(-1, 'unknown', -1, 'unknown');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_address`
--
ALTER TABLE `t_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_t_address_t_country_idx` (`country_id`);

--
-- Indexes for table `t_business_sector`
--
ALTER TABLE `t_business_sector`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_business_service`
--
ALTER TABLE `t_business_service`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `t_business_size`
--
ALTER TABLE `t_business_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_contact`
--
ALTER TABLE `t_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_contact_link_business_sector_supplier`
--
ALTER TABLE `t_contact_link_business_sector_supplier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  ADD UNIQUE KEY `business_sector_id_UNIQUE` (`business_sector_id`),
  ADD KEY `fk_t_contact_link_business_sector_t_business_sector_idx` (`business_sector_id`),
  ADD KEY `fk_t_contact_link_business_sector_t_contact_idx` (`contact_id`);

--
-- Indexes for table `t_contact_link_business_sector_target_client`
--
ALTER TABLE `t_contact_link_business_sector_target_client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_sector_id_UNIQUE` (`business_sector_id`),
  ADD UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  ADD KEY `fk_t_contact_link_business_sector_t_business_sector_idx` (`business_sector_id`),
  ADD KEY `fk_t_contact_link_business_sector_t_contact_idx` (`contact_id`);

--
-- Indexes for table `t_contact_link_business_service_supplier`
--
ALTER TABLE `t_contact_link_business_service_supplier`
  ADD PRIMARY KEY (`ìd`),
  ADD UNIQUE KEY `business_service_id_UNIQUE` (`business_service_id`),
  ADD UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  ADD KEY `fk_t_business_services_has_t_contact_t_contact1_idx` (`contact_id`),
  ADD KEY `fk_t_business_services_has_t_contact_t_business_services1_idx` (`business_service_id`);

--
-- Indexes for table `t_contact_link_business_size_target_client`
--
ALTER TABLE `t_contact_link_business_size_target_client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  ADD UNIQUE KEY `business_size_id_UNIQUE` (`business_size_id`),
  ADD KEY `fk_t_contact_link_business_size_target_client_t_business_si_idx` (`business_size_id`),
  ADD KEY `fk_t_contact_link_business_size_target_client_t_contact1_idx` (`contact_id`);

--
-- Indexes for table `t_contact_link_sales_project`
--
ALTER TABLE `t_contact_link_sales_project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  ADD UNIQUE KEY `sales_project_id_UNIQUE` (`sales_project_id`),
  ADD KEY `fk_t_contact_link_sales_project_t_sales_project_idx` (`sales_project_id`),
  ADD KEY `fk_t_contact_link_sales_project_t_contact_idx` (`contact_id`);

--
-- Indexes for table `t_contact_profile`
--
ALTER TABLE `t_contact_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `address_id_UNIQUE` (`address_id`),
  ADD UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  ADD KEY `fk_t_contact_site_t_address_idx` (`address_id`);

--
-- Indexes for table `t_contact_profile_link_email_address`
--
ALTER TABLE `t_contact_profile_link_email_address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_profile_id_UNIQUE` (`contact_profile_id`),
  ADD UNIQUE KEY `email_address_id_UNIQUE` (`email_address_id`),
  ADD KEY `fk_t_email_address_has_t_contact_profile_t_contact_profile1_idx` (`contact_profile_id`),
  ADD KEY `fk_t_email_address_has_t_contact_profile_t_email_address1_idx` (`email_address_id`);

--
-- Indexes for table `t_contact_profile_link_telephone`
--
ALTER TABLE `t_contact_profile_link_telephone`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_profile_id_UNIQUE` (`contact_profile_id`),
  ADD UNIQUE KEY `telephone_id_UNIQUE` (`telephone_id`),
  ADD KEY `fk_t_contact_profile_has_t_telephone_t_telephone1_idx` (`telephone_id`),
  ADD KEY `fk_t_contact_profile_has_t_telephone_t_contact_profile1_idx` (`contact_profile_id`);

--
-- Indexes for table `t_contact_profile_link_web_url`
--
ALTER TABLE `t_contact_profile_link_web_url`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_profile_id_UNIQUE` (`contact_profile_id`),
  ADD UNIQUE KEY `web_url_id_UNIQUE` (`web_url_id`),
  ADD KEY `fk_t_contact_profile_link_web_url_t_web_url_idx` (`web_url_id`),
  ADD KEY `fk_t_contact_profile_link_web_url_t_contact_profile_idx` (`contact_profile_id`);

--
-- Indexes for table `t_country`
--
ALTER TABLE `t_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_devtest_template_table`
--
ALTER TABLE `t_devtest_template_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_email_address`
--
ALTER TABLE `t_email_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_email_address_designation`
--
ALTER TABLE `t_email_address_designation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `t_sales_project`
--
ALTER TABLE `t_sales_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_telephone`
--
ALTER TABLE `t_telephone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_telephone_designation`
--
ALTER TABLE `t_telephone_designation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `t_web_url`
--
ALTER TABLE `t_web_url`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_contact_link_business_sector_supplier`
--
ALTER TABLE `t_contact_link_business_sector_supplier`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100002;
--
-- AUTO_INCREMENT for table `t_contact_link_business_sector_target_client`
--
ALTER TABLE `t_contact_link_business_sector_target_client`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_contact_link_business_service_supplier`
--
ALTER TABLE `t_contact_link_business_service_supplier`
  MODIFY `ìd` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_contact_link_business_size_target_client`
--
ALTER TABLE `t_contact_link_business_size_target_client`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_address`
--
ALTER TABLE `t_address`
  ADD CONSTRAINT `fk_t_address_t_country` FOREIGN KEY (`country_id`) REFERENCES `t_country` (`id`);

--
-- Constraints for table `t_contact_link_business_sector_supplier`
--
ALTER TABLE `t_contact_link_business_sector_supplier`
  ADD CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_business_sector` FOREIGN KEY (`business_sector_id`) REFERENCES `t_business_sector` (`id`),
  ADD CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_contact` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`);

--
-- Constraints for table `t_contact_link_business_sector_target_client`
--
ALTER TABLE `t_contact_link_business_sector_target_client`
  ADD CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_business_sector0` FOREIGN KEY (`business_sector_id`) REFERENCES `t_business_sector` (`id`),
  ADD CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_contact0` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`);

--
-- Constraints for table `t_contact_link_business_service_supplier`
--
ALTER TABLE `t_contact_link_business_service_supplier`
  ADD CONSTRAINT `fk_t_business_services_has_t_contact_t_business_services1` FOREIGN KEY (`business_service_id`) REFERENCES `t_business_service` (`id`),
  ADD CONSTRAINT `fk_t_business_services_has_t_contact_t_contact1` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`);

--
-- Constraints for table `t_contact_link_business_size_target_client`
--
ALTER TABLE `t_contact_link_business_size_target_client`
  ADD CONSTRAINT `fk_t_contact_link_business_size_target_client_t_business_size1` FOREIGN KEY (`business_size_id`) REFERENCES `t_business_size` (`id`),
  ADD CONSTRAINT `fk_t_contact_link_business_size_target_client_t_contact1` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`);

--
-- Constraints for table `t_contact_link_sales_project`
--
ALTER TABLE `t_contact_link_sales_project`
  ADD CONSTRAINT `fk_t_contact_link_sales_project_t_contact` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`),
  ADD CONSTRAINT `fk_t_contact_link_sales_project_t_sales_project` FOREIGN KEY (`sales_project_id`) REFERENCES `t_sales_project` (`id`);

--
-- Constraints for table `t_contact_profile`
--
ALTER TABLE `t_contact_profile`
  ADD CONSTRAINT `fk_t_contact_site_t_address1` FOREIGN KEY (`address_id`) REFERENCES `t_address` (`id`);

--
-- Constraints for table `t_contact_profile_link_email_address`
--
ALTER TABLE `t_contact_profile_link_email_address`
  ADD CONSTRAINT `fk_t_email_address_has_t_contact_profile_t_contact_profile1` FOREIGN KEY (`contact_profile_id`) REFERENCES `t_contact_profile` (`id`),
  ADD CONSTRAINT `fk_t_email_address_has_t_contact_profile_t_email_address1` FOREIGN KEY (`email_address_id`) REFERENCES `t_email_address` (`id`);

--
-- Constraints for table `t_contact_profile_link_telephone`
--
ALTER TABLE `t_contact_profile_link_telephone`
  ADD CONSTRAINT `fk_t_contact_profile_link_telephone_t_contact_profile` FOREIGN KEY (`contact_profile_id`) REFERENCES `t_contact_profile` (`id`);

--
-- Constraints for table `t_contact_profile_link_web_url`
--
ALTER TABLE `t_contact_profile_link_web_url`
  ADD CONSTRAINT `fk_t_contact_profile_has_t_web_url_t_contact_profile` FOREIGN KEY (`contact_profile_id`) REFERENCES `t_contact_profile` (`id`),
  ADD CONSTRAINT `fk_t_contact_profile_link_web_url_t_web_url` FOREIGN KEY (`web_url_id`) REFERENCES `t_web_url` (`id`);

--
-- Constraints for table `t_email_address`
--
ALTER TABLE `t_email_address`
  ADD CONSTRAINT `fk_t_email_address_email_t_address_designation_id` FOREIGN KEY (`id`) REFERENCES `t_email_address_designation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_telephone`
--
ALTER TABLE `t_telephone`
  ADD CONSTRAINT `fk_t_telephone_telephone_designation_id` FOREIGN KEY (`id`) REFERENCES `t_telephone_designation` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
