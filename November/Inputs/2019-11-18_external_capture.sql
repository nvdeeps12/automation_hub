CREATE DATABASE  IF NOT EXISTS `external_capture` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `external_capture`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: external_capture
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_address`
--

DROP TABLE IF EXISTS `t_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_address` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `address_line_1` varchar(100) DEFAULT NULL,
  `country_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_t_address_t_country_idx` (`country_id`),
  CONSTRAINT `fk_t_address_t_country` FOREIGN KEY (`country_id`) REFERENCES `t_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_address`
--

LOCK TABLES `t_address` WRITE;
/*!40000 ALTER TABLE `t_address` DISABLE KEYS */;
INSERT INTO `t_address` VALUES (-1,NULL,-1);
/*!40000 ALTER TABLE `t_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_business_sector`
--

DROP TABLE IF EXISTS `t_business_sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_business_sector` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_business_sector`
--

LOCK TABLES `t_business_sector` WRITE;
/*!40000 ALTER TABLE `t_business_sector` DISABLE KEYS */;
INSERT INTO `t_business_sector` VALUES (-1,'unknown'),(100001,'Financial services'),(100002,'Medical');
/*!40000 ALTER TABLE `t_business_sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_business_service`
--

DROP TABLE IF EXISTS `t_business_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_business_service` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_business_service`
--

LOCK TABLES `t_business_service` WRITE;
/*!40000 ALTER TABLE `t_business_service` DISABLE KEYS */;
INSERT INTO `t_business_service` VALUES (-1,'unknown');
/*!40000 ALTER TABLE `t_business_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_business_size`
--

DROP TABLE IF EXISTS `t_business_size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_business_size` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `business_valuation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_business_size`
--

LOCK TABLES `t_business_size` WRITE;
/*!40000 ALTER TABLE `t_business_size` DISABLE KEYS */;
INSERT INTO `t_business_size` VALUES (-1,'unknown',0);
/*!40000 ALTER TABLE `t_business_size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact`
--

DROP TABLE IF EXISTS `t_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `clutch_url` varchar(255) DEFAULT NULL,
  `clutch_id` int(11) NOT NULL DEFAULT '-1',
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact`
--

LOCK TABLES `t_contact` WRITE;
/*!40000 ALTER TABLE `t_contact` DISABLE KEYS */;
INSERT INTO `t_contact` VALUES (-1,'unknown',NULL,-1,'2019-11-16 12:43:49','2019-11-17 06:55:37'),(100001,'dev_test_1',NULL,-1,'2019-11-17 06:53:04','2019-11-17 06:55:37');
/*!40000 ALTER TABLE `t_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_link_business_sector_supplier`
--

DROP TABLE IF EXISTS `t_contact_link_business_sector_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_link_business_sector_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_id` bigint(20) NOT NULL,
  `business_sector_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  UNIQUE KEY `business_sector_id_UNIQUE` (`business_sector_id`),
  KEY `fk_t_contact_link_business_sector_t_business_sector_idx` (`business_sector_id`),
  KEY `fk_t_contact_link_business_sector_t_contact_idx` (`contact_id`),
  CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_business_sector` FOREIGN KEY (`business_sector_id`) REFERENCES `t_business_sector` (`id`),
  CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_contact` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100002 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_link_business_sector_supplier`
--

LOCK TABLES `t_contact_link_business_sector_supplier` WRITE;
/*!40000 ALTER TABLE `t_contact_link_business_sector_supplier` DISABLE KEYS */;
INSERT INTO `t_contact_link_business_sector_supplier` VALUES (-1,-1,-1),(100001,100001,100001);
/*!40000 ALTER TABLE `t_contact_link_business_sector_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_link_business_sector_target_client`
--

DROP TABLE IF EXISTS `t_contact_link_business_sector_target_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_link_business_sector_target_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_id` bigint(20) NOT NULL,
  `business_sector_id` bigint(20) NOT NULL,
  `target_percentage` int(11) DEFAULT NULL COMMENT '% of sales targeted at this client''s  business sector ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `business_sector_id_UNIQUE` (`business_sector_id`),
  UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  KEY `fk_t_contact_link_business_sector_t_business_sector_idx` (`business_sector_id`),
  KEY `fk_t_contact_link_business_sector_t_contact_idx` (`contact_id`),
  CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_business_sector0` FOREIGN KEY (`business_sector_id`) REFERENCES `t_business_sector` (`id`),
  CONSTRAINT `fk_t_contact_link_business_sector_supplier_t_contact0` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_link_business_sector_target_client`
--

LOCK TABLES `t_contact_link_business_sector_target_client` WRITE;
/*!40000 ALTER TABLE `t_contact_link_business_sector_target_client` DISABLE KEYS */;
INSERT INTO `t_contact_link_business_sector_target_client` VALUES (-1,-1,-1,NULL);
/*!40000 ALTER TABLE `t_contact_link_business_sector_target_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_link_business_service_supplier`
--

DROP TABLE IF EXISTS `t_contact_link_business_service_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_link_business_service_supplier` (
  `ìd` bigint(20) NOT NULL AUTO_INCREMENT,
  `business_service_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ìd`),
  UNIQUE KEY `business_service_id_UNIQUE` (`business_service_id`),
  UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  KEY `fk_t_business_services_has_t_contact_t_contact1_idx` (`contact_id`),
  KEY `fk_t_business_services_has_t_contact_t_business_services1_idx` (`business_service_id`),
  CONSTRAINT `fk_t_business_services_has_t_contact_t_business_services1` FOREIGN KEY (`business_service_id`) REFERENCES `t_business_service` (`id`),
  CONSTRAINT `fk_t_business_services_has_t_contact_t_contact1` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_link_business_service_supplier`
--

LOCK TABLES `t_contact_link_business_service_supplier` WRITE;
/*!40000 ALTER TABLE `t_contact_link_business_service_supplier` DISABLE KEYS */;
INSERT INTO `t_contact_link_business_service_supplier` VALUES (-1,-1,-1);
/*!40000 ALTER TABLE `t_contact_link_business_service_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_link_business_size_target_client`
--

DROP TABLE IF EXISTS `t_contact_link_business_size_target_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_link_business_size_target_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_id` bigint(20) NOT NULL,
  `business_size_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  UNIQUE KEY `business_size_id_UNIQUE` (`business_size_id`),
  KEY `fk_t_contact_link_business_size_target_client_t_business_si_idx` (`business_size_id`),
  KEY `fk_t_contact_link_business_size_target_client_t_contact1_idx` (`contact_id`),
  CONSTRAINT `fk_t_contact_link_business_size_target_client_t_business_size1` FOREIGN KEY (`business_size_id`) REFERENCES `t_business_size` (`id`),
  CONSTRAINT `fk_t_contact_link_business_size_target_client_t_contact1` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_link_business_size_target_client`
--

LOCK TABLES `t_contact_link_business_size_target_client` WRITE;
/*!40000 ALTER TABLE `t_contact_link_business_size_target_client` DISABLE KEYS */;
INSERT INTO `t_contact_link_business_size_target_client` VALUES (-1,-1,-1);
/*!40000 ALTER TABLE `t_contact_link_business_size_target_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_link_sales_project`
--

DROP TABLE IF EXISTS `t_contact_link_sales_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_link_sales_project` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `contact_id` bigint(20) NOT NULL,
  `sales_project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  UNIQUE KEY `sales_project_id_UNIQUE` (`sales_project_id`),
  KEY `fk_t_contact_link_sales_project_t_sales_project_idx` (`sales_project_id`),
  KEY `fk_t_contact_link_sales_project_t_contact_idx` (`contact_id`),
  CONSTRAINT `fk_t_contact_link_sales_project_t_contact` FOREIGN KEY (`contact_id`) REFERENCES `t_contact` (`id`),
  CONSTRAINT `fk_t_contact_link_sales_project_t_sales_project` FOREIGN KEY (`sales_project_id`) REFERENCES `t_sales_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_link_sales_project`
--

LOCK TABLES `t_contact_link_sales_project` WRITE;
/*!40000 ALTER TABLE `t_contact_link_sales_project` DISABLE KEYS */;
INSERT INTO `t_contact_link_sales_project` VALUES (-1,-1,-1);
/*!40000 ALTER TABLE `t_contact_link_sales_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_profile`
--

DROP TABLE IF EXISTS `t_contact_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_profile` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `contact_id` bigint(20) NOT NULL,
  `address_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address_id_UNIQUE` (`address_id`),
  UNIQUE KEY `contact_id_UNIQUE` (`contact_id`),
  KEY `fk_t_contact_site_t_address_idx` (`address_id`),
  CONSTRAINT `fk_t_contact_site_t_address1` FOREIGN KEY (`address_id`) REFERENCES `t_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_profile`
--

LOCK TABLES `t_contact_profile` WRITE;
/*!40000 ALTER TABLE `t_contact_profile` DISABLE KEYS */;
INSERT INTO `t_contact_profile` VALUES (-1,'unknown',-1,-1);
/*!40000 ALTER TABLE `t_contact_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_profile_link_email_address`
--

DROP TABLE IF EXISTS `t_contact_profile_link_email_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_profile_link_email_address` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `email_address_id` bigint(20) NOT NULL DEFAULT '-1',
  `contact_profile_id` bigint(20) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_profile_id_UNIQUE` (`contact_profile_id`),
  UNIQUE KEY `email_address_id_UNIQUE` (`email_address_id`),
  KEY `fk_t_email_address_has_t_contact_profile_t_contact_profile1_idx` (`contact_profile_id`),
  KEY `fk_t_email_address_has_t_contact_profile_t_email_address1_idx` (`email_address_id`),
  CONSTRAINT `fk_t_email_address_has_t_contact_profile_t_contact_profile1` FOREIGN KEY (`contact_profile_id`) REFERENCES `t_contact_profile` (`id`),
  CONSTRAINT `fk_t_email_address_has_t_contact_profile_t_email_address1` FOREIGN KEY (`email_address_id`) REFERENCES `t_email_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_profile_link_email_address`
--

LOCK TABLES `t_contact_profile_link_email_address` WRITE;
/*!40000 ALTER TABLE `t_contact_profile_link_email_address` DISABLE KEYS */;
INSERT INTO `t_contact_profile_link_email_address` VALUES (-1,-1,-1);
/*!40000 ALTER TABLE `t_contact_profile_link_email_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_profile_link_telephone`
--

DROP TABLE IF EXISTS `t_contact_profile_link_telephone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_profile_link_telephone` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `contact_profile_id` bigint(20) NOT NULL,
  `telephone_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_profile_id_UNIQUE` (`contact_profile_id`),
  UNIQUE KEY `telephone_id_UNIQUE` (`telephone_id`),
  KEY `fk_t_contact_profile_has_t_telephone_t_telephone1_idx` (`telephone_id`),
  KEY `fk_t_contact_profile_has_t_telephone_t_contact_profile1_idx` (`contact_profile_id`),
  CONSTRAINT `fk_t_contact_profile_link_telephone_t_contact_profile` FOREIGN KEY (`contact_profile_id`) REFERENCES `t_contact_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_profile_link_telephone`
--

LOCK TABLES `t_contact_profile_link_telephone` WRITE;
/*!40000 ALTER TABLE `t_contact_profile_link_telephone` DISABLE KEYS */;
INSERT INTO `t_contact_profile_link_telephone` VALUES (-1,-1,-1);
/*!40000 ALTER TABLE `t_contact_profile_link_telephone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contact_profile_link_web_url`
--

DROP TABLE IF EXISTS `t_contact_profile_link_web_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_contact_profile_link_web_url` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `contact_profile_id` bigint(20) NOT NULL,
  `web_url_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_profile_id_UNIQUE` (`contact_profile_id`),
  UNIQUE KEY `web_url_id_UNIQUE` (`web_url_id`),
  KEY `fk_t_contact_profile_link_web_url_t_web_url_idx` (`web_url_id`),
  KEY `fk_t_contact_profile_link_web_url_t_contact_profile_idx` (`contact_profile_id`),
  CONSTRAINT `fk_t_contact_profile_has_t_web_url_t_contact_profile` FOREIGN KEY (`contact_profile_id`) REFERENCES `t_contact_profile` (`id`),
  CONSTRAINT `fk_t_contact_profile_link_web_url_t_web_url` FOREIGN KEY (`web_url_id`) REFERENCES `t_web_url` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contact_profile_link_web_url`
--

LOCK TABLES `t_contact_profile_link_web_url` WRITE;
/*!40000 ALTER TABLE `t_contact_profile_link_web_url` DISABLE KEYS */;
INSERT INTO `t_contact_profile_link_web_url` VALUES (-1,-1,-1);
/*!40000 ALTER TABLE `t_contact_profile_link_web_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_country`
--

DROP TABLE IF EXISTS `t_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_country` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_country`
--

LOCK TABLES `t_country` WRITE;
/*!40000 ALTER TABLE `t_country` DISABLE KEYS */;
INSERT INTO `t_country` VALUES (-1,'unknown');
/*!40000 ALTER TABLE `t_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_devtest_template_table`
--

DROP TABLE IF EXISTS `t_devtest_template_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_devtest_template_table` (
  `id` bigint(20) NOT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='template table. These fields shoud appear in every other table.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_devtest_template_table`
--

LOCK TABLES `t_devtest_template_table` WRITE;
/*!40000 ALTER TABLE `t_devtest_template_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_devtest_template_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_email_address`
--

DROP TABLE IF EXISTS `t_email_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_email_address` (
  `id` bigint(20) NOT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `email_designation_id` bigint(20) DEFAULT NULL,
  `designation_custom` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_t_email_address_email_t_address_designation_id` FOREIGN KEY (`id`) REFERENCES `t_email_address_designation` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_email_address`
--

LOCK TABLES `t_email_address` WRITE;
/*!40000 ALTER TABLE `t_email_address` DISABLE KEYS */;
INSERT INTO `t_email_address` VALUES (-1,'unknown',-1,'unknown');
/*!40000 ALTER TABLE `t_email_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_email_address_designation`
--

DROP TABLE IF EXISTS `t_email_address_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_email_address_designation` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `name` varchar(100) NOT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_email_address_designation`
--

LOCK TABLES `t_email_address_designation` WRITE;
/*!40000 ALTER TABLE `t_email_address_designation` DISABLE KEYS */;
INSERT INTO `t_email_address_designation` VALUES (-1,'unknown','2019-11-16 12:54:01','2019-11-16 12:54:01',NULL),(100001,'main enquiries','2019-11-16 13:11:40','2019-11-16 13:11:40',NULL),(100002,'support','2019-11-16 13:11:40','2019-11-16 13:11:40',NULL),(100003,'sales','2019-11-16 13:11:40','2019-11-16 13:11:40',NULL);
/*!40000 ALTER TABLE `t_email_address_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sales_project`
--

DROP TABLE IF EXISTS `t_sales_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_sales_project` (
  `id` bigint(20) NOT NULL DEFAULT '100001',
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `client_name` varchar(100) DEFAULT NULL,
  `started_on` datetime DEFAULT NULL,
  `finished_on` datetime DEFAULT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='case study for projects sold by a supplier to clients. Gives examples of work completed';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sales_project`
--

LOCK TABLES `t_sales_project` WRITE;
/*!40000 ALTER TABLE `t_sales_project` DISABLE KEYS */;
INSERT INTO `t_sales_project` VALUES (-1,'unknown','unknown','unknown',NULL,NULL,'2019-11-17 06:51:11','2019-11-17 06:51:11',NULL),(100001,'dev_test_1','dev_test_1','dev_test_1',NULL,NULL,'2019-11-17 06:51:11','2019-11-17 06:51:11',NULL);
/*!40000 ALTER TABLE `t_sales_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_telephone`
--

DROP TABLE IF EXISTS `t_telephone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_telephone` (
  `id` bigint(20) NOT NULL,
  `telephone_number_full` varchar(45) DEFAULT NULL,
  `telephone_country_code_id` varchar(45) DEFAULT NULL,
  `telephone_designation_id` bigint(20) DEFAULT NULL,
  `telephone_designation_custom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_t_telephone_telephone_designation_id` FOREIGN KEY (`id`) REFERENCES `t_telephone_designation` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_telephone`
--

LOCK TABLES `t_telephone` WRITE;
/*!40000 ALTER TABLE `t_telephone` DISABLE KEYS */;
INSERT INTO `t_telephone` VALUES (-1,'0','01',-1,'unknown');
/*!40000 ALTER TABLE `t_telephone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_telephone_designation`
--

DROP TABLE IF EXISTS `t_telephone_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_telephone_designation` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) NOT NULL,
  `rec_added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `rec_custom_tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_telephone_designation`
--

LOCK TABLES `t_telephone_designation` WRITE;
/*!40000 ALTER TABLE `t_telephone_designation` DISABLE KEYS */;
INSERT INTO `t_telephone_designation` VALUES (-1,'unknown','2019-11-16 12:56:03','2019-11-16 12:56:03',NULL),(100001,'main enquries','2019-11-16 13:10:47','2019-11-16 13:10:47',NULL),(100002,'sales','2019-11-16 13:10:47','2019-11-16 13:10:47',NULL);
/*!40000 ALTER TABLE `t_telephone_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_web_url`
--

DROP TABLE IF EXISTS `t_web_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `t_web_url` (
  `id` int(11) NOT NULL,
  `web_url_number_full` varchar(45) DEFAULT NULL,
  `web_url_designation_id` bigint(20) DEFAULT NULL,
  `web_url_designation_custom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_web_url`
--

LOCK TABLES `t_web_url` WRITE;
/*!40000 ALTER TABLE `t_web_url` DISABLE KEYS */;
INSERT INTO `t_web_url` VALUES (-1,'unknown',-1,'unknown');
/*!40000 ALTER TABLE `t_web_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'external_capture'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-18 11:26:54
