from bs4 import BeautifulSoup
import xlwt
from selenium import webdriver
import time
wb = xlwt.Workbook()
options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='F:\chromedriver',options=options)
driver.get('https://www.ballertv.com/events/atlantic-city-showcase?sport=basketball')

click =driver.find_element_by_xpath('//*[@id="tab-event-replay"]').click()
time.sleep(5)
lenOfPage = driver.execute_script("window.scrollBy(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
match=False
count=0
while(count==10):
    count=count+1
    click2= driver.find_element_by_xpath('//*[@id="cta-show-more-past"]').click()
    if count<10:
        soup = BeautifulSoup(driver.page_source, u"html.parser")
        main=soup.find('div',attrs={'id':'past-streams-list'})
        Team1=main.find_all('span',attrs={'class':'text-span--team-1'})
        Team2=main.find_all('span',attrs={'class':'text-span--team-2'})

        Score=main.find_all('p',attrs={'class':'detail--score'})

        time=main.find_all('time')

        location=main.find_all('p',attrs={'class':'detail--location'})
        sheet1 = wb.add_sheet('1')
        sheet1.write(0, 0, 'Team1')
        sheet1.write(0, 1, 'Score')
        sheet1.write(0, 2, 'Team2')
        sheet1.write(0, 3, 'Score')
        sheet1.write(0, 4, 'Date')
        sheet1.write(0, 5, 'Location')
        sheet1.write(0, 6, 'Court')

        row = 0
        for j in range(len(time)):
            row = row + 1
            l=location[j].text.strip()
            T1=Team1[j].text.strip()
            T2=Team2[j].text.strip()
            S=Score[j].text.strip()
            t=time[j].text.strip()
            if "Court" in l:
                c=l.split('Court')[1]
                l=l.split('Court')[0]
                sheet1.write(row, 0, T1)
                sheet1.write(row, 1, S)
                sheet1.write(row, 2, T2)
                sheet1.write(row, 3, S)
                sheet1.write(row, 4, t)
                sheet1.write(row, 5, l)
                sheet1.write(row, 6, c)
                # wb.save('team.xls')
        print("Done.......")