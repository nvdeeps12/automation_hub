from time import sleep
from bs4 import BeautifulSoup
import requests,csv
import re,sys
from os import path
import datetime

mail_u = ['@yahoo.com','@gmail.com','@hotmail.com']


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

# categories = ['Animation','Artisan Crafts','Tattoo and Body Art','Design','Traditional','Digital Art','Photography','Sculpture','Street Art','Mixed Media','Poetry','Prose','Screenplays and Scripts','Characters and Settings']
categories = ['Action','Adventure','Abstract','Comedy','Drama','Documentary','Horror','Science Friction','Stock and Effects','Fantasy','Adoptables','Events','Memes','Meta']
# as we know array always start from 0 index
fieldnames=["Mail","Username","Source Category","Domain"]
statt='Y'
first_time = False
previous_page=''

def csv_writer(fil_name,lstData):
        global statt,fieldnames
        fill = 'D:/PycharmProjects/Files/json_outputs/DavianBulk.csv'
        # fill='/home/sky/Desktop/coloriz Project/Apple/DavianBulk.csv'
        print(fill)
        cfile=open(fill, 'a+')
        writer = csv.DictWriter(cfile,fieldnames=fieldnames)
        if statt == 'Y':
            writer.writeheader()
            statt='n'
        writer.writerow({"Mail" : lstData[0],"Username":lstData[1],"Source Category":lstData[2],"Domain":lstData[3]})
        cfile.close()
        print("Successful !") 

def file_checker(email_d):
    fill = 'D:/PycharmProjects/Files/json_outputs/DavianBulk.csv'
    # fill='/home/sky/Desktop/coloriz Project/Apple/DavianBulk.csv'
    cfile=open(fill, 'r')
    statsu="YES"
    csv_reader = csv.reader(cfile)
    for row in csv_reader:
        if row[0].find(email_d) != -1:
            statsu="NO"
            print(" Duplicate !"*10)
            break
        else:
            pass
    cfile.close()
    return statsu        

def timers():
    sleep(60)

def categori(cat_count,mails):
    pages = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290]
    r = 0
    global previous_page
    ordi=''
    for i in pages:
        num_page=i
        print("Page No :",num_page)
        print("Category --> "+cat_count+"            Mail-type --> "+mails)
        try:
            link = 'https://www.google.com/search?q=site:deviantart.com+' + ' ' + str(mails) + '+' + cat_count + '&start=' + str(i)
            print(link)
            
            # This will first process first 0 index link with first category
            res = requests.get(link,headers= headers)
            print("Status : "+str(res.status_code))
            if res.status_code != 200:
                previous_page = i
                timers()
                continue
        except:
            sleep(5)
            continue

        try:
            soup = BeautifulSoup(res.content, u'html.parser')
            # print(soup)
            section = soup.findAll('div',attrs={'class':'rc'})
            row=0
            r=0
            try:
                for i in section:
                    try:
                        row = row + 1
                        user_name = i.find('h3',attrs={'class': 'LC20lb'}).text.strip()
                        user_name=user_name.split('by')[1].split('on')[0]
                        if '@' in user_name:
                            user_name = user_name.split('@')[0]
                        if '.' in user_name:
                            user_name = 'None'

                        print('Username-----',user_name)
                    except:
                        user_name = None

                    try:
                        email =  i.find('span',  attrs={'class': 'st'}).text.split('@')[0].split(' ')[-1]
                        if ':' in email:
                            email = email.split(':')[1]
                        if '.' in email:
                            email = None
                        print('Email----',email)
                        print('___________________________'*10)

                    except:
                        email = None

                    if email != None and (user_name != None or user_name!='None'):
                        email = email + mails
                        email = email.replace('(','').replace(')','').lower()
                        dict_=[]
                        dict_.append(email)

                        if user_name == 'None' or user_name == None:
                            user_name = email.split('@')[0].replace('.',' ').title()

                        dict_.append(user_name.title())
                        dict_.append(cat_count.title())
                        dict_.append(mails.replace('(','').replace(')','').lower())
                        global first_time
                        if first_time:
                            ordi=file_checker(email)
                            if ordi == "YES":
                                pass
                            elif ordi == "NO":
                                sleep(5)
                                break

                        first_time=True
                        csv_writer(cat_count,dict_)
                        print('--' * 100)
                        print(datetime.datetime.now())
                        r = r + 1
                        print('--' * 150)
                    else:
                        pass
                if ordi == 'NO':
                    sleep(5)
                    continue
            # Here our first category will completely finish
            # now it is time to increment our counter by 1 so we can be able to access the next category        
            except:
                print(sys.exc_info())
                sleep(5)
                continue
            sleep(5)

        except:
            print('No Links Collected From->',i)
            sleep(5)
            continue

def for_mails(mails):
    for a in categories:
        categori(a,mails)
        sleep(10)


for a in mail_u:
    for_mails(a)
