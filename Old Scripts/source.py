
import pysolr
import json
SOLR_URL = 'http://52.214.237.114:8983/solr/jobs_ie/select?q=*:*&wt=json&indent=on'

SOLR_USER = "solr"
SOLR_PASSWORD = "jobs123!@#"

def getActiveJobsCount(limit, job_tier):
    active_jobs_count = 0
    try:
        authCredentials = (SOLR_USER, SOLR_PASSWORD)
        conn = pysolr.Solr(SOLR_URL, auth=authCredentials)
        if job_tier != None:
            data = conn.search(**{
                "q": "source_id:* AND job_tier:1",
                "wt": "json",
                "indent": "on",
                "rows": 1,
                "facet.limit": 100,
                "facet": "true",
                "facet.pivot": "job_tier,source_id",
                "facet.sort": "source_id",
                'fl': "facet_counts",
            })
        else:
            data = conn.search(**{
                "q": "source_id:*",
                "wt": "json",
                "indent": "on",
                "rows": 1,
                "facet.limit": limit,
                "facet": "true",
                "facet.pivot": "source_id",
                "facet.sort": "source_id",
                'fl': "facet_counts",
            })

        result = json.dumps(data.__dict__)
        res = json.loads(result)
        active_jobs_count = res['raw_response']['facet_counts']['facet_pivot']
    except:
        print('P')
    return active_jobs_count

job_tier = 1
source_data= getActiveJobsCount(10000,1)
if job_tier != None:
    print('I am in If')
    tier_data = source_data['job_tier,source_id']
    if len(tier_data) > 0:
        tier_pivot = tier_data[0]['pivot']

else:
    tier_data = source_data['source_id']
    if len(tier_data) > 0:
        tier_pivot = tier_data


for i in tier_pivot:
    print(i['value'],'Count-->>',i['count'])
    print(i)



