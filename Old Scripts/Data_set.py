import requests
import xlwt,csv
import xlrd,sys
from bs4 import BeautifulSoup

Pratt = 'Y'
FIELDS = ['Dominio','Dated','Created','CaraTerri','Idn','Vecchio Registrar']
referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }
infile='D:\PycharmProjects\Files\Links_Csv.csv'

def Master_csv_writer(lstData):
    global FIELDS
    global Pratt
    with open('D:\PycharmProjects\Files\Csv_Data.csv', 'a+') as cfile:
        writer = csv.DictWriter(cfile,fieldnames=FIELDS)
        if Pratt == 'Y':
            writer.writeheader()
            Pratt='n'
        writer.writerow({'Dominio':lstData[0],'Dated':lstData[1],'Created':lstData[2],'CaraTerri':lstData[3],'Idn':lstData[4],'Vecchio Registrar':lstData[5]})

counts=0
with open(infile,'r') as csvfile:
    csv_reader = csv.reader(csvfile)
    for row in csv_reader:
        print("Event ID :",row[0])
        try:
            value=row[0]
            print('Process Started For--', value)
            res=requests.get(value,headers=headers)
            soup=BeautifulSoup(res.content,u'html.parser')
            section = soup.find('table', attrs={'class': 'lista'}).findAll('tr')[1:]
            
            new_row = 0
            for res in section:
                if counts==0:
                    counts=1
                    continue
                print(res)
                data_list=[]
                try:
                    name = res.findAll('td')[0].text.strip()
                    print("AAkash --- >",name)
                    data_list.append(name)
                except:
                    name = 'None'
                    data_list.append(name)
                try:
                    add =  res.findAll('td')[1].text.strip()
                    data_list.append(add)
                except:
                    add = 'None'
                    data_list.append(add)

                try:
                    address = res.findAll('td')[2].text.strip()
                    data_list.append(address)
                except:
                    address = 'None'
                    data_list.append(address)

                try:
                    region = res.findAll('td')[3].text.strip()
                    data_list.append(region)
                except:
                    region = ''
                    data_list.append(region)

                try:
                    city = res.findAll('td')[4].text.strip()
                    data_list.append(city)
                except:
                    city = 'None'
                    data_list.append(city)

                try:
                    zip = res.findAll('td')[5].text.strip()
                    data_list.append(zip)
                except:
                    zip = 'None'
                    data_list.append(zip)
                Master_csv_writer(data_list)
        except:
            print(sys.exc_info())
            continue