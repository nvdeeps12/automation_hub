import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')
driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)

states_arr = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'District of Columbia', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'American Samoa', 'Guam', 'Northern Mariana Islands', 'Puerto Rico', 'U.S. Virgin Islands']



class ascp():
    def pass_links(self):

            for i in states_arr:

                try:
                    state_link = 'https://ascp.continuum.host/directory/result.php?State_Licensed_In='+(i)+'&Pharmacist_Provides_Services_To=Seniors+and+Caregivers&Submit=Search&page=1'
                    driver.get(state_link)
                    soup = BeautifulSoup(driver.page_source, u"html.parser")
                    total_pages = soup.find('li',attrs={'class':'details'}).text.strip()
                    total_pages = total_pages.split('of')[-1]
                    total_pages = int(total_pages)
                    total_pages = total_pages+1
                    print(total_pages)
                except:
                    continue

                for p in range(total_pages):
                    page = 'https://ascp.continuum.host/directory/result.php?State_Licensed_In='+(i)+'&Pharmacist_Provides_Services_To=Seniors+and+Caregivers&Submit=Search&page='+str(p)
                    print(page)
                    self.page_link_soup(page)


    def page_link_soup(self,page):
        driver.get(page)
        page_soup = BeautifulSoup(driver.page_source, u"html.parser")
        profile_links = page_soup.find('table',attrs={'class':'table table-striped'}).find('tbody').findAll('a')
        for i in profile_links:
            anchors = i['href']
            profile_link = 'https://ascp.continuum.host/directory/'+str(anchors)

            self.profile_link_soup(profile_link)

    def profile_link_soup(self, profile_link):

        profile_link = profile_link
        driver.get(profile_link)

        profile_soup = BeautifulSoup(driver.page_source, u"html.parser")
        # print(profile_soup)
        try:
            full_name = profile_soup.find('div',attrs={'class':'displayTitle'}).text.strip()
            # print(full_name)
            first_name =full_name.split(' ')[0].strip()
            last_name = full_name.split(' ')[1].strip()
            # print(first_name)
            # print(last_name)
        except:
            full_name = 'None'

        try:
            occupation = profile_soup.find('div',attrs={'class':'displayTitle'}).find_previous('td').text.strip()
            print(occupation)
            print('----'*100)

        except:
            occupation = 'None'







objs = ascp()
objs.pass_links()



