from selenium import webdriver
from bs4 import BeautifulSoup
import xlwt
import xlrd

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'File')
sheet1.write(0, 1, 'Ammont')
sheet1.write(0, 2, 'Address')
sheet1.write(0, 3, 'Bank _name')
sheet1.write(0, 4, 'Loan Servicer')
sheet1.write(0, 5, 'Parcel')
sheet1.write(0, 6, 'Notes')

row=0
rd=xlrd.open_workbook('D:\PycharmProjects\Files\Book1.xlsx')
sheet = rd.sheet_by_index(0)
sheet.cell_value(0, 0)




for i in range(sheet.nrows):
        try:
                data = sheet.cell_value(i,0)
                row = row + 1
                try:
                        file = data.split('-')[-1].split(',')[0]
                        print('File---', file)

                        amount = 0
                        try:
                                amount = data.split('DOLLARS')[1].split('),')[0]
                        except:
                                amount = data.split('$')[1].split('with')[0]
                                if ')' in amount:
                                        amount = amount.split(')')[0]

                        print('Amount--', amount)

                        address = data.split('known as')[1].split('.')[0]
                        if 'together' in address:
                                address = address.split('together')[0]
                        if 'and' in address:
                                address = address.split('and')[0]
                        if 'is' in address:
                                address = address.split('is')[0]

                        print('Address', address)
                        try:
                                bank_name = data.split('transferred to')[1].split(',')[0]
                                if 'or acquired by' in bank_name:
                                        bank_name = bank_name.split('or acquired by')[1]
                                elif 'by' in bank_name:
                                        bank_name = bank_name.split('by')[0]
                        except:
                                bank_name = 'None'
                        print('Bank _name', bank_name)

                        try:
                                parcel = data
                                if 'Parcel ID No.' in data:
                                        parcel = data.split('Tax Parcel ID No.')[1]
                                        parcel = parcel[0:14]

                                elif 'PARCEL ID #' in data:
                                        parcel = data.split('PARCEL ID #')[1]
                                        parcel = parcel[0:14]

                                elif 'Parcel ID #' in data:
                                        parcel = data.split('Parcel ID #')[1]
                                        parcel = parcel[0:14]
                                elif 'PARCEL' in data:
                                        parcel = data.split('PARCEL')[1]
                                        parcel = parcel[0:15]

                                elif 'sk' in parcel:
                                        parcel = parcel.split('sk')[0]

                                parcel = parcel.replace('OF LAND LYING','None')
                        except:
                                parcel = 'None'
                        print('Parcel--', parcel)

                        try:
                                loan_servicer = data.split('the property is')[1]
                                if ',' in loan_servicer:
                                        loan_servicer = loan_servicer.split(',')[0]
                                elif 'or' in loan_servicer:
                                        loan_servicer = loan_servicer.split('or')[0]
                        except:
                                loan_servicer = 'None'
                        print('LOan Serv---', loan_servicer)

                        try:
                                notes = data.split('(1)')[1]
                                notes = '(1)'+str(notes)
                        except:
                                notes = data.split('foreclosure')[1]
                        print('Notes-----',notes)
                except:
                        continue

                sheet1.write(row, 0, file)
                sheet1.write(row, 1, amount)
                sheet1.write(row, 2, address)
                sheet1.write(row, 3, bank_name)
                sheet1.write(row, 4, loan_servicer)
                sheet1.write(row, 5, parcel)
                sheet1.write(row, 6, notes)
                wb.save('D:\PycharmProjects\Files\json_outputs\Georgia_Monday.xls')

                print('--'*100)
                print('Done----',row)
                import datetime
                print('Date----',datetime.datetime.now())
                print('------------'*100)

        except:
                import sys
                print(sys.exc_info())

