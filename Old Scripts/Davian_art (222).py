from selenium import webdriver
import xlwt

import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime


options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
options.add_argument('--proxy-server=%s' % '77.238.79.111')
driver = webdriver.Chrome(executable_path='D:\chromedriver', options=options)
wb = xlwt.Workbook()
# rd = xlrd.open_workbook('D:\PycharmProjects\Files\zillow4500.xls')
# sheet = rd.sheet_by_index(0)



sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Username')
sheet1.write(0, 1, 'Email')

mail = ['@yahoo.com','@gmail.com','@hotmail.com']


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

categories = ['Digital Art','Animation','Design','Traditional']
# as we know array always start from 0 index

row = 0
r = 0
def categori(cat_count,mails):
    pages = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290]
    for i in pages:
        try:
            link = 'https://www.google.com/search?q=site:deviantart.com+' + ' ' + str(mails) + '+' + cat_count + '&start=' + str(i)
            print(link)
            
            # This will first process first 0 index link with first category
            res = requests.get(link,headers= headers)
            import time
        except:
            continue

        try:
            soup = BeautifulSoup(res.content, u'html.parser')
            # print(soup)
            section = soup.findAll('div',attrs={'class':'rc'})
            row=0
            try:
                for i in section:
                    try:
                        row = row + 1
                        user_name = i.find('h3',attrs={'class': 'LC20lb'}).text.strip()
                        user_name=user_name.split('by')[1].split('on')[0]
                        if '@' in user_name:
                            user_name = user_name.split('@')[0]
                        if '.' in user_name:
                            user_name = 'None'

                        print('Username-----',user_name)
                    except:
                        user_name = None

                    try:
                        email =  i.find('span',  attrs={'class': 'st'}).text.split('@')[0].split(' ')[-1]
                        if ':' in email:
                            email = email.split(':')[1]
                        if '.' in email:
                            email = None
                        print('Email----',email)
                        print('___________________________'*10)

                    except:
                        email = None

                    if email and user_name!=None:
                        email = email + str(mail)
                        sheet1.write(row, 0, user_name)
                        sheet1.write(row, 1, email)
                        
                        file_name = mails +"-"+ cat_count+"-"+str(i)+".xls"

                        wb.save('D:\PycharmProjects\Files\json_outputs\\'+(file_name))
                        print('--' * 100)
                        print(datetime.datetime.now())
                        print('--' * 150)
                    else:
                        continue
            # Here our first category will completely finish
            # now it is time to increment our counter by 1 so we can be able to access the next category        
            except:
                continue

        except:
            print('No Links Collected From->',i)
            continue

def for_mails(mails):
    for a in categories:
        categori(a,mails)

for a in mail:
    for_mails(a)
