import Algorithmia

input = {
  "image": "data://deeplearning/example_data/lincoln.jpg"
}
client = Algorithmia.client('YOUR_API_KEY')
algo = client.algo('deeplearning/ColorfulImageColorization/1.1.13')
algo.set_options(timeout=300) # optional
print(algo.pipe(input).result)