# coding: utf-8
# -*- coding: utf-8 -*-
import os, sys
rootPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(rootPath)

import datetime
from Logs.logHandler import LogHelper
import sys
import psycopg2
import psycopg2.extras
import json
import pysolr

DB_NAME = 'ezapi'
DB_USER = 'postgres'
DB_PASSWORD = '12'
DB_HOST = 'localhost'

SOLR_URL = 'http://52.214.237.114:8983/solr/jobs_ie/select?q=*:*&wt=json&indent=on'

SOLR_USER = "solr"
SOLR_PASSWORD = "jobs123!@#"

class CompaniesStatsHelper():
    def __init__(self):
        self.logger = LogHelper().create_rotating_log('job_stats', 'jobstats')
        self.RECORDS_LIMIT = 5
        self.OFFSET = 0
        self.Total_processed_sources = 0
        self.COMPANY_DATA = []

        self.DB_NAME = DB_NAME
        self.DB_USER = DB_USER
        self.DB_PASSWORD = DB_PASSWORD
        self.DB_HOST = DB_HOST

    def log(self, msg, log_type):
        if log_type == 'info':
            self.logger.info(msg)
        if log_type == 'error':
            self.logger.error(msg)
        if log_type == 'warn':
            self.logger.warning(msg)

    def startProcess(self):
        print('started-> ', str(datetime.datetime.now()))
        self.log("going to start script for job stats", 'info')
        start_time = datetime.datetime.now()
        try:
            companies_count = self.getCompaniesCount()
            print("companies_count == ", companies_count)
            if int(companies_count) > 0:
                self.processActiveRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT)
                self.COMPANY_DATA = self.getDailyJobsData(start_time.date())
                self.processJobTierRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT,1)
                self.processJobTierRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT,2)
                self.processJobTierRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT,3)
                self.processJobTierRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT,4)
                self.processJobTierRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT,5)
                self.processJobTierRecords(self.OFFSET, companies_count, self.RECORDS_LIMIT,None)
        except:
            self.log(sys.exc_info(), 'error')
        print('completed_at -> ', str(datetime.datetime.now()))

    def processActiveRecords(self, offset, companies_count, limit):
        msg = 'Going to process active records from company table, companies count is : ' + str(companies_count)
        self.log(msg, 'info')
        try:
            offset = offset
            limit = limit
            is_source = True
            while is_source:
                source_data= self.getCompanyData(offset, limit)

                new_jobs_data = []
                daily_jobs_data = []
                for u in source_data:
                    current_time = datetime.datetime.now()
                    try:
                        company_id = u['company_id']
                        jobs_count = 0
                        tier1 = 0
                        tier2 = 0
                        tier3 = 0
                        tier4 = 0
                        tier5 = 0

                        datatuple = (int(company_id), str(current_time), int(jobs_count), int(tier1), int(tier2), int(tier3),
                                     int(tier4), int(tier5))
                        new_jobs_data.append(datatuple)

                        new_datatuple = (int(company_id), str(current_time), int(tier1), int(tier2), int(tier3),
                                     int(tier4), int(tier5))
                        daily_jobs_data.append(new_datatuple)
                    except:
                        pass

                self.saveJobsStats(new_jobs_data)
                self.saveDailyJobsStats(daily_jobs_data)
                offset += len(source_data)

                self.Total_processed_sources += int(limit)

                if int(self.Total_processed_sources) >= int(companies_count):
                    is_source = False

                if len(source_data) == 0:
                    is_source = False
            msg = 'Jobs processed completely from companies table.'
            self.log(msg, 'info')
        except:
            self.log(sys.exc_info(), 'error')

    def processJobTierRecords(self, offset, companies_count, limit, job_tier):
        msg = 'Going to process active records from company table, companies count is : ' + str(companies_count)
        self.log(msg, 'info')
        print('companies_count-> ' + str(companies_count) + '    job_tier->  ' + str(job_tier))
        try:
            limit = 10000
            source_data= self.getActiveJobsCount(limit, job_tier)
            tier_pivot = []
            if job_tier != None:
                tier_data = source_data['job_tier,source_id']
                if len(tier_data) > 0:
                    tier_pivot = tier_data[0]['pivot']
            else:
                tier_data = source_data['source_id']
                if len(tier_data) > 0:
                    tier_pivot = tier_data


            new_jobs_data = []
            daily_jobs_data = []
            field_name = ''
            exclude_field_name = ''
            field_name1 = ''
            exclude_field_name1 = ''
            if len(tier_pivot)>0:
                for u in tier_pivot:
                    current_time = datetime.datetime.now()
                    company_id = u['value']
                    try:
                        if int(company_id) == 0:
                            continue
                        else:
                            jobs_count = 0
                            tier1 = 0
                            tier2 = 0
                            tier3 = 0
                            tier4 = 0
                            tier5 = 0

                            if str(job_tier) == '1':
                                tier1 = int(u['count'])
                                field_name = 'tier1'
                                exclude_field_name = 'excluded.tier1'
                                field_name1 = 'job_tier1_counts'
                                exclude_field_name1 = 'excluded.job_tier1_counts'
                            if str(job_tier) == '2':
                                tier2 = int(u['count'])
                                field_name = 'tier2'
                                exclude_field_name = 'excluded.tier2'
                                field_name1 = 'job_tier2_counts'
                                exclude_field_name1 = 'excluded.job_tier2_counts'
                            if str(job_tier) == '3':
                                tier3 = int(u['count'])
                                field_name = 'tier3'
                                exclude_field_name = 'excluded.tier3'
                                field_name1 = 'job_tier3_counts'
                                exclude_field_name1 = 'excluded.job_tier3_counts'
                            if str(job_tier) == '4':
                                tier4 = int(u['count'])
                                field_name = 'tier4'
                                exclude_field_name = 'excluded.tier4'
                                field_name1 = 'job_tier4_counts'
                                exclude_field_name1 = 'excluded.job_tier4_counts'
                            if str(job_tier) == '5':
                                tier5 = int(u['count'])
                                field_name = 'tier5'
                                exclude_field_name = 'excluded.tier5'
                                field_name1 = 'job_tier5_counts'
                                exclude_field_name1 = 'excluded.job_tier5_counts'

                            if job_tier == None:
                                jobs_count = int(u['count'])
                                field_name = 'total_jobs'
                                exclude_field_name = 'excluded.total_jobs'
                                field_name1 = 'active_count'
                                exclude_field_name1 = 'excluded.active_count'

                            datatuple = (int(company_id), str(current_time), int(jobs_count), int(tier1), int(tier2), int(tier3),
                            int(tier4), int(tier5))
                            new_jobs_data.append(datatuple)

                            try:
                                daily_job_data = [item for item in self.COMPANY_DATA if
                                                  int(item.get('company_id')) == int(company_id)]

                                if len(daily_job_data) == 0:
                                    daily_job_data = None
                            except:
                                daily_job_data = None

                            if daily_job_data == None:
                                continue
                            else:
                                job_id = int(daily_job_data[0]['id'])
                                new_datatuple = (int(company_id), str(current_time), int(tier1), int(tier2), int(tier3),
                                                 int(tier4), int(tier5), int(job_id), int(jobs_count))
                                daily_jobs_data.append(new_datatuple)

                            if len(new_jobs_data)>=1000:
                                self.updateJobsStats(new_jobs_data, field_name, exclude_field_name)
                                new_jobs_data = []

                            if len(daily_jobs_data)>=1000:
                                # if job_tier != None:
                                self.updateDailyJobsStats(daily_jobs_data, field_name1, exclude_field_name1)
                                daily_jobs_data = []

                    except:
                        pass

            if len(new_jobs_data) > 0:

                self.updateJobsStats(new_jobs_data, field_name, exclude_field_name)
                new_jobs_data = []

            if len(daily_jobs_data) > 0:
                # if job_tier != None:
                self.updateDailyJobsStats(daily_jobs_data, field_name1, exclude_field_name1)
                daily_jobs_data = []
            msg = 'Jobs processed completely from companies table.'
            self.log(msg, 'info')
        except:
            self.log(sys.exc_info(), 'error')

    def updateJobsStats(self, jobs_data, field_name, exclude_field_name):
        msg = 'start process to update ' + str(len(jobs_data)) + ' stats to job_summary'
        self.log(msg, 'info')

        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)
        cursor = conn.cursor()
        jobs_data = tuple(jobs_data)
        if len(jobs_data) > 0:
            if len(jobs_data) == 1:
                jobs_data = str(jobs_data).replace('),)', '))')
            try:
                query = "INSERT INTO company_app_jobsummary(company_id, date, total_jobs, tier1, tier2, tier3, tier4, tier5"

                query += ") VALUES"

                query += str(jobs_data).replace('((', '(').replace('))', ')')

                query += " ON CONFLICT (company_id) DO UPDATE SET"
                query += " {0} = {1}".format(field_name, exclude_field_name)

                cursor.execute(query)
                conn.commit()
            except:
                self.log(sys.exc_info(), 'error')
            finally:
                cursor.close()
                conn.close()

    def updateDailyJobsStats(self, jobs_data, field_name, exclude_field_name):
        msg = 'start process to save ' + str(len(jobs_data)) + ' stats to daily_job_summary'
        self.log(msg, 'info')

        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)
        cursor = conn.cursor()
        jobs_data = tuple(jobs_data)
        if len(jobs_data) > 0:
            if len(jobs_data) == 1:
                jobs_data = str(jobs_data).replace('),)', '))')
            try:
                query = "INSERT INTO company_app_dailyjobsummary(company_id, created_at, job_tier1_counts, job_tier2_counts, job_tier3_counts, job_tier4_counts, job_tier5_counts, id, active_count"

                query += ") VALUES"

                query += str(jobs_data).replace('((', '(').replace('))', ')')

                query += " ON CONFLICT (id) DO UPDATE SET"
                query += " {0} = {1}".format(field_name, exclude_field_name)

                cursor.execute(query)
                conn.commit()
            except:
                self.log(sys.exc_info(), 'error')
            finally:
                cursor.close()
                conn.close()

    def saveJobsStats(self, jobs_data):
        msg = 'start process to update ' + str(len(jobs_data)) + ' stats to job_summary'
        self.log(msg, 'info')

        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)
        cursor = conn.cursor()
        jobs_data = tuple(jobs_data)
        if len(jobs_data) > 0:
            if len(jobs_data) == 1:
                jobs_data = str(jobs_data).replace('),)', '))')
            try:
                query = "INSERT INTO company_app_jobsummary(company_id, date, total_jobs, tier1, tier2, tier3, tier4, tier5"

                query += ") VALUES"

                query += str(jobs_data).replace('((', '(').replace('))', ')')

                query += " ON CONFLICT (company_id) DO UPDATE SET"
                query += " date = excluded.date, total_jobs = excluded.total_jobs, tier1 = excluded.tier1,"
                query += " tier2 = excluded.tier2, tier3 = excluded.tier3, tier4 = excluded.tier4, tier5 = excluded.tier5"

                cursor.execute(query)
                conn.commit()
            except:
                self.log(sys.exc_info(), 'error')
            finally:
                cursor.close()
                conn.close()

    # This method is used to save today jobs stats in daily_job_summary
    def saveDailyJobsStats(self, jobs_data):
        msg = 'start process to save ' + str(len(jobs_data)) + ' stats to daily_job_summary'
        self.log(msg, 'info')

        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)
        cursor = conn.cursor()
        jobs_data = tuple(jobs_data)
        if len(jobs_data) > 0:
            if len(jobs_data) == 1:
                jobs_data = str(jobs_data).replace('),)', '))')
            try:
                query = "INSERT INTO company_app_dailyjobsummary(company_id, created_at, job_tier1_counts, job_tier2_counts, job_tier3_counts, job_tier4_counts, job_tier5_counts"

                query += ") VALUES"

                query += str(jobs_data).replace('((', '(').replace('))', ')')

                cursor.execute(query)
                conn.commit()
            except:
                self.log(sys.exc_info(), 'error')
            finally:
                cursor.close()
                conn.close()

    def getActiveJobsCount(self,limit, job_tier):
        active_jobs_count = 0
        try:
            authCredentials = (SOLR_USER, SOLR_PASSWORD)
            conn = pysolr.Solr(SOLR_URL, auth=authCredentials)
            if job_tier != None:
                data = conn.search(**{
                    "q": "source_id:* AND job_tier: " + str(job_tier),
                    "wt": "json",
                    "indent": "on",
                    "rows": 1,
                    "facet.limit": 10000,
                    "facet": "true",
                    "facet.pivot": "job_tier,source_id",
                    "facet.sort": "source_id",
                    'fl': "facet_counts",
                })
            else:
                data = conn.search(**{
                    "q": "source_id:*",
                    "wt": "json",
                    "indent": "on",
                    "rows": 1,
                    "facet.limit": limit,
                    "facet": "true",
                    "facet.pivot": "source_id",
                    "facet.sort": "source_id",
                    'fl': "facet_counts",
                })

            result = json.dumps(data.__dict__)
            res = json.loads(result)
            active_jobs_count = res['raw_response']['facet_counts']['facet_pivot']
        except:
            self.log(sys.exc_info(), 'error')
        return active_jobs_count

    def getDailyJobsData(self, current_date):
        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)

        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        jobs_data = None
        try:
            query = "SELECT id, company_id FROM company_app_dailyjobsummary where date(created_at) = '{0}'".format(current_date)
            cursor.execute(query)
            jobs_data = cursor.fetchall()
        except:
            self.log(sys.exc_info(), 'error')
        finally:
            cursor.close()
            conn.close()

        return jobs_data

    def getCompanyData(self, offset, limit):
        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)

        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        company_data = None
        try:
            query = 'SELECT company_id FROM company_company ORDER BY company_id LIMIT {0} OFFSET {1};'.format(int(limit), int(offset))
            cursor.execute(query)
            company_data = cursor.fetchall()
        except:
            self.log(sys.exc_info(), 'error')
        finally:
            cursor.close()
            conn.close()

        return company_data

    def getCompaniesCount(self):
        self.log('going to get companies count', 'info')
        conn = psycopg2.connect(database=self.DB_NAME, user=self.DB_USER, password=self.DB_PASSWORD, host=self.DB_HOST)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        companies_count = 0
        try:
            query = 'SELECT count(*) FROM company_company;'
            cursor.execute(query)
            companies_count = cursor.fetchone()
            companies_count = int(companies_count['count'])

            MSG = 'Total ' + str(companies_count) + ' active companies found from company table.'
            self.log(MSG, 'info')
        except:
            self.log(sys.exc_info(), 'error')
        finally:
            cursor.close()
            conn.close()

        return companies_count

if __name__ == '__main__':
    objJIS = CompaniesStatsHelper()
    objJIS.startProcess()