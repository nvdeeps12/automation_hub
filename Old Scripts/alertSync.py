#!/usr/bin/env python
import time, os, sys
rootPath = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(rootPath)
os.environ["DJANGO_SETTINGS_MODULE"] = "ezapi.settings"
import django
django.setup()



from logs.LogHandler import LogHelper
import psycopg2, MySQLdb
import datetime, re, urllib, json
import requests

class AlertSyncHandler():
    def __init__(self):
        self.objLog = LogHelper('alertSync', 'AlertSync')

        self.ALERT_DB_MYSQL = settings.ALERT_DB_MYSQL
        self.ALERT_DB_USER_MYSQL = settings.ALERT_DB_USER_MYSQL
        self.ALERT_DB_PWD_MYSQL = settings.ALERT_DB_PWD_MYSQL
        self.ALERT_DB_HOST_MYSQL = settings.ALERT_DB_HOST_MYSQL

        self.ALERT_DB_PSQL = settings.ALERT_DB_PSQL
        self.ALERT_DB_USER_PSQL = settings.ALERT_DB_USER_PSQL
        self.ALERT_DB_PWD_PSQL = settings.ALERT_DB_PWD_PSQL
        self.ALERT_DB_HOST_PSQL = settings.ALERT_DB_HOST_PSQL

        self.API_DB = settings.DATABASES['default']['NAME']
        self.API_DB_USER = settings.DATABASES['default']['USER']
        self.API_DB_PWD = settings.DATABASES['default']['PASSWORD']
        self.API_DB_HOST = settings.DATABASES['default']['HOST']

        self.ALERT_SEND_AFTER = 4

    def log(self, msg, log_type):
        self.objLog.doLog(msg, 'alertSync.py', log_type)

    def createAlert(self, alert_id):
        msg = "going to create alert in Alert DB"
        self.log(msg, 'info')
        talents = self.getJobSeekers(alert_id)
        if len(talents) > 0:
            lst_data, alerts_schedule, talents_psql_new = self.searlizeData(talents)
            msg = "Got " + str(len(lst_data)) + " records after re-check data."
            self.log(msg, 'info')

            if len(lst_data) > 0:
                msg = "Going to save : " + str(len(lst_data)) + " Job seekers in alert db."
                self.log(msg, 'info')
                self.createMysqlAlert(lst_data)
                # self.createPsqlAlert(talents_psql_new)
                msg = "Going to schedule alert"
                self.log(msg, 'info')
                self.saveAlertSchedule(alerts_schedule)


    def identifyMX(self, email):

        MX = None

        if email.find('gmail.') >= 0:
            MX = 'gmail'

        if email.find('yahoo.') >= 0:
            MX = 'yahoo'

        if email.find('outlook.') >= 0 or email.find('live.') >= 0 \
                or email.find('hotmail.') >= 0 or email.find('msn.') >= 0:
            MX = 'microsoft'

        if MX == None:
            domain = email.split("@")[1]
            MX = self.chech_MX(domain)

        return MX

    def chech_MX(self, email_domain):
        MX = "others"
        try:
            API_ENDPOINT = "https://dns-api.org/MX/"
            request_url = API_ENDPOINT + email_domain

            r = requests.get(request_url)
            dt = json.loads(r.content.decode(r.encoding))

            mx_name = dt[0]['value']

            if mx_name.lower().find('.google.') > 0 or mx_name.lower().find('.gmail.') > 0:
                MX = 'gmail'

        except:
            MX = "others"

        return MX


    def searlizeData(self,talents):
        msg = "Going to searlize data with length : " + str(len(talents))
        self.log(msg, 'info')

        current_time = datetime.datetime.now()
        after_date = current_time + datetime.timedelta(hours=self.ALERT_SEND_AFTER)
        after_date = str(after_date).rsplit(".", 1)[0]
        after_date = datetime.datetime.strptime(str(after_date), '%Y-%m-%d %H:%M:%S')

        talents_new = []
        talents_psql_new = []
        alert_schedule = []
        for talent in talents:
            email = str(talent[4])
            mx = self.identifyMX(email)

            try:
                if talent[9] == None or talent[9] == '':
                    continue
                usr_name = str(talent[2]) + " " + str(talent[3])
                if usr_name == '' or usr_name == ' ' or usr_name == None:
                    usr_name = str(talent[4]).split('@')[0]
                normalize_title = self.normalizeTitle(talent[5])
                time.sleep(.005)
                if normalize_title == None:
                    normalize_title = talent[5]


                status = 'ActiveEmailSubscriber'
                source = 'employzone'
                alert_bucket = 'A'

                if talent[8] == None or talent[8]== '{}':
                    ip = ''
                else:
                    ip = talent[8]

                talents_new.append((talent[0], talent[1], str(usr_name), talent[4], normalize_title,
                                    str(talent[6]) + ', ' + str(talent[7]), ip, talent[9], talent[10],
                                    talent[11], str(talent[12][0]), datetime.datetime.now(), datetime.datetime.now(),
                                    talent[5], mx, source, alert_bucket, status))

                talents_psql_new.append((int(talent[0]), int(talent[1]), str(usr_name).replace("'", ''), talent[4], str(normalize_title).replace("'", ''),
                                    str(talent[6]) + ', ' + str(talent[7]), ip, talent[9], talent[10],
                                    talent[11], str(talent[12][0]).replace("'", ''), str(datetime.datetime.now()), str(datetime.datetime.now()),
                                    str(talent[5]).replace("'", ''), mx, source, alert_bucket, status))
                alert_schedule.append((talent[0], after_date))
            except:
                self.log(sys.exc_info(), 'error')
        return talents_new, tuple(alert_schedule), talents_psql_new

    # This method is used to return data from talent_alertmeta, talent, talent_api tables.
    def getJobSeekers(self, alert_id):
        try:
            msg = 'Going to get talent data from talent_alertmeta, talent, talent_api tables using postgres sql.'
            self.log(msg, 'info')
            connection = psycopg2.connect(dbname=self.API_DB, user=self.API_DB_USER, password=self.API_DB_PWD, host=self.API_DB_HOST)
            cursor = connection.cursor()
            data = []
            try:
                query = "SELECT talent_talentalertmeta.id, talent_talentalertmeta.user_id, auth_user.first_name, auth_user.last_name, auth_user.email, "
                query += "talent_talentalertmeta.job_title, talent_talentalertmeta.physical_latitude, talent_talentalertmeta.physical_longitude, talent_talentalertmeta.ip_address, "
                query += "talent_talentapi.api_key, talent_talentapi.pass_key, talent_talentalertmeta.is_active, talent_talentalertmeta.alert_frequency, auth_user.date_joined "
                query += "FROM talent_talentalertmeta FULL OUTER JOIN auth_user ON talent_talentalertmeta.user_id::int = auth_user.id  "
                query += "FULL OUTER JOIN talent_talentapi ON auth_user.id = talent_talentapi.user_id WHERE  talent_talentalertmeta.id = {}".format(alert_id)

                cursor.execute(query)
                data = cursor.fetchall()
                msg = 'Total ' +str(len(data)) + ' records found from talent table.'
                self.log(msg, 'info')
            except:
                self.log(sys.exc_info(), 'error')
            finally:
                cursor.close()
                connection.close()
            return data
        except:
            self.log(sys.exc_info(), 'error')
            return []

    def createMysqlAlert(self, talents):
        msg = 'Going to save talent data in jobseeker table using MYSQL.'
        self.log(msg, 'info')
        conn = MySQLdb.connect(self.ALERT_DB_HOST_MYSQL, self.ALERT_DB_USER_MYSQL, self.ALERT_DB_PWD_MYSQL, self.ALERT_DB_MYSQL)
        cursor = conn.cursor()
        try:
            query = ("INSERT INTO jobseeker "
                     "(alert_id, user_id, user_name, user_email, title, late_long, ip, api_key, api_pass, is_active, frequency, created_at, updated_at, original_title, email_mx, source, alert_bucket, status)"
                     "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
                     "ON DUPLICATE KEY UPDATE "
                     "alert_id=VALUES(alert_id), user_id=VALUES(user_id), user_name=VALUES(user_name), user_email=VALUES(user_email), "
                     "title=VALUES(title), late_long=VALUES(late_long), ip=VALUES(ip), api_key=VALUES(api_key), api_pass=VALUES(api_pass),"
                     "is_active=VALUES(is_active), frequency=VALUES(frequency), created_at=VALUES(created_at), updated_at=VALUES(updated_at), original_title=VALUES(original_title), email_mx=VALUES(email_mx), source=VALUES(source), alert_bucket=VALUES(alert_bucket)")

            cursor.executemany(query, talents)
            conn.commit()
            msg = 'talent data saved successfully in jobseeker table'
            self.log(msg, 'info')
        except:
            self.log(sys.exc_info(), 'error')
        finally:
            cursor.close()
            conn.close()



    def createPsqlAlert(self, lst_data):
        connection = psycopg2.connect(dbname=self.ALERT_DB_PSQL, user=self.ALERT_DB_USER_PSQL, password=self.ALERT_DB_PWD_PSQL, host=self.ALERT_DB_HOST_PSQL)
        cursor = connection.cursor()

        lst_data1 = tuple(lst_data)
        if len(lst_data1) > 0:
            if len(lst_data1) == 1:
                lst_data1 = str(lst_data1).replace('),)', '))')
            try:
                query = "INSERT INTO jobseeker("
                query += "alert_id, user_id, user_name, user_email, title, late_long, ip, api_key, api_pass, is_active, frequency,"
                query += "created_at,updated_at, original_title, country, email_mx, alert_bucket, source, is_old, status,"
                query += "RestartDate,last_visit, clean_title, onet_title) VALUES"
                query += str(lst_data1).replace("'NULL'", "NULL").replace(str(None), "NULL").replace('((', '(').replace(
                    '))', ')')

                cursor.execute(query)
                connection.commit()
            except:
                self.log(sys.exc_info(), 'error')
            finally:
                cursor.close()
                connection.close()

    def checkSchedule(self, alert_id):
        conn = MySQLdb.connect(self.ALERT_DB_HOST_MYSQL, self.ALERT_DB_USER_MYSQL, self.ALERT_DB_PWD_MYSQL, self.ALERT_DB_MYSQL)
        cursor = conn.cursor()
        dt = None
        try:
            query = "select id  from jobseekeralertschedule where alert_id = " + (
            str(alert_id)) + " and sent_date is null"
            cursor.execute(query)
            dt = cursor.fetchone()
        except:
            self.log(sys.exc_info(), 'error')
        finally:
            cursor.close()
            conn.close()
        return dt

    # This method is for save data in jobseekeralertschedule table.
    def saveAlertSchedule(self, job_data):
        msg = 'Going to save jobs in jobseekeralertschedule table using MySQL.'
        self.log(msg, 'info')
        conn = MySQLdb.connect(self.ALERT_DB_HOST_MYSQL, self.ALERT_DB_USER_MYSQL, self.ALERT_DB_PWD_MYSQL, self.ALERT_DB_MYSQL)
        cursor = conn.cursor()
        try:
            schedule_id = self.checkSchedule(job_data[0][0])
            if (schedule_id != None and int(schedule_id[0]) > 0):
                pass
            else:
                query = ("INSERT INTO jobseekeralertschedule "
                         "(alert_id, send_after) "
                         "VALUES (%s, %s) "
                         "ON DUPLICATE KEY UPDATE "
                         "alert_id=VALUES(alert_id), send_after=VALUES(send_after) ")

                cursor.executemany(query, job_data)
                conn.commit()
            msg = 'Data saved successfully in job table using MySQL'
            self.log(msg, 'info')
        except:
            self.log(sys.exc_info(), 'error')
        finally:
            cursor.close()
            conn.close()

    def normalizeTitle(self, title):
        title = title.strip()
        title = title.replace('\n', '').replace('\t', '').replace('\r', '').replace('/', '')
        title = str(title).replace(' ', '+')
        title = re.sub(r'[^\x00-\x7F]+', ' ', title)
        title = title.replace('&#8211;', '')
        url = "http://ai.workharmony.com/clean-title/?job_title=" + str(title) + "&output=json"

        try:
            res = urllib.request.urlopen(url)
            html = res.read()
            json_data = json.loads(html)
            if len(json_data['matches']) > 0:
                data = self.removeBreacesText(json_data['matches'][0])
                title = data.strip()
            else:
                title = None
        except:
            title = None
        return title

        #### method to remove text with braces

    def removeBreacesText(self, title):
        ret = ''
        skip1c = 0
        skip2c = 0
        for i in title:
            if i == '[':
                skip1c += 1
            elif i == '(':
                skip2c += 1
            elif i == ']' and skip1c > 0:
                skip1c -= 1
            elif i == ')' and skip2c > 0:
                skip2c -= 1
            elif skip1c == 0 and skip2c == 0:
                ret += i
        return ret