import requests
from bs4 import BeautifulSoup
# from python_anticaptcha import AnticaptchaClient, NoCaptchaTaskProxylessTask
from selenium import webdriver
import sys
import xlwt
import xlrd
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
options = webdriver.FirefoxOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='D:\geckodriver',options=options)

rd=xlrd.open_workbook('zillow_pages.xls')
sheet = rd.sheet_by_index(0)
# cities=['New-York-NY', 'Los-Angeles', 'Chicago', 'Houston', 'Phoenix', 'Philadelphia', 'San-Antonio', 'San-Diego', 'Dallas', 'San-Jose', 'Austin', 'Jacksonville', 'Fort-Worth', 'San-Francisco', 'Columbus', 'Charlotte', 'Indianapolis', 'Seattle', 'Denver', 'Washington-Dc', 'El-Paso', 'Boston', 'Nashville', 'Nashville', 'Portland', 'Las-Vegas', 'Detroit', 'Oklahoma-City', 'Memphis', 'Louisville', 'Louisville', 'Baltimore', 'Milwaukee', 'Albuquerque', 'Tucson', 'Fresno', 'Sacramento', 'Mesa', 'Atlanta', 'Kansas-City', 'Colorado-Springs', 'Miami', 'Raleigh', 'Long-Beach', 'Virginia-Beach', 'Omaha', 'Oakland', 'Minneapolis', 'Arlington', 'Tampa', 'Tulsa', 'New-Orleans', 'Wichita', 'Bakersfield', 'Cleveland', 'Aurora', 'Anaheim', 'Honolulu', 'Riverside', 'Santa-Ana', 'Lexington', 'Lexington', 'Corpus-Christi', 'Henderson', 'Stockton', 'St.-Paul', 'Pittsburgh', 'St.-Louis', 'Cincinnati', 'Anchorage', 'Orlando', 'Irvine', 'Plano', 'Greensboro', 'Lincoln', 'Newark', 'Durham', 'Toledo', 'St.-Petersburg', 'Chula-Vista', 'Fort-Wayne', 'Scottsdale', 'Jersey-City', 'Laredo', 'Madison', 'Lubbock', 'Reno', 'Chandler', 'Glendale', 'Buffalo', 'North-Las-Vegas', 'Gilbert', 'Winston-Salem', 'Chesapeake', 'Irving', 'Norfolk', 'Fremont', 'Hialeah', 'Richmond', 'Boise', 'Boise', 'Garland', 'Baton-Rouge', 'Spokane', 'Tacoma', 'Modesto', 'San-Bernardino', 'Fontana', 'Des-Moines', 'Oxnard', 'Moreno-Valley', 'Birmingham', 'Fayetteville', 'Rochester', 'Amarillo', 'Port-St.-Lucie', 'Yonkers', 'Mckinney', 'Grand-Prairie', 'Salt-Lake-City', 'Grand-Rapids', 'Little-Rock', 'Huntsville', 'Huntington-Beach', 'Augusta', 'Augusta', 'Overland-Park', 'Montgomery', 'Tempe', 'Akron', 'Cape-Coral', 'Tallahassee', 'Frisco', 'Mobile', 'Knoxville', 'Shreveport', 'Brownsville', 'Worcester', 'Santa-Clarita', 'Sioux-Falls', 'Fort-Lauderdale', 'Vancouver', 'Rancho-Cucamonga', 'Chattanooga', 'Newport-News', 'Ontario', 'Providence', 'Elk-Grove', 'Salem', 'Oceanside', 'Santa-Rosa', 'Corona', 'Eugene', 'Garden-Grove', 'Peoria', 'Pembroke-Pines', 'Fort-Collins', 'Cary', 'Springfield', 'Jackson', 'Alexandria', 'Hayward', 'Hollywood', 'Lakewood', 'Lancaster', 'Salinas', 'Sunnyvale', 'Palmdale', 'Clarksville', 'Escondido', 'Pomona', 'Pasadena', 'Killeen', 'Macon-Bibb-County', 'Joliet', 'Murfreesboro', 'Mcallen', 'Savannah', 'Naperville', 'Paterson', 'Thornton', 'Bellevue', 'Torrance', 'Rockford', 'Miramar', 'Bridgeport', 'Mesquite', 'Fullerton', 'Denton', 'Waco', 'Syracuse', 'Roseville', 'Orange', 'Surprise', 'Dayton', 'Charleston', 'Olathe', 'Midland', 'West-Valley-City', 'Warren']
row=0
r=0
for i in range(sheet.nrows):
        try:
                r = r + 1
                value=sheet.cell_value(i, 0)
                driver.get(value)
                soup = BeautifulSoup(driver.page_source, u'html.parser')
                print('Going For ----->'+str(value))
                div=soup.find('div',attrs={'id':'yui_3_18_1_1_1566624294298_509'})
                p=soup.find_all('p',attrs={'class':'ldb-contact-name'})
                for pp in p:
                        a=pp.find('a').get('href')
                        link='https://www.zillow.com/'+str(a)
                        sheet1.write(row,0,link)
                        row = row + 1
                        wb.save('zillow_profiles4325.xls')
                        print(link)

        except:
                # print(sys.exc_info())
                print('Loop Continue........')
                continue
        print('___' * 100)
        print(str(r) + '/' + str(sheet.nrows))
        import datetime
        print(datetime.datetime.now())
        print('___' * 100)