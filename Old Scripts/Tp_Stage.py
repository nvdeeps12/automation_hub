from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime
import sys
#
# options = webdriver.FirefoxOptions()
# # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# # options.add_argument('--headless')
# driver = webdriver.Firefox(executable_path='E:\geckodriver',options=options)
wb = xlwt.Workbook()
# rd = xlrd.open_workbook('Tp_pages.xlsx')
# sheet = rd.sheet_by_index(0)

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'SUPPLIER P/N')
sheet1.write(0, 1, 'NAME')
sheet1.write(0, 2, 'SHORT DESCRIPTION')
sheet1.write(0, 3, 'DESCRIPTION')
sheet1.write(0, 4, 'IMAGE URL')
sheet1.write(0, 5, 'LINK')

referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

arr = ['https://www.travisperkins.co.uk/p/230413']
# arr=['https://www.travisperkins.co.uk/p/230414']
row = 0
r = 0
img=''
fail=[]
features_des=''
for i in arr:
    try:
        row = row + 1
        value = i
        pid=value
        print('Process Started For--', value)
        if value == '':
            continue
        res = requests.get(value,headers=headers)
        # driver.get(value)
    except:
        print('hsdghas')
        continue

    try:
        soup = BeautifulSoup(res.content, u'html.parser')
        # print(soup)
        try:
            pid = pid.split('p/')[1]
        except:
            pid = ''
        print('Pid ->' + pid)

        try:
            name = soup.find('h1',  attrs={'class': 'tpProductTitle'}).text.strip()
            print('Name-',name)
        except:
            name = 'None'
        #
        try:
            shortdes = soup.find('div', attrs={'id': 'tab-overview'}).text.strip()
            # print('Des--',shortdes)
        except:
            shortdes = 'None'
        try:
            features = soup.find('div', attrs={'id':'tab-techspecs'})
            features_des=features.find('table').text.strip()
            print(features_des)
        except:
            features = 'None'
        #
        try:
            image = soup.find('div',attrs={'id':'s7ProductDetailsImage'})
            img=image.find('img').get('src')
            # print('Img--', img)
            # print('Img--',image)
        except:
            img = 'None'
        print('Img--', img)
        if img=='None' :
            fail.append(value)
        sheet1.write(row,0,pid)
        sheet1.write(row,1,name)
        sheet1.write(row,2,shortdes)
        sheet1.write(row,3,features_des)
        sheet1.write(row,4,img)
        sheet1.write(row,5,value)
        wb.save('D:\PycharmProjects\Files\Tp_Data.xls')
    except:
        print(sys.exc_info())
        print('No Links Collected From->')
        continue

