import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import psycopg2 as sql


options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')
driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)

states_arr = []

driver.get('https://ascp.continuum.host/directory/search.php')
# time.sleep(5)

soup = BeautifulSoup(driver.page_source, u"html.parser")
divs = soup.find('div',attrs={'class':'form-group'}).findAll('option')

for i in divs:
    states = i.text.strip()
    states_arr.append(states)




print(states_arr)