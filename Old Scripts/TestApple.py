from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime
import sys

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='D:\chromedriver', options=options)
wb = xlwt.Workbook()
rd = xlrd.open_workbook('D:\PycharmProjects\Files\Apple_uu.xlsx')
sheet = rd.sheet_by_index(0)

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Id')
sheet1.write(0, 1, 'Product Name')
sheet1.write(0, 2, 'Url')
sheet1.write(0, 3, 'Title')
sheet1.write(0, 4, 'Thumb')
sheet1.write(0, 5, 'Finish/Processor')
sheet1.write(0, 6, 'Memory')
sheet1.write(0, 7, 'Size')
sheet1.write(0, 8, 'Display')
sheet1.write(0, 9, 'Chip/Graphics')
sheet1.write(0, 10, 'Camera')
sheet1.write(0, 11, 'Audio ')
sheet1.write(0, 12, 'Playback')
sheet1.write(0, 13, 'Tv Audio')
sheet1.write(0, 14, 'Siri/WIreless')
sheet1.write(0, 15, 'Button')
sheet1.write(0, 16, 'Power')
sheet1.write(0, 17, 'Electrical')
sheet1.write(0, 18, 'Os')
sheet1.write(0, 19, 'ACc')
sheet1.write(0, 20, 'Apps')
sheet1.write(0, 21, 'Free_app')
sheet1.write(0,22,'Headphones')
sheet1.write(0,23,'Sim')
sheet1.write(0,24,'Rating')
sheet1.write(0,25,'Mail')
sheet1.write(0,26,'Input')
sheet1.write(0,27,'Connections')
sheet1.write(0,28,'Inbox')
sheet1.write(0,29,'App_Env')
sheet1.write(0,30,'App_Giv')
sheet1.write(0,31,'Splash')
sheet1.write(0,32,'Modified Date')


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

app_dict = dict({})

arr = ['https://support.apple.com/kb/SP691?viewlocale=en_US&locale=en_US']

row = 0
r = 0
for i in range(sheet.nrows):
    try:
        row = row + 1
        value = sheet.cell_value(i, 0)
        print('Process Started For--', value)
        if value == '':
            continue
        res = requests.get(value , headers = headers)
        print('Status_Code--',res.status_code)
    except:
        continue

    try:
        soup = BeautifulSoup(res.content, u'html.parser')
        id = str(value).split('/kb/')[1].split('?')[0]

        app_dict.update({"id": id})
        try:
            name = soup.find('h1',  attrs={'id': 'main-title'}).text.strip()
            if '-' in name:
                name = name.split('-')[0]
            if '(' in name:
                name = name.split('(')[0]
            name = name.replace('.','')

            print('PRoduct---',name)
            app_dict.update({"Prod_name":name})
            url = value
            app_dict.update({"url": url})
            title = soup.find('h1',  attrs={'id': 'main-title'}).text.strip()
            app_dict.update({"title": title})
            thumb_nail = 'https://support.apple.com/kb/image.jsp?productid='+str(id)
            app_dict.update({"thumbnail": thumb_nail})
        except:
            name = 'None'


        specs = soup.find('div',  attrs={'class': 'attr SPECIFICATION'})

        try:
            try:
                finish_ul = specs.find('h3', text=re.compile('Pro')).find_next('ul').text.strip()

            except:
                finish_ul = ''


        except:
            finish_ul = ''


        try:
            try:
                cap_ul = specs.find('h3', text=re.compile('Memory')).find_next('ul').text.strip()

            except:
                cap_ul = ''
        except:
            cap_ul = ''



        try:
            try:
                size_ul = specs.find('h3', text=re.compile('Size')).find_next('ul').text.strip()
            except:
                size_ul = ''

        except:
            size_ul = []



        try:
            display_ul = specs.find('h3', text=re.compile('Display')).find_next('ul').text.strip()

            print('Disspplayy', display_ul)
        except:
            display_ul = ''


        try:
            splash_ul = specs.find('h3', text=re.compile('Splash')).find_next('ul').text.strip()

        except:
            splash_ul = ''



        try:
            try:
                chip_ul = specs.find('h3', text=re.compile('Graphics')).find_next('ul').text.strip()
            except:
                chip_ul = ''
        except:
            chip_ul = ''


        try:
            cam_ul = specs.find('h3', text=re.compile('Camera')).find_next('ul').text.strip()
        except:
            cam_ul = ''



        try:
            vid_ul = specs.find('h3', text=re.compile('Video Recording')).find_next('ul').text.strip()
        except:
            vid_ul = ''


        try:
            face_ul = specs.find('h3', text=re.compile('FaceTime')).find_next('ul').text.strip()
        except:
            face_ul = ''



        try:
            touch_ul = specs.find('h3', text=re.compile('Touch ID')).find_next('ul').text.strip()
        except:
            touch_ul = ''


        try:
            app_ul = specs.find('h3', text=re.compile('Apple Pay')).find_next('ul').text.strip()
        except:
            app_ul = ''



        try:
            cel_ul = specs.find('h3', text=re.compile('Wireless')).find_next('ul').text.strip()
            cel_ul = cel_ul.replace("'",'')
        except:
            cel_ul = ''




        try:
            location_ul = specs.find('h3', text=re.compile('Location')).find_next('ul').text.strip()
        except:
            location_ul = ''



        try:
            vid_ul =specs.find('h3', text=re.compile('Video Calling')).find_next('ul').text.strip()
        except:
            vid_ul = ''




        try:
            audio_ul = specs.find('h3', text=re.compile('Audio')).find_next('ul').text.strip()
        except:
            audio_ul = ''



        try:
            play_ul =specs.find('h3', text=re.compile('Audio Playback')).find_next('ul').text.strip()
        except:
            play_ul = ''




        try:
            tv_ul = specs.find('h3', text=re.compile('Input')).find_next('ul').text.strip()
        except:
            tv_ul = ''



        try:
            siri_ul = specs.find('h3', text=re.compile('Wireless')).find_next('ul').text.strip()

        except:
            siri_ul = ''



        try:
            button_ul = specs.find('h3', text=re.compile('Buttons')).find_next('ul').text.strip()
        except:
            button_ul = ''

        try:
            pow_ul = specs.find('h3', text=re.compile('Power')).find_next('ul').text.strip()
        except:
            pow_ul = ''


        try:
            sens_ul = specs.find('h3', text=re.compile('Electrical')).find_next('ul').text.strip()
        except:
            sens_ul = ""



        try:
            os_ul = specs.find('h3', text=re.compile('Operating System')).find_next('ul').text.strip()
            os_ul = [os_ul]
        except:
            os_ul = ""



        try:
            acc_ul =specs.find('h3', text=re.compile('Accessibility')).find_next('ul').text.strip()
        except:
            acc_ul = ''



        try:
            built_apps = specs.find('h3', text=re.compile('Built-in Apps')).find_next('ul').text.strip()
        except:
            built_apps = ''


        try:
            free_apps_ul = specs.find('h3', text=re.compile('Free Apps from Apple')).find_next('ul').text.strip()
        except:
            free_apps_ul = ''



        try:
            headphones_ul = specs.find('h3', text=re.compile('Headphones')).find_next('ul').text.strip()
        except:
            headphones_ul = ''
        try:
            sim_ul = specs.find('h3', text=re.compile('SIM Card')).find_next('ul').text.strip()
        except:
            sim_ul = ''


        try:
            rating_ul =specs.find('h3', text=re.compile('Rating')).find_next('ul').text.strip()
        except:
            rating_ul = ''


        try:
            mail_ul =specs.find('h3', text=re.compile('Mail')).find_next('ul').text.strip()
        except:
            mail_ul = ''

        try:
            sys_ul = specs.find('h3', text=re.compile('Input')).find_next('ul').text.strip()
        except:
            sys_ul = ''


        try:
            lang_ul =specs.find('h3', text=re.compile('Connections')).find_next('ul').text.strip()

        except:
            lang_ul = ''




        try:
            in_box_ul = specs.find('h3', text=re.compile('In the Box')).find_next('ul').text.strip()
        except:
            in_box_ul = ''

        try:
            app_env_ul = specs.find('h3', text=re.compile('iPhone and the Environment')).find_next('ul').text.strip()
        except:
            app_env_ul = ''



        try:
            app_giv_ul = specs.find('h3', text=re.compile('Apple GiveBack')).find_next('ul').text.strip()
        except:
            app_giv_ul = ''



        try:
            mod_date = soup.find('div',attrs={'class':'mod-date'}).text.strip()
        except:
            mod_date = ''



        import json

        name = name.strip()
        file_name = name +str('.json')
        print('File_Name--',file_name)
        #
        # with open('D:\PycharmProjects\Files\json_outputs\ '+file_name, 'w') as f:
        #     json.dump(app_dict, f)

        print('--'*100)


        sheet1.write(row, 0, id)
        sheet1.write(row, 1, name)
        sheet1.write(row, 2, url)
        sheet1.write(row, 3, title)
        sheet1.write(row, 4, thumb_nail)
        sheet1.write(row, 5, finish_ul)
        sheet1.write(row, 6, cap_ul)
        sheet1.write(row, 7, size_ul)
        sheet1.write(row, 8, display_ul)
        sheet1.write(row, 9, chip_ul)
        sheet1.write(row, 10, cam_ul)
        sheet1.write(row, 11, audio_ul)
        sheet1.write(row, 12, play_ul)
        sheet1.write(row, 13, tv_ul)
        sheet1.write(row, 14, siri_ul)
        sheet1.write(row, 15, button_ul)
        sheet1.write(row, 16, pow_ul)
        sheet1.write(row, 17, sens_ul)
        sheet1.write(row, 18, os_ul)
        sheet1.write(row, 19, acc_ul)
        sheet1.write(row, 20, built_apps)
        sheet1.write(row, 21, free_apps_ul)
        sheet1.write(row, 22, headphones_ul)
        sheet1.write(row, 23, sim_ul)
        sheet1.write(row, 24, rating_ul)
        sheet1.write(row, 25, mail_ul)
        sheet1.write(row, 26, sys_ul)
        sheet1.write(row, 27, lang_ul)
        sheet1.write(row, 28, in_box_ul)
        sheet1.write(row, 29, app_env_ul)
        sheet1.write(row, 30, app_giv_ul)
        sheet1.write(row, 31, splash_ul)
        sheet1.write(row, 32 ,mod_date)
        wb.save('D:\PycharmProjects\Files\json_outputs\Apple_New.xls')

    except:
        print('No Links Collected From->')
        print(sys.exc_info())
        continue



feild=app_dict.keys()
print(app_dict)
import csv
with open('data.csv','w',newline='') as f:
    writer=csv.DictWriter(f,fieldnames=feild)
    writer.writeheader()
    writer.writerow(app_dict)