import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import re
import xlwt

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')
driver = webdriver.Chrome(executable_path='F:\Projects\Fiver\Drivers\chromedriver', chrome_options=options)


wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Ad Number')
sheet1.write(0, 1, 'Name')
sheet1.write(0, 2, 'Address')
sheet1.write(0, 3, 'Amount')

driver.get('https://www.law.com/dailyreportonline/public-notices/?atex-class=FDR-30102&keyword=Fulton&from=10%2F07%2F2019&to=10%2F11%2F2019')
time.sleep(5)


driver.find_element_by_id('loadmore1').click()
time.sleep(3)

soup = BeautifulSoup(driver.page_source, u"html.parser")

section = soup.findAll('li',attrs={'class':'announcement-item'})
print('SEc-->',section)
row = 0
for i in section:
    row = row+1
    try:
        ad_number = i.find('h6', text=re.compile('AD NUMBER')).find_next('div').text.strip()
    except:
        ad_number = 'None'
    print(ad_number)

    try:
        name =  i.findAll('b')[2].text.strip()
    except:
        name ='None'
    print(name)

    try:
        address = i.find(text=re.compile('known as')).find_next('b').text.strip()
    except:
        address = 'None'
    print(address)

    try:
        amount = i.find(text=re.compile("DOLLARS")).split('DOLLARS')[1].split('),')[0].split('(')[1]
        amount = str(amount)
    except:
        amount = 'None'
    print('Am--->',amount)


    sheet1.write(row, 0, ad_number)
    sheet1.write(row, 1, name)
    sheet1.write(row, 2, address)
    sheet1.write(row, 3, amount)
    wb.save('Law_Data.xls')



    print('--' * 100)






