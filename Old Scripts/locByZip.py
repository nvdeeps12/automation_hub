from ezapi import settings
import requests
import json

class LocationDetailsFromPostalCode():

    def getLocationDetail(self, zip):
        retdata = {}
        try:
            GeoUrl = 'https://maps.googleapis.com/maps/api/geocode/json?key='+ settings.GOOGLE_LOCATION_API_KEY +'&components=postal_code:' + str(zip)
            print(GeoUrl)

            response = requests.get(GeoUrl)
            if response.status_code == 200:
                content = response.content
                jsonData = json.loads(content)
                if jsonData['status'] == 'OK':
                    results = jsonData['results'][0]['address_components']
                    for r in results:
                        if 'locality' in r['types']:
                            city = r['long_name']
                            retdata.update({'city':city})
                        if 'administrative_area_level_1' in r['types']:
                            region = r['short_name']
                            retdata.update({'region': region})
                        if 'country' in r['types']:
                            country = r['short_name']
                            retdata.update({'country': country})
                    latlon = jsonData['results'][0]['geometry']['location']
                    retdata.update({'lat': latlon['lat']})
                    retdata.update({'lng': latlon['lng']})
                    retdata.update({'zip': zip})
                    return retdata
                else:
                    return None
            else:
                return None
        except:
            return None


objk = LocationDetailsFromPostalCode()
print (objk.getLocationDetail(152002))