import time
from selenium import webdriver
from bs4 import BeautifulSoup
import xlwt
import datetime
import re


class dataset():

    def Englishdata(self):

        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
        options.add_argument('--disable-gpu')
        options.add_argument('--headless')
        # options.add_argument('--lang=fr-FR')
        driver = webdriver.Chrome(executable_path='chromedriver' )
        wb = xlwt.Workbook()


        brands =   ['della toyota']

        count = 0
        for i in brands:
            count = count+1
            sheet1 = wb.add_sheet(str(i)+str(count))
            try:
                driver.get('https://www.google.com/search?hl=en&ei=JkQFXcPoG8OoyAOSzo-AAQ&q='+str(i)+str(' reviews'))
                time.sleep(5)
                click = driver.find_element_by_xpath("//a[@data-async-trigger='reviewDialog']").click()
                # print('click---',click)
            except:
                driver.get('https://www.google.com/search?hl=en&ei=JkQFXcPoG8OoyAOSzo-AAQ&q=' + str(i))
                time.sleep(5)
                click2 = driver.find_element_by_xpath("//a[@data-async-trigger='reviewDialog']").click()
                # print('click2---', click2)
            print('Going To Scrape----->>',i)

            time.sleep(3)
            souprev = BeautifulSoup(driver.page_source, u"html.parser")
            rev = souprev.find('a',attrs={'data-async-trigger':'reviewDialog'}).find_next('span').text.strip()
            rev = rev.split(' ')[0]
            rev = int(rev)
            print('Total Reviews-->',rev)
            if rev >10:
                try:
                    scroll_count = int(rev/10)
                    scroll_count=int(scroll_count)
                    print('How Much Scrolll-----',scroll_count)
                    count= 0
                    while(count!= scroll_count):
                        count = count + 1
                        elem = driver.find_element_by_xpath("//div[@data-start_index='10']")
                        driver.execute_script("arguments[0].scrollIntoView(true);",elem)
                        print('Scrolled Times->',count)
                        time.sleep(5)
                except:
                     pass
            soup = BeautifulSoup(driver.page_source, u"html.parser")


            Title = soup.find('div', attrs={'class': 'P5Bobd'}).text.strip()
            Rating = soup.find('span', attrs={'class': 'rtng'}).text.strip()
            tol_rev = soup.find('span', attrs={'class': 'Vfp4xe p13zmc'}).text.strip()
            all_reviews = soup.findAll('div', attrs={'class': 'WMbnJf gws-localreviews__google-review'})
            print('---One Brand Done---')


            sheet1.write(0, 0, 'Title')
            sheet1.write(0, 1, 'Address')
            sheet1.write(0, 2, 'Rating')
            sheet1.write(0, 3, 'Review')
            sheet1.write(0, 4, 'Review Reply')
            sheet1.write(0, 5, 'Review Rating')
            sheet1.write(0, 6, 'Total Reviews')
            sheet1.write(0, 7, 'Review Date')
            sheet1.write(0, 8, 'Review Reply Date')
            sheet1.write(0, 9, 'Script Ran Date')
            row  = 0
            for i in all_reviews:
                row = row+1
                all_reviews = i.text.strip()
                if  '(Original)' in all_reviews:
                        all_reviews = all_reviews.split('(Original)')[1]
                        # print(all_reviews)
                all_reviews = i.text.strip()

                Address = soup.find('div', attrs={'class': 'T6pBCe'}).text.strip()
                comment_date = i.find('span', attrs={'class': 'dehysf'}).text.strip()
                review_rating = i.find('span', attrs={'class': 'fTKmHE99XE4__star fTKmHE99XE4__star-s'})['aria-label']

                try:
                    review_reply_date = i.find('strong', text=re.compile('Response from the owner')).find_next( 'span').find_next('span').text.strip()
                    review_reply = i.find('strong', text=re.compile('Response from the owner')).find_next('div').text.strip()
                    if '(Original)' in review_reply:
                        review_reply = review_reply.split('(Original)')[1]
                    else:
                        review_reply = i.find('strong', text=re.compile('Response from the owner')).find_next('div').text.strip()


                except:
                    review_reply_date = 'None'
                    review_reply = 'None'

                date = datetime.datetime.today()
                date = str(date)

                sheet1.write(row, 0, Title)
                sheet1.write(row, 1, Address)
                sheet1.write(row, 2, Rating)
                sheet1.write(row, 3, all_reviews)
                sheet1.write(row, 4, review_reply)
                sheet1.write(row, 5, review_rating)
                sheet1.write(row, 6, tol_rev)
                sheet1.write(row, 7, comment_date)
                sheet1.write(row, 8, review_reply_date)
                sheet1.write(row, 9, date)
                wb.save('GR.xls')


ob = dataset()
ob.Englishdata()


