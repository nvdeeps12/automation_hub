import requests
import json

class GetLocationFromIP():

    def getLocationByIP(self, ip):
        try:
            url = "https://geoip-db.com/jsonp/" + str(ip)
            print(url)
            res = requests.get(url)
            loc = {}
            if res.status_code == 200:
                loc_detail = res.content.decode()
                loc_detail = loc_detail.split("(")[1].strip(")")
                loc_detail = json.loads(loc_detail)
                loc.update({'country':loc_detail['country_code']})
                loc.update({'city':loc_detail['city']})
                loc.update({'postal':loc_detail['postal']})
                loc.update({'state':loc_detail['state']})
                loc.update({'lat':loc_detail['latitude']})
                loc.update({'lng':loc_detail['longitude']})
                return loc
            else:
                return None
        except:
            return None

obj = GetLocationFromIP()
print (obj.getLocationByIP('117.197.43.72'))
