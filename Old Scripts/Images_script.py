import time
from selenium import webdriver
from bs4 import BeautifulSoup
from scripts.downloadimage import downloader
import sys
import xlrd
import xlwt
from selenium.webdriver.common.keys import Keys
loc = ("images.xlsx")

wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)
urls = (sheet.col_values(1))
file_name = (sheet.col_values(0))
wbw = xlwt.Workbook()
Processed = wbw.add_sheet('Processed')
Not_Processed = wbw.add_sheet('Not Processed')

class dataset():
    def Englishdata(self):

        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
        options.add_argument('--disable-gpu')
        options.add_argument('--headless')
        # options.add_argument('--lang=fr-FR')
        driver = webdriver.Chrome(executable_path='F:\chromedriver', chrome_options=options)

        try:
            driver.get('https://demos.algorithmia.com/colorize-photos/')
            time.sleep(3)
            img_box = driver.find_element_by_id('imgUrl')

            count = 1
            for i in range(6300):
                images = urls[count]
                file = file_name[count]
                print('link--', images)
                images = str(images).strip()
                print('File_name--',file)
                img_box.send_keys(images)
                time.sleep(4)
                count = count + 1
    
                try:
                    try:
                        driver.find_element_by_xpath("//a[@onclick='callAlgorithm()']").click()
                        time.sleep(10)
                        soup = BeautifulSoup(driver.page_source, u"html.parser")
                        downlink = soup.find('a', attrs={'id': 'resultLink'})['href']
                        print('Download----', downlink)
                        print('==' * 100)
                        file = str(file)
                        Processed.write(count, 0,file)
                        Processed.write(count, 1,downlink)
                        wbw.save('Processed.xls')
                        print('Images Done---------', count)
                        downloader(downlink, file)
                        img_box.clear()
                    except:
                        Not_Processed.write(count, 0, file)
                        Not_Processed.write(count, 1, images)
                        print(sys.exc_info())
                        wbw.save('Not_processed.xls')
                except:
                    print(sys.exc_info())
        except:
            print(sys.exc_info())
            pass
ob = dataset()
ob.Englishdata()


