import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import xlwt
import xlrd
import requests
from work.scripts.employzone_mail import createEmailContent,sendEmail
from datetime import datetime
import re
rd=xlrd.open_workbook('D:\PycharmProjects\Files\\Urls\Higheredjobs Accounting.xlsx')
sheet = rd.sheet_by_index(0)
sheet.cell_value(0, 0)
# "This Will Calcullate Minutes"
options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')

driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)

names_arr = []

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Institution')
sheet1.write(0, 1, 'Institution Website')
sheet1.write(0, 2, 'Location')
sheet1.write(0, 3, 'Category')
sheet1.write(0, 4, 'Job Posted Date')
sheet1.write(0, 5, 'Application Due Date')
sheet1.write(0, 6, 'Job Type')
sheet1.write(0, 7, 'Job Ad on Text')
# sheet1.write(0, 8, 'Licence No.')

referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }


class ascp():
    def pass_links(self):
        self.row = 0
        self.profile_link_soup()


    def profile_link_soup(self):

        for i in range(sheet.nrows):
            self.row = self.row+1
            link = sheet.cell_value(i,0)
            if len(sheet.cell_value(i,0))<8:
                continue

            profile_link = link
            print(profile_link)
            page= driver.get(profile_link)

            time.sleep(5)
            profile_soup = BeautifulSoup(driver.page_source, u"html.parser")
            # print(profile_soup)

            try:
                institution = profile_soup.find('div', text=re.compile('Institution')).find_next('div').text.strip()
                institution_website = profile_soup.find('div', text=re.compile('Institution')).find_next('div').find('a')['href']

            except:
                institution = 'None'
                institution_website = 'None'

            print('Institution---',institution)
            print('Website---',institution_website)

            try:
                location = profile_soup.find('div', text=re.compile('Location')).find_next('div').text.strip()
                # print('Street--', street_add)
            except:
                location = ''

            print('Loc--',location)

            try:
                category = profile_soup.find('div', text=re.compile('Category')).find_next('div').text.strip()
            except:
                category = ''
            print('Cats--',category)

            try:
                posted = profile_soup.find('div', text=re.compile('Posted')).find_next('div').text.strip()
                # print('Reg--', region)
            except:
                posted = ''

            print('Pos--',posted)

            try:
                application_due = profile_soup.find('div', text=re.compile('Application Due')).find_next('div').text.strip()
                # print('Pos--', postal_code)
            except:
                application_due = ''

            print('App--',application_due)

            try:
                type = profile_soup.find('div', text=re.compile('Type')).find_next('div').text.strip()
                # print('Mobile--', mobile)
            except:
                type = ''

            print('Type--', type)

            try:
                job_ad = profile_soup.find('div', attrs={'class':'jobDesc'}).text.strip()
                # print('Mobile--', mobile)
            except:
                job_ad = ''

            print('JobAd---',job_ad)

            sheet1.write(self.row, 0, institution)
            sheet1.write(self.row, 1, institution_website)
            sheet1.write(self.row, 2, location)
            sheet1.write(self.row, 3, category)
            sheet1.write(self.row, 4, posted)
            sheet1.write(self.row, 5, application_due)
            sheet1.write(self.row, 6, type)
            sheet1.write(self.row, 7, job_ad)
            # sheet1.write(self.row, 8, lis_no)
            import datetime

            # wb.save('D:\PycharmProjects\Files\json_outputs\HierdJobs.xls')
            print('Done------->',self.row)
            print(datetime.datetime.now())
            print('----' * 100)

objs = ascp()
objs.pass_links()



