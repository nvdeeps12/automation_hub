import requests
import xlwt
import xlrd
from bs4 import BeautifulSoup
import datetime


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }
wb = xlwt.Workbook()
rd = xlrd.open_workbook('D:\PycharmProjects\Files\zillow4500.xls')
sheet = rd.sheet_by_index(0)

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First_name')
sheet1.write(0, 1, 'Last_name')
sheet1.write(0, 2, 'Company_Name')
sheet1.write(0, 3, 'Address')
sheet1.write(0, 4, 'City')
sheet1.write(0, 5, 'State')
sheet1.write(0, 6, 'Zip')
sheet1.write(0, 7, 'Cell_Phone')
sheet1.write(0, 8, 'Broker_phone')
sheet1.write(0, 9, 'Website')
sheet1.write(0, 10, 'No. of Transactions')
sheet1.write(0, 11, 'Profile_Link')


row = 0
r = 0
for i in range(sheet.nrows):
    try:
        row = row + 1
        value = sheet.cell_value(i, 0)
        value = value.replace('//profile','/profile')
        print('Process Started For--', value)
        res=requests.get(value,headers=headers)
    except:
        continue

    try:
        soup=BeautifulSoup(res.content,u'html.parser')
        section = soup.find('dl',attrs={'id':'profile-information'})

        try:
            name = soup.find('span',  attrs={'class': 'ctcd-user-name'}).text.strip()
            first_name = name.split(' ')[0].strip()
            last_name = name.split(' ')[1].strip()
            print('Name-',name)
        except:
            name = 'None'

        try:
            add = section.find('dd', attrs={'class': 'profile-information-address'}).find_next('span').text.strip()
            company = soup.find('dd', attrs={'class': 'profile-information-address'}).text.strip()
            company = company.split(add)[0]
            print('Company--',company)
        except:
            company = 'None'

        try:
            address = section.find('span', attrs={'class': 'street-address'}).text.strip()
            print('Address--',address)
        except:
            address = 'None'

        try:
            region = section.find('span', attrs={'class': 'region'}).text.strip()
            print('Region--',region)
        except:
            region = ''

        try:
            city = section.find('span', attrs={'class': 'locality'}).text.strip()
            print('City--',city)
        except:
            city = 'None'

        try:
            zip = section.find('span', attrs={'class': 'postal-code'}).text.strip()
            print('Zip--',zip)
        except:
            zip = 'None'

        try:
            check = section.find('dd', attrs={'class': 'zsg-lg-3-5 profile-information-mobile'})
            if check==None:
                mobile = section.find('dd', attrs={'class': 'zsg-lg-3-5 profile-information-cell'}).text.strip()
            else:
                mobile = check.text.strip()

            print('Mobile--',mobile)
        except:
            mobile = 'None'

        try:
            website = section.find('dd', attrs={'class': 'zsg-lg-3-5 profile-information-websites'}).find_next('a')['href']
            print('Website--',website)
        except:
            website = 'None'

        try:
            broker_phone = section.find('dd',attrs={'class':'profile-information-brokerage'}).text.strip()
            print('Broker Phone--',broker_phone)
        except:
            broker_phone = 'None'


        # try:
        #     email = section.find('div', attrs={'class': 'hero-license'}).text.strip()
        #     print('Email--',email)
        # except:
        #     email = 'None'


        try:
            transactions = soup.find('li', attrs={'class': 'ctcd-item_sales'}).text.strip()
            print('Transactions--',transactions)
        except:
            transactions = 'None'


        profile_link = value
        sheet1.write(row, 0, first_name)
        sheet1.write(row, 1, last_name)
        sheet1.write(row, 2, company)
        sheet1.write(row, 3, address)
        sheet1.write(row, 4, city)
        sheet1.write(row, 5, region)
        sheet1.write(row, 6, zip)
        sheet1.write(row, 7, mobile)
        sheet1.write(row, 8, broker_phone)
        sheet1.write(row, 9, website)
        sheet1.write(row, 10, transactions)
        sheet1.write(row, 11, profile_link)
        wb.save('D:\PycharmProjects\Files\Zillow_Profiles8967.xls')
        print('--'*100)
        print(datetime.datetime.now())
        r = r + 1
        print(str(r) + "/" + str(sheet.nrows))
        print('--' * 150)

    except:
        print('No Links Collected From->',i)
        continue

