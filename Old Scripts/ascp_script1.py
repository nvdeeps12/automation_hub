import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import re
options = webdriver.FirefoxOptions()
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='D:\geckodriver',options=options)
import xlwt


states_arr = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'District of Columbia', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'American Samoa', 'Guam', 'Northern Mariana Islands', 'Puerto Rico', 'U.S. Virgin Islands']
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First_Name')
sheet1.write(0, 1, 'Last_Name')
sheet1.write(0, 2, 'Email')
sheet1.write(0, 3, 'Occupation')
sheet1.write(0, 4, 'Number')
sheet1.write(0, 5, 'City')
sheet1.write(0, 6, 'State')
sheet1.write(0, 7, 'License')
sheet1.write(0, 8, 'Speciality')
sheet1.write(0, 9, 'Qualification')
sheet1.write(0, 10, 'Expeience')
sheet1.write(0, 11, 'Languages Served')
sheet1.write(0, 12, 'Payment Options')
sheet1.write(0, 13, 'Provides Services To')


class ascp():
    def pass_links(self):
            self.row = 0
            for i in states_arr:
                try:
                    state_link = 'https://ascp.continuum.host/directory/result.php?State_Licensed_In='+(i)+'&Pharmacist_Provides_Services_To=Health+Care+Service+Providers&Submit=Search&page=1'
                    driver.get(state_link)
                    soup = BeautifulSoup(driver.page_source, u"html.parser")
                    total_pages = soup.find('li',attrs={'class':'details'}).text.strip()
                    total_pages = total_pages.split('of')[-1]
                    total_pages = int(total_pages)
                    total_pages = total_pages+1
                    print(total_pages)
                except:
                    continue

                for p in range(total_pages):
                    page = 'https://ascp.continuum.host/directory/result.php?State_Licensed_In='+(i)+'&Pharmacist_Provides_Services_To=Health+Care+Service+Providers&Submit=Search&page='+str(p)
                    print(page)
                    self.page_link_soup(page)


    def page_link_soup(self,page):
        driver.get(page)
        page_soup = BeautifulSoup(driver.page_source, u"html.parser")
        profile_links = page_soup.find('table',attrs={'class':'table table-striped'}).find('tbody').findAll('a')
        for i in profile_links:
            anchors = i['href']
            profile_link = 'https://ascp.continuum.host/directory/'+str(anchors)
            self.row = self.row+1
            self.profile_link_soup(profile_link)

    def profile_link_soup(self, profile_link):
        state = ''
        city = ''
        print('Row-----',self.row)
        profile_link = profile_link
        driver.get(profile_link)

        profile_soup = BeautifulSoup(driver.page_source, u"html.parser")
        # print(profile_soup)
        try:
            full_name = str(profile_soup.find('div', attrs={'class': 'displayTitle'}).text.strip())

            first_name = full_name.split(' ')[0].strip()
            last_name = full_name.split(first_name)[1].strip()
            at = re.compile('@')
            e = profile_soup.find('tr', attrs={'class': 'record_no_hover'})
            email = e.find(text=at)
            number = e.find_next('br').next_element
        except:
            full_name = 'None'

        try:
            occupation = profile_soup.find('div', attrs={'class': 'displayTitle'}).find_previous('td').find_next( 'br').previous_element
            if '@' in occupation:
                occupation = 'None'
            else:
                occupation = profile_soup.find('div', attrs={'class': 'displayTitle'}).find_previous('td').find_next('br').previous_element
            # res2 = [int(i) for i in occupation.split() if i.isdigit()]
            # print(res2)

            #     print(r+r)

        except:
            occupation = 'None'

        try:
            numm = profile_soup.find('div', attrs={'class': 'displayTitle'}).find_previous('td').text.strip()
            # res2 = [int(i) for i in numm.split() if i.isdigit()]
            rg = re.findall(
                "(?:(?:\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?",
                numm)
            num = rg[0][0] + rg[0][1] + rg[0][2] + rg[0][3] + rg[0][4] + rg[0][5]
            if len(num) == 10:
                number = num
            elif '@' in num:
                number = "None"
            else:
                number = num[0:10]
            # for r in rg:
        except:
            number = "None"
        try:
            li = []
            address = str(profile_soup.find('tr', attrs={'class': 'record_no_hover'}).text.strip())
            # address ="".join(reversed(address))
            address = address.strip()
            state = str(address.split(',')[1]).strip()
            if '\t\t \n\t\t' in address:
                city = str(address.split(',')[0].split('\t\t \n\t\t')[1])


            elif '\t\t\n\t\t' in address:
                city = address.split(',')[0].split('\t\t\n\t\t')[1].strip()
                if str(number[6:10]) in address:
                    city = address.split(',')[0].split(str(number[6:10]))[1].strip()
            # state = "".join(reversed(state))
            # city = "".join(reversed(city))
            #
            # elif email in address:
            #
            #     city = address.split(',')[0].split('\t\t\n\t\t')[1]
            # elif str(number[6:10]) in address:
            #     city = address.split(',')[0].split(str(number[6:10]))[1]

            li.append(address)


            new_section = profile_soup.find('table',attrs={'class':'table table-striped'})

            try:
                licence = new_section.find('td', text=re.compile('Licensed To')).find_next('td').text.strip()
            except:
                licence = 'None'

            sheet1.write(self.row, 7, licence)

            try:
                speciality = new_section.find( text=re.compile('Specialty')).find_next('td').text.strip()
            except:
                speciality = 'None'



            sheet1.write(self.row, 8, speciality)


            try:
                qualification = new_section.find( text=re.compile('Qualifications')).find_next('td').text.strip()
            except:
                qualification = 'None'


            sheet1.write(self.row, 9, qualification)

            try:
                experience = new_section.find('td', text=re.compile('Years')).find_next('td').text.strip()
            except:
                experience = 'None'

            sheet1.write(self.row, 10, experience)


            try:
                languages = new_section.find('td', text=re.compile('Languages')).find_next('td').text.strip()
            except:
                languages = 'None'

            sheet1.write(self.row, 11, languages)


            try:
                payment = new_section.find( text=re.compile('Payment')).find_next('td').text.strip()
            except:
                payment = 'None'


            sheet1.write(self.row, 12, payment)


            try:
                services = new_section.find('td', text=re.compile('Provides')).find_next('td').text.strip()
            except:
                services = 'None'

            sheet1.write(self.row, 13, services)


        except:
            pass
        # print('First Name ' + first_name)
        # print('Last Name ' + last_name)
        # print('Email ' + email.strip())
        # print('Ocupation ' + occupation.strip())
        # print('Number ' + number.strip())
        # print('City ' + city.strip())
        # print('State ' + state.strip())

        sheet1.write(self.row, 0, first_name)
        sheet1.write(self.row, 1, last_name)
        sheet1.write(self.row, 2, email)
        sheet1.write(self.row, 3, occupation)
        sheet1.write(self.row, 4, number)
        sheet1.write(self.row, 5, city)
        sheet1.write(self.row, 6, state)
        import datetime
        print(datetime.datetime.now())

        wb.save('D:\PycharmProjects\Files\json_outputs\Ascp_DataSc.xls')

        print('----' * 100)


objs = ascp()
objs.pass_links()



