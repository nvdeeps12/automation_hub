#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 22 21:36:28 2019

@author: aneelasaleem
"""
import os.path
from os.path import expanduser
from os import path

import pylint.lint
from pylint.reporters.text import TextReporter
from io import StringIO 
import re
from operator import itemgetter
import fileinput
from difflib import SequenceMatcher
import json

    
class CodePerformance(object):
        
    #original formula for calculating the rating
    formula = "evaluation=10.0 - ((float(5 * error + warning + refactor + convention) / statement) * 10)"
    
    #data copied from website
    data = {'scraagr@protonmail.com':{'longTestCases':[(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0)], 'shortTestCases': [(638,1), (1020,1), (725,0), (1001,1), (916,0), (1045,0), (997,0), (1117,0), (1016,0), (969,1)], 'files' : ['long_scraagr.py', 'short_scraagr.py']},
       'cloud.engineerdwh@gmail.com':{'longTestCases':[(1999,1),(1685,1),(2159,1),(1923,1),(2078,1),(2270,1),(1659,1),(2262,1),(1733,1),(1901,1)], 'shortTestCases': [(1085,1),(686,1),(1233,1),(728,1),(1212,1),(845,1),(1278,1),(766,1),(1280,1),(815,1)], 'files' : ['long_cloud.py', 'short_cloud.py']},
       'aneela.pucitian@gmail.com':{'longTestCases':[(2814,1),(980,1),(986,1),(2643,1),(874,1),(839,1),(1047,1),(2785,1),(792,1),(2773,1)], 'shortTestCases': [(1204,0),(614,0),(675,1),(1103,0),(740,1),(1151,1),(722,1),(702,1),(698,1),(1138,1)], 'files' : ['long_aneela.py', 'short_aneela.py']},
       'naeemahasan@gmail.com':{'longTestCases':[(952,1),(967,1),(963,0),(838,1),(857,0),(1012,1),(1011,0),(1040,0),(1057,1),(966,0)], 'shortTestCases': [(838,1),(1213,0),(1142,1),(1062,0),(1122,1),(1174,1),(1098,1),(1200,1),(1186,1),(1179,1)], 'files' : ['long_naeemahasan.py', 'short_naeemahasan.py']},
       'jalis.tarif55@gmail.com':{'longTestCases':[(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0)], 'shortTestCases': [(792,1),(630,1),(366,1),(622,1),(691,1),(549,1),(508,1),(396,1),(630,1),(555,1)], 'files' : ['long_jalis.py', 'short_jalis.py']}}
    
    #for storing all metrics separately for long and short questions
    result = {}

    #parse cyclomatic complexity response
    def parseCyclomaticComplexity(self, text):
        texts = text.strip().split('\n')
        complexity = 0
        if len(texts) == 0:     #if no complexity
            return complexity
        else:
            line = [ line for line in texts if 'too-complex' in line][0] #filter line containing complexity
            msgid, symbol, msg = line.split(',')
            complexity = int(msg.split()[-1])
             
            return complexity         
    
    #parse convention error score
    def parseConventionError(self, text):
        texts = text.strip().split('\n')
        rate = 0
        #if empty record
        if len(texts) == 0:
            return rate
        else:
            if len(texts[-1]) > 0:
                rate = texts[-1].split()[6].split('/')[0]

            return max(float(rate), 0)  #round negative values to zero
    
    #update .pylintrc file for running different commands
    def updateRCfile(self, customFormula, revert):
         #update .pylintrc file 
        s = open(expanduser("~")+"/.pylintrc").read()
        
        if revert==True: #if wants to revert back with original formula
            s = s.replace(customFormula, self.formula)
        else:            #if wants to update the formula
            s = s.replace(self.formula, customFormula)
            
        f = open(expanduser("~")+"/.pylintrc", 'w')
        f.write(s)
        f.close()
        
    #get the candidate with minimum processing time (most efficient)
    def getMinAverageCandidate(self, question):
        user = {}
        maxTestCases = 0
        averagEfficiency = 1000000
        for k, v in self.data.items():
            #calculate number of completed test cases
            passedTestCases = sum(n for _, n in v[question])          
            if passedTestCases >= maxTestCases:     #first check for maximum number of test cases passed by the candidate
                maxTestCases = passedTestCases
                #calculate average efficiency
                tempEfficiency = sum(n for n, _ in v[question])/10                   
                if tempEfficiency != 0 and tempEfficiency < averagEfficiency: #if the processing time is less
                    averagEfficiency = tempEfficiency
                    user = k                  #get the key (user id)
        return user
    
    #get maximum number of test cases passed in the whole data for long/short question
    def getMaximumPassedTestCases(self, question):
        maxTestCases = 0
        for k, v in self.data.items():
            #calculate number of completed test cases
            passedTestCases = sum(n for _, n in v[question])          
            if passedTestCases >= maxTestCases:
                maxTestCases = passedTestCases
        return maxTestCases
    
    #get number of test cases passed by a particular user
    def getPassesTestCases(self, user, questionType):
        scores = map(lambda t:t[1],self.data[user][questionType])
        passed = list(filter(lambda t: t==1, scores))
        return len(passed)
    
    #1. Calculate Test Case Score
    def calculateTestCaseScore(self, questionType):
        dataKey = resultKey = ''
        scoreFactor = 0 
        if 'long' in questionType:    
            scoreFactor = 1.5
            dataKey = 'longTestCases'            #to be fetched from data
            resultKey = 'longTestCaseScore'      #to be stored in the result
        else:
            scoreFactor = 0.5
            dataKey = 'shortTestCases'
            resultKey = 'shortTestCaseScore'
   
        for key in self.data.keys():
            dataObj = self.data[key]             #get the object from the data
            if key not in self.result.keys():   #if the key doesn't exist in the result, create empty
                self.result[key] = {}
            scores = map(lambda t:t[1],dataObj[dataKey])        #get the test scores [1,0,...]
            passed = list(filter(lambda t: t==1, scores))       #count number of passed test cases
            total = len(passed) * scoreFactor * 5               #multiply by 1.5/0.5 for Mettl score and multiply by 5 to scale it to 25-75
            self.result[key][resultKey]= round(total,2)         #store the test case score for each user in the result
        
    def getTotalTestCaseScore(self, user, refresh):
        if refresh != None: #if want to refresh the results
            if 'longTestCaseScore' not in self.result.keys():   
                self.calculateTestCaseScore(self, 'long')
                self.calculateTestCaseScore(self, 'short')
        #otherwise return from the json result (add long and short score)
        return self.result[user]['longTestCaseScore'] + self.result[user]['shortTestCaseScore']
        
    #2. Calculate Efficiency Score
    def calculateEfficiencyScore(self, questionType):
        
        questionKey = resultKey = ''
        scaleFactor = rangeFactor = 0 
        if 'long' in questionType:
            scaleFactor = 7.5
            rangeFactor = 100
            questionKey = 'longTestCases'     #to be fetched from data
            resultKey = 'longEfficiencyScore' #to be inserted in the result
        else:
            scaleFactor = 2.5
            rangeFactor = 50
            questionKey = 'shortTestCases'
            resultKey = 'shortEfficiencyScore'

        
        efficientCandidateKey = self.getMinAverageCandidate(self, questionKey) #get the most efficient candidate id 
        efficientCandidate = self.data[efficientCandidateKey]                   #get the candidate object
        
        for key in self.data.keys():    #every user's id
            dataObj = self.data[key]   
            if key not in self.result.keys():   #if doesn't exist in result
                self.result[key] = {}           #create new
    
            testCases = dataObj[questionKey]    #get the test cases array
            score = i = 0
            while i < 10:
                if testCases[i][1] == 1:    #if the test case is completed
                    if testCases[i][0]  <= efficientCandidate[questionKey][i][0] :   #if it's less than or equal to bench mark code
                        score += scaleFactor                                         #give 7.5 for long question, 2.5 for short question
                    else: #if the difference is less than or equal to 100 ms for long question and less than or equal to 50 ms for short question
                        remainder = testCases[i][0] - efficientCandidate[questionKey][i][0] #get the difference of running times
                        if remainder <= rangeFactor:                                        
                            score += scaleFactor                                    #give maximum score otherwise 0
                i += 1
            self.result[key][resultKey] = round(score,2)
    
    def getEfficiencyScore(self, user, refresh):
        if refresh != None:#if want to refresh the results
            if 'longEfficiencyScore' not in self.result[user].keys():   
                self.calculateEfficiencyScore(CodePerformance, 'long')
                self.calculateEfficiencyScore(CodePerformance, 'short')
        #otherwise return from the json result (add long and short score)
        return self.result[user]['longEfficiencyScore'] + self.result[user]['shortEfficiencyScore']

    #3. Calculate Cyclomatic Complexity  
    def calculateCyclomaticComplexity(self, questionType):
        questionKey = questionType+"TestCases"
        resultKey = questionType+"ComplexityScore"
        maxScore = factor = 0
        
        if 'long' in questionType:
            maxScore = 75
            factor = 12
        else:
            maxScore = 25
            factor = 4
        
        #get the maximum number of test cases passed int the data
        maxTestCasesCompleted = self.getMaximumPassedTestCases(self, questionKey)
       
        minComplexity = 100000000
        for key in self.data.keys():
            passedTestCases = self.getPassesTestCases(self, key, questionKey) #get number of test cases passed by this user
            if key not in self.result.keys():                                   #if doesn't exist in result
                self.result[key] = {}
            if passedTestCases == maxTestCasesCompleted:        #calculate complexity of the candidates who passed the maximum test cases, give zero to others
                for file in self.data[key]['files']:
                    if questionType in file:                    #if long/short
                        my_output = StringIO()
                        reporter = TextReporter(output=my_output)
                        pylint_opts = [os.path.normpath('codes/'+file), '-r','n', '--load-plugins=pylint.extensions.mccabe', '--max-complexity=0','--rcfile=~/.pylintrc','--msg-template= {msg_id},{symbol},{msg}']
                        pylint.lint.Run(pylint_opts,reporter=reporter, exit=False)
                        output_str = my_output.getvalue()       #get the result and parse it
                        self.result[key][resultKey] = round(self.parseCyclomaticComplexity(None, output_str),2)
                        my_output.close()
                        #keep track of the minimum complexity for adjusting the scores
                        if self.result[key][resultKey] < minComplexity:
                            minComplexity = self.result[key][resultKey]
            else:           #if didn't complete maximum test cases then assign 0
                self.result[key][resultKey] = 0
        
        #adjust scores
        for key in self.data.keys():
            complexity = self.result[key][resultKey]
            if complexity != 0: #multiply remainder with 12 or 4 and subtract from 75 or 25
                newComplexity = maxScore - ((complexity-minComplexity)*factor)
                self.result[key][resultKey] = newComplexity
            
    def getComplexityScore(self, user, refresh):
        if refresh != None:#if want to refresh the results
            if 'longComplexityScore' not in self.result[user].keys():   
                self.calculateCyclomaticComplexity(self, 'long')
                self.calculateCyclomaticComplexity(self, 'short')
        #otherwise return from the json result (add long and short score)
        return self.result[user]['longComplexityScore'] + self.result[user]['shortComplexityScore']
        
    #4. Calculate Convention Error   
    def calculateConventionError(self, questionType):
        
        questionKey = resultKey = ''
        factor = 0
        if 'long' in questionType:
            questionKey = 'longTestCases'
            resultKey = 'longConventionErrorScore'
            factor = 7.5
        else:
            questionKey = 'shortTestCases'
            resultKey = 'shortConventionErrorScore'
            factor = 2.5

        customFormula = "evaluation=10.0 - ((float(5 * error + convention) / statement) * 10)"
        self.updateRCfile(CodePerformance, customFormula, False) #update RC file with custom formula
        
        for key in self.data.keys():
    
            testCases = self.data[key][questionKey]
            scores = map(lambda t:t[1],testCases)
            passed = list(filter(lambda t: t==1, scores))
            
            if key not in self.result.keys():   
                self.result[key] = {}           #create new
                
            if len(passed) == 0:                #if no completed test case, assign zero
                self.result[key][resultKey] = 0
            else:
                for file in self.data[key]['files']:    
                    if questionType in file:            #else calculate the score for long/short question separately
                        my_output = StringIO()
                        reporter = TextReporter(output=my_output)
                        pylint_opts = [os.path.normpath('codes/'+file),'--rcfile=~/.pylintrc','--msg-template= {msg_id},{symbol},{msg}']
                        pylint.lint.Run(pylint_opts,reporter=reporter, exit=False)
                        output_str = my_output.getvalue()
                        self.result[key][resultKey] = round(self.parseConventionError(None, output_str) * factor,2)
                        my_output.close()
            
        #revert back the RC file
        self.updateRCfile(CodePerformance, customFormula, True)

    def getConventionErrorScore(self, user, refresh):
        if refresh != None:#if want to refresh the results
            if 'longConventionErrorScore' not in self.result[user].keys():   
                self.calculateConventionError(self, 'long')
                self.calculateConventionError(self, 'short')
        #otherwise return from the json result (add long and short score)
        return self.result[user]['longConventionErrorScore'] + self.result[user]['shortConventionErrorScore']

    #5. Calculate Error Score
    def calculateErrorScore(self, questionType):
        questionKey = resultKey = ''
        factor = 0
        if 'long' in questionType:
            questionKey = 'longTestCases'
            resultKey = 'longErrorScore'
            factor = 7.5
        else:
            questionKey = 'shortTestCases'
            resultKey = 'shortErrorScore'
            factor = 2.5
       
        customFormula = "evaluation=10.0 - ((float(5 * error) / statement) * 10)"
        for key in self.data.keys():
    
            testCases = self.data[key][questionKey]
            scores = map(lambda t:t[1],testCases)
            passed = list(filter(lambda t: t==1, scores))
            
            if key not in self.result.keys():   
                self.result[key] = {}           #create new
                
            if len(passed) == 0:        #if None of the test case passed the assign 0 score
                self.result[key][resultKey] = 0
            else:
                for file in self.data[key]['files']:    #else calculate the score for long/short question separately
                    my_output = StringIO()
                    reporter = TextReporter(output=my_output)
                    pylint_opts = [os.path.normpath('codes/'+file), '--rcfile=~/.pylintrc','--msg-template= {msg_id},{symbol},{msg}']
                    pylint.lint.Run(pylint_opts,reporter=reporter, exit=False)
                    output_str = my_output.getvalue()
                    self.result[key][resultKey] = round(self.parseConventionError(None, output_str) * factor, 2)
                    my_output.close()
        #revert back the RC file
        self.updateRCfile(CodePerformance, customFormula, True)

    def getErrorScore(self, user, refresh):
        if refresh != None:#if want to refresh the results
            if 'longErrorScore' not in self.result[user].keys():   
                self.calculateErrorScore(self, 'long')
                self.calculateErrorScore(self, 'short')
        #otherwise return from the json result (add long and short score)
        return self.result[user]['longErrorScore'] + self.result[user]['shortErrorScore']


    def calculateFinalScore(self, tc, eff, comp, conv, err):
            #multiple each metric with predefined weight
         return tc*.2853 + eff*.2140 + comp*.2107 + conv*.1153 + err*.1747

        
    def getFinalScore(self, refresh):
        
        if path.exists("result.json") == True and refresh == None: #if doesn't want to refresh, load from file
            self.result = json.load( open( "result.json" ) )
          
        for key in self.data.keys():
            print("{} : {}".format(key, self.calculateFinalScore(self,
            self.getTotalTestCaseScore(self, key, refresh),
            self.getEfficiencyScore(self, key, refresh),
            self.getComplexityScore(self, key, refresh),
            self.getConventionErrorScore(self, key, refresh),
            self.getErrorScore(self, key, refresh))))
            
            #dump the result in json file
        json.dump( self.result, open( "result.json", 'w' ) )
            
            
if __name__ == "__main__":
    
    while True:
        refresh = input("If you want to refresh the results type yes otherwise no: ")
        if refresh == 'yes' or refresh == 'no':
            break
        else:
            print("Wrong input. Please Try again!")
    
    if refresh == 'no': refresh = None
    
    CodePerformance.getFinalScore(CodePerformance, refresh)

