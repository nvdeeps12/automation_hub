import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import xlwt

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')
driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)

states_arr = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'District of Columbia', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'American Samoa', 'Guam', 'Northern Mariana Islands', 'Puerto Rico', 'U.S. Virgin Islands']

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Title')
sheet1.write(0, 1, 'Address')
sheet1.write(0, 2, 'Phone')
sheet1.write(0, 3, 'Website')
sheet1.write(0, 4, 'Directions')




class ascp():
    def pass_links(self):
        self.row = 0
        try:
            state_link = 'http://www.trainingpages.com/listings/?ls'
            driver.get(state_link)
            soup = BeautifulSoup(driver.page_source, u"html.parser")
            total_pages = soup.find('div',attrs={'class':'page-count pull-right'}).text.strip()
            total_pages = total_pages.split('of')[-1]
            total_pages = int(total_pages)
            total_pages = total_pages+1
            print(total_pages)
        except:
            total_pages = 0

        print("Total Pages---",total_pages)

        for p in range(total_pages):
            if p==0:
                p = 1
            page = 'http://www.trainingpages.com/listings/page/'+str(p)+str('/?ls')
            print('Running_Page--',page)

            self.page_link_soup(page)


    def page_link_soup(self,page):
        driver.get(page)
        page_soup = BeautifulSoup(driver.page_source, u"html.parser")
        profile_links = page_soup.findAll('a',attrs={'class':'entry-thumbnail'})
        for i in profile_links:
            anchors = i['href']
            profile_link = str(anchors)
            self.row = self.row + 1
            self.profile_link_soup(profile_link)

    def profile_link_soup(self, profile_link):

        profile_link = profile_link
        driver.get(profile_link)

        profile_soup = BeautifulSoup(driver.page_source, u"html.parser")
        # print(profile_soup)

        try:
            title = profile_soup.find('h1',attrs={'class':'entry-title'}).text.strip()
            print(title)
        except:
            title = 'None'



        try:
            address = profile_soup.find('div',attrs={'class':'listing-address-wrap'}).text.strip()
            print(address)
        except:
            address = 'None'

        try:
            phone = profile_soup.find('li',attrs={'class':'listing-phone'}).text.strip()
            print(phone)
        except:
            phone = 'None'


        try:
            website = profile_soup.find('li',attrs={'id':'listing-website'}).find('a')['href']
            print(website)
        except:
            website = 'None'

        try:
            directions = profile_soup.find('li',attrs={'id':'listing-directions'}).find('a')['href']
            print(directions)


        except:
            directions = 'None'
        sheet1.write(self.row, 0, title)
        sheet1.write(self.row, 1, address)
        sheet1.write(self.row, 2, phone)
        sheet1.write(self.row, 3, website)
        sheet1.write(self.row, 4, directions)
        import datetime
        print(datetime.datetime.now())
        wb.save('D:\PycharmProjects\Files\json_outputs\Trainng_Pages.xls')
        print('Done------->',self.row)
        print('----' * 100)





objs = ascp()
objs.pass_links()



