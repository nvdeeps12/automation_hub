from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime
import sys

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='D:\chromedriver', options=options)
wb = xlwt.Workbook()
rd = xlrd.open_workbook('D:\PycharmProjects\Files\Apple_uu.xlsx')
sheet = rd.sheet_by_index(0)

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Id')
sheet1.write(0, 1, 'Product Name')
sheet1.write(0, 2, 'Url')
sheet1.write(0, 3, 'Title')
sheet1.write(0, 4, 'Thumb')
sheet1.write(0, 5, 'Finish')
sheet1.write(0, 6, 'Capacity')
sheet1.write(0, 7, 'Size')
sheet1.write(0, 8, 'Display')
sheet1.write(0, 9, 'Chip')
sheet1.write(0, 10, 'Camera')
sheet1.write(0, 11, 'Video')
sheet1.write(0, 12, 'Face_time')
sheet1.write(0, 13, 'Touch_id')
sheet1.write(0, 14, 'Apple_pay')
sheet1.write(0, 15, 'Cell_Wireless')
sheet1.write(0, 17, 'Location')
sheet1.write(0, 18, 'Video Call')
sheet1.write(0, 19, 'Audio ')
sheet1.write(0, 20, 'Playback')
sheet1.write(0, 21, 'Tv Audio')
sheet1.write(0, 22, 'Siri')
sheet1.write(0, 23, 'Button')
sheet1.write(0, 24, 'Power')
sheet1.write(0, 25, 'Sensor')
sheet1.write(0, 26, 'Os')
sheet1.write(0, 27, 'ACc')
sheet1.write(0, 28, 'Apps')
sheet1.write(0, 29, 'Free_app')
sheet1.write(0,30,'Headphones')
sheet1.write(0,31,'Sim')
sheet1.write(0,32,'Rating')
sheet1.write(0,33,'Mail')
sheet1.write(0,34,'System')
sheet1.write(0,35,'Lang')
sheet1.write(0,36,'Inbox')
sheet1.write(0,37,'App_Env')
sheet1.write(0,38,'App_Giv')
sheet1.write(0,39,'Splash')
sheet1.write(0,40,'Modified Date')


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

app_dict = dict({})

arr = ['https://support.apple.com/kb/SP768?viewlocale=en_US&locale=en_US']

row = 0
r = 0
for i in arr:
    try:
        row = row + 1
        value = i
        print('Process Started For--', value)
        if value == '':
            continue
        res = requests.get(value , headers = headers)
        print('Status_Code--',res.status_code)
    except:
        continue

    try:
        soup = BeautifulSoup(res.content, u'html.parser')
        id = str(value).split('/kb/')[1].split('?')[0]

        app_dict.update({"id": id})
        try:
            name = soup.find('h1',  attrs={'id': 'main-title'}).text.strip()
            if '-' in name:
                name = name.split('-')[0]
            if '(' in name:
                name = name.split('(')[0]
            name = name.replace('.','')

            print('PRoduct---',name)
            app_dict.update({"Prod_name":name})
            url = value
            app_dict.update({"url": url})
            title = soup.find('h1',  attrs={'id': 'main-title'}).text.strip()
            app_dict.update({"title": title})
            thumb_nail = 'https://support.apple.com/kb/image.jsp?productid='+str(id)
            app_dict.update({"thumbnail": thumb_nail})
        except:
            name = 'None'

        try:
            specs = soup.find('div',  attrs={'class': 'attr SPECIFICATION'})

            try:
                finish_head = specs.find('h3', text=re.compile('Finish')).text.strip()

            except:
                finish_head = ''
            try:
                capacity_head = specs.find('h3', text=re.compile('Finish')).find_next('h3').text.strip()

            except:
                capacity_head = ''

            try:
                size_head = specs.find('h3', text=re.compile('Finish')).find_next('h3').find_next('h3').text.strip()
            except:
                size_head = ''
            try:
                display_head = specs.find('h3', text=re.compile('Display')).text.strip()
            except:
                display_head = ''

            try:
                splash = specs.find('h3', text=re.compile('Splash')).text.strip()
            except:
                splash = ''

            try:
                chip_head = specs.find('h3', text=re.compile('Chip')).find_next('ul').text.strip()
            except:
                chip_head = ''
            try:
                camera = specs.find('h3', text=re.compile('Camera')).find_next('ul').text.strip()
            except:
                camera = ''
            try:
                vid_rec = specs.find('h3', text=re.compile('Video Recording')).text.strip()
            except:
                vid_rec = ''
            try:
                face_time = specs.find('h3', text=re.compile('FaceTime HD Camera')).text.strip()
            except:
                face_time = ''


            try:
                touch_id = specs.find('h3', text=re.compile('Touch ID')).text.strip()
            except:
                touch_id = ''
            try:
                app_pay = specs.find('h3', text=re.compile('Apple Pay')).text.strip()
            except:
                app_pay = ''

            try:
                cel_wireless = specs.find('h3', text=re.compile('Cellular and Wireless')).text.strip()
            except:
                cel_wireless = ''


            try:
                location = specs.find('h3', text=re.compile('Location')).text.strip()
            except:
                location = ''

            try:
                vid_call = specs.find('h3', text=re.compile('Video Calling')).text.strip()
            except:
                vid_call = ''
            try:
                audio  = specs.find('h3', text=re.compile('Audio Calling')).text.strip()
            except:
                audio = ''
            try:
                playback = specs.find('h3', text=re.compile('Audio Playback')).text.strip()
            except:
                playback = ''
            try:
                tv_audio = specs.find('h3', text=re.compile('Video Playback')).find_next('ul').text.strip()
            except:
                tv_audio = ''
            try:
                siri = specs.find('h3', text=re.compile('Siri')).text.strip()
            except:
                siri = ''
            try:
                button = specs.find('h3', text=re.compile('Buttons')).text.strip()
            except:
                button = ''
            try:
                power = specs.find('h3', text=re.compile('Power')).text.strip()
            except:
                power = ''
            try:
                sensor = specs.find('h3', text=re.compile('Sensors')).text.strip()
            except:
                sensor = ''
            try:
                os = specs.find('h3', text=re.compile('Operating System')).text.strip()
            except:
                os = ''
            try:
                acccssebility = specs.find('h3', text=re.compile('Accessibility')).text.strip()
            except:
                acccssebility = ''

            try:
                built_apps_head = specs.find('h3', text=re.compile('Built-in Apps')).text.strip()
            except:
                built_apps_head = ''

            try:
                free_apps = specs.find('h3', text=re.compile('Free Apps from Apple')).text.strip()
            except:
                free_apps = ''
            try:
                headphones = specs.find('h3', text=re.compile('Headphones')).text.strip()
            except:
                headphones = ''
            try:
                sim = specs.find('h3', text=re.compile('SIM Card')).text.strip()
            except:
                sim = ''
            try:
                rating = specs.find('h3', text=re.compile('Rating')).text.strip()
            except:
                rating = ''
            try:
                mail = specs.find('h3', text=re.compile('Mail')).text.strip()
            except:
                mail = ''
            try:
                system = specs.find('h3', text=re.compile('System')).text.strip()
            except:
                system = ''
            try:
                lang = specs.find('h3', text=re.compile('Languages')).text.strip()
            except:
                lang = ''
            try:
                in_box = specs.find('h3', text=re.compile('In the Box')).text.strip()
            except:
                in_box = ''
            try:
                app_env = specs.find('h3', text=re.compile('Apple and the Environment')).text.strip()
            except:
                app_env =''
            try:
                app_giv = specs.find('h3', text=re.compile('Apple GiveBack')).text.strip()

            except:
                app_giv = ''

        except:
            specs = ''
            continue

        try:
            finish_ul = specs.find('h3', text=re.compile('Finish')).find_next('ul').text.strip()
            finish_ul = finish_ul.split('\n')
            finish_ul = [finish_ul]
        except:
            finish_ul = []
        app_dict.update({finish_head: finish_ul})


        try:
            cap_ul = specs.find('h3', text=re.compile('Finish')).find_next('ul').find_next('ul').text.strip()
            cap_ul = cap_ul.split('\n')
            cap_ul = [cap_ul]
        except:
            cap_ul = []
        app_dict.update({capacity_head: cap_ul})


        try:
            size_ul =specs.find('h3', text=re.compile('Finish')).find_next('h3').find_next('h3').find_next('ul').text.strip()
            size_ul = size_ul.split('\n')
            size_ul = [size_ul]
            print('Size---',size_ul)

        except:
            size_ul = []
        app_dict.update({size_head: size_ul})


        try:
            display_ul = specs.find('h3', text=re.compile('Display')).find_next('ul').text.strip()
            display_ul = display_ul.split('\n')
            display_ul = [display_ul]
        except:
            display_ul = []
        app_dict.update({display_head: display_ul})

        try:
            splash_ul = specs.find('h3', text=re.compile('Splash')).find_next('ul').text.strip()
            splash_ul = splash_ul.split('\n')
            splash_ul = [splash_ul]
        except:
            splash_ul = []
        app_dict.update({splash: splash_ul})


        try:
            chip_ul = specs.find('h3', text=re.compile('Chip')).find_next('ul').text.strip()
            chip_ul = chip_ul.split('\n')
            chip_ul = [chip_ul]
        except:
            chip_ul = []
        app_dict.update({chip_head: chip_ul})

        try:
            cam_ul = specs.find('h3', text=re.compile('Camera')).find_next('ul').text.strip()
            cam_ul = cam_ul.split('\n')
            cam_ul = [cam_ul]
        except:
            cam_ul = []
        app_dict.update({camera: cam_ul})


        try:
            vid_ul = specs.find('h3', text=re.compile('Video Recording')).find_next('ul').text.strip()
            vid_ul = vid_ul.split('\n')
            vid_ul = [vid_ul]
        except:
            vid_ul = []
        app_dict.update({vid_rec: vid_ul})

        try:
            face_ul = specs.find('h3', text=re.compile('FaceTime')).find_next('ul').text.strip()
            face_ul = face_ul.split('\n')
            face_ul = [face_ul]
        except:
            face_ul = []
        app_dict.update({face_time: face_ul})

        try:
            touch_ul = specs.find('h3', text=re.compile('Touch ID')).find_next('ul').text.strip()
            touch_ul = touch_ul.split('\n')
            touch_ul = [touch_ul]
        except:
            touch_ul = []
        app_dict.update({touch_id: touch_ul})

        try:
            app_ul = specs.find('h3', text=re.compile('Apple Pay')).find_next('ul').text.strip()
            app_ul = app_ul.split('\n')
            app_ul = [app_ul]
        except:
            app_ul = []

        app_dict.update({app_pay: app_ul})

        try:
            cel_ul = specs.find('h3', text=re.compile('Cellular and Wireless')).find_next('ul').text.strip()
            cel_ul = cel_ul.replace("'",'')
            cel_ul = cel_ul.split('\n')

            cel_ul = [cel_ul]
        except:
            cel_ul = []

        app_dict.update({cel_wireless: cel_ul})


        try:
            location_ul = specs.find('h3', text=re.compile('Location')).find_next('ul').text.strip()
            location_ul = location_ul.split('\n')
            location_ul = [location_ul]
        except:
            location_ul = []
        app_dict.update({location: location_ul})


        try:
            vid_ul =specs.find('h3', text=re.compile('Video Calling')).find_next('ul').text.strip()
            vid_ul = vid_ul.split('\n')
            vid_ul = [vid_ul]
        except:
            vid_ul = []

        app_dict.update({vid_call: vid_ul})


        try:
            audio_ul = specs.find('h3', text=re.compile('Audio Calling')).find_next('ul').text.strip()
            audio_ul = audio_ul.split('\n')
            audio_ul = [audio_ul]
        except:
            audio_ul = []
        app_dict.update({audio: audio_ul})


        try:
            play_ul =specs.find('h3', text=re.compile('Audio Playback')).find_next('ul').text.strip()
            play_ul = play_ul.split('\n')
            play_ul = [play_ul]
        except:
            play_ul = []

        app_dict.update({playback: play_ul})


        try:
            tv_ul = specs.find('h3', text=re.compile('Video Playback')).find_next('ul').text.strip()
            tv_ul = tv_ul.split('\n')[1]
            tv_ul = [tv_ul]
        except:
            tv_ul = []
        app_dict.update({tv_audio: tv_ul})


        try:
            siri_ul = specs.find('h3', text=re.compile('Siri')).find_next('ul').text.strip()
            siri_ul = siri_ul.split('\n')
            size_ul = [siri_ul]
        except:
            siri_ul = []
        app_dict.update({siri: siri_ul})


        try:
            button_ul = specs.find('h3', text=re.compile('Buttons')).find_next('ul').text.strip()
            button_ul = button_ul.split('\n')
            button_ul = [button_ul]
        except:
            button_ul = []
        app_dict.update({button: button_ul})

        try:
            pow_ul = specs.find('h3', text=re.compile('Power')).find_next('ul').text.strip()
            pow_ul = pow_ul.split('\n')
            pow_ul = [pow_ul]
        except:
            pow_ul = []
        app_dict.update({power: pow_ul})

        try:
            sens_ul = specs.find('h3', text=re.compile('Sensors')).find_next('ul').text.strip()
            sens_ul = sens_ul.split('\n')
            sens_ul = [sens_ul]
        except:
            sens_ul = []
        app_dict.update({sensor: sens_ul})


        try:
            os_ul = specs.find('h3', text=re.compile('Operating System')).find_next('ul').text.strip()
            os_ul = os_ul.split('\n')

            os_ul = [os_ul]
        except:
            os_ul = []

        app_dict.update({os: os_ul})

        try:
            acc_ul =specs.find('h3', text=re.compile('Accessibility')).find_next('ul').text.strip()
            acc_ul = acc_ul.split('\n')
            acc_ul = [acc_ul]
        except:
            acc_ul = []
        app_dict.update({acccssebility: acc_ul})


        try:
            built_apps = specs.find('h3', text=re.compile('Built-in Apps')).find_next('ul').text.strip()
            built_apps =built_apps.split('\n')

            built_apps = [built_apps]
        except:
            built_apps = []
        app_dict.update({built_apps_head: built_apps})

        try:
            free_apps_ul = specs.find('h3', text=re.compile('Free Apps from Apple')).find_next('ul').text.strip()
            free_apps_ul = free_apps_ul.split('\n')
            free_apps_ul = [free_apps_ul]
        except:
            free_apps_ul = []

        app_dict.update({free_apps: free_apps_ul})

        try:
            headphones_ul = specs.find('h3', text=re.compile('Headphones')).find_next('ul').text.strip()
            headphones_ul = headphones_ul.split('\n')
            headphones_ul = [headphones_ul]
        except:
            headphones_ul = []
        app_dict.update({headphones: headphones_ul})


        try:
            sim_ul = specs.find('h3', text=re.compile('SIM Card')).find_next('ul').text.strip()
            sim_ul = sim_ul.split('\n')
            sim_ul = [sim_ul]
        except:
            sim_ul = []
        app_dict.update({sim: sim_ul})

        try:
            rating_ul =specs.find('h3', text=re.compile('Rating')).find_next('ul').text.strip()
            rating_ul = rating_ul.split('\n')
            rating_ul = [rating_ul]
        except:
            rating_ul = []
        app_dict.update({rating: rating_ul})

        try:
            mail_ul =specs.find('h3', text=re.compile('Mail')).find_next('ul').text.strip()
            mail_ul = mail_ul.split('\n')
            mail_ul = [mail_ul]
        except:
            mail_ul = []
        app_dict.update({mail: mail_ul})

        try:
            sys_ul = specs.find('h3', text=re.compile('System')).find_next('ul').text.strip()
            sys_ul = sys_ul.split('\n')
            sys_ul = [sys_ul]
        except:
            sys_ul = []
        app_dict.update({system: sys_ul})

        try:
            lang_ul =specs.find('h3', text=re.compile('Languages')).find_next('ul').text.strip()
            lang_ul = lang_ul.split('\n')
            lang_ul = [lang_ul]
        except:
            lang_ul = []
        app_dict.update({lang: lang_ul})



        try:
            in_box_ul = specs.find('h3', text=re.compile('In the Box')).find_next('ul').text.strip()
            in_box_ul = in_box_ul.split('\n')
            in_box_ul = [in_box_ul]
        except:
            in_box_ul = []
        app_dict.update({in_box: in_box_ul})

        try:
            app_env_ul = specs.find('h3', text=re.compile('iPhone and the Environment')).find_next('ul').text.strip()
            app_env_ul = app_env_ul.split('\n')
            app_env_ul = [app_env_ul]
        except:
            app_env_ul = []
        app_dict.update({app_env: app_env_ul})


        try:
            app_giv_ul = specs.find('h3', text=re.compile('Apple GiveBack')).find_next('ul').text.strip()
            app_giv_ul = app_giv_ul.split('\n')
            app_giv_ul = [app_giv_ul]
        except:
            app_giv_ul = []
        app_dict.update({app_giv: app_giv_ul})


        try:
            mod_date = soup.find('div',attrs={'class':'mod-date'}).text.strip()
            mod_date = [mod_date]
        except:
            mod_date = []
        app_dict.update({'Last_Mod': mod_date})


        import json

        name = name.strip()
        file_name = name +str('.json')
        print('File_Name--',file_name)
        #
        with open('D:\PycharmProjects\Files\json_outputs\ '+file_name, 'w') as f:
            json.dump(app_dict, f)

        print('--'*100)


        sheet1.write(row, 0, id)
        sheet1.write(row, 1, name)
        sheet1.write(row, 2, url)
        sheet1.write(row, 3, title)
        sheet1.write(row, 4, thumb_nail)
        sheet1.write(row, 5, finish_ul)
        sheet1.write(row, 6, cap_ul)
        sheet1.write(row, 7, size_ul)
        sheet1.write(row, 8, display_ul)
        sheet1.write(row, 9, chip_ul)
        sheet1.write(row, 10, cam_ul)
        sheet1.write(row, 11, vid_ul)
        sheet1.write(row, 12, face_ul)
        sheet1.write(row, 13, touch_ul)
        sheet1.write(row, 14, app_ul)
        sheet1.write(row, 15, cel_ul)
        sheet1.write(row, 17, location_ul)
        sheet1.write(row, 18, vid_ul)
        sheet1.write(row, 19, audio_ul)
        sheet1.write(row, 20, play_ul)
        sheet1.write(row, 21, tv_ul)
        sheet1.write(row, 22, siri_ul)
        sheet1.write(row, 23, button_ul)
        sheet1.write(row, 24, pow_ul)
        sheet1.write(row, 25, sens_ul)
        sheet1.write(row, 26, os_ul)
        sheet1.write(row, 27, acc_ul)
        sheet1.write(row, 28, built_apps)
        sheet1.write(row, 29, free_apps_ul)
        sheet1.write(row, 30, headphones_ul)
        sheet1.write(row, 31, sim_ul)
        sheet1.write(row, 32, rating_ul)
        sheet1.write(row, 33, mail_ul)
        sheet1.write(row, 34, sys_ul)
        sheet1.write(row, 35, lang_ul)
        sheet1.write(row, 36, in_box_ul)
        sheet1.write(row, 37, app_env_ul)
        sheet1.write(row, 38, app_giv_ul)
        sheet1.write(row, 39, splash_ul)
        sheet1.write(row, 40 ,mod_date)
        # wb.save('D:\PycharmProjects\Files\json_outputs\Apple_Updated.xls')


    except:
        print('No Links Collected From->')
        print(sys.exc_info())
        continue



# feild=app_dict.keys()
# print(app_dict)
# import csv
# with open('data.csv','w',newline='') as f:
#     writer=csv.DictWriter(f,fieldnames=feild)
#     writer.writeheader()
#     writer.writerow(app_dict)