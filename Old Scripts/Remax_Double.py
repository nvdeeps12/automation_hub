import time
from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import xlwt
import requests
from work.scripts.employzone_mail import createEmailContent,sendEmail
from datetime import datetime
import xlrd
from logs.LogHandler2 import Mylogger

log_obj = Mylogger()

rd = xlrd.open_workbook('D:\PycharmProjects\Files\\Urls\Links2Remax.xlsx')
sheet = rd.sheet_by_index(0)



# options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')

# driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)


names_arr = []

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First_name')
sheet1.write(0, 1, 'Last_name')
sheet1.write(0, 2, 'Street')
sheet1.write(0, 3, 'Locality')
sheet1.write(0, 4, 'Region')
sheet1.write(0, 5, 'Postal')
sheet1.write(0, 6, 'Mobile')
sheet1.write(0, 7, 'Office')
sheet1.write(0, 8, 'Licence No.')

referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }


class ascp():
    def pass_links(self):
        self.row = 0

        for i in range(sheet.nrows):
            try:
                state_link = sheet.cell_value(i, 0)
                print(state_link)
                page =requests.get(state_link,headers = headers)
                soup = BeautifulSoup(page.content, u"html.parser")
                total_pages = soup.find('span', attrs={'id': 'js-agent-result-count'}).text.strip()
                total_pages = int(total_pages) / 24
                total_pages = int(total_pages)

                if total_pages==0:
                    continue

                print('Total_Pages-->', total_pages)
                log_obj.Writelog(state_link)


                # if total_pages>20:
                #     email_content = createEmailContent(str(state_link),str(self.row), str(total_pages))
                #     sendEmail(email_content)

                self.supply_pages(total_pages, state_link)

            except:
                total_pages = 0


    def supply_pages(self,total_pages,state_link):
        for p in range(total_pages):
            p = p+1
            if p==0:
                p= 1
            if p>10:
                page = state_link.replace('p001','p0'+str(p))
            else:
                page = state_link.replace('p001','p00'+str(p))
            # print('Running_Page--',page)

            self.page_link_soup(page)

    def page_link_soup(self,page):
        req = requests.get(page,headers =headers)
        page_soup = BeautifulSoup(req.content, u"html.parser")
        profile_links = page_soup.find('ol', attrs={'class': 'js-module-agents-searchresults js-module-agents-search results results-list fullwidth-content-container'})
        profile_links = profile_links.find_all('a', attrs={'itemprop': 'url'})
        for i in profile_links:
            anchors = i.get('href')
            profile_link = 'https://www.remax.com'+str(anchors)

            if profile_link in names_arr:
                continue
            names_arr.append(profile_link)
            self.row = self.row + 1
            self.profile_link_soup(profile_link)

    def profile_link_soup(self, profile_link):
        profile_link = profile_link
        page= requests.get(profile_link,headers = headers)

        profile_soup = BeautifulSoup(page.content, u"html.parser")
        # print(profile_soup)

        try:
            name = profile_soup.find('span', attrs={'class': 'hero-name hero-name--agent js-background-check'}).text.strip()
            first_name = name.split(' ')[0].strip()
            last_name = name.split(' ')[1].strip()
            if ',' in last_name:
                last_name = last_name.split(',')[0]
            print('Name-', name)
        except:
            first_name = None
            last_name = None

        try:
            street_add = profile_soup.find('span', attrs={'itemprop': 'streetAddress'}).text.strip()
            # print('Street--', street_add)
        except:
            street_add = ''

        try:
            locality = profile_soup.find('span', attrs={'itemprop': 'addressLocality'}).text.strip()
            if ',' in locality:
                locality = locality.split(',')[0]
            # print('Local--', locality)
        except:
            locality = ''

        try:
            region = profile_soup.find('span', attrs={'itemprop': 'addressRegion'}).text.strip()
            # print('Reg--', region)
        except:
            region = ''

        try:
            postal_code = profile_soup.find('span', attrs={'itemprop': 'postalCode'}).text.strip()
            # print('Pos--', postal_code)
        except:
            postal_code = ''

        try:
            mobile = profile_soup.find('div', attrs={'class': 'hero-phone-container'}).findAll('a')[0].text.strip()
            # print('Mobile--', mobile)
        except:
            mobile = ''

        try:
            office = profile_soup.find('div', attrs={'class': 'hero-phone-container'}).findAll('a')[1].text.strip()
            # print('Office--', office)
        except:
            office = ''

        try:
            lis_no = profile_soup.find('div', attrs={'class': 'hero-license'}).text.strip()
            lis_no = lis_no.split('#:')[1].strip()
            # if '#:' in lis_no:
            #     lis_no = lis_no.split('#:')[1]
            # else:
            #     lis_no = profile_soup.find('span', attrs={'itemprop': 'branchCode'}).text.strip()
        except:
            lis_no = ''
        print('License---',lis_no)

        sheet1.write(self.row, 0, first_name)
        sheet1.write(self.row, 1, last_name)
        sheet1.write(self.row, 2, street_add)
        sheet1.write(self.row, 3, locality)
        sheet1.write(self.row, 4, region)
        sheet1.write(self.row, 5, postal_code)
        sheet1.write(self.row, 6, mobile)
        sheet1.write(self.row, 7, office)
        sheet1.write(self.row, 8, lis_no)
        import datetime
        wb.save('D:\PycharmProjects\Files\json_outputs\Remax_Final2.xls')
        print('Done------->',self.row)
        print(datetime.datetime.now())
        print('----' * 100)


objs = ascp()
objs.pass_links()



