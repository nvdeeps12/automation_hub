from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime
import sys

# options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# # options.add_argument('--headless')
# driver = webdriver.Chrome(executable_path='D:\chromedriver', options=options)
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, ' ')
row = 0
r = 0
# states = ['Arizona-City-AZ', 'California-MD', 'California-MO', 'California-City-CA', 'Colorado-Springs-CO', 'Colorado-City-CO', 'Delaware-OH', 'Delaware-Water-Gap-PA', 'Delaware-AR', 'Delaware-NJ', 'Florida-City-FL', 'Florida-NY', 'Georgia-VT', 'Georgiana-AL', 'Hawaiian-Gardens-CA', 'Idaho-Falls-ID', 'Idaho-Springs-CO', 'Indianapolis-IN', 'Indiana-PA', 'Iowa-Park-TX', 'Iowa-City-IA', 'Iowa-Colony-TX', 'Kansas-City-MO', 'Kansas-City-North-MO', 'Kansas-City-KS', 'Kansas-OH', 'Kansasville-WI', 'Louisiana-MO', 'Maineville-OH', 'Maryland-Heights-MO', 'Michigan-City-IN', 'Michigan-Center-MI', 'Missouri-City-TX', 'Montana-City-MT', 'Nevada-IA', 'Nevada-City-CA', 'New-York-Mills-MN', 'New-York-NY', 'Ohiopyle-PA', 'Oklahoma-City-OK', 'Oregon-City-OR', 'Oregon-OH', 'Oregon-WI', 'Oregon-IL', 'Oregon-House-CA', 'Texas-City-TX', 'Texas-Creek-CO', 'Vermontville-MI', 'Vermont-IL', 'Virginia-Beach-VA', 'Virginia-IL', 'Virginia-City-NV', 'Virginia-MN', 'Washington-DC', 'Washington-Township-MI', 'Washington-Crossing-PA', 'Washington-PA', 'Washington-IL', 'Washingtonville-NY', 'Washington-MI', 'Washington-Township-NJ', 'Washington-MO', 'Washington-NJ', 'Wisconsin-Dells-WI', 'Wisconsin-Rapids-WI', 'Wyoming-MN', 'Wyoming-MI', 'Wyoming-OH', 'Wyoming-DE', 'Wyoming-IL']
# states = ['Vermontville-MI', 'Vermont-IL', 'Virginia-Beach-VA', 'Virginia-IL', 'Virginia-City-NV', 'Virginia-MN', 'Wisconsin-Dells-WI', 'Wisconsin-Rapids-WI', 'Wyoming-MN', 'Wyoming-MI', 'Wyoming-OH', 'Wyoming-DE', 'Wyoming-IL']
# states = ['Washington-DC', 'Washington-Township-MI', 'Washington-Crossing-PA', 'Washington-PA', 'Washington-IL', 'Washingtonville-NY', 'Washington-MI', 'Washington-Township-NJ', 'Washington-MO', 'Washington-NJ']
# states=['Maryland-Heights-MO', 'Montana-City-MT', 'Wyoming-MN', 'Wyoming-MI', 'Wyoming-OH', 'Wyoming-DE', 'Wyoming-IL', 'Iowa-Park-TX', 'Iowa-City-IA', 'Iowa-Colony-TX', 'Hawaiian-Gardens-CA', 'Vermontville-MI', 'Vermont-IL', 'Kansas-City-MO', 'Kansas-City-North-MO', 'Kansas-City-KS', 'Kansas-OH', 'Kansasville-WI']
link_arr = []
# states=['https://www.remax.com/real-estate-agents/philadelphiaarea-philadelphia-pa-p001.html?query=rnd-sortorder/']

rd = xlrd.open_workbook('D:\PycharmProjects\Files\json_outputs\Rmx.xls')
sheet = rd.sheet_by_index(0)
sheet.cell_value(0, 0)
# https://www.remax.com/real-estate-agents/columbia-sc-p001.html?query=rnd-sortorder/
for i in range(sheet.nrows):

    link=sheet.cell_value(i,0)
    print(link)
    res = requests.get(link)
    print('Process Started For--',link)
    soup = BeautifulSoup(res.content, u'html.parser')

    try:
        try:
            total =  soup.find('span',attrs={'id':'js-agent-result-count'}).text.strip()
            total = int(total)
            pages = total%24
            pages = int(pages)
            # pages = int(pages)
            print('Pages--', pages)
            if pages>10:
                for p in range(pages):
                    links = link.replace('p001','p0'+str(p))
                    print('Links--',links)
                    link_arr.append(links)

            elif pages<10:
                for p in range(pages):
                    links = link.replace('p001','p00'+str(p))
                    print('Links--',links)
                    link_arr.append(links)
            link_arr.append(link)

            print('Arr------',link_arr)

            for lin in link_arr:
                row=row+1
                sheet1.write(row, 0, lin)
                wb.save('D:\PycharmProjects\Files\json_outputs\Remax_StatePages.xls')





            # new =  BeautifulSoup(res.content, u'html.parser')
            # ol = soup.find('ol', attrs={'class': 'js-module-agents-searchresults js-module-agents-search results results-list fullwidth-content-container'})
            # # article = soup.find('article', attrs={'class': 'agent-pane'})
            # urls = ol.find_all('a', attrs={'itemprop': 'url'})
            # for url in urls:
            #     link = url.get('href')
            #     link = 'https://www.remax.com' + str(link)
            #     print(link)
            #     # print(link)
            #     # row=row+1
            #     # sheet1.write(row, 0, link)
            #     # wb.save('remax_pending22.xls')
            # print(row)
        except:
            print(sys.exc_info())
            print('No Click')


    except:
        print('No Links Collected From->',i)
        print(sys.exc_info())
        continue
