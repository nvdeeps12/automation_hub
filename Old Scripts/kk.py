from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime


options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='D:\chromedriver', options=options)
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First Name')
sheet1.write(0, 1, 'Last Name')
sheet1.write(0, 2, 'Email')
sheet1.write(0, 3, 'Location')
sheet1.write(0, 4, 'City')
sheet1.write(0, 5, 'State')
sheet1.write(0, 6, 'Zip Code')
sheet1.write(0, 7, 'Mobile')
sheet1.write(0, 8, 'Office')
sheet1.write(0, 9, 'Website')
sheet1.write(0, 10, 'Ticket No')
sheet1.write(0, 11, 'Profile_link')

row = 0
r = 0

agentsarr = []


rd=xlrd.open_workbook('links1-60.xls')
sheet = rd.sheet_by_index(0)
sheet.cell_value(0, 0)
for a in range(1):
    try:
        print('Agent arr Link-',sheet.cell_value(a, 0))
        row = row + 1
        r = r + 1
        Response = driver.get('https://www.remax.com/officeagentsearch/#agentsearch')
        print(Response.status_code)
        agents_soup = BeautifulSoup(driver.page_source, u'html.parser')


        input_box = agents_soup.find('input', attrs={'id': 'js-txtagentlocation'})

        try:
            ul = agents_soup.find('ul', attrs={'class': 'hidden-xs'}).find_all('li')
        except:
            ul = '--'

        try:
            tag = agents_soup.find('i', attrs={'class': 'fa fa-tag'}).find_previous('li').text.strip()
        except:
            tag = '--'

        print('Tag - ',tag)

        try:
            address = agents_soup.find('i', attrs={'class': 'fa fa-map-marker'}).find_previous('li').text.strip()
            state=address.split(',')[1][0:3]
            zip=address.split(',')[1][3:9]
            try:
                city= agents_soup.find('i', attrs={'class': 'fa fa-map-marker'}).find_previous('li').find_next('i').next_sibling.next_sibling.next_sibling.split(',')[0]
            except:
                city = '--'

            state = address.split(',')[1][0:3]
            zip = address.split(',')[1][3:9]
            address=address.split(city)[0]
        except:
            address = '--'

        print('City--',city)
        print('Address--',address)
        print('State--',state)
        print('Zip--',zip)

        try:
            mail=agents_soup.find('i', attrs={'class': 'fa fa-envelope'}).find_next('a').get('href')
            mail=mail.split(':')[1]
        except:
            mail='--'

        print('Mail',mail)

        try:
            Office =agents_soup.find('i', attrs={'class': 'fa fa-phone'}).find_previous('li').text.strip()
            print('Office--', Office)
        except:
            phone = '--'
        try:
            Mobile=agents_soup.find('i', attrs={'class': 'fa fa-mobile'}).find_previous('li').text.strip()
        except:
            Mobile='--'
        print('Mobile- ',Mobile)

        try:
            website=agents_soup.find('i', attrs={'class': 'fa fa-desktop'}).find_next('a').get('href')
        except:
            website = '--'
        print('Web--',website)

        print('Done-->'+str(r) + '/' + str(sheet.nrows))
        print(datetime.datetime.now())
        print('--'*100)
        sheet1.write(row, 0, first_name)
        sheet1.write(row, 1, last_name)
        sheet1.write(row, 2, mail)
        sheet1.write(row, 3, address)
        sheet1.write(row, 4, city)
        sheet1.write(row, 5, state)
        sheet1.write(row, 6, zip)
        sheet1.write(row, 7, Mobile)
        sheet1.write(row, 8, Office)
        sheet1.write(row, 9, website)
        sheet1.write(row, 10, tag)
        sheet1.write(row, 11, sheet.cell_value(a, 0))
        # wb.save('agent1-60_3862.xls')
    except:
        import sys
        print(sys.exc_info())
        print('Loop Broken For Link',a)
        print('-----------------')
        continue



