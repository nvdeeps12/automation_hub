from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime


options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='D:\chromedriver', options=options)
wb = xlwt.Workbook()
rd = xlrd.open_workbook('zillow4500.xls')
sheet = rd.sheet_by_index(0)

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First_name')
sheet1.write(0, 1, 'Last_name')
sheet1.write(0, 2, 'Company_Name')
sheet1.write(0, 3, 'Address')
sheet1.write(0, 4, 'City')
sheet1.write(0, 5, 'State')
sheet1.write(0, 6, 'Zip')
sheet1.write(0, 7, 'Cell_Phone')
sheet1.write(0, 8, 'Broker_phone')
sheet1.write(0, 9, 'Website')
sheet1.write(0, 10, 'Email')
sheet1.write(0, 11, 'No. of Transactions')


row = 0
r = 0
for i in range(sheet.nrows):
    try:
        row = row + 1
        value = sheet.cell_value(i, 0)
        print('Process Started For--', value)
        res = requests.get(value)
    except:
        continue

    try:
        soup = BeautifulSoup(res.content, u'html.parser')
        print(soup)
        try:
            name = soup.find('span',  attrs={'class': 'hero-name hero-name--agent js-background-check'}).text.strip()
            first_name = name.split(' ')[0].strip()
            last_name = name.split(' ')[1].strip()
            if ',' in last_name:
                last_name = last_name.split(',')[0]
            print('Name-',name)
        except:
            name = ''

        try:
            street_add = soup.find('span', attrs={'itemprop': 'streetAddress'}).text.strip()
            print('Street--',street_add)
        except:
            street_add = ''

        try:
            locality = soup.find('span', attrs={'itemprop': 'addressLocality'}).text.strip()
            if ',' in locality:
                locality = locality.split(',')[0]
            print('Local--',locality)
        except:
            locality = ''

        try:
            region = soup.find('span', attrs={'itemprop': 'addressRegion'}).text.strip()
            print('Reg--',region)
        except:
            region = ''

        try:
            postal_code = soup.find('span', attrs={'itemprop': 'postalCode'}).text.strip()
            print('Pos--',postal_code)
        except:
            postal_code = ''

        try:
            mobile = soup.find('div', attrs={'class': 'hero-phone-container'}).findAll('a')[0].text.strip()
            print('Mobile--',mobile)
        except:
            mobile = ''

        try:
            office = soup.find('div', attrs={'class': 'hero-phone-container'}).findAll('a')[1].text.strip()
            print('Office--',office)
        except:
            office = ''

        try:
            lis_no = soup.find('div', attrs={'class': 'hero-license'}).text.strip()
            if '#' in lis_no:
                lis_no = lis_no.split(':')[1].strip()
                print('License_No--',lis_no)
            else:
                lis_no = lis_no
        except:
            lis_no = ''

        #
        # sheet1.write(row, 0, first_name)
        # sheet1.write(row, 1, last_name)
        # sheet1.write(row, 2, company)
        # sheet1.write(row, 3, address)
        # sheet1.write(row, 4, city)
        # sheet1.write(row, 5, state)
        # sheet1.write(row, 6, zip)
        # sheet1.write(row, 7, cell_phone)
        # sheet1.write(row, 8, broker_phone)
        # sheet1.write(row, 9, website)
        # sheet1.write(row, 10, email)
        # sheet1.write(row, 11, transactions)
        # wb.save('zillow_pros.xls')
        #
        # print('--'*100)
        # print(datetime.datetime.now())
        # r = r + 1
        # print(str(r) + "/" + str(sheet.nrows))
        # print('--' * 150)
    except:
        print('No Links Collected From->',i)
        continue

