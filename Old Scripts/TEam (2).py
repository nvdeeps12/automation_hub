from bs4 import BeautifulSoup
import xlwt
from selenium import webdriver
import time
import sys



wb = xlwt.Workbook()
options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='F:\chromedriver',options=options)
driver.get('https://www.ballertv.com/events/atlantic-city-showcase?sport=basketball')

click = driver.find_element_by_xpath('//*[@id="tab-event-replay"]').click()
time.sleep(5)

try:
    count=0
    while(count<59):
        count = count + 1
        print(count)
        click2= driver.find_element_by_xpath('//*[@id="cta-show-more-past"]').click()
        print("click....",click2)
        time.sleep(5)
except:
    pass

soup = BeautifulSoup(driver.page_source, u"html.parser")

print('-----------')


sheet1 = wb.add_sheet('1')
sheet1.write(0, 0, 'Team1')
sheet1.write(0, 1, 'Score')
sheet1.write(0, 2, 'Team2')
sheet1.write(0, 3, 'Score')
sheet1.write(0, 4, 'Date')
sheet1.write(0, 5, 'Location')
sheet1.write(0, 6, 'Court')


container = soup.find('div',attrs={'id':'past-streams-list'})
all_cards = container.findAll('div', attrs={'class': 'card-container'})

row = 0
for i in all_cards:
    row = row+1
    print(row)
    team1 = i.find('span', attrs={'class': 'text-span text-span--team-name text-span--team-1'}).text.strip()
    if 'None' in team1:
        team1 = team1.replace('None','')

    score1 = i.find('p', attrs={'class': 'details__detail detail--score'}).text.strip()
    score1 = score1.split('Final Score:')[1].split('–')[0].strip()
    team2 = i.find('span', attrs={'class': 'text-span text-span--team-name text-span--team-2'}).text.strip()
    score2  = i.find('p', attrs={'class': 'details__detail detail--score'}).text.strip()
    score2 = score2.split('Final Score:')[1].split('–')[1].strip()
    try:
        date = i.find('p',attrs= {'class':'details__detail detail--date'}).text.strip()
        location = i.find('p',attrs ={'class':'details__detail detail--location'}).text.strip()
        if 'Court' in location:
            location = location.split('Court')[0].strip()

        court = i.find('p',attrs ={'class':'details__detail detail--location'}).text.strip()
        if 'Court' in court:
            court = court.split('Court')[1].strip()
            court = 'Court'+' '+str(court)
    except:
        print(sys.exc_info())
        date = 'None'
        location = 'None'
        court = 'None'




    print('Team1-->',team1)
    print('Score1-->', score1)
    print('Team2-->', team2)
    print('Score2-->', score2)
    print('Date-->', date)
    print('Location-->', location)
    print('Court-->',court)
    print('---'*100)

    sheet1.write(row, 0, team1)
    sheet1.write(row, 1, score1)
    sheet1.write(row, 2, team2)
    sheet1.write(row, 3, score2)
    sheet1.write(row, 4, date)
    sheet1.write(row, 5, location)
    sheet1.write(row, 6, court)
    wb.save('Teamsdata.xls')



