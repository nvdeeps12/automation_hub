
import  logging.handlers
import traceback
logging.raiseExceptions = 1
import os
import smtplib




class LogHelper():
    def __call__(self,log_msg , FileName, log_type):
        # print "hello in logs"
        pass

    def myLogger(self, log_folder, source):

        f = logging.Formatter("%(asctime)s %(levelname)-9s %(name)-8s %(thread)5s %(message)s")

        root = logging.getLogger(self.SOURCE)

        root.setLevel(logging.INFO)

        log_root = os.path.dirname(__file__)

        source_log = os.path.join(log_root, log_folder)

        _logFileName = os.path.join(source_log, source +".log")


        _MaxFileSize = 500000
        _MaxFiles = 30


        #h = logging.FileHandler(logFileName, 'w')
        h = logging.handlers.RotatingFileHandler(_logFileName, "a", _MaxFileSize, _MaxFiles)
        root.addHandler(h)
        h.setFormatter(f)
        h = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
        root.addHandler(h)
    # ===============================================================================================================================================================
    #Constructor - Logger object created here
    def __init__(self, log_folder, source):
        self.SOURCE = source
        self.LogFolder = log_folder
        # self.logger = logging.getLogger(source)
        # print self.logger
        #
        #
        # self.logger = self.myLogger(log_folder, source)
        # print self.logger


        # f = logging.Formatter("%(asctime)s %(levelname)-9s %(name)-8s %(thread)5s %(message)s")
        #
        # root = logging.getLogger(self.SOURCE)
        #
        # root.setLevel(logging.INFO)
        #
        # log_root = os.path.dirname(__file__)
        #
        # source_log = os.path.join(log_root, log_folder)
        #
        # _logFileName = os.path.join(source_log, source +".log")
        #
        #
        # _MaxFileSize = 500000
        # _MaxFiles = 30
        #
        #
        # #h = logging.FileHandler(logFileName, 'w')
        # h = logging.handlers.RotatingFileHandler(_logFileName, "a", _MaxFileSize, _MaxFiles)
        # root.addHandler(h)
        # h.setFormatter(f)
        # h = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
        # root.addHandler(h)

    # ===============================================================================================================================================================
    # Method to log the error in log file
    def doLog(self, log_msg , FileName, log_type):

        # self.myLogger(self.LogFolder, self.SOURCE)
        # logger = logging.getLogger()

        logger = logging.getLogger(self.SOURCE)
        # print len(logger.handlers)
        if len(logger.handlers) <= 0:
            self.myLogger(self.LogFolder, self.SOURCE)
            logger = logging.getLogger(self.SOURCE)

        # print logger
        strMsg = log_msg

        strMsg = str(strMsg)
        strMsg = "    " +strMsg

        if log_type == 'error':
            strMsg = self.formatExceptionInfo(log_msg)
            # self.SendEmail(strMsg, FileName)
            logEntry=(40, strMsg)
        elif log_type == 'warn':
            logEntry=(30, strMsg)
        else:
            logEntry=(20, strMsg)

        # apply(logger.log, logEntry)
        logger.log(*logEntry)

    # =============================================================================================================================================================== # Method to reformat the error message in simple readable format    
    def formatExceptionInfo(self, objexp):
        maxTBlevel=1
        eMsg = objexp[1]
        cla, exc, trbk = objexp
        excName = cla.__name__
        try:
            excArgs = exc.__dict__["args"]
        except KeyError:
            excArgs = "<no args>"
        excTb = traceback.format_tb(trbk, maxTBlevel)
        return (excName, excArgs,  excTb , eMsg)
    #return (excName, excTb , eMsg)       
    # ===============================================================================================================================================================


    # def SendEmail(self, ErrorMsg, FileName):
    #     if self.LogFolder == 'awscli':
    #         # recipient = 'hgillh@gmail.com, amurphy@epic59.com, gcrew.sunny@gmail.com'
    #         recipient = ''
    #     else:
    #         recipient = settings.SMTP_RECIPIENT
    #     gmail_user = settings.SMTP_USER
    #     gmail_pwd = settings.SMTP_PWD
    #     FROM = 'JobExport Support Team'
    #     TO = recipient if type(recipient) is list else [recipient]
    #
    #
    #     errMsg= "Hello Admin,\n Some error occurred while processing data in Job Export."
    #     errMsg += "\n Error  : " + FileName
    #     errMsg += " \n Error Derails : " + str(ErrorMsg) + "\n You can check the details of error in error log \n\n Thanks, \n Team JobExport."
    #
    #     SUBJECT = 'Error in file ' + str(FileName)
    #     TEXT = errMsg
    #
    #     # Prepare actual message
    #     message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    #     """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    #     try:
    #         server = smtplib.SMTP("smtp.gmail.com", 587)
    #         server.ehlo()
    #         server.starttls()
    #         server.login(gmail_user, gmail_pwd)
    #         server.sendmail(FROM, TO, message)
    #         server.close()
    #     except:
    #         pass