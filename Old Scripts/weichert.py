import time
# from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import xlwt
import requests
import re
# from work.scripts.employzone_mail import createEmailContent,sendEmail
from datetime import datetime
import xlrd
# from logs.LogHandler import Mylogger

# log_obj = Mylogger()

# rd = xlrd.open_workbook('D:\PycharmProjects\Files\\Urls\Huston.xlsx')
# sheet = rd.sheet_by_index(0)



# options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')

# driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)


names_arr = []

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First_name')
sheet1.write(0, 1, 'Last_name')
sheet1.write(0, 2, 'Street')
sheet1.write(0, 3, 'Locality')
sheet1.write(0, 4, 'Region')
sheet1.write(0, 5, 'Postal')
sheet1.write(0, 6, 'Mobile')
sheet1.write(0, 7, 'Office')
sheet1.write(0, 8, 'Email')
sheet1.write(0, 9, 'Profile_image')
sheet1.write(0, 10, 'Licence No.')
sheet1.write(0, 11, 'Source')
referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }


class ascp():
    def pass_links(self):
        self.row = 0
        try:
            state_link = 'https://www.weichert.com/offices/'
            print(state_link)
            page =requests.get(state_link,headers = headers)
            soup = BeautifulSoup(page.content, u"html.parser")
            total_pages = soup.find('div',attrs={'id':'browsebystate'}).find('ul').findAll('li')
            for state in total_pages:
                href = state.find('a')['href']
                # print(href)
                state_links = 'https://www.weichert.com'+str(href)
                print('State Links-------',state_links)
                self.supply_pages(state_links)
        except:
                total_pages = 0


    def supply_pages(self,state_links):



        page = requests.get(state_links,headers=headers)
        about_soup = BeautifulSoup(page.content, u"html.parser")
        office_links = about_soup.findAll('ul',attrs={'class':'list-unstyled'})[1].findAll('a')
        for i in office_links:
            href = i['href']
            page_link = 'https://www.weichert.com'+str(href)
            print(page_link)
            # self.page_link_soup(page_link)

    def page_link_soup(self,page):
        req = requests.get(page,headers =headers)
        page_soup = BeautifulSoup(req.content, u"html.parser")
        profile_links = page_soup.find('a', text=re.compile('About Us'))
        profile_links = profile_links.find_all('a', attrs={'itemprop': 'url'})


        for i in profile_links:
            anchors = i.get('href')
            profile_link = 'https://www.remax.com'+str(anchors)

            if profile_link in names_arr:
                continue


            names_arr.append(profile_link)
            self.row = self.row + 1
            self.profile_link_soup(profile_link)

    def profile_link_soup(self, profile_link):
        profile_link = profile_link
        page= requests.get(profile_link,headers = headers)

        profile_soup = BeautifulSoup(page.content, u"html.parser")


        # print(profile_soup)



        try:
            source = profile_link
        except:
            source = 'None'


        try:
            name = profile_soup.find('span', attrs={'class': 'hero-name hero-name--agent js-background-check'}).text.strip()
            first_name = name.split(' ')[0].strip()
            last_name = name.split(' ')[1].strip()
            if ',' in last_name:
                last_name = last_name.split(',')[0]
        except:
            first_name = 'None'
            last_name = 'None'

        try:
            street_add = profile_soup.find('span', attrs={'itemprop': 'streetAddress'}).text.strip()
            # print('Street--', street_add)
        except:
            street_add = 'None'

        try:
            locality = profile_soup.find('span', attrs={'itemprop': 'addressLocality'}).text.strip()
            if ',' in locality:
                locality = locality.split(',')[0]
            # print('Local--', locality)
        except:
            locality = 'None'

        try:
            region = profile_soup.find('span', attrs={'itemprop': 'addressRegion'}).text.strip()
            # print('Reg--', region)
        except:
            region = 'None'

        try:
            postal_code = profile_soup.find('span', attrs={'itemprop': 'postalCode'}).text.strip()
            # print('Pos--', postal_code)
        except:
            postal_code = 'None'

        try:
            mobile = profile_soup.find('div', attrs={'class': 'hero-phone-container'}).findAll('a')[0].text.strip()
            # print('Mobile--', mobile)
        except:
            mobile = 'None'

        try:
            office = profile_soup.find('div', attrs={'class': 'hero-phone-container'}).findAll('a')[1].text.strip()
            # print('Office--', office)
        except:
            office = 'None'


        try:
            email = profile_soup.find('button', attrs={'class': 'hero-button btn medium-white-ghost-btn js-email-contactform'})['data-agentemail']
            if '-' in email:
                email = email.split('-')[0]
        except:
            email = 'None'

        # print(email)

        try:
            image = profile_soup.find('img', attrs={'class': 'hero-photo'}).get('src')
        except:
            image = 'None'

        # print(image)


        try:
            lis_no = profile_soup.find('div', attrs={'class': 'hero-license'}).text.strip()
            lis_no = lis_no.split('#:')[1].strip()
            # if '#:' in lis_no:
            #     lis_no = lis_no.split('#:')[1]
            # else:
            #     lis_no = profile_soup.find('span', attrs={'itemprop': 'branchCode'}).text.strip()
        except:
            lis_no = 'None'
        # print('License---',lis_no)

        # sheet1.write(self.row, 0, first_name)
        # sheet1.write(self.row, 1, last_name)
        # sheet1.write(self.row, 2, street_add)
        # sheet1.write(self.row, 3, locality)
        # sheet1.write(self.row, 4, region)
        # sheet1.write(self.row, 5, postal_code)
        # sheet1.write(self.row, 6, mobile)
        # sheet1.write(self.row, 7, office)
        # sheet1.write(self.row, 8, email)
        # sheet1.write(self.row, 9, image)
        # sheet1.write(self.row, 10, lis_no)
        # sheet1.write(self.row, 11, source)
        #
        # import datetime
        # wb.save('D:\PycharmProjects\Files\json_outputs\Remax_Huston_Dup.xls')
        # print('Done------->',self.row)
        # print(datetime.datetime.now())
        # print('----' * 100)


objs = ascp()
objs.pass_links()



