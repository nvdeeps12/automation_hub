#!/usr/bin/env python
import time, os, sys
rootPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(rootPath)
os.environ["DJANGO_SETTINGS_MODULE"] = "jobexport.settings"
import django
django.setup()

import MySQLdb
import datetime
from logs.LogHandler import LogHelper
import sys, re
import pysolr
import psycopg2
import urllib, json
from datasync import config
from multiprocessing import Process
from jobexport import settings
from processMonitor import ProcessMonitor
from categories import categories_list
import ast
import psycopg2.extras


class JobImportSolrPsqlHelperInt():
    def __init__(self, objLog):
        # self.objLog = LogHelper('poststhrd', 'jobimportsolrPsqlInt')
        self.objLog = objLog
        self.fileName = 'jobImportSolrPsqlInt.py'

        self.US_CRW_HOST = config.US_CRW_HOST
        self.US_CRW_DB_USER = config.US_CRW_DB_USER
        self.US_CRW_DB_PASSWORD = config.US_CRW_DB_PASSWORD
        self.US_CRW_DATABASE = config.US_CRW_DATABASE

        self.TALENTSPOT_HOST = config.TALENTSPOT_HOST
        self.TALENTSPOT_DB = config.TALENTSPOT_DB
        self.TALENTSPOT_USER = config.TALENTSPOT_USER
        self.TALENTSPOT_PWD = config.TALENTSPOT_PWD

        self.RECORDS_LIMIT = 1000
        self.TIME_INTERVAL = 24  ### time interval to get jobs in hours
        self.NO_OF_THREADS = 18
        self.objLogThrd = None
        self.Total_processed_jobs = 0


        self.solrUrl = "http://" + str(settings.SOLR_USER) + ":" + str(settings.SOLR_PASSWORD) + "@" + str(
            settings.SOLR_URL) + 'jobs/'
        self.DOMAIN_NAME = 'https://www.employzone.com'
        self.USER_DATA = []
        self.ATS_LIST_INTERNAL_APPLY = ['workable', 'googlehire', 'lever', 'recruiterbox', 'jobvite', 'greenhouse','jazzhr', 'bamboohr', 'applicantstack', 'automotohr']

    def startProcessInt(self):
        current_time = datetime.datetime.now()
        talent_date = current_time - datetime.timedelta(hours=self.TIME_INTERVAL)
        talent_date = str(talent_date).rsplit(".", 1)[0]
        talent_date = datetime.datetime.strptime(str(talent_date), '%Y-%m-%d %H:%M:%S')

        self.startThreads(talent_date)

    def startThreads(self, talent_date):
        start_time = datetime.datetime.now()
        MSG = "Processes Satrted."
        self.objLog.doLog(MSG, self.fileName, 'info')

        posts_count = self.getCountPosts(talent_date)

        print("posts_count=="), posts_count

        self.USER_DATA = selfoffset.getUserData()
        print("users_count=="), len(self.USER_DATA)

        if posts_count > 0:
            records_count = posts_count / self.NO_OF_THREADS

            remainder = posts_count % self.NO_OF_THREADS
            if remainder > 0:
                records_count += 1

            offset = records_count
            limit = records_count
            print("offset=="), offset
            print("limit=="), limit

            th_arr = []
            for td in range(self.NO_OF_THREADS):
                self.objLogThrd = LogHelper('poststhrd', 'jobimport_int_thrd' + str(int(td + 1)))
                t = Process(target=self.threadHandler, args=(td + 1, talent_date, offset, limit, posts_count))
                t.start()
                th_arr.append(t)

            for t in th_arr:
                t.join()

        MSG = "All processes completed their task, Goint to exit."
        self.objLog.doLog(MSG, self.fileName, 'info')
        end_time = datetime.datetime.now()
        # self.sendMail(start_time, end_time)

    def threadHandler(self, thrd_index, talent_date, offset, limit, posts_count):
        print("thrd_index=="), thrd_index
        brk_limit = offset
        offset = int(offset) * int(thrd_index - 1)
        self.processEmployzoneJobs(talent_date, offset, limit, posts_count, brk_limit)

    def processEmployzoneJobs(self, talent_date, offset, limit, posts_count, brk_limit):
        try:
            msg = 'Going to process jobs'

            self.objLogThrd.doLog(msg, self.fileName, 'info')

            self.processActiveRecords(talent_date, offset, limit, posts_count, brk_limit)
        except:
            self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'error')

    # This method is used to process active jobs.
    def processActiveRecords(self, talent_date, offset, limit, posts_count, brk_limit):
        msg = 'Going to process active records from posts table, jobs count is : ' + str(limit)
        self.objLogThrd.doLog(msg, self.fileName, 'info')

        try:
            if posts_count > 0:
                offset = offset
                print("offset_new=="), offset
                limit = self.RECORDS_LIMIT
                is_external_jobs = True
                while is_external_jobs:
                    external_jobs, jobs_data = self.getPostsJobs(talent_date, offset, limit)
                    self.addJobsDataSolr(jobs_data)

                    lst_posts = self.createDataTuple(jobs_data)
                    self.saveJobsToTalentspot(lst_posts)

                    offset += len(external_jobs)

                    self.Total_processed_jobs += int(limit)

                    if int(self.Total_processed_jobs) >= int(brk_limit):
                        is_external_jobs = False

                    if len(external_jobs) == 0:
                        is_external_jobs = False

            msg = 'Jobs processed completely from posts table.'
            self.objLogThrd.doLog(msg, self.fileName, 'info')
        except:
            self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'error')

    # This method is used to return active jobs count from posts table.
    def getCountPosts(self, talent_date):
        msg = 'Going to get jobs count from posts table using MySQL.'
        self.objLog.doLog(msg, self.fileName, 'info')

        conn = MySQLdb.connect(host=self.US_CRW_HOST, user=self.US_CRW_DB_USER, passwd=self.US_CRW_DB_PASSWORD,
                               db=self.US_CRW_DATABASE)

        cursor = conn.cursor()
        jobPosts_count = 0
        try:
            # query = "SELECT count(*) FROM posts WHERE (created > '{0}' or recovered_on > '{0}') and is_deleted = 0 and source_platform='Job-Wrapper'".format(talent_date)
            query = "SELECT count(*) FROM posts WHERE and is_deleted = 0 and is_synced = 0 and tier is not null and source_platform='Job-Wrapper'"

            cursor.execute(query)
            jobPosts_count = cursor.fetchone()
            jobPosts_count = int(jobPosts_count[0])

            MSG = 'Total ' + str(jobPosts_count) + ' active jobs found from posts table.'
            self.objLog.doLog(MSG, self.fileName, 'info')
        except:
            self.objLog.doLog(sys.exc_info(), self.fileName, 'error')
        finally:
            cursor.close()
            conn.close()

        return jobPosts_count

    # This method is used to return active jobs from posts table.
    def getPostsJobs(self, talent_date, offset, limit):
        msg = 'Going to get jobs from posts table using MySQL.'
        self.objLogThrd.doLog(msg, self.fileName, 'info')

        conn = MySQLdb.connect(host='localhost', user='', password='password',db='ezus')
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        data = []
        posts_data = []
        try:
            # query = "SELECT *, is_deleted as status FROM posts WHERE is_deleted = 0 and (created > '{0}' or recovered_on > '{0}')".format(talent_date) + " and source_platform='Job-Wrapper' ORDER BY id LIMIT " + str(limit) + " OFFSET " + str(offset)
            query = "SELECT *, is_deleted as status FROM posts WHERE is_deleted = 0 and is_synced=0 and tier is not null and (created > '{0}' or recovered_on > '{0}')".format(
                talent_date) + " and source_platform='Job-Wrapper' ORDER BY id LIMIT " + str(limit) + " OFFSET " + str(
                offset)

            cursor.execute(query)
            posts_data = cursor.fetchall()
            print('Data---',posts_data)
            for post in posts_data:
                try:
                    d_tuple = (post['id'], "External", 1)
                    data.append(d_tuple)
                except:
                    pass

            MSG = 'Total ' + str(len(data)) + ' active jobs found from posts table.'
            self.objLogThrd.doLog(MSG, self.fileName, 'info')
        except:
            self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'error')
        finally:
            cursor.close()
            conn.close()

        return data, posts_data

    ## This method is used to add or update jobs in solr
    def addJobsDataSolr(self, posts_data):
        MSG = 'Going to save jobs on solr'
        self.objLogThrd.doLog(MSG, self.fileName, 'info')
        job_records = []
        try:
            try:
                # Setup a Solr instance. The timeout is optional.
                solr = pysolr.Solr(self.solrUrl, timeout=10)

                for job in posts_data:
                    now_date = datetime.datetime.today()
                    closing_dt = now_date + datetime.timedelta(days=30)
                    closing_dt = str(closing_dt).split(' ')[0]
                    closing_dt = closing_dt + 'T00:00:00Z'

                    desc = job['description']

                    try:
                        job_country = job['country']
                    except:
                        job_country = 'US'

                    try:
                        job_region = job['region']
                        job_region = re.sub('[^ a-zA-Z0-9]', ' ', job_region)
                        job_region = job_region.strip()
                        jb_region = job_region
                        job_region = job_region.replace(' ', "-")
                        job_region = re.sub('-+', '-', job_region)
                    except:
                        job_region = ''
                        jb_region = ''

                    try:
                        job_city = job['city']
                        job_city = re.sub('[^ a-zA-Z0-9]', ' ', job_city)
                        job_city = job_city.strip()
                        jb_city = job_city
                        job_city = job_city.replace(' ', "-")
                        job_city = re.sub('-+', '-', job_city)
                    except:
                        job_city = ''
                        jb_city = ''

                    try:
                        job_loc = job_country + '-' + job_region + '-' + job_city
                        job_loc = str(job_loc).replace(' ', "-")
                        job_loc = re.sub('-+', '-', job_loc)
                    except:
                        job_loc = 'US'

                    try:
                        job_title = job['title']
                        job_title = re.sub('[^ a-zA-Z0-9]', ' ', job_title)
                        job_title = job_title.strip()
                        jb_title = job_title
                        job_title = job_title.replace(' ', "-")
                        job_title = re.sub('-+', '-', job_title)
                    except:
                        job_title = job['category_name']
                        jb_title = ''

                    try:
                        job_source = job['source']
                        job_source = re.sub('[^ a-zA-Z0-9]', ' ', job_source)
                        job_source = job_source.strip()
                        jb_source = job_source
                        job_source = job_source.replace(' ', "-")
                        job_source = re.sub('-+', '-', job_source)
                    except:
                        job_source = ''
                        jb_source = ''

                    try:
                        job_company = job['advertiser_name']
                        job_company = re.sub('[^ a-zA-Z0-9]', ' ', job_company)
                        job_company = job_company.strip()
                        jb_company = job_company
                        job_company = job_company.replace(' ', "-")
                        job_company = re.sub('-+', '-', job_company)
                    except:
                        job_company = ''
                        jb_company = ''

                    try:
                        if job['ai_clean_title'] == None or job['ai_clean_title'] == 'None':
                            clean_title = ''
                        else:
                            clean_title = re.sub(r'[^\x00-\x7F]+', '', str(job['ai_clean_title'])).replace('u"',
                                                                                                           '').replace(
                                '"', '').replace("u'", '').replace("'", '').replace('%', '')
                    except:
                        clean_title = ''
                    try:
                        if job['ai_matches'] == None or job['ai_matches'] == 'None':
                            ai_matches = ''
                        else:
                            ai_matches = re.sub(r'[^\x00-\x7F]+', '', str(job['ai_matches'])).replace('u"', '').replace(
                                '"', '').replace("u'", '').replace("'", '').replace('%', '')
                    except:
                        ai_matches = ''
                    try:
                        if job['onet_responsibilities'] == None or job['onet_responsibilities'] == 'None':
                            responsibilities = []
                        else:
                            responsibilities = ast.literal_eval(job['onet_responsibilities'])
                    except:
                        responsibilities = []
                    try:
                        if job['onet_abilities'] == None or job['onet_abilities'] == 'None':
                            abilities = []
                        else:
                            abilities = ast.literal_eval(job['onet_abilities'])
                    except:
                        abilities = []
                    try:
                        if job['onet_toolstechnology'] == None or job['onet_toolstechnology'] == 'None':
                            toolstechnology = []
                        else:
                            toolstechnology = ast.literal_eval(job['onet_toolstechnology'])
                    except:
                        toolstechnology = []
                    try:
                        if job['onet_knowledge'] == None or job['onet_knowledge'] == 'None':
                            knowledge = []
                        else:
                            knowledge = ast.literal_eval(job['onet_knowledge'])
                    except:
                        knowledge = []
                    try:
                        if job['onet_skills'] == None or job['onet_skills'] == 'None':
                            skills = []
                        else:
                            skills = ast.literal_eval(job['onet_skills'])
                    except:
                        skills = []
                    try:
                        if job['onet_education'] == None or job['onet_education'] == 'None':
                            education = []
                        else:
                            education = ast.literal_eval(job['onet_education'])
                    except:
                        education = []
                    try:
                        if job['onet_reportedjobtitle'] == None or job['onet_reportedjobtitle'] == 'None':
                            reportedjobtitle = []
                        else:
                            reportedjobtitle = ast.literal_eval(job['onet_reportedjobtitle'])
                    except:
                        reportedjobtitle = []

                    try:
                        if job['onet_personality'] == None or job['onet_personality'] == 'None':
                            personality = []
                        else:
                            personality = ast.literal_eval(job['onet_personality'])
                    except:
                        personality = []

                    try:
                        if job['export_to_ezus'] == None or job['export_to_ezus'] == 'None':
                            export_to_ezus = 0
                        else:
                            export_to_ezus = job['export_to_ezus']
                    except:
                        export_to_ezus = 0

                    try:
                        if job['export_to_lensa'] == None or job['export_to_lensa'] == 'None':
                            export_to_lensa = 0
                        else:
                            export_to_lensa = job['export_to_lensa']
                    except:
                        export_to_lensa = 0

                    try:
                        if job['post_url'] == None or job['post_url'] == 'None':
                            post_url = ''
                        else:
                            post_url = re.sub(r'[^\x00-\x7F]+', '', str(job['post_url'])).replace('"', '').replace("'",
                                                                                                                   '').replace(
                                '%', '')
                    except:
                        post_url = ''

                    try:
                        onet_code = str(job['onet_code'])
                    except:
                        onet_code = ''

                    try:
                        job_tier = int(job['tier'])
                    except:
                        job_tier = None
                    try:
                        ats_name = str(job['ats']).strip()
                        if ats_name.lower() in self.ATS_LIST_INTERNAL_APPLY:
                            is_internal_apply = 1
                        else:
                            is_internal_apply = 0
                    except:
                        is_internal_apply = 0

                    direct_link = str(self.DOMAIN_NAME) + '/job-view/' + str(job_loc) + '/' + str(
                        job_title) + '/' + str(job_source) + '/' + str(
                        job['id']) + "?ez_source=alert&ez_campaign=daily-alert&ez_medium=azcs"
                    direct_link = str(direct_link).lower()
                    job_record = {
                        'advertiser_type_id': job['advertiser_type_id'],
                        'apply_url': job['application_url'],
                        'apply_email': '',
                        'ats': job['ats'],
                        'ats_txt': job['ats'],
                        'categories': [job['category_name']],
                        'categories_txt': [job['category_name']],
                        'categories_onet': '',
                        'closing_date': closing_dt,
                        'company': str(jb_company),
                        'company_txt': str(jb_company),
                        'cpc': 0.0,
                        'description': re.sub(r'[^\x00-\x7F]+', '', str(desc)).strip(),
                        'description_ai': '',
                        'description_onet': '',
                        'direct_link': direct_link,
                        'id': job['id'],
                        'is_internal_job': 1,
                        'job_location': job['city'],
                        'job_type': job['employment_type'],
                        'latitude': job['latitude'],
                        'longitude': job['longitude'],
                        'latlon': str(job['latitude']) + ',' + str(job['longitude']),
                        'occupation_ai': job['occupation_ai'],
                        'occupation_desc': job['occupation_desc'],
                        'postal_code': job['zip'],
                        'posting_date': job['post_date'],
                        'remote_id': '',
                        'salary': '',
                        'skills': [job['skill_1'], job['skill_2'], job['skill_3'], job['skill_4'], job['skill_5'],
                                   job['skill_6']],
                        'skills_txt': [job['skill_1'], job['skill_2'], job['skill_3'], job['skill_4'], job['skill_5'],
                                       job['skill_6']],
                        'skills_onet': '',
                        'source': str(jb_source),
                        'source_txt': str(jb_source),
                        'title': re.sub(r'[^\x00-\x7F]+', '', str(jb_title)).strip(),
                        'title_ai': '',
                        'title_onet': '',
                        'type_id': job['type_id'],
                        'value': "High",
                        'city': str(jb_city),
                        'region': str(jb_region),
                        'country': str(job_country),
                        'source_platform': job['source_platform'],
                        'clean_title': clean_title,
                        'ai_matches': ai_matches,
                        'onet_responsibilities': responsibilities,
                        'onet_abilities': abilities,
                        'onet_toolstechnology': toolstechnology,
                        'onet_knowledge': knowledge,
                        'onet_skills': skills,
                        'onet_education': education,
                        'onet_reportedjobtitle': reportedjobtitle,
                        'onet_personality': personality,
                        'export_to_ezus': export_to_ezus,
                        'export_to_lensa': export_to_lensa,
                        'post_url': post_url,
                        'onet_code': onet_code,
                        'is_updated': 1,
                        'job_tier': job_tier,
                        'is_internal_apply': is_internal_apply
                    }
                    if job_title == None or len(job_title) == 0 or job_title == '':
                        continue
                    else:
                        job_records.append(job_record)

                # How you'd index data.
                solr.add(job_records)
                solr.get_session().close()
            except:
                # Setup a Solr instance. The timeout is optional.
                solr = pysolr.Solr(self.solrUrl, timeout=10)

                for job in posts_data:

                    now_date = datetime.datetime.today()
                    closing_dt = now_date + datetime.timedelta(days=30)
                    closing_dt = str(closing_dt).split(' ')[0]
                    closing_dt = closing_dt + 'T00:00:00Z'

                    desc = job['description']

                    try:
                        job_country = job['country']
                    except:
                        job_country = 'US'

                    try:
                        job_region = job['region']
                        job_region = re.sub('[^ a-zA-Z0-9]', ' ', job_region)
                        job_region = job_region.strip()
                        jb_region = job_region
                        job_region = job_region.replace(' ', "-")
                        job_region = re.sub('-+', '-', job_region)
                    except:
                        job_region = ''
                        jb_region = ''

                    try:
                        job_city = job['city']
                        job_city = re.sub('[^ a-zA-Z0-9]', ' ', job_city)
                        job_city = job_city.strip()
                        jb_city = job_city
                        job_city = job_city.replace(' ', "-")
                        job_city = re.sub('-+', '-', job_city)
                    except:
                        job_city = ''
                        jb_city = ''

                    try:
                        job_loc = job_country + '-' + job_region + '-' + job_city
                        job_loc = str(job_loc).replace(' ', "-")
                        job_loc = re.sub('-+', '-', job_loc)
                    except:
                        job_loc = 'US'

                    try:
                        job_title = job['title']
                        job_title = re.sub('[^ a-zA-Z0-9]', ' ', job_title)
                        job_title = job_title.strip()
                        jb_title = job_title
                        job_title = job_title.replace(' ', "-")
                        job_title = re.sub('-+', '-', job_title)
                    except:
                        job_title = job['category_name']
                        jb_title = ''

                    try:
                        job_source = job['source']
                        job_source = re.sub('[^ a-zA-Z0-9]', ' ', job_source)
                        job_source = job_source.strip()
                        jb_source = job_source
                        job_source = job_source.replace(' ', "-")
                        job_source = re.sub('-+', '-', job_source)
                    except:
                        job_source = ''
                        jb_source = ''

                    try:
                        job_company = job['advertiser_name']
                        job_company = re.sub('[^ a-zA-Z0-9]', ' ', job_company)
                        job_company = job_company.strip()
                        jb_company = job_company
                        job_company = job_company.replace(' ', "-")
                        job_company = re.sub('-+', '-', job_company)
                    except:
                        job_company = ''
                        jb_company = ''

                    try:
                        if job['ai_clean_title'] == None or job['ai_clean_title'] == 'None':
                            clean_title = ''
                        else:
                            clean_title = re.sub(r'[^\x00-\x7F]+', '', str(job['ai_clean_title'])).replace('u"',
                                                                                                           '').replace(
                                '"', '').replace("u'", '').replace("'", '').replace('%', '')
                    except:
                        clean_title = ''
                    try:
                        if job['ai_matches'] == None or job['ai_matches'] == 'None':
                            ai_matches = ''
                        else:
                            ai_matches = re.sub(r'[^\x00-\x7F]+', '', str(job['ai_matches'])).replace('u"', '').replace(
                                '"', '').replace("u'", '').replace("'", '').replace('%', '')
                    except:
                        ai_matches = ''
                    try:
                        if job['onet_responsibilities'] == None or job['onet_responsibilities'] == 'None':
                            responsibilities = []
                        else:
                            responsibilities = ast.literal_eval(job['onet_responsibilities'])
                    except:
                        responsibilities = []
                    try:
                        if job['onet_abilities'] == None or job['onet_abilities'] == 'None':
                            abilities = []
                        else:
                            abilities = ast.literal_eval(job['onet_abilities'])
                    except:
                        abilities = []
                    try:
                        if job['onet_toolstechnology'] == None or job['onet_toolstechnology'] == 'None':
                            toolstechnology = []
                        else:
                            toolstechnology = ast.literal_eval(job['onet_toolstechnology'])
                    except:
                        toolstechnology = []
                    try:
                        if job['onet_knowledge'] == None or job['onet_knowledge'] == 'None':
                            knowledge = []
                        else:
                            knowledge = ast.literal_eval(job['onet_knowledge'])
                    except:
                        knowledge = []
                    try:
                        if job['onet_skills'] == None or job['onet_skills'] == 'None':
                            skills = []
                        else:
                            skills = ast.literal_eval(job['onet_skills'])
                    except:
                        skills = []
                    try:
                        if job['onet_education'] == None or job['onet_education'] == 'None':
                            education = []
                        else:
                            education = ast.literal_eval(job['onet_education'])
                    except:
                        education = []
                    try:
                        if job['onet_reportedjobtitle'] == None or job['onet_reportedjobtitle'] == 'None':
                            reportedjobtitle = []
                        else:
                            reportedjobtitle = ast.literal_eval(job['onet_reportedjobtitle'])
                    except:
                        reportedjobtitle = []

                    try:
                        if job['onet_personality'] == None or job['onet_personality'] == 'None':
                            personality = []
                        else:
                            personality = ast.literal_eval(job['onet_personality'])
                    except:
                        personality = []

                    try:
                        if job['export_to_ezus'] == None or job['export_to_ezus'] == 'None':
                            export_to_ezus = 0
                        else:
                            export_to_ezus = job['export_to_ezus']
                    except:
                        export_to_ezus = 0

                    try:
                        if job['export_to_lensa'] == None or job['export_to_lensa'] == 'None':
                            export_to_lensa = 0
                        else:
                            export_to_lensa = job['export_to_lensa']
                    except:
                        export_to_lensa = 0

                    try:
                        if job['post_url'] == None or job['post_url'] == 'None':
                            post_url = ''
                        else:
                            post_url = re.sub(r'[^\x00-\x7F]+', '', str(job['post_url'])).replace('"', '').replace("'",
                                                                                                                   '').replace(
                                '%', '')
                    except:
                        post_url = ''

                    try:
                        onet_code = str(job['onet_code'])
                    except:
                        onet_code = ''

                    try:
                        job_tier = int(job['tier'])
                    except:
                        job_tier = None
                    try:
                        ats_name = str(job['ats']).strip()
                        if ats_name.lower() in self.ATS_LIST_INTERNAL_APPLY:
                            is_internal_apply = 1
                        else:
                            is_internal_apply = 0
                    except:
                        is_internal_apply = 0

                    direct_link = str(self.DOMAIN_NAME) + '/job-view/' + str(job_loc) + '/' + str(
                        job_title) + '/' + str(job_source) + '/' + str(
                        job['id']) + "?ez_source=alert&ez_campaign=daily-alert&ez_medium=azcs"
                    direct_link = str(direct_link).lower()

                    job_record = {
                        'advertiser_type_id': job['advertiser_type_id'],
                        'apply_url': job['application_url'],
                        'apply_email': '',
                        'ats': job['ats'],
                        'ats_txt': job['ats'],
                        'categories': [job['category_name']],
                        'categories_txt': [job['category_name']],
                        'categories_onet': '',
                        'closing_date': closing_dt,
                        'company': str(jb_company),
                        'company_txt': str(jb_company),
                        'cpc': 0.0,
                        'description': re.sub(r'[^\x00-\x7F]+', '', str(desc)).strip(),
                        'description_ai': '',
                        'description_onet': '',
                        'direct_link': direct_link,
                        'id': job['id'],
                        'is_internal_job': 1,
                        'job_location': job['city'],
                        'job_type': job['employment_type'],
                        'latitude': job['latitude'],
                        'longitude': job['longitude'],
                        'latlon': str(job['latitude']) + ',' + str(job['longitude']),
                        'occupation_ai': job['occupation_ai'],
                        'occupation_desc': job['occupation_desc'],
                        'postal_code': job['zip'],
                        'posting_date': job['post_date'],
                        'remote_id': '',
                        'salary': '',
                        'skills': [job['skill_1'], job['skill_2'], job['skill_3'], job['skill_4'], job['skill_5'],
                                   job['skill_6']],
                        'skills_txt': [job['skill_1'], job['skill_2'], job['skill_3'], job['skill_4'], job['skill_5'],
                                       job['skill_6']],
                        'skills_onet': '',
                        'source': str(jb_source),
                        'source_txt': str(jb_source),
                        'title': re.sub(r'[^\x00-\x7F]+', '', str(jb_title)).strip(),
                        'title_ai': '',
                        'title_onet': '',
                        'type_id': job['type_id'],
                        'value': "High",
                        'city': str(jb_city),
                        'region': str(jb_region),
                        'country': str(job_country),
                        'source_platform': job['source_platform'],
                        'clean_title': clean_title,
                        'ai_matches': ai_matches,
                        'onet_responsibilities': responsibilities,
                        'onet_abilities': abilities,
                        'onet_toolstechnology': toolstechnology,
                        'onet_knowledge': knowledge,
                        'onet_skills': skills,
                        'onet_education': education,
                        'onet_reportedjobtitle': reportedjobtitle,
                        'onet_personality': personality,
                        'export_to_ezus': export_to_ezus,
                        'export_to_lensa': export_to_lensa,
                        'post_url': post_url,
                        'onet_code': onet_code,
                        'is_updated': 1,
                        'job_tier': job_tier,
                        'is_internal_apply': is_internal_apply
                    }
                    if job_title == None or len(job_title) == 0 or job_title == '':
                        continue
                    else:
                        job_records.append(job_record)

                # How you'd index data.
                solr.add(job_records)
                solr.get_session().close()

            MSG = 'Total ' + str(len(job_records)) + ' active jobs saved in solr.'
            print(MSG)
            self.objLogThrd.doLog(MSG, self.fileName, 'info')
        except:
            self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'error')

    # This method is used to get user_id and account_id from company table.
    def getUserData(self):
        connection = "dbname='" + str(self.TALENTSPOT_DB) + "' user='" + str(self.TALENTSPOT_USER) + "' " \
                                                                                                     "host='" + str(
            self.TALENTSPOT_HOST) + "' password='" + str(self.TALENTSPOT_PWD) + "'"

        conn = psycopg2.connect(connection)

        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        usr_data = None
        try:
            query = 'SELECT company_id as source_id, user_id as manager_id, account_id FROM company'
            cursor.execute(query)
            usr_data = cursor.fetchall()
        except:
            self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'error')
        finally:
            cursor.close()
            conn.close()

        return usr_data

    def createDataTuple(self, lst_posts):
        lst_data = []
        for post in lst_posts:
            try:
                source_id = post['source_id']
                try:
                    user_data = [item for item in self.USER_DATA if int(item.get('source_id')) == int(source_id)]
                    if len(user_data) == 0:
                        user_data = None
                except:
                    user_data = None
                if user_data == None:
                    continue
                else:

                    status = 'active'
                    source_job_id = post['external_job_id']
                    post_id = int(post['id'])

                    title = re.sub(r'[^\x00-\x7F]+', '', str(post['title'])).replace('"', '').replace("'", '').replace(
                        '%', '')
                    desc = re.sub(r'[^\x00-\x7F]+', '', str(post['description'])).replace('"', '').replace("'",
                                                                                                           '').replace(
                        '%', '')

                    try:
                        created = post['created']
                        created = str(created).replace('T', ' ').replace('Z', '')
                        if created == None or created == 'None' or created == 'None 00:00:00':
                            created = str(datetime.datetime.now())
                    except:
                        created = datetime.datetime.now()

                    try:
                        updated = post['updated']
                        updated = str(updated).replace('T', ' ').replace('Z', '')
                        if updated == None or updated == 'None' or updated == 'None 00:00:00':
                            updated = str(datetime.datetime.now())
                    except:
                        updated = datetime.datetime.now()

                    try:
                        country = re.sub(r'[^\x00-\x7F]+', '', str(post['country'])).replace('"', '').replace("'",
                                                                                                              '').replace(
                            '%', '')
                        if int(len(str(country))) > 254:
                            country = ''
                    except:
                        country = ''

                    try:
                        state = re.sub(r'[^\x00-\x7F]+', '', str(post['region'])).replace('"', '').replace("'",
                                                                                                           '').replace(
                            '%', '')
                        if int(len(str(state))) > 254:
                            state = ''
                    except:
                        state = ''

                    try:
                        city = re.sub(r'[^\x00-\x7F]+', '', str(post['city'])).replace('"', '').replace("'",
                                                                                                        '').replace('%',
                                                                                                                    '')
                        if int(len(str(city))) > 254:
                            city = ''
                    except:
                        city = ''

                    try:
                        address = re.sub(r'[^\x00-\x7F]+', '', str(post['address'])).replace('"', '').replace("'",
                                                                                                              '').replace(
                            '%', '')
                        if int(len(str(address))) > 254:
                            address = ''
                    except:
                        address = ''

                    try:
                        desc = post['description']
                    except:
                        desc = 'NULL'


                    try:
                        latitude = post['latitude']
                    except:
                        latitude = ''

                    try:
                        longitude = post['longitude']
                    except:
                        longitude = ''

                    try:
                        publish_date = post['post_date']
                        publish_date = str(publish_date).replace('T', ' ').replace('Z', '')
                        if publish_date == None or publish_date == 'None' or publish_date == 'None 00:00:00' or len(
                                publish_date) == 0:
                            publish_date = str(post['created'])
                            publish_date = str(publish_date).replace('T', ' ').replace('Z', '')
                        else:
                            try:
                                publish_date = datetime.datetime.strptime(str(publish_date), '%Y-%m-%d %H:%M:%S')
                                publish_date = str(publish_date).replace('T', ' ').replace('Z', '')
                            except:
                                publish_date = datetime.datetime.now()
                                publish_date = str(publish_date).replace('T', ' ').replace('Z', '')
                    except:
                        publish_date = datetime.datetime.now()
                        publish_date = str(publish_date).replace('T', ' ').replace('Z', '')

                    try:
                        close_date = post['close_date']
                        close_date = str(close_date).replace('T', ' ').replace('Z', '')
                        if close_date == None or close_date == 'None' or close_date == 'None 00:00:00' or len(
                                close_date) == 0:
                            close_date = datetime.datetime.now() + datetime.timedelta(days=30)
                            close_date = str(close_date).replace('T', ' ').replace('Z', '')
                        else:
                            try:
                                close_date = datetime.datetime.strptime(str(close_date), '%Y-%m-%d %H:%M:%S')
                                close_date = str(close_date).replace('T', ' ').replace('Z', '')
                            except:
                                close_date = datetime.datetime.now() + datetime.timedelta(days=30)
                                close_date = str(close_date).replace('T', ' ').replace('Z', '')
                    except:
                        close_date = datetime.datetime.now() + datetime.timedelta(days=30)
                        close_date = str(close_date).replace('T', ' ').replace('Z', '')

                    try:
                        job_category = re.sub(r'[^\x00-\x7F]+', '', str(post['category_name'])).replace('"',
                                                                                                        '').replace("'",
                                                              '').replace('%', '')
                        job_category = job_category.lower()
                        category_id = [(i['id']) for i in categories_list if str(i['category']).lower() == job_category]
                        if len(category_id) > 0:
                            category_id = category_id[0]
                        else:
                            category_id = 1
                    except:
                        category_id = 1

                    try:
                        ats = post['ats']
                    except:
                        ats = ''

                    try:
                        job_tier = int(post['tier'])
                    except:
                        job_tier = 'NULL'

                    try:
                        onet_code = str(post['onet_code'])
                    except:
                        onet_code = 'NULL'

                    try:
                        slug = '/'
                        try:
                            country = country.lower()
                            slug += country + '-'
                        except:
                            slug = '/'

                        try:
                            state = state.lower()
                            slug += state + '-'
                        except:
                            slug = '/'

                        try:
                            city = city.lower()
                            slug += city
                        except:
                            slug = '/'

                        try:
                            slug += '/' + title.lower().strip().replace(' ', '-').replace('(', '').replace(')', '')
                        except:
                            slug = '/'

                    except:
                        slug = 'NULL'

                    try:
                        type_id = post['type_id']
                    except:
                        type_id = 'NULL'

                    try:
                        advertiser_type_id = post['advertiser_type_id']
                    except:
                        advertiser_type_id = 'NULL'

                    try:
                        occupation_title = post['occupation_title']
                    except:
                        occupation_title = 'NULL'

                    try:
                        company_id = post['company_id']
                    except:
                        company_id = 'NULL'

                    try:
                        post_url = re.sub(r'[^\x00-\x7F]+', '', str(post['post_url'])).replace('"', '').replace("'", '').replace( '%', '')
                    except:
                        post_url = ''

                    try:
                        zip = post['zip']
                    except:
                        zip = 'NULL'

                    try:
                        positions = post['positions']
                    except:
                        positions = 'NULL'



                    datatuple = (post_id, str(title),created, updated,slug,str(desc),
                                 str(status),  str(country), str(state), str(city),
                                 str(address),str(zip),str(positions) , publish_date, close_date, int(category_id),
                                 str(latitude), str(longitude), str(ats), str(post_url),type_id,advertiser_type_id,
                                 str(source_job_id), job_tier, onet_code,str(occupation_title),int(company_id))
                    if title == None or len(title) == 0 or title == '':
                        continue
                    else:
                        lst_data.append(datatuple)

            except:
                self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'warn')

        return lst_data

    def saveJobsToTalentspot(self, lst_data):
        msg = 'start process to save ' + str(len(lst_data)) + ' jobs to talentspot'
        print(msg)
        self.objLogThrd.doLog(msg, self.fileName, 'info')

        connection = "dbname='" + str(self.TALENTSPOT_DB) + "' user='" + str(self.TALENTSPOT_USER) + "' " \
 "host='" + str(
            self.TALENTSPOT_HOST) + "' password='" + str(self.TALENTSPOT_PWD) + "'"

        conn = psycopg2.connect(connection)
        cursor = conn.cursor()
        lst_data = tuple(lst_data)
        if len(lst_data) > 0:
            if len(lst_data) == 1:
                lst_data = str(lst_data).replace('),)', '))')
            try:
                query = "INSERT INTO job(id, title,  created, updated,slug,description,status,"
                query += " country, state, city, address,zip,positions, publish_date, close_date, category_id,"
                query += "latitude, longitude, ats, post_url,type_id,advertiser_type_id, source_job_id, job_tier, onet_code,occupation_title,company_id) VALUES"

                query += str(lst_data).replace("'NULL'", "NULL").replace('((', '(').replace('))', ')')

                query += " ON CONFLICT (id) DO UPDATE SET"
                query += " title = excluded.title, description = excluded.description,"
                query += " updated = excluded.updated, status = excluded.status,slug = excluded.slug,"
                query += " close_date=excluded.close_date, category_id=excluded.category_id,"
                query += " country = excluded.country, state= excluded.state, city= excluded.city, ats=excluded.ats,"
                query += " address=excluded.address, zip= excluded.zip,positions = excluded.positions, publish_date=excluded.publish_date,"
                query += "latitude=excluded.latitude, longitude=excluded.longitude,  post_url=excluded.post_url,"
                query += "source_job_id=excluded.source_job_id, job_tier=excluded.job_tier, onet_code=excluded.onet_code,company_id=excluded.company_id,occupation_title=excluded.occupation_title"
                cursor.execute(query)
                conn.commit()
            except:
                self.objLogThrd.doLog(lst_data, self.fileName, 'warn')
                self.objLogThrd.doLog(sys.exc_info(), self.fileName, 'error')
            finally:
                cursor.close()
                conn.close()


    def sendMail(self, started_on, end_on):
        try:
            posts_count = self.getPostsCount()
            postgrace_jobs_count = self.getPostgraceCount()
            solr_jobs_count = self.getSolrCount()
            time_taken = end_on - started_on

            t = str(time_taken)
            t = t[:t.rfind(":")]
            tmp_t = t.split(':')
            total_minutes = (int(tmp_t[0]) * 60) + int(tmp_t[1])

            from emailHandler import EmailManager
            objEM = EmailManager()

            started_on = str(started_on).split('.')[0]
            end_on = str(end_on).split('.')[0]
            time_taken = str(time_taken).split('.')[0]

            msg = "Hello Admin,<br/>"
            msg += "Following are the stats for the synchronization,<br/><br/>"
            msg += "<b>Start Time :</b> " + str(started_on) + "<br/>"
            msg += "<b>End Time :</b> " + str(end_on) + "<br/>"
            msg += "<b>Time Taken :</b> " + str(time_taken) + "<br/>"
            msg += "<b>Total Minutes :</b> " + str(total_minutes) + "<br/>"
            msg += "<b>Central DB Posts :</b> " + str(posts_count) + "<br/>"
            msg += "<b>Solr Jobs :</b> " + str(solr_jobs_count) + "<br/>"
            msg += "<b>Job Table Jobs :</b> " + str(postgrace_jobs_count) + "<br/><br/>"
            msg += "Thanks,<br/>"
            msg += "Job Wrapper"
            subject = "Internal US Jobs Synchronization Report"
            attachment = []

            response = objEM.SendEmail('gcrew.honey@gmail.com, hgillh@gmail.com', msg, subject, attachment)

            if response.status_code == 200:
                MSG = 'Report Email Sent.'
                self.objLog.doLog(MSG, self.fileName, 'info')
            else:
                MSG = 'Problem during sending email'
                self.objLog.doLog(MSG, self.fileName, 'warn')

        except:
            self.objLog.doLog(sys.exc_info(), self.fileName, 'error')

    def getPostsCount(self):
        conn = MySQLdb.connect(host=self.US_CRW_HOST, user=self.US_CRW_DB_USER, passwd=self.US_CRW_DB_PASSWORD,
                               db=self.US_CRW_DATABASE)
        cursor = conn.cursor()
        posts_count = 0
        try:
            query = "SELECT count(*) FROM posts WHERE is_deleted = 0 and source_platform='Job-Wrapper'"
            cursor.execute(query)
            jobPosts_count = cursor.fetchone()
            posts_count = int(jobPosts_count[0])
        except:
            self.objLog.doLog(sys.exc_info(), self.fileName, 'error')
        finally:
            cursor.close()
            conn.close()
        return posts_count

    def getPostgraceCount(self):
        connection = "dbname='" + str(self.TALENTSPOT_DB) + "' user='" + str(self.TALENTSPOT_USER) + "' " \
                                                                                                     "host='" + str(
            self.TALENTSPOT_HOST) + "' password='" + str(self.TALENTSPOT_PWD) + "'"

        conn = psycopg2.connect(connection)
        cursor = conn.cursor()
        posts_count = 0
        try:
            query = "SELECT count(*) FROM job WHERE status = 'active'"
            cursor.execute(query)
            jobPosts_count = cursor.fetchone()
            posts_count = int(jobPosts_count[0])
        except:
            self.objLog.doLog(sys.exc_info(), self.fileName, 'error')
        finally:
            cursor.close()
            conn.close()
        return posts_count

    def getSolrCount(self):
        posts_count = 0
        try:
            job_url = self.solrUrl + 'select?q=is_internal_job:1&wt=json&indent=on&rows=1'
            job_response = urllib.urlopen(job_url)
            job_jsonRaw = job_response.read()
            job_results = json.loads(job_jsonRaw)
            posts_count = job_results['response']['numFound']
        except:
            self.objLog.doLog(sys.exc_info(), self.fileName, 'error')
        return posts_count


if __name__ == '__main__':
    objLog = LogHelper('sync', 'jobimportsolrPsqlInt')
    objLog.doLog('Starting Solr, PSQL Job Import process', 'jobImportSolrPsqlInt.py', 'info')
    objPM = ProcessMonitor()
    dir_path = os.path.dirname(__file__)
    file_path = str(dir_path) + '/jobImportSolrPsqlInt.py'
    objLog.doLog('Checking existing process for job import', 'jobImportSolrPsqlInt.py', 'info')
    if objPM.checkRunningProcess(file_path):
        objLog.doLog('Going to exit as 1 process is already running.', 'jobImportSolrPsqlInt.py', 'info')
    else:
        objLog.doLog('Found no running process, starting new one.', 'jobImportSolrPsqlInt.py', 'info')
        objJIS = JobImportSolrPsqlHelperInt(objLog)
        objJIS.startProcessInt()