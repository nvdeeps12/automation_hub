from selenium import webdriver
from bs4 import BeautifulSoup
import xlwt
import xlrd
import time
import urllib
from urllib.request import Request
from work.scripts.downloadimage import downloader
from selenium.webdriver.common.keys import Keys
options = webdriver.FirefoxOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='C:\geckodriver',options=options)
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Name')
sheet1.write(0, 1, 'Description')
sheet1.write(0, 2, 'Features')
sheet1.write(0, 3, 'ImageUrl')
sheet1.write(0, 4, 'Link')

row=0
rd=xlrd.open_workbook('672book.xlsx')
sheet = rd.sheet_by_index(0)
sheet.cell_value(0, 0)
for i in range(sheet.nrows):
        try:
                driver.get(sheet.cell_value(i, 0))
                row = row + 1
                name=driver.find_element_by_xpath('/html/body/div[5]/div[2]/div/div[2]/div/div[1]/div[1]/header/header/h1').text.strip()
                lnk = sheet.cell_value(i,0)
                print('Process Started For---',lnk)

                print(name)
                des=driver.find_element_by_class_name('ProductOverviewInformation-description').text
                driver.find_element_by_xpath('//*[@id="CollapseToggle-2"]').click()
                time.sleep(2)
                try:
                        # f=driver.find_element_by_xpath('/html/body/div[5]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[3]/ul/li/div/div/div/div[1]/div[1]/table')
                        f=driver.find_element_by_class_name('Specifications-table')
                except:
                        continue
                r=f.find_elements_by_tag_name('tr')

                imglink = driver.find_element_by_xpath('/html/body/div[5]/div[2]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/ul/li[1]/div/div/div/div/img').get_attribute('src')
                downloader(imglink,name)
                fe=[]
                for i in r:
                    t=i.find_elements_by_tag_name('td')
                    u=t[0].text +' - '+t[1].text+','
                    fe.append(u)
                    # for tt in t:
                    #     f=tt.text
                    #     print(tt.text)

                sheet1.write(row, 0, name)
                sheet1.write(row, 1, des)
                sheet1.write(row, 2, fe)
                sheet1.write(row, 3, imglink)
                sheet1.write(row, 4, lnk)
                wb.save('1340sofa.xls')
                print('Done----',row)
                import datetime
                print(datetime.datetime.now())
                print('------------'*100)

        except:
                name='--'
                des='--'
                fe='--'
                imglink='--'

