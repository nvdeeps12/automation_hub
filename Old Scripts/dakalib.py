data = """420-426110 7/11,7/18,7/25,8/1skNotice of Sale Under PowerGeorgia, DeKalb CountyUnder and by virtue of the Power of Sale contained in a Deed to Secure Debt given by Charles L. Martin to Mortgage Electronic Registration Systems, Inc. ("MERS") as nominee for SouthTrust Mortgage Corp. dba Equibanc Mortgage, dated November 30, 2004, and recorded in Deed Book 16985, Page 261, DeKalb County, Georgia records, as last transferred to U.S. Bank NA, successor trustee to Bank of America, NA, successor in interest to LaSalle Bank National Association, as trustee, on behalf of the holders of the Bear Stearns Asset Backed Securities Trust 2005-4, Asset-Backed Certificates, Series 2005-4 by Assignment recorded in Deed Book 23143, Page 660, DeKalb County, Georgia records, conveying the after-described property to secure a Note of even date in the original principal amount of $125,600.00, with interest at the rate specified therein, there will be sold by the undersigned at public outcry to the highest bidder for cash before the Courthouse door of DeKalb County, Georgia, within the legal hours of sale on the first Tuesday in August, 2019, to wit: August 6, 2019, the following described property:All that tract or parcel of land lying and being in Land Lot 71 of the 18th District, DeKalb County, Georgia, being Lot 4, Block B of Hearthstone Subdivision, Unit One, as per plat recorded in Plat Book 48, Page 21, DeKalb County records, to which plat reference is made for a more detailed description. Being improved property known as 839 Fireside Way, Stone Mountain, according to the present system of numbering houses in DeKalb County, Georgia.The debt secured by said Deed to Secure Debt has been and is hereby declared due because of, among other possible events of default, failure to pay the indebtedness as and when due and in the manner provided in the Note and Deed to Secure Debt. The debt remaining in default, this sale will be made for the purpose of paying the same and all expenses of this sale, as provided in the Deed to Secure Debt and by law, including attorney's fees (notice of intent to collect attorney's fees having been given).Said property is commonly known as 839 Firesideway, Stone Mountain, GA 30083, together with all fixtures and personal property attached to and constituting a part of said property. To the best knowledge and belief of the undersigned, the party (or parties) in possession of the subject property is (are): Charles L. Martin or tenant or tenants.Said property will be sold subject to (a) any outstanding ad valorem taxes (including taxes which are a lien, but not yet due and payable), (b) any matters which might be disclosed by an accurate survey and inspection of the property, and (c) all matters of record superior to the Deed to Secure Debt first set out above, including, but not limited to, assessments, liens, encumbrances, zoning ordinances, easements, restrictions, covenants, etc.The sale will be conducted subject to (1) confirmation that the sale is not prohibited under the U.S. Bankruptcy Code; (2) O.C.G.A. Section 9-13-172.1; and (3) final confirmation and audit of the status of the loan with the holder of the security deed.Pursuant to O.C.G.A. Section 9-13-172.1, which allows for certain procedures regarding the rescission of judicial and nonjudicial sales in the State of Georgia, the Deed Under Power and other foreclosure documents may not be provided until final confirmation and audit of the status of the loan as provided in the preceding paragraph.Pursuant to O.C.G.A. Section 44-14-162.2, the entity that has full authority to negotiate, amend and modify all terms of the mortgage with the debtor is:Select Portfolio Servicing, Inc.Attention: Loss Mitigation Department3217 S. Decker Lake DriveSalt Lake City, Utah 841191-888-818-6032The foregoing notwithstanding, nothing in OC.G.A. Section 44-14-162.2 shall be construed to require the secured creditor to negotiate, amend or modify the terms of the Deed to Secure Debt described herein.This sale is conducted on behalf of the secured creditor under the power of sale granted in the aforementioned security instrument, specifically beingU.S. Bank NA, successor trustee to Bank of America, NA, successor in interest to LaSalle Bank National Association, as trustee, on behalf of the holders of the Bear Stearns Asset Backed Securities Trust 2005-4, Asset-Backed Certificates, Series 2005-4as attorney in fact forCharles L. MartinRichard B. Maner, P.C.180 Interstate N Parkway, Suite 200Atlanta, GA 30339404.252.6385++FC19-209/MARTIN++THIS LAW FIRM IS ACTING AS A DEBT COLLECTOR ATTEMPTING TO COLLECT A DEBT. ANY INFORMATION OBTAINED WILL BE USED FOR THAT PURPOSE."""

print(data)

file = data[0:10]
print('File---',file)

amount = 0
try:
    amount = data.split('DOLLARS')[1].split('),')[0]
except:
    amount = data.split('$')[1].split('with')[0]
    if ')' in amount:
        amount = amount.split(')')[0]

print('Amount--',amount)

address = data.split('known as')[1].split('.')[0]
if 'together' in address:
    address = address.split('together')[0]

print('Address',address)
try:
    bank_name = data.split('transferred to')[1].split(',')[0]
    if 'or acquired by' in bank_name:
        bank_name = bank_name.split('or acquired by')[1]
    if 'by' in bank_name:
        bank_name = bank_name.split('by')[0]
except:
    bank_name = '---'

print('Bank _name',bank_name)

try:
    loan_servicer = data.split('the property is')[1].split('or')[0]
except:
    loan_servicer = '---'
print('LOan Serv---',loan_servicer)


