import requests
from bs4 import BeautifulSoup
import sys
import re
import datetime
import xlwt,xlrd
referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }
# rd = xlrd.open_workbook('.\Inputs\List Needed.xlsx')
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('Sheet1')
sheet1.write(0, 0, 'First Name')
sheet1.write(0, 1, 'Last Name')
sheet1.write(0, 2, 'Phone')
sheet1.write(0, 3, 'Office')
sheet1.write(0, 4, 'Mobile')
sheet1.write(0, 5, 'Address')
sheet1.write(0, 6, 'Website')
sheet1.write(0, 7, 'Profile Link')
sheet1.write(0, 8, 'Agent Pic')
sheet1.write(0, 9, 'UTC')
# sheet = rd.sheet_by_index(0)
# sheet.cell_value(0, 0)
states=['NC']

class keller():
    row = 0
    def states(self,states):
        for s in states:
            print('State - '+s)
            try:
                link = requests.get('https://kw.com/kw/AgentSearchSubmit.action?__checkbox_otherSpecialtiesFl=true&__checkbox_farmAndRanchFl=true&zip=&designations=&__checkbox_luxuryFl=true&__checkbox_gpsFl=true&stateProvId='+s+'&lastname=&firstname=&city=&specialties=Ex:%20Foreclosure&__checkbox_commercialFl=true&rows=20',headers = headers)
                print("status code 1 - "+ str(link.status_code))
                soup = BeautifulSoup(link.content, 'html.parser')
                self.rec = soup.find('div', attrs={'class': 'podRowLeft'}).find('h3').text.strip().split('out of ')[1]
                print('Total Agents in '+s+' - '+self.rec)
                link2='https://kw.com/kw/AgentSearchSubmit.action?__checkbox_otherSpecialtiesFl=true&__checkbox_farmAndRanchFl=true&zip=&designations=&__checkbox_luxuryFl=true&__checkbox_gpsFl=true&stateProvId='+s+'&lastname=&firstname=&city=&specialties=Ex:%20Foreclosure&__checkbox_commercialFl=true&rows='+self.rec

                self.get_links(link2)
            except:
                print(sys.exc_info())
                continue
    def get_links(self,link2):
        try:
            # print(link2)
            # link = requests.get('https://kw.com/kw/AgentSearchSubmit.action?__checkbox_otherSpecialtiesFl=true&__checkbox_farmAndRanchFl=true&zip=&designations=&__checkbox_luxuryFl=true&__checkbox_gpsFl=true&stateProvId=AL&lastname=&firstname=&city=&specialties=Ex:%20Foreclosure&__checkbox_commercialFl=true&rows=20',headers = headers)
            # soup = BeautifulSoup(link.content,'html.parser')
            # rec=soup.find('div',attrs={'class':'podRowLeft'}).find('h3').text.strip().split('out of ')[1]
            # print(rec)
            link = requests.get(link2,headers = headers)
            print("status code 2 - " + str(link.status_code))
            # print(soup)
            soup2= BeautifulSoup(link.content,'html.parser')

            ul = soup2.find('ul',attrs={'class':'searchResults'}).findAll('ul',attrs = {'class':'searchResultsInfo'})

            for a in ul:
                try:
                    anchors = a.find('a')['href']
                    if '.com' in anchors:
                        continue
                    full_link = 'https://kw.com'+str(anchors)
                except:
                    full_link = None
                    print(sys.exc_info())
                    continue
                self.row+=1
                print(full_link)
                print(str(self.row)+' / '+self.rec)
                self.agents_soup(full_link)
        except:
            full_link = ''


    def agents_soup(self,agent_link):
        a_link=agent_link
        try:
            agent_link  = requests.get(agent_link,headers = headers)
            print("status code 3 - " + str(agent_link.status_code))
            soup = BeautifulSoup(agent_link.content,'html.parser')

            try:
                agent_name_f = soup.find('div',attrs={'class':'headerTag'}).find('h1').text.split(' ')
                # agent_name_l= soup.find('div',attrs={'class':'headerTag'}).find('h1').text.split(' ')[1]
                agent_name = ''.join(agent_name_f)
                agent_name=agent_name.strip()
                fname=agent_name.split('\n')[0]
                lname=agent_name.split('\n')[1]
                # agent_name_l=agent_name_l.strip()
            except:
                agent_name = 'None'
                fname='None'
                lname='None'
                # agent_name_l = 'None'
            print(fname)
            print(lname)
            # print(agent_name)
            # print(agent_name_l)
            div_data = soup.find('div', attrs={'class': 'colLeft'})
            try:
                # div_data=soup.find('div',attrs={'class':'colLeft'})
                phone=div_data.find('strong',text=re.compile('Phone:')).find_previous('td').text.strip().split('Phone:')[1].strip()
                # phone = ''
            except:
                phone = 'None'
            try:
                mobile=div_data.find('strong',text=re.compile('Mobile:')).find_previous('td').text.strip().split('Mobile:')[1].strip()
            except:
                mobile='none'
            try:
                office=div_data.find('strong',text=re.compile('Office:')).find_previous('td').text.strip().split('Office:')[1].strip();
            except:
                office='none'
            try:
                web=div_data.find('a',text=re.compile('Visit My Website')).get('href')
            except:
                web='None'

            try:
                pic=div_data.find('img',attrs={'id':'agent-photo'}).get('src')
            except:
                pic='None'
            print(a_link)
            print(web)
            print(mobile)
            print(office)
            print(phone)
            print(pic)

            print(datetime.datetime.now())
            print('---'*20)
            sheet1.write(self.row, 0, fname)
            sheet1.write(self.row, 1, lname)
            sheet1.write(self.row, 2, phone)
            sheet1.write(self.row, 3, office)
            sheet1.write(self.row, 4, mobile)
            sheet1.write(self.row, 5, 'Not Avaialable')
            sheet1.write(self.row, 6, web)
            sheet1.write(self.row, 7, a_link)
            sheet1.write(self.row, 8, pic)
            sheet1.write(self.row, 9, str(datetime.datetime.now()))
            wb.save('Agents_keller_nc.xls')

        except:
            print(sys.exc_info())
            pass



obj=keller()
obj.states(states)
