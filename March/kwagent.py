import requests
from bs4 import BeautifulSoup
import xlwt
from selenium import webdriver
import time

class KwAgent():
    def __init__(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--disable-gpu')
        # options.add_argument('--headless')
        self.driver = webdriver.Chrome(executable_path='F:\Projects\Fiver\Drivers\chromedriver.exe', options=self.options)
        self.wb = xlwt.Workbook()
        self.sheet1 = self.wb.add_sheet('Sheet')
        self.header = False
        self.row = 0

    def get_link(self):
        try:
            link = "https://www.kw.com/agent/search/"
            # states = ['TX','AZ']
            states = ['FL', 'MO', 'SC', 'WI']

            for st in states:
                self.final_link = link + st
                print(self.final_link)
                self.driver.get(self.final_link)
                time.sleep(5)
                total_agent = self.driver.find_element_by_css_selector(".FindAgentRoute__totalCount > div").text
                total_agent = total_agent.split(" ")[1]
                no_of_scroll = int(int(total_agent)/50) - 1
                rem = int(total_agent)%50

                if rem != 0:
                    no_of_scroll += 1
                print(no_of_scroll)
                # no_of_scroll = 0

                for count in range(int(total_agent)):


                    i = 0
                    while i < no_of_scroll:
                        print(i)
                        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                        time.sleep(4)
                        i += 1

                    xpath_element = '//*[@id="root"]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div[' + str(count) + ']/div'
                    print('Hitting Xpath-->', xpath_element)
                    try:
                        self.driver.find_element_by_xpath(xpath_element).click()
                    except:
                        print('I am in Except')
                        self.driver.quit()
                        print('Opened Chrome Again--')
                        self.driver = webdriver.Chrome(executable_path='F:\Projects\Fiver\Drivers\chromedriver.exe',options=self.options)
                    self.getAgentData()
                    import datetime

                    print('T->',datetime.datetime.now())
                    print("--------------------"*50)

                    if int(total_agent) != (count+1):
                        print("Getting Back")
                        self.driver.get(self.final_link)
                        time.sleep(5)
        except:
            import sys
            print(sys.exc_info())
        finally:
            self.driver.close()

    def getAgentData(self):
        time.sleep(5)
        soup = BeautifulSoup(self.driver.page_source, u'html.parser')
        fields = ['firstname','lastname','zipcode','license','city','state','mobile_number','office_number','email','url']
        data = []

        try:
            fullname = soup.find('div', attrs={'class': 'AgentHeader__name'}).text.strip()
            if ',' in fullname:
                fullname = fullname.split(",")[0].strip()
        except:
            fullname = 'None'

        if fullname != 'None':
            try:
                firstname = fullname.split(" ")[:-1]
            except:
                firstname = 'None'
            data.append(firstname)

            try:
                lastname = fullname.split(" ")[-1]
            except:
                lastname = 'None'
            data.append(lastname)

        try:
            full_licence = soup.find('div', attrs={'class': 'AgentHeader__team'}).text.strip()
        except:
            full_licence = 'None'

        if full_licence != 'None':
            try:
                zipcode = int(full_licence.split("License")[0].strip().split(" ")[-1].strip())
            except:
                zipcode = 'None'
            data.append(zipcode)

            try:
                license = full_licence.split("License #: ")[1]
            except:
                license = 'None'
            data.append(license)

        try:
            fulllocation = soup.find('div', attrs={'class': 'AgentHeader__location'}).text.strip()
        except:
            fulllocation = 'None'

        if fulllocation != 'None':
            try:
                city = fulllocation.split(", ")[0]
            except:
                city = 'None'
            data.append(city)

            try:
                state = fulllocation.split(", ")[1]
            except:
                state = 'None'
            data.append(state)

        try:
            contacts = soup.findAll('div', attrs={'class': 'AgentContent__factBody'})
        except:
            contacts = 'None'

        if contacts != 'None':
            try:
                mobile_number = contacts[0].text.strip()
                mobile_number = str(mobile_number).split("Mobile:")[1].strip()
            except:
                mobile_number = 'None'
            data.append(mobile_number)

            try:
                office_number = contacts[1].text.strip()
                office_number = str(office_number).split("Office:")[1].strip()
            except:
                office_number ='None'
            data.append(office_number)

            try:
                email = contacts[2].text.strip()
            except:
                email = 'None'
            data.append(email)
        data.append(self.final_link)
        print(data)
        self.saveInXls(fields,data)


    def saveInXls(self,fields,data_arr,file_name):
        if self.header != True:
            colno = 0
            for f in fields:
                self.sheet1.write(0,colno,f)
                colno += 1
            self.header = True

        col_no = 0
        self.row += 1
        import datetime
        print('Done->',self.row)
        print(datetime.datetime.now())
        for d in data_arr:
            self.sheet1.write(self.row, col_no, d)
            file_name = file_name+str('.xls')
            self.wb.save(file_name)
            col_no += 1


obj = KwAgent()
obj.get_link()