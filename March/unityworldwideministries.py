import requests
from bs4 import BeautifulSoup
import re
import xlwt
wb = xlwt.Workbook()

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Title')
sheet1.write(0, 1, 'Website')
sheet1.write(0, 2, 'Phone')
sheet1.write(0, 3, 'Meeting Address')
sheet1.write(0, 4, 'City')
sheet1.write(0, 5, 'State')
sheet1.write(0, 6, 'Postal')
sheet1.write(0, 7, 'Mailing Address')
sheet1.write(0, 8, 'Email')
sheet1.write(0, 9, 'Link')


def get_churches():
    row = 0
    for p in range(25):
        if p<8:
            continue
        page = 'https://www.unityworldwideministries.org/ministry-search?field_postal_code_value=&distance%5Bpostal_code%5D=&distance%5Bsearch_distance%5D=100&distance%5Bsearch_units%5D=mile&field_city_value=&field_state_province_region_value=All&field_country_value_1=UNITED%20STATES&keys=&page='+str(p)
        print('Page->',p)
        con = requests.get(page)
        soup = BeautifulSoup(con.content, u'html.parser')
        table = soup.find('table',attrs={'class':'views-table'})
        rows = table.find_all('tr')[1:]
        for r in rows:
            try:
                hrefs = r.find('a')['href']
                links = 'https://www.unityworldwideministries.org'+str(hrefs)
                row+=1
            except:
                links = 'None'

            get_data(links,row)

def get_data(links,row):
    con = requests.get(links)
    print('Link->',links)
    print(con.status_code)
    soup = BeautifulSoup(con.content, u'html.parser')

    try:
        title = soup.find('h1').text.strip()
    except:
        title ='None'

    try:
        Website = soup.find('div' ,text=re.compile('Website')).find_next('div').text.strip()
    except:
        Website ='None'

    try:
        Phone = soup.find('div' ,text=re.compile('Phone')).find_next('div').text.strip()
    except:
        Phone ='None'

    try:
        Meeting_Address = soup.find('div' ,text=re.compile('Meeting')).find_next('div').text.strip()
    except:
        Meeting_Address ='None'

    try:
        City = soup.find('div' ,text=re.compile('City')).find_next('div').text.strip()
    except:
        City ='None'

    try:
        State = soup.find('div' ,text=re.compile('State')).find_next('div').text.strip()
    except:
        State ='None'

    try:
        Postal_Code = soup.find('div' ,text=re.compile('Postal')).find_next('div').text.strip()
    except:
        Postal_Code ='None'

    try:
        Mailing_Address = soup.find('div' ,text=re.compile('Mailing')).find_next('div').text.strip()
    except:
        Mailing_Address ='None'

    try:
        Email = soup.find('div' ,text=re.compile('Email')).find_next('div').text.strip()
    except:
        Email ='None'


    print('Title->',title)
    print('WEb->', Website)
    print('PH->', Phone)
    print('Mett->', Meeting_Address)
    print('City->', City)
    print('State->', State)
    print('Postal->', Postal_Code)
    print('Mail->',Mailing_Address)
    print('Eail->', Email)
    print('--'*90)

    sheet1.write(row, 0, title)
    sheet1.write(row, 1, Website)
    sheet1.write(row, 2, Phone)
    sheet1.write(row, 3, Meeting_Address)
    sheet1.write(row, 4, City)
    sheet1.write(row, 5, State)
    sheet1.write(row, 6, Postal_Code)
    sheet1.write(row, 7, Mailing_Address)
    sheet1.write(row, 8, Email)
    sheet1.write(row, 9, links)
    wb.save('Churches_page8.xls')
    print('Done->',row)




# get_data('https://www.unityworldwideministries.org/ministry/unity-church-anderson',1)


get_churches()