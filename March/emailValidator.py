import re
import xlrd
import xlwt
import dns.resolver
import socket
import smtplib
import sys

class CheckEmail():

	def __init__(self):
		self.workbook = xlwt.Workbook()
		self.sheet1 = self.workbook.add_sheet('sheet')
		self.sheet1.write(0, 0, 'Email')
		self.sheet1.write(0, 1, 'Response')

	def readAndwriteFile(self):
		"""
		method to read emails and write in xls after check email
		:return:
		"""
		loc = "C:\Python_Projects\\fiver\March\Left830.xlsx"
		wb = xlrd.open_workbook(loc)
		sheet = wb.sheet_by_index(0)

		for i in range(sheet.nrows):
			print('Running-->',i)
			print('Email-->',sheet.cell_value(i+1, 0))

			dns_query = str(sheet.cell_value(i+1, 0)).split('@')[1]
			self.checkEmail(sheet.cell_value(i+1, 0),dns_query, i+1)
			
			# if i == 200:
			# 	break

	def checkEmail(self,email,dns_query,row_no):
		print('Dns',dns_query)
		"""
		method to check email is valid or not
		:param email:
		:param dns_query:
		:param row_no:
		:return:
		"""
		try:
			addressToVerify =str(email)
			match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', addressToVerify)

			if match == None:
				print('Bad Syntax')
				msg = "bad"

			else:

				records = dns.resolver.query(dns_query, 'MX')
				mxRecord = records[0].exchange
				mxRecord = str(mxRecord)

				# Get local server hostname
				host = socket.gethostname()

				# SMTP lib setup (use debug level for full output)
				server = smtplib.SMTP()
				server.set_debuglevel(0)
				try:
				# SMTP Conversation
					if 'yahoo' in mxRecord:
						msg = 'Invalid Email'
					else:
						server.connect(mxRecord)
						server.helo(host)
						server.mail('deepak.dhanjal12@live.com')

						code, message = server.rcpt(str(addressToVerify))
						server.quit()

						print('Code->',code)

						if code == 250:
							msg = 'Valid Email'
							print('Response-->','Success')
						else:
							msg = 'Invalid Email'
							print('Response-->','Bad')

							print('--'*70)
				except:
					msg ='Invalid Email'
		except:
			msg = 'Bad'
			print("Bad")
		self.saveInXls(row_no,email,msg)


	def saveInXls(self,row_no,email,msg):
		"""
		method to write data in xls
		:param row_no:
		:param email:
		:param msg:
		:return:
		"""
		try:
			self.sheet1.write(row_no, 0, email)
			self.sheet1.write(row_no, 1, msg)
			self.workbook.save('ValidEmails.xls')
		except:
			print(sys.exc_info())


obj = CheckEmail()
obj.readAndwriteFile()