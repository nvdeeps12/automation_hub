# import pprint
# from time import sleep
# from InstagramAPI import InstagramAPI
# import pandas as pd


# users_list = []
# following_users = []
# follower_users = []


# api = InstagramAPI("harmm5714", "Hamu@1997")

# api.login()

# api.searchUsername('hamudhillon')

# u=api.LastJson['user']['pk']
# api.follow(u)
# api.searchTags('tokunbocars')
# # pprint.pprint(api.LastJson['results'][0])
# api.getHashtagFeed('tokunbocars')
# result=api.LastJson
# items=result['items']
# print(len(items))
# for users in items:
#     try:
#         user=users['user']['username']
#         pprint.pprint(user)
#         api.searchUsername(user)
#         result=api.LastJson
#         Name=result['user']['full_name']
#         bio=result['user']['biography'][4]
#         email=result['user']['public_email']
#         phone=result['user']['public_phone_number']
#         print(Name,bio,str(email),phone)
#     except:
#         continue
# api.getUserFeed('tokunbocars')
# pprint.pprint(api.LastJson)


import os, platform, time, urllib.request, openpyxl, operator
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from openpyxl import Workbook



class InstagramScrapper:
    def Create_Dir(self, dir_name):
        if not os.path.exists("data"):
            try:
                os.mkdir("data")
                print("Created directory 'data'")
            except:
                print("Unable to create directory 'data': Directory already exists")
        else:
            print("Unable to create directory 'data': Directory already exists")

        if not os.path.exists("data/data_" + dir_name):
            try:
                os.mkdir("data/data_" + dir_name)
                print("Created directory 'data/data_" + dir_name + "'")
            except:
                print("Unable to create directory 'data/data_" + dir_name + "': Directory already exists")
        else:
            print("Unable to create directory 'data/data_" + dir_name + "': Directory already exists")

        if not os.path.exists("data/data_" + dir_name + '/img'):
            try:
                os.mkdir("data/data_" + dir_name + '/img')
                print("Created directory 'data/data_" + dir_name + "/img'")
            except:
                print("Unable to create directory 'data/data_" + dir_name + "/img': Directory already exists")
        else:
            print("Unable to create directory 'data/data_" + dir_name + "/img': Directory already exists")

        
    def Scrape_Instagram(self, tag, limit=29000, browser='chrome'):
        self.Create_Dir(tag)

        print("Starting Scrapping Instagram")
        file_path = "data/data_" + tag
        keyword = tag
        # Adding path.
        if not os.getcwd() in os.get_exec_path():
            # print('adding path')
            if platform.system() == "Windows":
                os.environ["PATH"] = os.environ["PATH"] + ";" + os.getcwd()
            else:
                os.environ["PATH"] = os.environ["PATH"] + ":" + os.getcwd()

        # opening instagram in browser
        if 'chrome' in browser.lower():
            options = webdriver.ChromeOptions()
            # options.add_extension('F:/PycharmProjects/MainKam/fiver/March/0.7.0_0.crx')
            options.add_argument('--disable-gpu')
            # options.add_argument('--headless')
            driver = webdriver.Chrome(executable_path='F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe', options=options)
            # driver = webdriver.Chrome()
        else:
            driver = webdriver.Firefox()
        
        driver.get('https://www.instagram.com/')
        time.sleep(3)
        uname=driver.find_element_by_name('username')
        uname.click()
        uname.send_keys('harmm5714')
        upass=driver.find_element_by_name('password')
        upass.click()
        upass.send_keys('Hamu@1997')
        driver.find_element_by_xpath('/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div[4]').click()
        time.sleep(10)
        driver.get("https://www.instagram.com/" + "explore/tags/" + tag)

        print("Loading Posts")
        time.sleep(10)
        print("Loading Data")

        # Clicking on load more once to load more images. Afterwards we will just
        # tap space to scroll to the page end to load more images
        actions = ActionChains(driver)
        clear = lambda: os.system('cls')
        msg = "Loading Images"
        links=[]
        for i in range(50):
            actions.send_keys(Keys.SPACE).perform()
            time.sleep(5)
            clear = lambda: os.system('cls')
            msg = "Loading Images"
            
            for a in driver.find_elements_by_class_name('v1Nh3'):
                links.append(a.find_element_by_tag_name('a').get_attribute('href'))
                print(a.find_element_by_tag_name('a').get_attribute('href'))
        print(links) 

        # Create a workbook for excel
        tag_File = file_path + "/" + tag + "_Instagram.xlsx"
        wb = openpyxl.Workbook()
        
        print("Dumping data in excel file")
        
        ws_Tags = wb.create_sheet(title="user")
        tagName = 'A'
        
        row = 1

        for l in links:
            ws_Tags[tagName + str(row)] = l
            row += 1
        wb.save(tag_File)
        print("Closing Instagram")
        driver.quit()

myob=InstagramScrapper()
myob.Scrape_Instagram('tokunbocars')