import requests
from bs4 import BeautifulSoup
import xlwt
from selenium import webdriver
import time
# from uszipcode import SearchEngine, SimpleZipcode, Zipcode
# with SearchEngine() as search:
#     res = search.by_city_and_state(city="cicago", state="ilinoy")
#     print(res)
import zipcodes

class KwAgent():
    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--disable-gpu')
        # options.add_argument('--headless')
        driver = webdriver.Chrome(executable_path='F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe', options=options)
        self.wb = xlwt.Workbook()
        self.sheet1 = self.wb.add_sheet('Sheet')
        self.sheet1.write(0,0,'FirstName')
        self.sheet1.write(0,1,'lastname')
        self.sheet1.write(0,2,'zipcode')
        self.sheet1.write(0,3,'city')
        self.sheet1.write(0,4,'state')
        self.sheet1.write(0,5,'mobile_number')
        self.sheet1.write(0,6,'email')
        self.sheet1.write(0,7,'Profile_Image_url')
        self.row = 0


        link = "https://www.kw.com/agent/search/"
        # states = ['TX','AZ']
        # states = ['WI']
        states = ['TX/Austin','AZ/Tempe','FL/Orlando','FL/Tampa','FL/Miami','MO/Kansas City','MO/Springfield','MO/St. Louis',"MO/Lee's Summit","MO/O'Fallon",'SC/Anderson','SC/Myrtle Beach','SC/Columbia','SC/Greenville','SC/Hilton Head Island', 'WI/Madison','WI/Appleton','WI/Eau Claire','AZ/Scottsdale','AZ/Gilbert']
        
        # states = ['FL', 'MO', 'SC', 'WI'] FL/Orlando

        for st in states:
            self.final_link = link + st
            print(self.final_link)
            driver.get(self.final_link)
            time.sleep(10)
            total_agent = driver.find_element_by_css_selector(".FindAgentRoute__totalCount > div").text
            total_agent = total_agent.split(" ")[1].replace(',','').strip()
            no_of_scroll = int(int(total_agent)/50) - 1
            rem = int(total_agent)%50
            fields = ['firstname','lastname','zipcode','city','state','mobile_number','email','Profile_Image_url']
            

            data = []
            self.scroll(driver,5)
            soup = BeautifulSoup(driver.page_source, u'html.parser')
            ags=soup.findAll('div',attrs={'class':'AgentCard'})
            
            for ag in ags:
                try:
                    name=ag.find('div',attrs={'class':'AgentCard__name'}).text.strip()
                    fname=str(name).split(' ')[0]
                    lname=str(name).split(' ')[1]
                    
                except:
                    name='None'
                print(name)
                try:
                    location=ag.find('div',attrs={'class':'AgentCard__location'}).text.strip()
                    city=str(location).split(',')[0]
                    state=str(location).split(',')[1]
                    zipp=zipcodes.filter_by(city=city)[0]['zip_code']
                    
                except:
                    city='none'
                    state='none'
                    zipp='none'
                # print(city,state,zipp)
                try:
                    contact=ag.find('div',attrs={'class':'AgentCard__contacts'}).text.strip()
                    if '|' in contact:
                        mail=str(contact).split('|')[0]
                        phone=str(contact).split('|')[1]
                    elif '@kw.com' in contact:
                        mail=contact
                        phone='none'
                    elif '@kw.com' not in contact:
                        mail='none'
                        phone=contact
                except:
                    mail='None'
                    phone='None'
                # print(mail)
                # print(phone)
                try:
                    profile_img=str(ag.find('div',attrs={'class':'KWAvatar__bg'}).get('style')).split('background-image: url("')[1].split('");')[0]
                    # print(profile_img)
                except:
                    profile_img='None'
                self.row=self.row +1
                self.sheet1.write(self.row,0,fname)
                self.sheet1.write(self.row,1,lname)
                self.sheet1.write(self.row,2,zipp)
                self.sheet1.write(self.row,3,city)
                self.sheet1.write(self.row,4,state)
                self.sheet1.write(self.row,5,phone)
                self.sheet1.write(self.row,6,mail)
                self.sheet1.write(self.row,7,profile_img)
                self.wb.save('F:/PycharmProjects/MainKam/fiver/March/Kw_data1.xls')
                print('~~~~~'*20)
        # print(data)
        # self.saveInXls(fields,data)
    def scroll(self,driver, timeout):
            scroll_pause_time = timeout

        # Get scroll height
            last_height = driver.execute_script("return document.body.scrollHeight")

            while True:
                # Scroll down to bottom
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                # Wait to load page
                time.sleep(scroll_pause_time)

                # Calculate new scroll height and compare with last scroll height
                new_height = driver.execute_script("return document.body.scrollHeight")
                if new_height == last_height:
                    # If heights are the same it will exit the function
                    break
                last_height = new_height

    def saveInXls(self,fields,data):
        if self.header != True:
            colno = 0
            for f in fields:
                self.sheet1.write(0,colno,f)
                colno += 1
            self.header = True

        col_no = 0
        self.row += 1
        print('Done-->',self.row)
        for d in data:
            self.sheet1.write(self.row, col_no, d)
            self.wb.save('F:/PycharmProjects/MainKam/fiver/March/Kw_Florida.xls')
            col_no += 1


obj = KwAgent()
# obj.__init__()