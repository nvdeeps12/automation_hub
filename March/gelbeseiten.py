import requests
from bs4 import BeautifulSoup
import csv
out_file = open('F:\Projects\Fiver\Files\pgelbeseiten.csv','w')
fields = ['Name','Address','Website','Email','Phone']
csvwriter = csv.DictWriter(out_file, delimiter=',', fieldnames=fields)
csvwriter.writeheader()

titles_arr = []
location='Leipzig'
search='Immobilienmakler'
agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'

link='https://www.gelbeseiten.de/Branchen/'+str(search)+'/'+str(location)
res=requests.get(link)

print("Entering --> "+str(link))
soup=BeautifulSoup(res.content,u'html.parser')
total_rec=soup.find('p',attrs={'class':'mod-LoadMore--text'}).findAll('span')[2].text.strip()
pages=int(total_rec)/50
rem=int(total_rec)%50
if rem!=0:
    pages+=1
pages=int(pages)
print('Total Pages ~~~> '+str(pages))
for i in range(pages):
        print('Current Page number ~~~~> '+str(i))
        link2=link+'/Seite-'+str(i)
        res = requests.get(link2,headers ={'User-Agent':agent})
        print('STatus',res.status_code)
        print('Page Link ~~~~~> '+str(link2))
        soup=BeautifulSoup(res.content,u'html.parser')
        divs=soup.findAll('article',attrs={'class':'mod-Treffer'})

        for div in divs:
            prof_dict = {}
            try:
                title=div.find('h2',attrs={'data-wipe-name':'Titel'}).text.strip()
                titles_arr.append(title)
            except:
                title='none'

            try:
                address=div.find('address',attrs={'class':'mod mod-AdresseKompakt'}).text.strip()
                address = address.replace('\t','').replace(' ','')
                # " ".join(address).strip()

                address = (address).encode('utf-8-sig')
                address = (address).decode('utf-8-sig')
                address = address.split('(')[0].strip()
                address=address.replace('/',' ')
                # print(sys.getfilesystemencoding())
                print(address)
            except:
                address='none'

            try:
                phone = address.split(')')[1].replace(')','').strip()
            except:
                phone = 'None'

            print('PPP',phone)
            try:
                website=div.find('a',attrs={'class':'contains-icon-homepage'})['href']
            except:
                website='none'

            try:
                email=div.find('a',attrs={'class':'contains-icon-email'})['href'].split('mailto:')[1].split('?')[0]
            except:
                email='none'

            # print(title)
            # print(address)
            # print(website)
            # print(email)

            print('Len-->',len(titles_arr))
            try:
                prof_dict['Name'] = title
                prof_dict['Address'] = address
                prof_dict['Website'] = website
                prof_dict['Email'] = email
                prof_dict['Phone'] = phone

                titles_arr.append(prof_dict)
                # print(prof_dict)

                print('--'*30)

                # print('Prof---',prof_dict)
                csvwriter.writerow(prof_dict)
            except:
                import sys
                print(sys.exc_info())
                pass


