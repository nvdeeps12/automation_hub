import pprint
from time import sleep
import os
import requests
import json
import xlwt
import urllib.parse as urlparse
session = requests.Session()
# import rootpath
import os
# rootpath = rootpath.detect()
wb=xlwt.Workbook()
sheet1=wb.add_sheet('Hotels')
sheet1.write(0,0,'name')
sheet1.write(0,1,'full_add')
sheet1.write(0,2,'street')
sheet1.write(0,3,'locality')
sheet1.write(0,4,'region')
sheet1.write(0,5,'countryName')
sheet1.write(0,6,'pin')
sheet1.write(0,7,'rating')
sheet1.write(0,8,'price')
sheet1.write(0,9,'total_price')
sheet1.write(0,10,'features')
sheet1.write(0,11,'Check-in')
sheet1.write(0,12,'Check-out')
sheet1.write(0,13,'Images')

row=0

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/javascript',
    'X-Requested-With': 'XMLHttpRequest',
    'Connection': 'keep-alive',
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INSTRUCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Install Libraries ->(requests, bs4, xlwt, re, time)
# Just Enter desired inputs down there
# try to make all vars in given formate :-)

## Needed Vars
destination='Las Vegas, Nevada, United States of America'
rooms=1
check_out='2020-04-08'
check_in='2020-04-06'
# room_0_adults=2
# room_0_children=0
breakpoint = 5
pn=1
c=session.get('https://www.hotels.com/search.do?resolved-location=CITY%3A1504033%3AUNKNOWN%3AUNKNOWN&destination-id=1504033&q-destination=Las%20Vegas,%20Nevada,%20United%20States%20of%20America&q-check-in=2020-04-23&q-check-out=2020-04-24&q-rooms=1&q-room-0-adults=2&q-room-0-children=0')
# pprint.pprint(session.cookies.get_dict())
cookies=session.cookies.get_dict()
# link='https://hotels.com/search/listings.json?pos=HCOM_US&locale=en_US&q-check-out='+str(check_out)+'&q-destination='+str(destination)+'&q-check-in='+str(check_in)+'&q-room-0-adults='+str(room_0_adults)+'&q-room-0-children='+str(room_0_children)+'&q-rooms='+str(rooms)+'&pn='+str(pn)
link='https://hotels.com/search/listings.json?pos=HCOM_US&locale=en_US&q-check-out='+str(check_out)+'&q-destination='+str(destination)+'&q-check-in='+str(check_in)+'&q-rooms='+str(rooms)+'&pn='+str(pn)
req=requests.get(link,headers=headers,cookies=cookies)

# req=requests.get('https://hotels.com/search/listings.json',params=params,headers=headers)
data=req.json()['data']['body']
results=data['searchResults']
# pprint.pprint(results['results'][11])
url=results['pagination']['nextPageUrl']
parsed = urlparse.urlparse(url)
next_pn=urlparse.parse_qs(parsed.query)['pn'][0]

pn=0
arr = []
while (next_pn!=1):
    pn+=1
    print('Len--------------->',len(arr))
    if len(arr)>breakpoint:
            break

    link='https://hotels.com/search/listings.json?pos=HCOM_US&locale=en_US&q-check-out='+str(check_out)+'&q-destination='+str(destination)+'&q-check-in='+str(check_in)+'&q-rooms='+str(rooms)+'&pn='+str(pn)

    res=requests.get(link,headers=headers,cookies=cookies)
    # data=req.json()['data']['body']
    data=res.json()['data']['body']
    results=data['searchResults']
    url=results['pagination']['nextPageUrl']
    parsed = urlparse.urlparse(url)
    next_pn=urlparse.parse_qs(parsed.query)['pn'][0]
    

    b=0
    
    for r in results['results']:
        b+=1
        if len(arr)>breakpoint:
            break

        print('Len-->',len(arr))
        name=r['name']
        print(name)
        address=r['address']
        try:
            full_add=address['streetAddress']+', '+address['extendedAddress']+', '+address['locality']+', '+address['region']+', '+address['countryName']+', '+address['postalCode']
            street=address['streetAddress']
            locality=address['locality']
            region=address['region']
            countryName=address['countryName']
            pin=address['postalCode']
        except:
            street='none'
            locality='none'
            region='none'
            countryName='none'
            pin='none'
        features=[]
        try:
            fe=r['popularAmenities']
            for f in fe:
                features.append(f['description']+", ")
        except:
            features='none'
        
        try:
            rating=r['guestReviews']['rating']
        except:
            rating='none'

        arr.append(rating)
        try:
            per=r['ratePlan']['price']['info']
        except:
            per=''
        
        try:
            price=r['ratePlan']['price']['current']+', '+per
        except:
            price='None'
        try:
            total_price=r['ratePlan']['price']['totalPricePerStay']
            total_price=str(total_price).split('<strong>')[1].split('</strong>')[0]
        except:
            total_price='None'
        
        try:
            imgs=[]
            imgg=r['optimizedImages']
            for im in imgg:
                imgs.append(str(im['overlayImagesUrls']['desktop'])+", ")
        except:
            imgs='none'
        row=row+1
        sheet1.write(row,0,name)
        sheet1.write(row,1,full_add)
        sheet1.write(row,2,street)
        sheet1.write(row,3,locality)
        sheet1.write(row,4,region)
        sheet1.write(row,5,countryName)
        sheet1.write(row,6,pin)
        sheet1.write(row,7,rating)
        sheet1.write(row,8,price)
        sheet1.write(row,9,total_price)
        sheet1.write(row,10,features)
        sheet1.write(row,11,check_in)
        sheet1.write(row,12,check_out)
        sheet1.write(row,13,imgs)
        wb.save('hotels.xls')
        print(name)
        os.system('cls')
        print('| Current Page - '+str(pn))
        print('| Next Page - '+str(next_pn))
        tt=data['searchResults']['totalCount']
        print('| Done '+str(int(row/tt*100))+'%')