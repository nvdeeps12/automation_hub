import requests,json

payload = {
"pid":"fa2e6ee0-70fb-4746-800f-4b202382cd2c",
"uid":"h94a09s7-301e-46d4-835a-d34537cb96d4",
"ip":"34.56.32.2",
"useragent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/53.36",
"keyword":"Sales",
"location":"New York",
"slots":1
}

headers = {"Content-type": "application/json"}
jsondata = json.dumps((payload))

# print(jsondata)

con = requests.post(url = 'https://api.jobadx.com/v1/exchange/auctions',data =jsondata,headers = headers)

job_data = con.content
print(job_data)
print('---'*66)