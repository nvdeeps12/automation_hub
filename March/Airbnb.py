import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
import re


options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver',options = options)
links_arr = []

wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Title')
sheet1.write(0,1,'Description')
sheet1.write(0,2,'Image_link')
sheet1.write(0,3,'Price')
sheet1.write(0,4,'Page_link')


## Needed Vars
location = ['New-York-United-States','New-Jersey--United-States']
checkin = '2020-03-17'
checkout = '2020-03-26'
adults = '1'
childrens = '0'
infants = '0'
guests = '1'

break_point = 2

print('Breakpoint Is '+str(break_point))

class Airb():

    def get_links(self):
        self.row = 0
        self.agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        for loc in location:

            page  = 'https://www.airbnb.co.in/s/'+loc+'/homes?query='+loc+'&checkin='+checkin+'&checkout='+checkout+'&adults='+adults+'&children='+childrens+'&infants='+infants+'&guests='+guests
            print('Running Loc',loc)
            print('Page--', page)
            result = requests.get(page,headers ={'User-Agent':self.agent})
            soup = BeautifulSoup(result.content, u'html.parser')
            total_pages = soup.find('div',attrs={'class':'_1h559tl'}).text.strip()
            total_pages = total_pages.split('of')[1].split('places')[0]
            total_pages = total_pages.replace('+','')
            total_pages = int(total_pages)
            remainder = total_pages%20
            total_pages = total_pages/20
            total_pages = int(total_pages)
            if remainder!=0:
                total_pages+=2

            print('Total Pages->',total_pages)

            # total_pages = 500
            anc_arr = []
            for p in range(total_pages):
                ## Break If Breakpoint Is Not None
                if len(anc_arr) > break_point:
                    break

                ###################################
                p = p*10
                print('Running Page->', p)
                pages = page+'&section_offset=1&items_offset='+str(p)
                print('Page-->',pages)
                cons = requests.get(pages)
                page_soup = BeautifulSoup(cons.content,u'html.parser')

                anchors = page_soup.findAll('div',attrs={'class':'_dx669kc'})
                for d in anchors:
                    try:
                        link = d.find('a')['href']
                        anc_arr.append(link)
                        if len(anc_arr)>break_point:
                            break

                        link = 'https://www.airbnb.co.in'+str(link)
                        title = d.find('a')['aria-label']
                        price = d.find('span', attrs = {'class':'_1p7iugi'}).text.strip()
                        description = d.findAll('div', attrs = {'class':'_1ulsev2'})
                        description_arr = []
                        for des in description:
                            try:
                                des = des.text.strip()
                                description_arr.append(des)
                            except:
                                pass

                        print('--'*70)
                        self.row+=1
                        self.get_hotelsinfo(link,title,price,description_arr)
                    except:
                        pass
                print('--'*100)


    def get_hotelsinfo(self,grplink,title,price,des_arr):
        print('Link-->',grplink)
        print('Title->',title)
        driver.get(grplink)
        import time
        time.sleep(5)
        page_soup = BeautifulSoup(driver.page_source, u'html.parser')

        images_arr = []
        try:
            images=page_soup.findAll('img',attrs={'class':'_uttz43'})
            for img in images:
                img = img['src']
                img = img+str('   ')
                images_arr.append(img)
        except:
            pass

        try:
            sheet1.write(self.row, 0, title)
            sheet1.write(self.row, 1, des_arr)
            sheet1.write(self.row, 2, images_arr)
            sheet1.write(self.row, 3, price)
            sheet1.write(self.row, 4, grplink)
            print('Done->',self.row)
        except:
            import sys
            print(sys.exc_info())
        wb.save('AirBnb.xls')


objs = Airb()
objs.get_links()