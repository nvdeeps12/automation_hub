CREATE TABLE hotels
(
  id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  description VARCHAR(550),
  images VARCHAR(255),
  price VARCHAR(255),
  page_link VARCHAR(255),
  created_at TIMESTAMP NOT NULL
);

CREATE TABLE flights
(
  id SERIAL PRIMARY KEY,
  airline_name TEXT DEFAULT NULL,
  departures TEXT DEFAULT NULL,
  duration TEXT DEFAULT NULL,
  arrival TEXT DEFAULT NULL,
  price TEXT DEFAULT NULL,
  origin TEXT DEFAULT NULL,
  Location TEXT DEFAULT NULL,
  images TEXT DEFAULT NULL,
  trip TEXT DEFAULT NULL,
  departure_date TEXT DEFAULT NULL,
  return_date TEXT DEFAULT NULL,
  created_at TIMESTAMP NOT NULL
);