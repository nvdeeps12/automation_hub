import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
import re
import time

options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
options.add_extension('F:/PycharmProjects/MainKam/fiver/March/Setup.crx')
chromedriver_path = 'F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe'

driver = webdriver.Chrome(executable_path=chromedriver_path,options = options)

driver.get('https://www.trvl.priceline.com')
time.sleep(30)



links_arr = []

wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Airline Name')
sheet1.write(0,1,'Departure Time')
sheet1.write(0,2,'Flight Duration')
sheet1.write(0,3,'Arrival_time')
sheet1.write(0,4,'Price')
sheet1.write(0,5,'Origin')
sheet1.write(0,6,'Ran Location')
sheet1.write(0,7,'Airline Image')
sheet1.write(0,8,'Trip Type')
sheet1.write(0,9,'Departure_date')
sheet1.write(0,10,'Return Date')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INSTRUCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Install Libraries ->(requests, bs4, selenium, xlwt, re, time)
# Just Enter desired inputs down there
# try to make all vars in given formate :-)

## Needed Vars
origin = 'NYC'
LocDep = ['LAX','LON']
departure_date = '2020-04-15'
adults = '1'
Type = 'Economy'

# Date Format is YYYY/MM/DD
return_date = '2020-04-25'
breakpoint = 3

if return_date!=None:
    trip = 'Roundtrip'
    print('Running For Roundtrip')
else:
    trip = 'Oneway'
    print('Running For Oneway')




def scroll(driver, timeout):
    scroll_pause_time = timeout
    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(scroll_pause_time)
        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            # If heights are the same it will exit the function
            break
        last_height = new_height




class PriceLine():

    def get_links(self):
        row = 0
        for loc in LocDep:
            
            if return_date!=None:
                page  = 'https://www.trvl.priceline.com/flights/results?departureFrom='+origin+'&departureFromType=0&arrivalTo='+loc+'&arrivalToType=1&departDate='+departure_date+'&returnDate='+return_date+'&adults='+adults+'&searchType=2&cabinType='+Type+str('&Currency=Usd')
            
            else:
                page  = 'https://www.trvl.priceline.com/flights/results?departureFrom='+origin+'&departureFromType=0&arrivalTo='+loc+'&arrivalToType=1&departDate='+departure_date+'&adults='+adults+'&searchType=1&cabinType='+Type+str('&Currency=Usd')
            
            import time
            page =str(page)
            print('Page--', page)
            print('Breakpoint is -->',breakpoint)
            # page = 'https://www.trvl.priceline.com/flights/results?departureFrom=NYC&departureFromType=0&arrivalTo=LAX&arrivalToType=1&departDate=2020-05-06&returnDate=2020-05-09&searchType=2&cabinType=Economy&adults=1'
            driver.get(page)
            time.sleep(8)
            ## Scrolled 
            
            scroll(driver,5)
    
            soup = BeautifulSoup(driver.page_source, u'html.parser')



            flight_cards = soup.findAll('li',attrs = {'class':'flight-card-container'})
            arr = []
            # airline_arr = []
            # dep_arr = []
            # duration_arr = []
            # arrival_arr = []
            # airmg_arr = []

            for fc in flight_cards:
                try:
                    airline_arr = []
                    airline_name = fc.find_all('span',attrs= {'data-component':'flight-card-displayName'})
                    for ar in airline_name:
                        ar = ar.text.strip()+str('  ')
                        airline_arr.append(ar)

                except:
                    airline_name = 'None'
                
                
                try:
                    dep_arr = []
                    departure_time = fc.find_all('div',attrs= {'data-component':'flight-card-departureTime'})
                    for dep in departure_time:
                        dep = dep.text.strip()+str('  ')
                        dep_arr.append(dep)
                except:
                    departure_time = 'None'

                try:
                    duration_arr = []
                    flight_duration = fc.find_all('div',attrs= {'data-component':'flight-card-duration'})
                    for fl in flight_duration:
                        fl = fl.text.strip()+str('  ')
                        duration_arr.append(fl)
                except:
                    flight_duration = 'None'

                try:
                    arrival_arr = []
                    arrival_time = fc.find_all('div',attrs= {'data-component':'flight-card-arrivalTime'})
                    for at in arrival_time:
                        at = at.text.strip()+str('  ')
                        arrival_arr.append(at)
  
                except:
                    airline_name = 'None'


                price_arr = []
                try:
                    price = fc.find('div',attrs= {'class':'FlightPrice__Amount__Optimized--OuterDisplay'}).text.strip()
                    price = price+str(' USD')
                    arr.append(price)

                    # Breakpoint 
                    if len(arr)>breakpoint:
                        break
                except:
                    price = 'None'


                try:
                    airmg_arr = []
                    air_img = fc.find_all('div',attrs= {'class':'FlightIcon__Airline'})
                    # air_img = price+str(' USD')
                    for ai in air_img:
                        ai = ai.find_next('img')['src']
                        ai = str(ai)+str('  ')
                        airmg_arr.append(ai)
                except:
                    air_img = 'None'

                print(airmg_arr)



                row=row+1
                sheet1.write(row,0,airline_arr)
                sheet1.write(row,1,dep_arr )
                sheet1.write(row,2,duration_arr )
                sheet1.write(row,3,arrival_arr )
                sheet1.write(row,4,price )
                sheet1.write(row,5,origin )
                sheet1.write(row,6,loc )
                sheet1.write(row,7,airmg_arr)
                sheet1.write(row,8,trip)
                sheet1.write(row,9,departure_date)
                if return_date!=None:
                    sheet1.write(row,10,return_date)

                wb.save('Priceline.xls')
                print('Done-->',row)
                




                print('--'*50)



# driver.quit()
    
objs = PriceLine()
objs.get_links()