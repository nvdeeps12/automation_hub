import requests
from bs4 import BeautifulSoup
import xlwt
from selenium import webdriver
import time

class KwAgent():
    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--disable-gpu')
        # options.add_argument('--headless')
        self.driver = webdriver.Chrome(executable_path='C:\Python_Projects\\fiver\Drivers\chromedriver.exe', options=options)
        self.wb = xlwt.Workbook()
        self.sheet1 = self.wb.add_sheet('Sheet')
        self.header = False
        self.row = 0

    def get_link(self):
        try:
            link = "https://www.kw.com/agent/search/"
            # states = ['TX','AZ']
            states = ['MO', 'SC', 'WI']

            for st in states:
                self.final_link = link + st
                print(self.final_link)
                self.driver.get(self.final_link)
                time.sleep(15)
                total_agent = self.driver.find_element_by_css_selector(".FindAgentRoute__totalCount > div").text
                total_agent = total_agent.split(" ")[1].replace(',','').strip()

                print('Total Agents->',total_agent)
                # no_of_scroll = int(int(total_agent)/50) - 1
                # rem = int(total_agent)%50
                #
                # if rem != 0:
                #     no_of_scroll += 1
                # print(no_of_scroll)
                no_of_scroll = 0

                for count in range(int(total_agent)):
                    
                    if count > 49 and count< 99:
                        no_of_scroll = 1
                    if count > 99 and count < 149:
                        no_of_scroll = 2
                    if count > 149 and count < 199:
                        no_of_scroll = 3
                    if count > 199 and count < 249:
                        no_of_scroll = 4
                    if count > 249 and count < 299:
                        no_of_scroll = 5
                    if count > 299 and count < 349:
                        no_of_scroll = 6
                    if count > 349 and count < 399:
                        no_of_scroll = 7
                    if count > 399 and count < 449:
                        no_of_scroll = 8
                    if count > 449 and count < 499:
                        no_of_scroll = 9
                    if count > 499 and count < 549:
                        no_of_scroll = 10
                    if count > 549 and count < 599:
                        no_of_scroll = 11
                    if count > 599 and count < 649:
                        no_of_scroll = 12
                    if count > 649 and count < 699:
                        no_of_scroll = 13
                    if count > 699 and count < 749:
                        no_of_scroll = 14

                    print('No Of Scrolls-->',no_of_scroll)

                    i = 0
                    while i < no_of_scroll:
                        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                        time.sleep(10)
                        i += 1
                        print('Scrolled->',i)
                    # time.sleep(10)
                    self.driver.find_element_by_xpath('//*[@id="root"]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div['+str(count+1)+']/div').click()
                    self.getAgentData()
                    
                    print("Getting Agent Data")

                    if int(total_agent) != (count+1):
                        print("I am Here")
                        self.driver.get(self.final_link)
                        time.sleep(10)
        except:
            import sys
            print(sys.exc_info())
        finally:
            self.driver.close()

    def getAgentData(self):
        time.sleep(10)
        soup = BeautifulSoup(self.driver.page_source, u'html.parser')
        fields = ['firstname','lastname','zipcode','license','city','state','mobile_number','office_number','email','url']
        data = []

        try:
            fullname = soup.find('div', attrs={'class': 'AgentHeader__name'}).text.strip()
            print('Running Agent->',fullname)

            if ',' in fullname:
                fullname = fullname.split(",")[0].strip()
        except:
            fullname = 'None'

        if fullname != 'None':
            try:
                firstname = fullname.split(" ")[:-1]
            except:
                firstname = 'None'
            data.append(firstname)

            try:
                lastname = fullname.split(" ")[-1]
            except:
                lastname = 'None'
            data.append(lastname)

        try:
            full_licence = soup.find('div', attrs={'class': 'AgentHeader__team'}).text.strip()
        except:
            full_licence = 'None'

        if full_licence != 'None':
            try:
                zipcode = int(full_licence.split("License")[0].strip().split(" ")[-1].strip())
            except:
                zipcode = 'None'
            data.append(zipcode)

            try:
                license = full_licence.split("License #: ")[1]
            except:
                license = 'None'
            data.append(license)

        try:
            fulllocation = soup.find('div', attrs={'class': 'AgentHeader__location'}).text.strip()
        except:
            fulllocation = 'None'

        if fulllocation != 'None':
            try:
                city = fulllocation.split(", ")[0]
            except:
                city = 'None'
            data.append(city)

            try:
                state = fulllocation.split(", ")[1]
            except:
                state = 'None'
            data.append(state)

        try:
            contacts = soup.findAll('div', attrs={'class': 'AgentContent__factBody'})
        except:
            contacts = 'None'

        if contacts != 'None':
            try:
                mobile_number = contacts[0].text.strip()
                mobile_number = str(mobile_number).split("Mobile:")[1].strip()
            except:
                mobile_number = 'None'
            data.append(mobile_number)

            try:
                office_number = contacts[1].text.strip()
                office_number = str(office_number).split("Office:")[1].strip()
            except:
                office_number ='None'
            data.append(office_number)

            try:
                email = contacts[2].text.strip()
            except:
                email = 'None'
            data.append(email)
        data.append(self.final_link)
        
        self.saveInXls(fields,data)


    def saveInXls(self,fields,data):
        if self.header != True:
            colno = 0
            for f in fields:
                self.sheet1.write(0,colno,f)
                colno += 1
            self.header = True

        col_no = 0
        self.row += 1
        print('Done->',self.row)
        for d in data:
            self.sheet1.write(self.row, col_no, d)
            self.wb.save('kwAgentMo.xls')
            col_no += 1

            


obj = KwAgent()
obj.get_link()