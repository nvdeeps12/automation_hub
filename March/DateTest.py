import datetime


Date_of_IPO = str(datetime.datetime.now().date())

Lockup_Exp_date = '03/25/2020'


try:
    d1 = datetime.datetime.strptime(str(Date_of_IPO), "%Y-%m-%d")
    d2 = datetime.datetime.strptime(Lockup_Exp_date, "%m/%d/%Y")
    no_of_Days_Until_Lock_up_Exp = abs((d2 - d1).days)
except:
    no_of_Days_Until_Lock_up_Exp=0


print(no_of_Days_Until_Lock_up_Exp)