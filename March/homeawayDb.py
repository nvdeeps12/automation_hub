import requests
from bs4 import BeautifulSoup
from selenium import webdriver
# import xlwt
import re
import time
import psycopg2 as sql
# from March.logHandler import LogHelper
from sc_settings import insert_sql, chromepath


options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--headless')
driver = webdriver.Chrome(
    executable_path=chromepath, options=options)
links_arr = []

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INSTRUCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Install Libraries ->(requests, bs4, selenium, xlwt, re, time)
# Just Enter desired inputs down there
# try to make all vars in given formate :-)


# Needed Vars
location = ['New-York-United-States', 'New-Jersey--United-States']
checkin = '2020-04-17'
checkout = '2020-04-26'
adults = '1'
childrens = '0'
pets = 'true'
guests = '1'
break_point = 2

print('Breakpoint Is '+str(break_point))


class Airb():

    

    def get_links(self):

        self.row = 0
        # self.log('Going to get detail from homeaway.py', 'info')
        self.agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        for loc in location:

            page = 'https://www.homeaway.com/search/keywords:'+loc+'/arrival:'+checkin+'/departure:' + \
                checkout+'?adultsCount='+adults+'&childrenCount='+childrens+'&petIncluded='+pets
            import time
            print('Page--', page)
            driver.get(page)
            time.sleep(3)
            soup = BeautifulSoup(driver.page_source, u'html.parser')
            # total_pages = soup.find('div',attrs={'class':'ResultsCount'}).text.strip()
            # total_pages = total_pages.split('of')[1].split('places')[0]
            # total_pages = total_pages.replace('+','')
            # total_pages = int(total_pages)
            # remainder = total_pages%20
            # total_pages = total_pages/20
            total_pages = int(50)
            # if remainder!=0:
            #     total_pages+=2

            print('Total Pages->', total_pages)
            anc_arr = []

            for p in range(total_pages):
                if p != 0:
                    print('Running Page->', p)
                    # self.log('Running_Page-'+str(p), 'error')

                    # Break If Breakpoint Is Not None
                    if len(anc_arr) > break_point:
                        break
                    pages = 'https://www.homeaway.com/search/keywords:'+loc+'/page:' + \
                        str(p)+'/arrival:'+checkin+'/departure:'+checkout+'?adultsCount=' + \
                        adults+'&childrenCount='+childrens+'&petIncluded='+pets
                    driver.get(pages)
                    import time
                    # driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                    y = 1000
                    for timer in range(0, 14):
                        driver.execute_script("window.scrollTo(0, "+str(y)+")")
                        y += 1000
                        time.sleep(3)
                    # myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'Pager__li--page')))
                    # time.sleep(3)
                    page_soup = BeautifulSoup(
                        driver.page_source, u'html.parser')
                    anchors = page_soup.findAll(
                        'a', attrs={'class': 'media-flex__content'})
                    if len(anchors) != 0:
                        for d in anchors:
                            try:
                                link = d.get('href')
                                anc_arr.append(link)
                                if len(anc_arr) > break_point:
                                    break

                                print('Anchors Len', len(anc_arr))

                                link = 'https://www.homeaway.com'+str(link)
                                print('--'*20)
                                print('Link-->', link)
                                driver.get(link)
                                import time
                                time.sleep(2)
                                page_soup = BeautifulSoup(
                                    driver.page_source, u'html.parser')
                                title = page_soup.find(
                                    'div', attrs={'class': 'property-headline u-freetext-fix'}).text.strip()
                                price = page_soup.find(
                                    'span', attrs={'class': 'rental-price__amount'}).text.strip()
                                description = page_soup.find(
                                    'div', attrs={'id': 'property-description'}).text.strip()
                                print(price)
                                print('Title->', title)
                                images_arr = []
                                driver.find_element_by_xpath(
                                    '//*[@id="photos"]/div/div[2]/button').click()
                                time.sleep(2)
                                img_soup = BeautifulSoup(
                                    driver.page_source, u'html.parser')
                                imgs = img_soup.findAll(
                                    'img', attrs={'class': 'carousel-slide__img'})
                                for img in imgs:
                                    img_link = img['src']+', '
                                    images_arr.append(img_link)
                                print(images_arr)
                                # try:
                                #     images=page_soup.findAll('img',attrs={'class':'_uttz43'})
                                #     for img in images:
                                #         img = img['src']
                                #         images_arr.append(img)
                                # except:
                                #     pass
                                self.row += 1

                                # sheet1.write(self.row, 0, title)
                                # sheet1.write(self.row, 1, description)
                                # sheet1.write(self.row, 2, images_arr)
                                # sheet1.write(self.row, 3, price)
                                # sheet1.write(self.row, 4, link)

                                sc_settings.insert_sql(
                                    title, description, images_arr, price, link)

                                print('Done->', self.row)

                                # wb.save('Homeaway.xls')

                                print('--'*100)
                            except:
                                import sys
                                print(sys.exc_info())
                                # self.log(sys.exc_info(), 'error')

                                continue
                    else:
                        print('Nothing else Dude....')
                        break


objs = Airb()
objs.get_links()
