import json, base64
from March import settings
import os



def upload_talentinc():
        email = 'gcrew.nvdeepst@gmail.com'
        city = 'New York'
        state = 'Albama'
        expected_salary = '5544'
        resume = ''
        resume_uuid = 'iii'
        resume_extn = '.jpg'
        payload = {
            'partner_key': settings.API_KEY,
            'secret_key': settings.API_SECRET,
            'email': email,
            'country': str(settings.TALENTINC_COUNTRY).upper(),
            'tags': {
                'city': city,
                'state': state,
                'expected_salary': expected_salary
            }
        }
        requestfrom = 'landingpage'

        if requestfrom == 'landingpage':
            payload.update({'utm_source':'career_services'})

        elif requestfrom == 'alertsystem':
            payload.update({'utm_source':'1clicksubmit'})

        encoded_resume = resume.encode()
        root_path = os.path.dirname(os.path.abspath(__file__))
        file_path = root_path + "/user_resumes"
        file = file_path + '/' + resume_uuid + resume_extn
        url = "https://api.talentinc.com/v1/resume"
        with open(file, "wb") as fh:
            fh.write(base64.decodebytes(encoded_resume))
        file_dict = {'resume_file': open(file, 'rb')}
        response = requests.post(url, params=payload, files=file_dict)
        status_code = response.status_code
        status_res = response.content.decode()
        response_msg = json.loads(status_res)
        response_message = response_msg['status']['message']
        status_code_msg = response_msg['status']['code']
        return status_code,file,status_code_msg,response_message