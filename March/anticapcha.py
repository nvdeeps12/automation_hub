import requests
import datetime
from bs4 import BeautifulSoup

def scrape_url(url_scrape):
    try:
        print('Requested On-->',datetime.datetime.now())
        if __name__ == '__main__':
            API_KEY = '0739a8f0ec5d03452a0ec570e62ac875'
            URL_TO_SCRAPE = url_scrape
        payload = {'api_key': API_KEY, 'url': URL_TO_SCRAPE}
        r = requests.get('http://api.scraperapi.com', params=payload, timeout=60)
        print(r.status_code)
        soup = BeautifulSoup(r.content,u'html.parser')
        print('Requested Done-->',datetime.datetime.now())
        print('--'*50)
        return soup
    except:
        return None

soup = scrape_url()
print(soup)