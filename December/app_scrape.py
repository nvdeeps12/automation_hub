import csv
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


options = webdriver.FirefoxOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')
driver = webdriver.Firefox(executable_path='../Drivers/geckodriver',options =options )
data = driver.get('https://play.google.com/store/apps/details?id=com.delta.mobile.android&hl=en_US')
driver.find_element_by_xpath("//span[text()='Read All Reviews']").click()

import time
time.sleep(200)

##Scroll Down
# SCROLL_PAUSE_TIME = 2
count = 0
for i in range(100):
# # Scroll down to bottom

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    count = count+1
    print(count)
    if count==20:
        break
    print('Scroll Times-->',count)
# # Wait to load page
#     import time
#     time.sleep(SCROLL_PAUSE_TIME)
#
# # Calculate new scroll height and compare with last scroll height
#
#     new_height = driver.execute_script("return document.body.scrollHeight")
#     last_height = new_height
#     if new_height == last_height:
#         break






import time
time.sleep(20)
soup = BeautifulSoup(driver.page_source, u"html.parser")

review_div = soup.findAll('div',attrs={'class':'d15Mdf bAhLNe'})

for rev in review_div:
    try:
        author =rev.find('span',attrs={'class':'X43Kjb'}).text.strip()
    except:
        author = 'None'

    print('Auth-->',author)

    try:
        review_date = rev.find('span',attrs={'class':'p2TkOb'}).text.strip()
    except:
        review_date = 'None'
    print('Date->',review_date)
    try:
        review =rev.find('div',attrs={'class':'UD7Dzf'}).text.strip()
    except:
        review = 'None'
    print('REview-->',review)
    try:
        rating = rev.find('span',attrs={'class':'X43Kjb'}).find_next('div').find('div',attrs={'role':'img'})['aria-label']
    except:
        rating = 'None'

    try:
        driver.find_element_by_xpath("//span[text()='Show More']").click()
    except:
        pass

    print('--'*50)

    try:
        with open('Csv_Data.csv', 'a+') as cfile:
            FIELDS = ['Author', 'Dated', 'Review', 'Rating']
            writer = csv.DictWriter(cfile,fieldnames=FIELDS)
            writer.writerow({'Author':author,'Dated':review_date,'Review':review,'Rating':rating})
    except:
        continue



