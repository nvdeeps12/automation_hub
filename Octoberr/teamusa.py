
import requests
from bs4 import BeautifulSoup
import sys
from selenium import webdriver
import re
import xlwt
options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver',options=options)

arr = ['Alpine Skiing', 'American Canoe', 'Biathlon', 'Bobsled', 'Cross-Country', 'Curling', 'Diving',
                   'Field Hockey', 'field hockey', 'Figure Skating', 'Freestyle Skiing', 'Goalball', 'Ice Hockey',
                   'Long', 'Nordic', 'Roller', 'Short', 'Skeleton', 'Ski Jumping', 'Sled Hockey', 'Snowboarding',
                   'Soccer', 'Speed', 'Squash', 'Swimming', 'Track and Field', 'Water', 'Wheelchair', 'bobsled',
                   'curling', 'luge', 'roller']


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

wb = xlwt.Workbook()

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Name')
sheet1.write(0, 1, 'Sports')
sheet1.write(0, 2, 'Height')
sheet1.write(0, 3, 'DOB')
sheet1.write(0, 4, 'HomeTown')
sheet1.write(0, 5, 'Instagram Link')
sheet1.write(0, 6, 'Followers')



class get_athletes():

    def get_anchors(self):
        self.row = 0
        for p in range(715):
            try:
                # if p<510:
                if p<550:
                    continue
                page = 'https://www.teamusa.org/athletes?pg='+str(p)
                print('Page-->>>',page)
                driver.get(page)
                soup = BeautifulSoup(driver.page_source, u"html.parser")

                try:
                    ul = soup.find('ul',attrs={'class':'thumb-row athletes'}).findAll('li')
                    # print(ul)
                    for a in ul:
                        try:
                            name = a.find('h4').text.strip()
                        except:
                            name = 'None'

                        try:
                            anc = a.find('a')['href']
                            print('Anch-->>>',anc)

                            self.search_google(name,anc)
                        except:
                            continue
                except:
                    continue

            except:
                continue


    def search_google(self,name,pro_link):
        name = 'https://www.google.com/search?q='+str(name) +'+instagram'
        google_link = str(name)
        driver.get(google_link)
        soup = BeautifulSoup(driver.page_source, u"html.parser")
        # print(soup)
        div = soup.find('div',attrs={'class':'bkWMgd'})
        anch = div.find('a')['href']
        print('Instagram-->>>',anch)


        self.get_instagram(anch,pro_link)


    def get_instagram(self,anch,pro_link):

        profile_link=pro_link
        driver.get(anch)
        print('Cunter-->>>',self.row)
        if self.row==0:
            import time
            time.sleep(30)

        # print('Instagram Status Code-->',ins.status_code)
        soup = BeautifulSoup(driver.page_source, u"html.parser")
        meta = soup.find('meta',attrs={'name':'description'})['content']
        followers = meta.split('Followers')[0]
        if 'k' in followers:
            followers = followers.replace('k','').replace('.','00')
        elif 'm' in followers:
            followers = followers.replace('m','').replace('.','000')
        followers = int(followers)
        print('Followers-->',followers)
        print('====='*100)

        if followers>9000:
            print('Followers Valid-->>')
            print(followers)
            self.get_profile(profile_link,anch,followers)


        # print(anch)

    def get_profile(self, profile_url,insta,follower):
        self.row = self.row + 1
        print(profile_url)
        # profile_res=requests.get('https://www.teamusa.org/usa-softball/athletes/Monica-Abbott',headers)
        driver.get(profile_url)
        # print(profile_res.status_code)
        profile_soup = BeautifulSoup(driver.page_source, u"html.parser")

        try:
            if "Name:" in profile_soup:
                n = profile_soup.find('span', text=re.compile('Name:')).find_previous('div').text.strip()
                name = n.split('Name:')[1]
            else:
                name=profile_soup.find('div',attrs={'class':'athlete-name'}).text.strip()
        except:

            name = "None"
            # print("none")
        print('Name-->>',name)
        try:

            try:
                s = profile_soup.find('span', text=re.compile('Sport:')).find_previous('div').text.strip()
                sports = s.split('Sport:')[1].strip()
            except:
                sports = profile_url.split('/usa-')[1].split('/')[0].strip()
        except:
            sports = "none"

        if sports in arr:
            sports = 'Game_Not_Valid'


        print('Sport----->>>>>>',sports)
        try:
            h = profile_soup.find(text=re.compile('Height:')).find_previous('div').text.strip()
            Height = h.split('Height:')[1]
            if len(Height)>6:
                h = profile_soup.find(text=re.compile('Height:')).find_previous('p').text.strip()
                Height = h.split('Height:')[1]
            else:
                h = profile_soup.find('span',text=re.compile('Height:')).find_previous('div').text.strip()
                Height = h.split('Height:')[1]
        except:
            Height = "none"
            # print("none")
        # print(Height)
        try:
            if "DOB:" in profile_soup:
                d = profile_soup.find(text=re.compile('DOB:')).find_previous('div').text.strip()
                DOB = d.split('DOB:')[1]
            else:
                d = profile_soup.find('strong', text=re.compile('Date Of Birth:')).find_previous('p').text.strip()
                DOB = d.split('Date Of Birth:')[1]
        except:
            print(sys.exc_info())
            DOB = "none"
            # print("none")
        # print(DOB)
        try:
            if "Hometown:" in profile_soup:
                hm = profile_soup.find('span', text=re.compile('Hometown:')).find_previous('div').text.strip()
                Hometown = hm.split('Hometown:')[1]
            else:
                hm = profile_soup.find('strong', text=re.compile('Residence:')).find_previous('p').text.strip()
                Hometown = hm.split('Residence:')[1]

        except:
            # print(sys.exc_info())
            Hometown = "none"
            # print("none")
        # print(Hometown)

        print('---' * 100)
        try:
            sheet1.write(self.row, 0,str(name))
            sheet1.write(self.row, 1,str(sports))
            sheet1.write(self.row, 2, str(Height))
            sheet1.write(self.row, 3, str(DOB))
            sheet1.write(self.row, 4,str(Hometown))
            sheet1.write(self.row, 5, str(insta))
            sheet1.write(self.row, 6, str(follower))
            wb.save('TeamUsa_Datap510.xls')
            wb.save('outputs/TeamUsa_Datap555.xls')

            print('---'*100)
            print("Done " + str(self.row))

        except:
            pass





obj = get_athletes()
obj.get_anchors()