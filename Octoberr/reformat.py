import csv

# csv file name
filename = "F:\Projects\Fiver\Octoberr\Outputs\Inputs\Bad.csv"

# initializing the titles and rows list
file = open(filename,newline='')

reader = csv.reader(file)
header = next(reader)
data = [row for row in reader]

# print(header)

for i in range(len(data)):
    sales_record_number = data[i][0]
    if sales_record_number=='' or len(sales_record_number)==0 or 'Se' in sales_record_number:
        continue
    order_no = data[i][1]
    if 'rec' in order_no:
        continue
    buyer_username = data[i][2]
    Buyer_name = data[i][3]
    buyer_email = data[i][4]
    buyer_note = data[i][5]
    buyer_ad1 = data[i][6]
    buyer_ad2 = data[i][7]
    buyer_city = data[i][8]
    buyer_county = data[i][9]
    buyer_post = data[i][10]
    buyer_country = data[i][11]
    postname = data[i][12]
    post_phone = data[i][13]
    post1 = data[i][14]
    post2 = data[i][15]
    post_city = data[i][16]
    post_county = data[i][17]
    post_postalcode = data[i][18]
    post_country = data[i][19]
    item_no = data[i][20]
    item_title = data[i][21]
    cust_label = data[i][22]
    sold_via_link = data[i][23]
    quantity = data[i][24]
    sold_for = data[i][25]
    postage_and_pack = data[i][26]
    inc_vat = data[i][27]
    seller_coll_tax = data[i][28]
    ebay_tax = data[i][29]
    total_price = data[i][30]
    pay_method = data[i][31]
    sale_date = data[i][32]
    payed_date = data[i][33]
    post_date = data[i][34]
    min_est_del = data[i][35]
    max_est = data[i][36]
    dis_date = data[i][37]
    feed_left = data[i][38]
    feed_recieved = data[i][39]
    my_item_note = data[i][40]
    paypal_id = data[i][41]
    del_service = data[i][42]
    track_no = data[i][43]
    trans_id = data[i][44]
    var_detail = data[i][45]
    global_prog = data[i][46]
    global_ref = data[i][47]
    click_col = data[i][48]
    click_col_ref_no = data[i][49]
    ebay_plus = data[i][50]
    print(ebay_plus)

    with open('F:\Projects\Fiver\Octoberr\Outputs\Reformatted.csv', mode='a+') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([sales_record_number, order_no, buyer_username, Buyer_name, buyer_email, buyer_note,
                         buyer_ad1,buyer_ad2,buyer_city,buyer_county,buyer_post,
                         buyer_country,postname,post_phone,post1,post_city,post_county,
                         post_postalcode,post_country,item_no,item_title,cust_label,sold_via_link,quantity,
                         sold_for,postage_and_pack,inc_vat,seller_coll_tax,ebay_tax,total_price,pay_method,sale_date,
                         payed_date,post_date,min_est_del,max_est,dis_date,feed_left,feed_recieved,my_item_note,
                         paypal_id,del_service,track_no,trans_id,var_detail,global_prog,global_ref,click_col,click_col_ref_no,ebay_plus])
