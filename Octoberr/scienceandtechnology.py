import requests
from bs4 import BeautifulSoup
import re
import sys


game_list = []
def get_games():
    agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
    html = requests.get('http://60fps.live/league/redditnflstreams/',headers={'user-agent':agent})
    soup = BeautifulSoup(html.content,'html.parser')
    a = soup.find_all('div',class_={'rapidwp-posts-content'})
    for data in a:
        title = data.find('h3',class_={'rapidwp-fp05-post-title'}).text
        title = title.encode('ascii','ignore')
        title = title.decode('utf-8','ignore')
        game_list.append({'title':title})

    return game_list


stream = []

def get_stream(game):
    agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
    html = requests.get('http://60fps.live/league/redditnflstreams/',headers={'user-agent':agent})
    soup = BeautifulSoup(html.content,'html.parser')
    a = soup.find_all('div',class_={'rapidwp-posts-content'})
    for data in a:
        title = data.find('h3',class_={'rapidwp-fp05-post-title'}).text
        title = title.encode('ascii','ignore')
        title = title.decode('utf-8','ignore')
        if game in title:
            url = data['href']
            html = requests.get(url,headers={'user-agent':agent}).content
            soup = BeautifulSoup(html,'html.parser')
            master = requests.get(url,headers={'referer':url}).content
            soup = BeautifulSoup(master,'html.parser')
            m3u8 = re.compile('var hanturl = {source:"(.+?)"',re.DOTALL).findall(str(soup.prettify))
            m3u8 = m3u8[0]
            stream.append({"stream":m3u8})

            print(stream)
            return stream

get_stream('http://60fps.live/new-england-patriots-vs-baltimore-ravens/')