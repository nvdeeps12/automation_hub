import requests
from bs4 import BeautifulSoup
import re

game_list = []
stream_arr = []
nhlToken = requests.get('https://www.revelationmedia.tk/tokens/nhl.txt').content
nhlToken = nhlToken.decode()
nhl_auth = "|Cookie=Authorization=" + nhlToken

class GetGamescls():



    def pass_schedule(self):
        arr = ['http://www.volokit.com/all-games/schedule/nhl.php', 'http://www.volokit.com/all-games/schedule/nba.php',
               'http://www.volokit.com/all-games/schedule/nfl.php']

        for p in arr:
            sch_link = p
            print('Schdule_link-->',sch_link)
            self.get_games(sch_link)
    def get_games(self,sch_link):
        agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        html = requests.get(sch_link, headers={'user-agent': agent}).content
        soup = BeautifulSoup(html, 'html.parser')
        anchor = soup.find_all('a', attrs={'class': "url hidden-xs-down summary"})
        for a in anchor:
            link = a['href']
            title = a.text.strip()
            # print('Schedule Links-->>>',link)
            self.get_stream(link,title)
            # title = link.split('/')[-2]
            # title = title.upper()
            # game_list.append({'title': title.encode('ascii', 'ignore'), 'link': link.encode('ascii', 'ignore')})
        # return game_list
    def get_stream(self,link,title):
        agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        html = requests.get(link, headers={'user-agent': agent}).content
        soup = BeautifulSoup(html, 'html.parser')
        buttons = soup.findAll('button')
        for b in buttons:
            try:
                btn_link = b['onclick'].split('src=')[1].replace("'",'')
            except:
                btn_link = 'None'

            if btn_link=='None':
                continue

            full_link = 'www.volokit.com'+str(btn_link)
            # print(full_link)

            if '.php' not in full_link:
                continue

            full_link = str(full_link)
            full_link = full_link.replace('www.volokit.com/','http://www.volokit.com/')
            print(full_link)
            self.get_frames(full_link,title,link)



    def get_frames(self,full_link,title,link):
        print('Full__>>',full_link)
        agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        html = requests.get(full_link, headers={'user-agent': agent}).content
        soup = BeautifulSoup(html, 'html.parser')
        try:
            frame = soup.find('iframe', {'id': 'volokit-feed'})
            frame = frame['src']
            frame = 'http://www.volokit.com' + frame
            master = requests.get(frame, headers={'user-agent': agent}).content
            soup = BeautifulSoup(master, 'html.parser')
        except:
            pass


        m3u8 = re.compile('var data = {source:"(.+?)"', re.DOTALL).findall(str(soup.prettify))

        try:
            m3u8 = m3u8[0]
        except:
            m3u8 = 'None'

        stream_dict = {'Game_Link':link,'Game_Title':title,'Stream_Link':m3u8}
        stream_arr.append(stream_dict)

        print('Stream Arr-->>',stream_arr)
        print('--'*100)

        # print('--'*100)



obj_gm = GetGamescls()
obj_gm.pass_schedule()



# print(get_stream("http://www.volokit.com/volostream/nhl-games/vancouver-new-jersey/"))
# print(get_games())
