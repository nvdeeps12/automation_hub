import mysql.connector
from mysql.connector import Error
import sys
try:
    connection = mysql.connector.connect(host='1.chduxuboansz.us-east-1.rds.amazonaws.com',
                                         database='database-1',
                                         user='admin',
                                         password='mypassword',port= 3306)

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("Your connected to database: ", record)

except Error as e:
    print(sys.exc_info())
    print("Error while connecting to MySQL", e)
