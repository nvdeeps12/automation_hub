import requests
from bs4 import BeautifulSoup
import xlwt
import csv

import re

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')


# sheet2 = wb.add_sheet('sheet2')
sheet1.write(0, 0, 'Title')
sheet1.write(0, 1, 'Status')
sheet1.write(0, 2, 'Address')
sheet1.write(0, 3, 'Phone')
sheet1.write(0, 4, 'Item Title')
sheet1.write(0, 5, 'Item Description')
sheet1.write(0, 6, 'Item Price')

referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }




class memu_class():
    def pass_pages(self):
        self.row = 0
        self.row+=1
        running_row = 0
        for p in range(82):
            running_row+=1
            page_url = 'https://menupages.com/restaurants/ny-new-york/'+str(p)
            print('Running Page->',page_url)
            print('Running Row->',running_row)
            print('--' * 100)
            self.get_page_soup(page_url)


    def get_page_soup(self,page):
        page_cont = requests.get(page,headers = headers)
        soup = BeautifulSoup(page_cont.content,"html.parser")
        all_res = soup.findAll('li',attrs={'class':'restaurants__item'})
        for p in all_res:
            anchor = p.find('a')['href']
            full_link = 'https://menupages.com'+str(anchor)
            self.get_details(full_link)



    def get_details(self,link):
        res = requests.get(link,headers= headers)
        res_soup = BeautifulSoup(res.content,"html.parser")
        res_title = res_soup.find('h1',attrs={'class':'header__restaurant-name'}).text.strip()
        res_status = res_soup.find('span',attrs={'class':'header__primary-cuisines'}).text.strip()
        address = res_soup.find('span',attrs={'class':'address-display'}).text.strip()
        phone = res_soup.find('span',attrs={'class':'restaurant-phone'}).text.strip()
        # print(res_title)


        menus = res_soup.findAll('div',attrs={'class':'menu-item__information'})


        for m in menus:
            self.row =self.row+ 1
            try:
                menu_title = m.find('h3',attrs ={'class':'menu-item__title'}).text.strip()
            except:
                menu_title = 'None'
            try:
                menu_des = m.find('p',attrs ={'class':'menu-item__description'}).text.encode('utf-8')
            except:
                menu_des = 'None'
            try:
                price = m.find('span',attrs ={'class':'menu-item__price'}).text.split(' ')[0]
            except:
                price = 'None'


            try:
                res_title = (re.sub(r"[^a-zA-Z0-9]+", ' ', res_title))
            except:
                continue

            try:
                res_status = (re.sub(r"[^a-zA-Z0-9]+", ' ', res_status))
            except:
                continue

            try:
                phone = (re.sub(r"[^a-zA-Z0-9]+", ' ', phone))
            except:
                continue

            try:
                menu_title = (re.sub(r"[^a-zA-Z0-9]+", ' ', menu_title))
            except:
                continue

            try:
                # menu_des = (re.sub(r"[^a-zA-Z0-9]+", ' ', menu_des))

                menu_des = str(menu_des).replace("b'",'').replace("'",'')
            except:
                continue

            try:
                phone = (re.sub(r"[^a-zA-Z0-9]+", ' ', phone))
            except:
                continue

            print(res_title)
            print(res_status)
            print(address)
            print(phone)
            print(menu_title)
            print(menu_des)
            print(price)


            print('--'*100)


            # sheet1.write(self.row, 0, str(res_title))
            # sheet1.write(self.row, 1, str(res_status))
            # sheet1.write(self.row, 2, str(address))
            # sheet1.write(self.row, 3, str(phone))
            # sheet1.write(self.row, 4, str(menu_title))
            # sheet1.write(self.row, 5, str(menu_des))
            # sheet1.write(self.row, 6, str(price))
        # with open('MemuFile.csv', mode='a+') as csv_file:
        #     writer = csv.writer(csv_file, )
        #     fieldnames = ['Title', 'Status', 'Address', 'Phone', 'Item Title', 'Item Description', 'Price']
        #     writer.writerow(fieldnames)





            with open('MemuFile.csv', mode='a+') as csv_file:
                writer = csv.writer(csv_file)
                writer.writerow([res_title, res_status,  address, phone, menu_title,  menu_des, price])

                # wb.save('memu_main_new.xls')


            print('Done',self.row)







obj = memu_class()
obj.pass_pages()