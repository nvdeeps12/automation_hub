import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
import re
import sys
options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='E:\chromedriver', options=options)

url="https://www.teamusa.org/usa-softball/athletes/Monica-Abbott"
referer='https://google.co.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }

wb = xlwt.Workbook()

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'Name')
sheet1.write(0, 1, 'Sports')
sheet1.write(0, 2, 'Height')
sheet1.write(0, 3, 'DOB')
sheet1.write(0, 4, 'HomeTown')
sheet1.write(0, 5, 'Instagram Link')

def get_profile(self,profile_url):
    print(profile_url)
    # profile_res=requests.get('https://www.teamusa.org/usa-softball/athletes/Monica-Abbott',headers)
    driver.get(url)
    # print(profile_res.status_code)
    profile_soup=BeautifulSoup(driver.page_source,u"html.parser")
    div=profile_soup.find('div',attrs={'id':'athlete-profile'}).text.strip()
    try:
        # name=div.find('span',text=re.compile('Name:'))
        # n=div.find('span',text=re.compile("Name"))
        n =profile_soup.find('span', text=re.compile('Name:')).find_previous('div').text.strip()
        name=n.split('Name:')[1]
        print(name)
    except:
        # print(sys.exc_info())
        name="none"
        # print("none")
    try:
        s = profile_soup.find('span', text=re.compile('Sport:')).find_previous('div').text.strip()
        sports = s.split('Sport:')[1]
        print(sports)
    except:
        # print(sys.exc_info())
        sports= "none"
        # print("none")
    try:
        h = profile_soup.find('span', text=re.compile('Height:')).find_previous('div').text.strip()
        Height = h.split('Height:')[1]
        print(Height)
    except:
        # print(sys.exc_info())
        Height= "none"
        # print("none")
    try:
        d = profile_soup.find('span', text=re.compile('DOB:')).find_previous('div').text.strip()
        DOB = d.split('DOB:')[1]
        print(DOB)
    except:
        # print(sys.exc_info())
        DOB= "none"
        # print("none")
    try:
        hm = profile_soup.find('span', text=re.compile('Hometown:')).find_previous('div').text.strip()
        Hometown = hm.split('Hometown:')[1]
        print(Hometown)
    except:
        # print(sys.exc_info())
        Hometown= "none"
        # print("none")
    self.row=self.row+1
    sheet1.write(self.row, 0, 'Name')
    sheet1.write(self.row, 1, 'Sports')
    sheet1.write(self.row, 2, 'Height')
    sheet1.write(self.row, 3, 'DOB')
    sheet1.write(self.row, 4, 'HomeTown')
    sheet1.write(self.row, 5, 'Instagram Link')
    wb.save('outputs/TeamUsa.xls')
    print("Done "+str(self.row))

get_profile(url)