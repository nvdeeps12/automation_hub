from bs4 import BeautifulSoup
import sys
import xlwt
import requests

prolink1='https://www.weichert.com/search/offices/?site=wdc&office=50-M21'
prolink2=prolink1.split('offices/')[0]+'offices/office.aspx'+prolink1.split('offices/')[1]+'&nx='


def get_profile(prolink):
    try:
        res=requests.get(prolink)
        profilesoup=BeautifulSoup(res.content,u'html.parser')
        # agents1=profilesoup.find('div',attrs={'id':'ctl00_BodyContent_ucOfficeManager_ManagerSection'})
        # agents2=profilesoup.find('div',attrs={'id':'ctl00_BodyContent_ucOfficeManager_CoManagerSection'})
        # try:
        #     email = agents2.find('div', attrs={'class': 'agentEmail'}).text.strip()
        # except:
        #     email='None'
        try:
            viewlink=profilesoup.findAll('div',attrs={'class':'agentLinks'})
            for v in viewlink:
                vlink="https://www.weichert.com"+v.find('a').get('href')
                print(vlink)
                try:
                    res2=requests.get(vlink)
                    print(res2.status_code)
                    prosoup2=BeautifulSoup(res2.content,u'html.parser')
                    try:
                        name=prosoup2.find('span',attrs={'id':'ctl00_BodyContent_lblAgentName'}).text.strip()
                    except:
                        name='None'
                    try:
                        officename=prosoup2.find('span',attrs={'id':'ctl00_BodyContent_lblOfficeName'}).text.strip()
                    except:
                        officename='None'
                    try:
                        address=prosoup2.find('span',attrs={'id':'ctl00_BodyContent_lblAddress'}).text.strip()
                    except:
                        address='None'
                    try:
                        sate=prosoup2.find('span',attrs={'id':'ctl00_BodyContent_lblCityStateZip'}).text.strip()
                    except:
                        sate='None'
                    try:
                        em = prosoup2.find('span', attrs={'id': 'ctl00_BodyContent_lblEmail'}).text.strip()
                    except:
                        em = 'None'
                    try:
                        ph=prosoup2.find('a',attrs={'id':'ctl00_BodyContent_lblPhoneNum'}).text.strip()
                        agphone='None'
                        officeph='None'
                        if "Phone:" in ph:
                            agphone=ph.split('Phone:')[1]
                        elif 'Office:' in ph:
                            officeph=ph.split('Office:')[1]
                    except:
                        officeph='None'
                        agphone = 'None'
                    try:
                        fax=prosoup2.find('span',attrs={'id':'ctl00_BodyContent_lblFax'}).text.strip()
                        fax=fax.split('Fax:')[1]
                    except:
                        fax='None'
                    print("Name -"+name)
                    # print("Email - "+ email)
                    print("Address - "+ address)
                    print("Office Phone - "+ officeph)
                    print("Agent Phone - "+ agphone)
                    print("Sate Zip - "+ sate)
                    print("Fax - "+ fax)
                    print("Office Name - "+ officename)

                except:
                    print(sys.exc_info())
        except:
            viewlink='None'

        # try:
        #     name=profilesoup.find('div',attrs={'class':'agentName'}).text.strip()
        #     fname=name.split(' ')[0]
        #     lname=name.split(' ')[1]
        # except:
        #     fname='None'
        #     lname='None'
        # print(fname+"--"+lname)
        # print("Email - "+ email)
        # print("Address - "+ address)
        # print("Office Phone - "+ officephone)
        # print("Agent Phone - "+ agphone)
        # print("Fax - "+ fax)


    except:
        print(sys.exc_info())
        pass

get_profile(prolink2)