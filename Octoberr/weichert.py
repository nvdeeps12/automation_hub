import time
# from selenium import webdriver
from bs4 import BeautifulSoup
import sys
import xlwt
import requests
import re
# from work.scripts.employzone_mail import createEmailContent,sendEmail
from datetime import datetime
import xlrd
# from logs.LogHandler import Mylogger

# log_obj = Mylogger()

# rd = xlrd.open_workbook('D:\PycharmProjects\Files\\Urls\Huston.xlsx')
# sheet = rd.sheet_by_index(0)



# options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# options.add_argument('--headless')
# options.add_argument('--lang=fr-FR')

# driver = webdriver.Chrome(executable_path='D:\chromedriver', chrome_options=options)


names_arr = []

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('sheet1')
# sheet2 = wb.add_sheet('sheet2')
sheet1.write(0, 0, 'First_name')
sheet1.write(0, 1, 'Last_name')
sheet1.write(0, 2, 'Street')
sheet1.write(0, 3, 'Locality')
sheet1.write(0, 4, 'Region')
sheet1.write(0, 5, 'Postal')
sheet1.write(0, 6, 'Mobile')
sheet1.write(0, 7, 'Office')
sheet1.write(0, 8, 'fax')
sheet1.write(0, 9, 'Office Name')
sheet1.write(0, 10, 'Source')
referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }


class ascp():
    def pass_links(self):
        self.row = 0
        try:
            rd=xlrd.open_workbook('state.xls')
            sheet = rd.sheet_by_index(0)
            # state_link = 'https://www.weichert.com/offices/'
            # # print(state_link)
            # page =requests.get(state_link,headers = headers)
            # soup = BeautifulSoup(page.content, u"html.parser")
            # total_pages = soup.find('div',attrs={'id':'browsebystate'}).find('ul').findAll('li')
            for i in range(sheet.nrows):
                # href = state.find('a')['href']
                # print(href)
                # state_links = 'https://www.weichert.com'+str(href)
                state_links= sheet.cell_value(i, 0)
                print('State Links-------',state_links)
                self.supply_pages(state_links)
        except:
                total_pages = 0
                pass

    def supply_pages(self,state_links):
        page = requests.get(state_links,headers=headers)
        about_soup = BeautifulSoup(page.content, u"html.parser")
        try:
            office_links = about_soup.findAll('ul',attrs={'class':'list-unstyled'})[1].findAll('a')
            for i in office_links:
                href = i['href']
                if href=='':
                    continue
                else:
                    page_link = 'https://www.weichert.com'+str(href)
                    find_as_soup = requests.get(page_link,headers = headers)
                    new_soup = BeautifulSoup(find_as_soup.content, u'html.parser')

                    asociates = new_soup.find('ul',attrs={'id':'officeHeaderNavigation'}).findAll('li')[1].find('a')['href']

                    asociates_link = 'https://www.weichert.com'+str(asociates)
                    # print('A----',asociates_link)

                    try:
                        total_pages = requests.get(asociates_link,headers = headers)
                        total_pages_soup = BeautifulSoup(total_pages.content, u'html.parser')
                        # print(total_pages_soup)
                        tol = total_pages_soup.find('span',attrs={'id':'ctl00_BodyContent_lblAgentsFound'}).text.split('Found:')[1]
                        tol_page = int(tol)/25
                        remainder = int(tol) % 20
                        if remainder > 0:
                            tol_page = tol_page+1
                        tol_page = int(tol_page)
                        print('Total_pages-->>>',tol_page)
                        self.get_profile(asociates_link, tol_page)
                    except:
                        tol_page = 1
        except:
            pass

    def get_profile(self,asociates_link,tol_page):
        try:
            # res3 = requests.get(asociates_link)
            # profilesoup = BeautifulSoup(res3.content, u'html.parser')
            try:

                # print('Asociates__>>>',asociates_link)
                for p in range(tol_page):

                    p = p + 1
                    page_link = asociates_link+'&pg='+str(p)

                    print('Page_link-->',page_link)
                    profile = requests.get(page_link,headers= headers)
                    profile_soup = BeautifulSoup(profile.content, u'html.parser')

                    viewlink = profile_soup.findAll('a', text=re.compile('View'))
                    for v in viewlink:
                        vlink = "https://www.weichert.com" + v.get('href')
                        print(vlink)
                        try:
                            res2 = requests.get(vlink)
                            prosoup2 = BeautifulSoup(res2.content, u'html.parser')
                            try:
                                name = prosoup2.find('span', attrs={'id': 'ctl00_BodyContent_lblAgentName'}).text.strip()
                                ln = "".join(reversed(name))
                                last_name=ln.split(' ')[0]
                                last_name="".join(reversed(last_name))
                                first_name=name.split(last_name)[0]
                            except:
                                first_name = 'None'
                                last_name = 'None'
                            try:
                                officename = prosoup2.find('span',attrs={'id': 'ctl00_BodyContent_lblOfficeName'}).text.strip()
                            except:
                                officename = 'None'
                            try:
                                address = prosoup2.find('span', attrs={'id': 'ctl00_BodyContent_lblAddress'}).text.strip()
                            except:
                                address = 'None'
                            try:
                                sateaddress = prosoup2.find('span', attrs={'id': 'ctl00_BodyContent_lblCityStateZip'}).text.strip()
                                city=sateaddress.split(',')[0]
                                region=sateaddress.split(', ')[1].split(' ')[0]
                                postal_code=sateaddress.split(region)[1]
                            except:
                                city = 'None'
                                region = 'None'
                                postal_code = 'None'

                            try:
                                ph = prosoup2.find('a', attrs={'id': 'ctl00_BodyContent_lblPhoneNum'}).text.strip()
                                agphone = 'None'
                                officeph = "None"
                                if "Phone:" in ph:
                                    agphone = ph.split('Phone:')[1].strip()
                                elif 'Office:' in ph:
                                    officeph = ph.split('Office:')[1]
                            except:
                                officeph = 'None'
                                agphone = 'None'
                            try:
                                fax = prosoup2.find('span', attrs={'id': 'ctl00_BodyContent_lblFax'}).text.strip()
                                fax = fax.split('Fax:')[1]
                            except:
                                fax = 'None'

                            print("First Name -" + first_name)
                            print("Last Name -" + last_name)
                            # print("Email - "+ email)
                            print("Address - " + address)
                            print("Office Phone - " + officeph)
                            print("Agent Phone - " + agphone)
                            print("Sate Zip - " + city+"-"+region+"-"+postal_code)
                            print("Fax - " + fax)
                            print("Office Name - " + officename)

                            self.row=self.row+1
                            sheet1.write(self.row, 0, str(first_name))
                            sheet1.write(self.row, 1, str(last_name))
                            sheet1.write(self.row, 2, str(address))
                            sheet1.write(self.row, 3, str(city))
                            sheet1.write(self.row, 4, str(region))
                            sheet1.write(self.row, 5, str(postal_code))
                            sheet1.write(self.row, 6, str(agphone))
                            sheet1.write(self.row, 7, str(officeph))
                            sheet1.write(self.row, 8, str(fax))
                            sheet1.write(self.row, 9, str(officename))
                            sheet1.write(self.row, 10, str(vlink))
                            import datetime
                            wb.save('weichert_xls2.xls')
                            print('Done------->',self.row)
                            print(datetime.datetime.now())
                            print('----' * 100)
                        except:
                            print(sys.exc_info())
            except:
                print(sys.exc_info())

        except:
            print(sys.exc_info())

objs = ascp()
objs.pass_links()



