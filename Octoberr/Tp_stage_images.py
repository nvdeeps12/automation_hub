from selenium import webdriver
import xlwt
import xlrd
from bs4 import BeautifulSoup
import requests
import re
import datetime
import sys
#
options = webdriver.FirefoxOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='D:\Projects\Fpbot\FiverProjects\Work\Scripts\Drivers\geckodriver',options=options)


wb = xlwt.Workbook()
rd = xlrd.open_workbook('D:\Projects\Fpbot\FiverProjects\Files\Outputs\Tp_Missing_Images.xls')
sheet = rd.sheet_by_index(0)

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'SUPPLIER P/N')
sheet1.write(0, 1, 'Image')


referer='https://google.com.in'
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'referer':referer
    }


row = 0
r = 0
img=''
fail=[]
features_des=''
for i in range(sheet.nrows):
    try:
        row = row + 1
        value = sheet.cell_value(i,0)
        if value == '':
            continue
        urls = sheet.cell_value(i,1)

        if urls == 'None':

            link = 'https://www.travisperkins.co.uk/wood-screws/bullet-4-0-x-45mm-gold-wood-screw-230/p/'+str(value)
            print('Process Started For--', link)

            res = driver.get(link)
        # driver.get(value)
    except:
        print('Loop Continue')
        continue

    try:
        soup = BeautifulSoup(driver.page_source, u'html.parser')
        # print(soup)
        # try:
        #     pid = link.split('p/')[1]
        # except:
        #     pid = ''
        # print('Pid ->' + pid)
        #

        try:
            image = soup.find('div',attrs={'id':'scene7Wrapper_flyout'})
            img=image.find_next('img').get('src')
            print('Image 1st try--', img)
            # print('Img--',image)
        except:
            img = 'None'
        print('Images---->',img)


        sheet1.write(row,0,value)
        sheet1.write(row, 1, img)

        wb.save('D:\Projects\Fpbot\FiverProjects\Files\Outputs\Tp_Missing_Images2.xls')
        print('---'*100)
    except:
        print(sys.exc_info())
        print('No Links Collected From->')
        continue

