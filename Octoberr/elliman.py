import requests
from bs4 import BeautifulSoup
import xlwt
import re
wb = xlwt.Workbook()

sheet1 = wb.add_sheet('sheet1')
sheet1.write(0, 0, 'First Name')
sheet1.write(0, 1, 'Last Name')
sheet1.write(0, 2, 'Office Name')
sheet1.write(0, 3, 'Adress')
sheet1.write(0, 4, 'State')
sheet1.write(0, 5, 'Zipcode')
sheet1.write(0, 6, 'Mobile')
sheet1.write(0, 7, 'Office Number')
sheet1.write(0, 8, 'Email')
sheet1.write(0, 9, 'Link')


class eliman():
    def pass_states(self):
        self.row = 0
        # page = requests.get('https://www.elliman.com/agents')
        # soup = BeautifulSoup(page.content, u"html.parser")
        # # print(soup)
        # div = soup.find('div',attrs={'class':'_grid38 _omega wysiwyg'}).findAll('a')
        div = ['https://www.elliman.com/agents/california','https://www.elliman.com/agents/connecticut','https://www.elliman.com/agents/colorado','https://www.elliman.com/agents/new-jersey','https://www.elliman.com/agents/massachusetts','https://www.elliman.com/agents/texas']

        for a in div:
            links = a
            anchors = a
            print('Anch-->>>',anchors)

            self.get_offices(anchors)
    #
    def get_offices(self,anchors):
        new_page = requests.get(anchors)
        new_soup = BeautifulSoup(new_page.content, u"html.parser")
        # print(new_soup)
        div = new_soup.find('div',attrs={'class':'_grid39 _omega'}).findAll('a')

        for a in div:
            links = a['href']
            if 'elliman' in links:
                continue

            # if len(links)==23:
            #     continue

            print('LLLNN______>>>>>>>>>>>>>>>>>>>',links)
            anch = 'https://www.elliman.com'+str(links)
            self.get_agents(anch)



    def get_agents(self,anch):
        print('Page--->>',anch)
        agent_page = requests.get(anch)
        agents_soup = BeautifulSoup(agent_page.content, u"html.parser")
        try:
            agent_div = agents_soup.find('div',attrs={'class':'w_table'}).findAll('a')
        except:
            agent_div = 'None'
            pass
        for a in agent_div:
            try:
                office = agents_soup.find('div',attrs={'class':'w_box_header'}).find('h1').text.strip()
                office = office.split('Office')[0].strip()
            except:
                office_name = 'None'
                continue

            try:
                agent_name = a.text.strip()
            except:
                continue
            emails = a.find_next('a').text.strip()
            print(emails)
            hrf = a['href']
            if len(hrf)>40:
                continue
            if '.com' in hrf:
                agent_link = hrf
            else:
                agent_link = 'https://www.elliman.com'+str(hrf)

            print(agent_link)
            self.row = self.row+1


            self.get_agents_data(agent_link,emails,agent_name,office)


    def get_agents_data(self,agent_link,emails,agent_name,office):
        try:
            agent_url = requests.get(agent_link)
            self.agent_soup = BeautifulSoup(agent_url.content, u"html.parser")
            self.agent_div = self.agent_soup.find('div', attrs={'class': 'wysiwyg _dark _with_padding'}).find_next('p')
        except:
            pass
        try:
            try:
                agent_name = agent_name
            except:
                agent_name = self.agent_div.find('strong').text.strip()
        except:
            agent_name = 'None'

        try:
            first_name = agent_name.split(',')[0]
            last_name = agent_name.split(',')[1]
        except:
            first_name = 'None'
            last_name = 'None'
        print('Agent_name-->',agent_name)

        try:
            try:
                address = self.agent_div.text.split('Office:')[0]
            except:
                address = self.agent_soup.find('div',attrs={'class':'wysiwyg office-mobile _bigger'}).findAll('p')[1].text.strip()
                address2 = self.agent_soup.find('div',attrs={'class':'wysiwyg office-mobile _bigger'}).findAll('p')[2].text.strip()
                address = address+str(address2)
            if 'Broker' in address:
                address = address.split('Broker')[1]
            if 'Salesperson' in address:
                address = address.split('Salesperson')[1]
            if 'Associate' in address:
                address = address.split('Associate')[1]
            zipcode=""
            staddress=""
            state=''
            staddress = address.split(',')[0]
            state = address.split(staddress)[1].split(' ')[1]
            zipcode = address.split(state)[1].split(' ')[1]
        except:
            address = 'None'
            zipcode = "None"
            staddress = "None"
            state = 'None'

        print('Address->',address)
        print("st - " + staddress)
        print("state - " + state)
        print("Zip - " + zipcode)
        try:
            try:
                office_no = self.agent_div.text.split('Office:')[1].split('M')[0].strip()
            except:
                office_no = self.agent_soup.find('p',text=re.compile('Office:')).text.split('Office:')[1]
        except:
            office_no = 'None'

        print('Office-->',office_no)


        try:
            try:
                mobile = self.agent_soup.find('p',text=re.compile('Mobile:')).text.split('Mobile:')[1].strip()
            except:
                mobile = self.agent_div.text.split('Mobile:')[1].split('Fax')[0].strip()
            if ' ' in mobile:
                mobile = mobile.split(' ')[0]
        except:
            mobile = 'None'

        print('Mobile-->>',mobile)

        try:
            email = emails
        except:
            email = 'None'

        print('Email-->>>',email)

        try:
            office_name = office
        except:
            office_name = 'None'

        print('Ofiice--Name-', office_name)

        # try:
        #     try:
        #         street = agent_soup.find('p',text=re.compile('Mobile:')).find_next('p').find_next('p')
        #     except:
        #         street  = 'None'
        # except:
        #     street = 'None'
        #
        # print('Street-->>',street)
        sheet1.write(self.row, 0, first_name)
        sheet1.write(self.row, 1, last_name)
        sheet1.write(self.row, 2,office_name)
        sheet1.write(self.row, 3, address)
        sheet1.write(self.row, 4, state)
        sheet1.write(self.row, 5, zipcode)
        sheet1.write(self.row, 6, mobile)
        sheet1.write(self.row, 7, office_no)
        sheet1.write(self.row, 8, email)
        sheet1.write(self.row, 9, agent_link)

        # wb.save('eliman_agentscn.xls')
        print('---'*100)
        print("Done - " +str(self.row))
        print('---'*100)



obj = eliman()
obj.pass_states()