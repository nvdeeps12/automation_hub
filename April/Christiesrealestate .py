import json
import requests
from bs4 import BeautifulSoup

import time


import datetime
source = 'Christiesrealestate'
created = datetime.datetime.now()
created = str(created)
# table='CREATE TABLE properties(id SERIAL PRIMARY KEY,title VARCHAR(255),description VARCHAR(255),slug VARCHAR(255),price VARCHAR(255),location VARCHAR(255),country VARCHAR(255),page_link VARCHAR(255),proximities VARCHAR(255),pictures VARCHAR(255),source VARCHAR(255),created_at datetime NOT NULL)'
# mycursor.execute(table)
# connection.commit()
main_dict = []


class chris():

    # 1629
    row = 0

    def get_links(self):
        for i in range(1, 1629):
            url = 'https://www.christiesrealestate.com/sales/int/'+str(i)+'-pg'
            con = requests.get(url)
            print(url)
            soup = BeautifulSoup(con.content, u'html.parser')
            links = soup.findAll(
                'link', attrs={'itemprop': 'mainEntityOfPage'})
            for l in links:

                link = 'https://www.christiesrealestate.com'+str(l.get('href'))
                print(link)
                self.get_data(link)

    def get_data(self, link):
        con1 = requests.get(link)
        soup_data = BeautifulSoup(con1.content, u'html.parser')
        try:
            title = soup_data.find(
                'div', attrs={'class': 'main-address'}).text.strip()
        except:
            title = 'None'
        try:
            address = soup_data.find('div', attrs={
                                     'class': 'c-address'}).find('span', attrs={'class': 'address'}).text.strip()
        except:
            address = 'None'
        try:
            locality = soup_data.find('div', attrs={
                                      'class': 'c-address'}).find('span', attrs={'class': 'locality'}).text.strip()
        except:
            locality = 'None'
        try:
            region = soup_data.find('div', attrs={
                                    'class': 'c-address'}).find('span', attrs={'class': 'region'}).text.strip()
        except:
            region = 'None'
        try:
            postal_code = soup_data.find('div', attrs={
                                         'class': 'c-address'}).find('span', attrs={'class': 'postal-code'}).text.strip()
        except:
            postal_code = 'None'

        try:
            country_name = soup_data.find('div', attrs={
                                          'class': 'c-address'}).find('span', attrs={'class': 'country-name'}).text.strip()
        except:
            country_name = 'None'

        try:
            dis = soup_data.find(
                'div', attrs={"class": "prop-description__comments-long"}).text.strip()
        except:
            dis = 'None'
        try:
            feature = []
            features = soup_data.findAll(
                'span', attrs={'class': 'prop-description__amenities-list-item-text'})
            for f in features:
                feature.append(f.text.strip())
        except:
            feature = 'None'
        try:
            price = soup_data.find(
                'span', attrs={"class": "price__value"}).text.strip()
        except:
            price = 'None'

        try:
            img_arr = []
            imgs = soup_data.findAll(
                'div', attrs={'class': 'js-thumbnail-wrapper'})
            for im in imgs:
                img = im.find('img').get('src')
                img_arr.append(img)
        except:
            img_arr = 'none'
        print(title)
        # print(dis)
        # print(img_arr)
        # print(price)
        # print(feature)
        # print(address, locality, region, postal_code, country_name)
        try:
            full_location = soup_data.find(
                'div', attrs={'class': 'c-address'}).text.strip()
        except:
            full_location = 'none'
        slug = str(link).split('detail/')[1].split('/')[0]
        # print(slug)
        self.row += 1
        final_dict = {
            "id": str(self.row),
            "title": title,
            "description": dis,
            "slug": slug,
            "price": price,
            "location": full_location,
            "country": country_name,
            "page_link": link,
            "Proximeties ": feature,
            "pictures": img_arr,
            "source": source,
        }
        # print(final_dict)
        main_dict.append(final_dict)
        import pprint
        pprint.pprint(main_dict)


myob = chris()
myob.get_links()
file_name = 'F:/PycharmProjects/MainKam/fiver/April/' + source+str('.json')
print('File_Name--', file_name)
with open(file_name, 'a+') as f:
    sorted = json.dumps(main_dict, indent=4)
    f.write(sorted)
print('----')
