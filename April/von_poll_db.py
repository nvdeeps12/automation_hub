
from bs4 import BeautifulSoup
import requests
# import MySQLdb
import mysql.connector
import sys

URL = "https://www.von-poll.com/en/search?b=1&page=1"


connection = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="residential_propdb"
    )
mycursor = connection.cursor()
import datetime
source = 'Von Poll'
created = datetime.datetime.now()
created  = str(created)


def log(msg):
    print(msg)

def check_isexits(page_link):
    try:
        page_link = "'%s'"%page_link
        qry2="SELECT count(*) FROM properties WHERE page_link="+page_link+';'
        
        mycursor.execute(qry2)
        record=mycursor.fetchall()
        data_rec=str(record[0])
        rec=data_rec.split('(')[1]
        data_rec=rec.split(',)')[0]
        data_rec = int(data_rec)
        return data_rec
    except:
        import sys
        print(sys.exc_info())
        return 0


def get_content(url):
    res = requests.get(url)
    if res.status_code == 200:
        html = res.content
    else:
        log('request failed status code {0} for url : {1}'.format(res.status_code, url))
        return None
    bs_object = BeautifulSoup(html,u'html.parser')
    return bs_object

def get_next_links(soup):
    divs = soup.findAll('div', attrs={'class':'prop_button'})
    links = []
    for dv in divs:
        link = dv.find('a')['href']
        links.append('https://www.von-poll.com' + str(link))
    return links

def start_von_poll():
    total_pages = None
    log(URL)
    soup = get_content(URL)
    if not total_pages:
        total_pages = soup.find('div', attrs={'class':'pager__pages'}).text.strip().split()[-1]
    log('total pages found : '+str(total_pages))

    links = get_next_links(soup)
    log(links)
    for link in links:
        get_detailed_data(link)
    
    ## getting after 1st page
    for i in range(int(total_pages)):
        page = i + 1
        if page == 1:
            continue
        nxturl = "https://www.von-poll.com/en/search?b=1&page=" + str(page)
        log(nxturl)
        soup = get_content(nxturl)
        links = get_next_links(soup)
        for link in links:
            get_detailed_data(link)

        

def get_detailed_data(link):
    
    soup = get_content(link)
    if not soup:
        log('no data found for link : ' + str(link))
        return
    try:
        slug = link.split('-')[-1].strip()
    except:
        slug = link.replace('https://www.von-poll.com/en', '')
    
    try:
        title = soup.find('div', attrs={'class':'immo-top'}).h1.text.strip()
    except:
        title = None
    
    try:
        location = soup.find('div', attrs={'class':'immo-top'}).p.text.strip()  
        country = location.split('-')[-1].strip()
    except:
        location = None
        country = None
    
    try:
        desc = soup.findAll('div', attrs={'class':'panel-body'})
        for des in desc:
            if 'Building description' in des.text:
                des = des.p.text.strip()
                break
    except:
        log(sys.exc_info())
        des = None

    try:
        pics = soup.find('div', attrs={'class':'galleria'}).findAll('img')
        images = []
        for pic in pics:
            image = 'https://www.von-poll.com' + pic['src']
            images.append(image)
    except:
        log(sys.exc_info())
        images = []

    divs = soup.find('div', attrs={'class':'data-table'}).findAll('div')
    price = rooms = bedrooms = bathrooms = features = None
    for dv in divs:
        if dv.strong.text.strip() == 'Purchase price:':
            try:
                price = dv.text.strip().replace('Purchase price:', '').strip()
            except:
                log(sys.exc_info())
                price = 'On request'
        if dv.strong.text.strip() == 'Rooms:':
            try:
                rooms = dv.text.strip().replace('Rooms:', '').strip()
            except:
                log(sys.exc_info())
                rooms = None
        if dv.strong.text.strip() == 'Bedrooms:':
            try:
                bedrooms = dv.text.strip().replace('Bedrooms:', '').strip()
            except:
                log(sys.exc_info())
                bedrooms = None
        if dv.strong.text.strip() == 'Bathrooms:':
            try:
                bathrooms = dv.text.strip().replace('Bathrooms:', '').strip()
            except:
                log(sys.exc_info())
                bathrooms = None
        if dv.strong.text.strip() == 'Features:':
            try:
                features = dv.text.strip().replace('Features:', '').strip()
            except:
                log(sys.exc_info())
                features = None
    proximities = [{'features':features}, {'rooms':rooms, 'bedrooms':bedrooms, 'bathrooms':bathrooms}]


    # print(proximities)

    insert_sql(title, des, slug, price, location, country, link, proximities, images)


def insert_sql(title,desc,slug,price,location,country,page_link,proximities,pictures):
    try:
        pictures = '"%s"'%pictures
        proximities = '"%s"'%proximities

        arr = []

        data = (title,desc,slug,price,location,country,page_link,proximities,pictures,source,created)
        arr.append(data)

        data_tuple = tuple(arr)
        if len(data_tuple) == 1:
                data_tuple = str(data_tuple).replace('),)', '))')
        
        query = "INSERT INTO properties("
        query +=  "title,description,slug,price,location,country,page_link,proximities,pictures,source,created_at) VALUES"
        query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),"NULL").replace('((', '(').replace('))', ')')
        
        mycursor.execute(query)
        connection.commit()
        print(mycursor.rowcount, "record inserted.")
    except:
        log(sys.exc_info())


start_von_poll()
