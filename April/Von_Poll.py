
import datetime
import json
from bs4 import BeautifulSoup
import requests

import sys

URL = "https://www.von-poll.com/en/search?b=1&page=1"

source = 'Von Poll'
created = datetime.datetime.now()
created = str(created)


def log(msg):
    print(msg)


main_arr = []



class GetVon():
    

    def get_content(self,url):
        res = requests.get(url)
        if res.status_code == 200:
            html = res.content
        else:
            log('request failed status code {0} for url : {1}'.format(
                res.status_code, url))
            return None
        bs_object = BeautifulSoup(html, u'html.parser')
        return bs_object


    def get_next_links(self,soup):
        divs = soup.findAll('div', attrs={'class': 'prop_button'})
        links = []
        for dv in divs:
            
            link = dv.find('a')['href']
            links.append('https://www.von-poll.com' + str(link))
        return links


    def start_von_poll(self):
        self.row = 0
        total_pages = None
        log(URL)
        soup = self.get_content(URL)
        if not total_pages:
            total_pages = soup.find(
                'div', attrs={'class': 'pager__pages'}).text.strip().split()[-1]
        log('total pages found : '+str(total_pages))

        links = self.get_next_links(soup)
        
        self.breakpoint = 11
        # log(links)
        for link in links:
            self.get_detailed_data(link,self.breakpoint)

        # getting after 1st page

        # total_pages = 2


        for i in range(int(total_pages)):
            page = i + 1
            if page == 1:
                continue

            # Generating File If Records Reach to a Point
            if page==518:
                self.breakpoint = total_pages*8
            else:
                self.breakpoint = total_pages*10

            nxturl = "https://www.von-poll.com/en/search?b=1&page=" + str(page)
            log(nxturl)
            soup = self.get_content(nxturl)
            links = self.get_next_links(soup)
            for link in links:
                self.get_detailed_data(link,self.breakpoint)


            

            



    def get_detailed_data(self,link,breaks):

        soup = self.get_content(link)
        if not soup:
            log('no data found for link : ' + str(link))
            return
        breaks = breaks-1
        print('Length--->',len(main_arr))
        if int(len(main_arr))==int(breaks):    
            file_name = source+str('.json')
            print('File_Name--', file_name)
            with open(file_name, 'a+') as f:
                sorted = json.dumps(main_arr, indent=4)
                f.write(sorted)
            print('File Generated Successfully')
            print('Appended in File-->',len(main_arr)+1)
            print('--'*90)

        try:
            slug = link.split('-')[-1].strip()
        except:
            slug = link.replace('https://www.von-poll.com/en', '')

        try:
            title = soup.find('div', attrs={'class': 'immo-top'}).h1.text.strip()
        except:
            title = None

        try:
            location = soup.find('div', attrs={'class': 'immo-top'}).p.text.strip()
            country = location.split('-')[-1].strip()
        except:
            location = None
            country = None

        try:
            desc = soup.findAll('div', attrs={'class': 'panel-body'})
            for des in desc:
                if 'Building description' in des.text:
                    des = des.p.text.strip()
                    break
        except:
            log(sys.exc_info())
            des = None
        images = []
        try:
            pics = soup.find('div', attrs={'class': 'galleria'}).findAll('img')
            
            for pic in pics:
                image = 'https://www.von-poll.com' + pic['src']
                images.append(image)
        except:
            log(sys.exc_info())
            images = []

        divs = soup.find('div', attrs={'class': 'data-table'}).findAll('div')
        rooms = bedrooms = bathrooms = features = None
        price = 'On request'
        for dv in divs:
            if dv.strong.text.strip() == 'Purchase price:':
                try:
                    price = dv.text.strip().replace('Purchase price:', '').strip()
                except:
                    log(sys.exc_info())
                    price = 'On request'
            if dv.strong.text.strip() == 'Rooms:':
                try:
                    rooms = dv.text.strip().replace('Rooms:', '').strip()
                except:
                    log(sys.exc_info())
                    rooms = None
            if dv.strong.text.strip() == 'Bedrooms:':
                try:
                    bedrooms = dv.text.strip().replace('Bedrooms:', '').strip()
                except:
                    log(sys.exc_info())
                    bedrooms = None
            if dv.strong.text.strip() == 'Bathrooms:':
                try:
                    bathrooms = dv.text.strip().replace('Bathrooms:', '').strip()
                except:
                    log(sys.exc_info())
                    bathrooms = None
            if dv.strong.text.strip() == 'Features:':
                try:
                    features = dv.text.strip().replace('Features:', '').strip()
                except:
                    log(sys.exc_info())
                    features = None
        proximities = [{'features': features}, {'rooms': rooms,
                                                'bedrooms': bedrooms, 'bathrooms': bathrooms}]

        # print(proximities)

        ids = len(main_arr)

        final_dict = {
            "id": (ids),"title": title,
            "description": des,
            "slug": slug,
            "price": price,
            "location": location,
            "country": country,
            "page_link": link,
            "proximities": proximities,
            "pictures": images,
            "source": source,
        }
        main_arr.append(final_dict)




obj = GetVon()
obj.start_von_poll()