import requests
from bs4 import BeautifulSoup
import xlwt


class bookingagentinfo():
    
    def __init__(self):
        
        print('Hello')
        self.wb = xlwt.Workbook()
        self.sheet1 = self.wb.add_sheet('Booking Info')
        self.header = True
        self.row = 0
        
        fields = ['SubCategory','Username','Agent_name','Agent_email','Agent_phone',
                          'Manager_Name: Manager_Email','Manager_phone'
                          'Publicit_name','Publicit_email','Publicit_phone','Position','Page-Link'
                          
                          ]
        self.sheet1.write(0, 0, 'SubCategory')
        self.sheet1.write(0, 1, 'Username')
        self.sheet1.write(0, 2, 'Agent_name')
        self.sheet1.write(0, 3, 'Agent_email')
        self.sheet1.write(0, 4, 'Agent_phone')
        self.sheet1.write(0, 5, 'Manager_Name')
        self.sheet1.write(0, 6, 'Manager_Email')
        self.sheet1.write(0, 7, 'Manager_phone')
        self.sheet1.write(0, 8, 'Publicit_name')
        self.sheet1.write(0, 9, 'Publicit_email')
        self.sheet1.write(0, 10, 'Publicit_phone')
        self.sheet1.write(0, 11, 'Position')
        self.sheet1.write(0, 12, 'Company')
        self.sheet1.write(0, 13, 'Phone')
        self.sheet1.write(0, 14, 'Email')
        self.sheet1.write(0, 15, 'Page_link')
    
    def login_user(self):
        import requests
        from bs4 import BeautifulSoup
        import xlwt
        import xlrd
        import time
        from selenium import webdriver
        options = webdriver.ChromeOptions()
        options.add_argument('--disable-gpu')
        options.add_argument('--no-sandbox')
        options.add_argument("--log-level=3")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--headless')
       
        
        chromepath = '/home/ubuntu/chromedriver'
        
        headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'https://bookingagentinfo.com',
        'Connection': 'keep-alive',
        'Referer': 'https://bookingagentinfo.com/log-in/',
        'Upgrade-Insecure-Requests': '1',
        }
        # chromepath = 'C:\Python_Projects\Projects\\fiver\Drivers\chromedriver.exe'
        data = {
        'rcp_user_login': 'iverson@gandylabs.com',
        'rcp_user_pass': 'qJ6_2ZbNtN#E9sA',
        'rcp_action': 'login',
        'rcp_redirect': 'https://bookingagentinfo.com/dashboard/',
        'rcp_login_nonce': '3336820fb1'
        }


        with requests.Session() as s:
            url = 'https://bookingagentinfo.com/log-in/'
            r = s.get(url, headers=headers)
            soup = BeautifulSoup(r.content, u'html.parser')
            data['rcp_login_nonce'] = soup.find('input', attrs={'name': 'rcp_login_nonce'})['value']
            r = s.post(url, data=data, headers=headers)
            
            # contents = r.content
            
            urls = 'https://bookingagentinfo.com/browse/153,160,85,158,86,104,150,87,88,89,91,97,103,137,140,100,146,152,154,156,157,159,94,138,139,142,141,143,144,145,148,149,151,92,93,95,98,99,101,102,112,115,117,118,123,121,120,114,108,105'
            con = s.get(urls,headers = headers)
            soup=BeautifulSoup(con.content,u'html.parser')
            
            print('--'*70)
            print('Login')
            return soup,urls,s
        
    
    def log(self,para):
        print('Got-->',para)
        print('------------'*30)

        
    def get_links(self):
        try:
            
            # print(urls)
            
            soup,urls,session = self.login_user()
            
            import time
            # print(con)
            
            
            
            # total_pages = soup.find('div',attrs={'class':'celebrity-items-pagination'}).findAll('a',attrs={'class':'page-numbers'})[-2].text.strip()
            # print('Pages',total_pages)
            total_pages = 1227
            total_pages = int(total_pages)
            total_pages+=1
            
            for p in range(int(total_pages)):
                if p>600:
                    break
                
                pages = urls+str('/page/')+str(p)
                print('Page-->',p)
                
                con = session.get(url = pages)
                soup=BeautifulSoup(con.content,u'html.parser')
                                    
                try:
                    subcategory = soup.find('div',attrs={'id':'filtered-list-categories'}).find_next('li').text.strip()
                except:
                    subcategory = 'None'
                
                
                
                catlinks = soup.findAll('a',attrs={'class':'celebrity-item'})
                # Getting Links Now--
                for lnk in catlinks:
                    try:
                        link = lnk['href']
                    except:
                        link  = 'None'
                        
                    # link = 'https://bookingagentinfo.com/celebriry/julie-hasid/'
                    self.log(link)
                    
                    con = session.get(link)
                    soup=BeautifulSoup(con.content,u'html.parser')
                    
                    try:
                        issue404 = soup.find('h1',attrs = {'class':'page-404__title'})
                        
                        if issue404!=None:
                            print(issue404)
                            print('404 found and continuing')
                            continue
                        # 404 Error Occured then continue
                    except:
                        pass
                    # This is for Excel
                    
                    
                    cats_arr = []
                    agent_name = 'N/A' 
                    agent_email = 'N/A'
                    agent_phone = 'N/A'
                    manager_name = 'N/A' 
                    manager_email = 'N/A'
                    manager_phone = 'N/A'
                    pulicist_name = 'N/A' 
                    publicist_email = 'N/A'
                    publicist_phone = 'N/A'
                    
                    try:
                        try:
                            username = soup.find('div',attrs = {'class':'text-details celebrity-info-box'}).find_next('h2').text.strip()
                            if 'Contact' in username:
                                username = username.split('Contact')[1].strip()
                        except:
                            username = soup.find('h1',attrs={'class':'person-info-box-title'}).text.strip()
                    except:
                        continue
                    
                    
                    try:
                        try:
                            phone = soup.find('td',attrs={'td':'contact-info-group-item-col-phone-numbers'}).text.strip()
                        except:
                            phone = soup.find('div',attrs={'class':'contact-info-group-item-phone-number-item'}).text.strip()

                    except:
                        phone = 'None'
                        
                    try:
                        email = soup.find('td',attrs={'class':'contact-info-group-item-col-email-addresses'}).text.strip()
                    except:
                        email = 'None'
                        
                    try:
                        company = soup.find('h4',attrs={'class':'contact-info-group-item-company-name'}).text.strip()
                    except:
                        company = 'None'
                        
                    try:
                        position = soup.find('span',attrs={'class':'person-info-box-profession'}).text.strip()
                        cats_arr.append(position)
                    except:
                        position = 'None'
                    
                    
                        

                    # Start of Agents div
                    # -----------------------------------------------------------------------------------------------------------
                    try:
                        agent_div = soup.find('div',attrs={'class':'celebrity-contacts-group group-booking_agents'})
                        agent_div = agent_div.find('div',attrs={'class':'contact-block'})
                
                        # Going to get Agent Info 
                            
                        try:
                            agent_name  = agent_div.find('div',attrs = {'class':'contact-data-item contact-name'})
                            # print()
                            agent_name = agent_name.text.strip()
                            # print(agent_name)
                            
                            if 'Report' in agent_name:
                                agent_name = agent_name.split('Report')[0].strip()
                            if ':' in agent_name:
                                agent_name = agent_name.split(':')[1].strip()
                                
                        except:
                            agent_name = 'None'
                            
                        try:
                            agent_email  = agent_div.find('div',attrs = {'class':'contact-data-item contact-email'})
                            
                            if 'Email:' in agent_email:
                                agent_email = agent_email.split('Email:')[1].strip()
                            agent_email = agent_email.text.strip()
                        except:
                            agent_email = 'None'
                            
                        try:
                            agent_phone  = agent_div.find('p',attrs = {'class':'phone-number'})
                            
                            agent_phone = agent_phone.text.strip()
                        except:
                            agent_phone = 'None'
                            
                        
                    except:
                        # This is For None Image Ones
                        import sys
                        # print(sys.exc_info())
                        pass
                    # End of Agent Div
                    # --------------------------------------------------------------------------------
                    
                    # Start of Manger Div
                    
                    try:
                        manager_div = soup.find('div',attrs={'class':'celebrity-contacts-group group-managers'})
                        # print(manager_div)
                        manager_div = manager_div.find('div',attrs={'class':'contact-block'})
                
                        # Going to get Manger Info 
                            
                        try:
                            manager_name  = manager_div.find('div',attrs = {'class':'contact-data-item contact-name'})
                            
                            manager_name = manager_name.text.strip()
                            if ':' in manager_name:
                                manager_name = manager_name.split(':')[1].strip()
                                
                            if 'Report' in manager_name:
                                manager_name = manager_name.split('Report')[0].strip()
                        
                        except:
                            manager_name = 'None'
                            
                        try:
                            manager_email  = manager_div.find('div',attrs = {'class':'contact-data-item contact-email'})
                            # print()
                            if 'Email:' in manager_email:
                                manager_email = manager_email.split('Email:')[1].strip()
                            manager_email = manager_email.text.strip()
                        except:
                            manager_email = 'None'
                            
                        try:
                            manager_phone  = manager_div.find('p',attrs = {'class':'phone-number'})
                            # print()
                            manager_phone = manager_phone.text.strip()
                        except:
                            manager_phone = 'None'
                            
                        

                    except:
                        
                        pass
                    
                    # End of Manager Div
                    # --------------------------------------------------------------------------------------------

                    # Start of Publicist Div
                    
                    try:
                        publicist_div = soup.find('div',attrs={'class':'celebrity-contacts-group group-publicists'})
                        # print(publicist_div)
                        publicist_div = publicist_div.find('div',attrs={'class':'contact-block'})
                
                        # Going to get Publicist Info 
                            
                        try:
                            pulicist_name  = publicist_div.find('div',attrs = {'class':'contact-data-item contact-name'})
                            pulicist_name = pulicist_name.text.strip()
                            if ':' in pulicist_name:
                                pulicist_name = pulicist_name.split(':')[1].strip()
                            if 'Report' in pulicist_name:
                                pulicist_name = pulicist_name.split('Report')[0].strip()
                            
                        except:
                            pulicist_name = 'None'
                            
                        try:
                            publicist_email  = publicist_div.find('div',attrs = {'class':'contact-data-item contact-email'})
                            # print()
                            publicist_email = publicist_email.text.strip()
                            
                            if 'Email:' in publicist_email:
                                publicist_email = publicist_email.split('Email:')[1].strip()
                            
                            
                        except:
                            publicist_email = 'None'
                            
                        try:
                            publicist_phone  = publicist_div.find('p',attrs = {'class':'phone-number'})
                            # print()
                            publicist_phone = publicist_phone.text.strip()
                        except:
                            publicist_phone = 'None'
                            
                        # print('Agent-->',agent_name)
                        # print('Manager-->',manager_name,manager_phone,manager_email)
                        # print('Pub-->',pulicist_name,publicist_phone,publicist_email)
                        
                        print('--'*10)

                    except:
                        publicist_div = 'None'
                        pass
                
                    
                    if len(cats_arr)==0:
                        if agent_name!='N/A':
                            
                            cats_arr.append('Agent,')
                        if manager_name!='N/A':
                            
                            cats_arr.append('Manager,')
                        if pulicist_name!='N/A':
                            cats_arr.append('Publicist')
                        
                    
                    print('Positions-->',cats_arr)
                    # End of Publicist----------------------------------------------------------------
                
                    # TODO: Main Cats IS Unavaliable
                    
                    
                    
                    
                    self.row += 1

                    self.sheet1.write(self.row, 0, subcategory)
                    self.sheet1.write(self.row, 1, username)
                    self.sheet1.write(self.row, 2, agent_name)
                    self.sheet1.write(self.row, 3, agent_email)
                    self.sheet1.write(self.row, 4, agent_phone)
                    self.sheet1.write(self.row, 5, manager_name)
                    self.sheet1.write(self.row, 6, manager_email)
                    self.sheet1.write(self.row, 7, manager_phone)
                    self.sheet1.write(self.row, 8, pulicist_name)
                    self.sheet1.write(self.row, 9, publicist_email)
                    self.sheet1.write(self.row, 10, publicist_phone)
                    self.sheet1.write(self.row, 11, cats_arr)
                    self.sheet1.write(self.row, 12, company)
                    self.sheet1.write(self.row, 13, phone)
                    self.sheet1.write(self.row, 14, email)
                    self.sheet1.write(self.row, 15, link)
                    print('Done->', self.row)
                    self.wb.save('Booking_agents.xls')
        except:
            import sys 
            print(sys.exc_info())
            
            
        
        
        
objBI= bookingagentinfo()
objBI.get_links()