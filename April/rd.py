import xlwt

wbw = xlwt.Workbook()
sheet1 = wbw.add_sheet('Booking Info')
row = 0
sheet1.write(0, 0, 'SubCategory')
sheet1.write(0, 1, 'Username')
sheet1.write(0, 2, 'Agent_name')
sheet1.write(0, 3, 'Agent_email')
sheet1.write(0, 4, 'Agent_phone')
sheet1.write(0, 5, 'Manager_Name')
sheet1.write(0, 6, 'Manager_Email')
sheet1.write(0, 7, 'Manager_phone')
sheet1.write(0, 8, 'Publicit_name')
sheet1.write(0, 9, 'Publicit_email')
sheet1.write(0, 10, 'Publicit_phone')
sheet1.write(0, 11, 'Position')
sheet1.write(0, 12, 'Company')
sheet1.write(0, 13, 'Phone')
sheet1.write(0, 14, 'Email')
sheet1.write(0, 15, 'Page_link')


def fix_email(vars):
    vars = str(vars)
    if 'com' in vars:
        vars = vars.split('com')[0]
        vars = vars+str('com').strip()
        return vars
    
def fix_vars(vars):
    vars = str(vars)
    if 'None' in vars:
        vars = ' '
    elif 'N/A' in vars:
        vars = ' '
    else:
        vars = vars
    
    return vars
    
      
def fix_phones(vars):
    vars = str(vars)
    if '+' in vars:
        vars = vars.split('+')[1]
        vars = str('+')+vars.strip()
        return vars
    
    
# new = fix_phones('+1 310-417-4881   +1 310-873-8181')
# new = str('+') +new[1]



import xlrd

loc = "/home/navdeep/Downloads/Booking_data.xls"
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)
print(sheet.nrows)

for i in range(1,sheet.nrows):
    
    agent_email = sheet.cell_value(i, 3)
    agent_phone = sheet.cell_value(i, 4)
    manager_email = sheet.cell_value(i, 6)
    manager_phone = sheet.cell_value(i, 7)
    publicist_email = sheet.cell_value(i, 9)
    publicist_phone = sheet.cell_value(i, 10)
    
    phone = sheet.cell_value(i, 13)
    email = sheet.cell_value(i, 14)
    subcategory = sheet.cell_value(i, 0)
    username = sheet.cell_value(i, 1)
    agent_name = sheet.cell_value(i, 2)
    manager_name = sheet.cell_value(i, 5)
    pulicist_name = sheet.cell_value(i, 8)
    cats_arr = sheet.cell_value(i, 11)
    company = sheet.cell_value(i, 12)
    page_link = sheet.cell_value(i, 15)
    
    
    agent_email = agent_email.replace('Email:','').strip()
    if ' ' in agent_email:
        agent_email = agent_email.split(' ')[0].strip()
    manager_email = manager_email.replace('Email:','').strip()
    if ' ' in manager_email:
        manager_email = manager_email.split(' ')[0].strip()
    publicist_email = publicist_email.replace('Email:','').strip()
    if ' ' in publicist_email:
        publicist_email = publicist_email.split(' ')[0].strip()
        
    email = email.replace('Email:','').strip()
    if ' ' in email:
        email = email.split(' ')[0].strip()
        
    print(i)
    print(agent_email,'--',manager_email,'--',publicist_email,'--',email)
    
    agent_email = fix_email(agent_email)
    manager_email = fix_email(manager_email)
    publicist_email = fix_email(publicist_email)
    email = fix_email(email)
    agent_phone = fix_phones(agent_phone)
    manager_phone = fix_phones(manager_phone)
    publicist_phone = fix_phones(publicist_phone)
    phone = fix_phones(phone)
    
    subcategory = fix_vars(subcategory)
    agent_name = fix_vars(agent_name)
    pulicist_name = fix_vars(pulicist_name)
    company = fix_vars(company)
    manager_name = fix_vars(manager_name)
    agent_name = fix_vars(agent_name)
    pulicist_name = fix_vars(pulicist_name)
    
    
    row = i
    sheet1.write(row, 0, subcategory)
    sheet1.write(row, 1, username)
    sheet1.write(row, 2, agent_name)
    sheet1.write(row, 3, agent_email)
    sheet1.write(row, 4, agent_phone)
    sheet1.write(row, 5, manager_name)
    sheet1.write(row, 6, manager_email)
    sheet1.write(row, 7, manager_phone)
    sheet1.write(row, 8, pulicist_name)
    sheet1.write(row, 9, publicist_email)
    sheet1.write(row, 10, publicist_phone)
    sheet1.write(row, 11, cats_arr)
    sheet1.write(row, 12, company)
    sheet1.write(row, 13, phone)
    sheet1.write(row, 14, email)
    sheet1.write(row, 15, page_link)
    
    print('Done->', row)
    wbw.save('Booking_agentsFixed.xls')

    print('--'*20)