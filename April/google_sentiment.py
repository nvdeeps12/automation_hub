from google.cloud import language_v1
from google.cloud.language_v1 import enums
import requests,xlwt,xlrd

wbw = xlwt.Workbook()
sheet1 = wbw.add_sheet('Sentiments')
row = 0
sheet1.write(0, 0, 'Url')
sheet1.write(0, 1, 'Sentiment_Score')
sheet1.write(0, 2, 'Sentiment_Magnitude')
# sheet1.write(0, 3, 'Entities_Sentiment')
sheet1.write(0, 3, 'Entities')
import csv
loc = "updated url list.csv"

rows = []
with open(loc, 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
        if 'http' not in row[0]:
            continue
        rows.append(row[0])
        





def sentiment_entity_analyze(content_uri):
	"""
	Analyzing Sentiment in text file stored in Cloud Storage

	Args:
	content_uri: URL for which need to get sentiment
	"""
	try:
		client = language_v1.LanguageServiceClient.from_service_account_json('nlp.json') # path of json file
		
		# The text to analyze
		res = requests.get(content_uri)
		text = res.content
		
		# Available types: PLAIN_TEXT, HTML
		type_ = enums.Document.Type.HTML

		# Optional. If not specified, the language is automatically detected.
		# For list of supported languages:
		# https://cloud.google.com/natural-language/docs/languages

		#language = "en" 
		document = {"content": text, "type": type_}#, "language": language}

		# Available values: NONE, UTF8, UTF16, UTF32
		encoding_type = enums.EncodingType.UTF8
		
		# getting sentiment
		response = client.analyze_sentiment(document, encoding_type=encoding_type)
		
		sentiment_score = response.document_sentiment.score
		sentiment_magnitude = response.document_sentiment.magnitude
		response_dict = {
			'sentiment_score': sentiment_score,
			'sentiment_magnitude': sentiment_magnitude
		}
		
		# getting entities
		response = client.analyze_entity_sentiment(document, encoding_type=encoding_type)
		entities = []
		
		for entity in response.entities:
			ent_dict = {
					'entity_name': entity.name,
					'entity_type': enums.Entity.Type(entity.type).name,
					'entity_sentiment':entity.sentiment
				}
			
			entities.append(ent_dict)
		response_dict.update({'entities':entities})

		return response_dict
	except:
		return {}



## url to get sentiment
count = 0
dataarr = []
for ur in rows:
	url = ur
	print(url)
 	
	# print(url)
	resp = sentiment_entity_analyze(url)
	
	try:
		urls = url
	except:
		urls = 'None'

	try:
		score = resp['sentiment_score']
	except:
		score = 'None'
		
		# 
	try:
		sentiment_magnitude = resp['sentiment_magnitude']
	except:
		sentiment_magnitude = 'None'
  
	try:
		entity_sentiment = resp['entity_sentiment']
	except:
		entity_sentiment = 'None'

 	
	try:
		entities = resp['entities']
		if len(entities)>32000:
			entities = str(entities)
			entities = entities[32000]
	except:
		entities = 'None'
	
	csv_columns = ['Url','Score','Sentiment_Magnitude','Entities_Info']
	dict1 = {'Url':urls,'Score':score,
          'Sentiment_Magnitude':sentiment_magnitude,
          'Entities_Info':str(entities)}
	
	dataarr.append(dict1)
 

	
	

	with open('google_sentiments.csv', 'w') as csvfile:
		writer = csv.DictWriter(csvfile,fieldnames=csv_columns)
		writer.writeheader()
		for i in dataarr:
			writer.writerow(i)
			
			print('Done->',row)
			print('--'*40)
		
		csvfile.close()


 
	
	# sheet1.write(row, 0, urls)
	# sheet1.write(row, 1, score)
	# sheet1.write(row, 2, sentiment_magnitude)
 	# # sheet1.write(row, 3, 'entity_sentiment')
	# sheet1.write(row, 3, str(entities))
	# print('Done->', row)
	# print('--'*20)
	# wbw.save('Googlesentimets.xls')