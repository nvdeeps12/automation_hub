import requests
from requests.auth import HTTPBasicAuth
import json
import xlrd
import xlwt
import sys

class GetScore():
    

    def __init__(self):
        self.link ="https://api.us-south.natural-language-understanding.watson.cloud.ibm.com/instances/9d2bd18a-8017-4e44-8de9-acc01a9f0795/v1/analyze?version=2019-07-12"
        self.auth = HTTPBasicAuth('apikey','4xOPCnzaJrxOa3cpetn9ilPorY2JhPzKJwgWssCk4LHn')
        self.wb = xlwt.Workbook()
        self.sheet1 = self.wb.add_sheet('Sheet')
        self.header = True
        self.row = 0

    def get_score(self):
        

        loc = "C:\Python_Projects\Projects\\fiver\April\Sentiment_Urls.xlsx"
        wb = xlrd.open_workbook(loc)
        sheet = wb.sheet_by_index(0)
        print(sheet.nrows)
        missed_arr = []
        for i in range(1,sheet.nrows):
            url = sheet.cell_value(i, 0)
            
            # if url in arr:
            #     continue
        
            # print(url)
            
            missed_arr.append(url)
            
        # print(missed_arr)
            
        file_name = 'Missed'+str('.json')
        print('File_Name--', file_name)
        with open(file_name, 'a+') as f:
            sorted = json.dumps(missed_arr, indent=4)
            f.write(sorted)
            
        # print(missed_arr)
            
            # if 'http' not in url:
            #     continue
            # self.getdata(url)


    def getdata(self,url):
        fields = ['Url','Sentiment score','Emotion score: Sadness','Emotion score: Joy','Emotion score: Fear','Emotion score: Disgust','Emotion score: Anger']
        data = []

        try:
            dataset_sentiment = {"url":url,"features":{"sentiment":{}}}
            dataset_emotion = {"url":url,"features":{"emotion":{}}}

            resp = requests.get(self.link, params=dataset_sentiment, auth=self.auth, headers = {"Accept": "application/json"})

            if resp.status_code == 200:
                try:
                    sentiment = json.loads(resp.content)
                    sentiment_score = sentiment['sentiment']['document']['score']
                except:
                    sentiment_score = 'None'

                data.append(url)
                data.append(sentiment_score)

            resp = requests.get(self.link, params=dataset_emotion, auth=self.auth,headers={"Accept": "application/json"})

            if resp.status_code == 200:
                emotion = json.loads(resp.content)
                try:
                    sadness = emotion['emotion']['document']['emotion']['sadness']
                except:
                    sadness = 'None'
                data.append(sadness)

                try:
                    joy = emotion['emotion']['document']['emotion']['joy']
                except:
                    joy = 'None'
                data.append(joy)

                try:
                    fear = emotion['emotion']['document']['emotion']['fear']
                except:
                    fear = 'None'
                data.append(fear)

                try:
                    disgust = emotion['emotion']['document']['emotion']['disgust']
                except:
                    disgust = 'None'
                data.append(disgust)

                try:
                    anger = emotion['emotion']['document']['emotion']['anger']
                except:
                    anger = 'None'
                data.append(anger)

            self.saveInXls(fields,data)
        except:
            print(sys.exc_info())


    def saveInXls(self, fields, data):
        if self.header != False:
            colno = 0
            for f in fields:
                self.sheet1.write(0, colno, f)
                colno += 1
            self.header = False

        col_no = 0
        self.row += 1
        print(self.row)
        for d in data:
            self.sheet1.write(self.row, col_no, d)
            self.wb.save('Updated_Sheet.xls')
            col_no += 1


obj = GetScore()
obj.get_score()