import datetime
import requests
from bs4 import BeautifulSoup
import mysql.connector

import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd
import time
from selenium import webdriver
options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
options.add_argument("--log-level=3")
options.add_argument('--ignore-certificate-errors')
options.add_argument('--headless')
driver = webdriver.Chrome(
    executable_path='F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe', options=options)
soup = BeautifulSoup(driver.page_source, u'html.parser')


connection = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="residential_propdb"
)
mycursor = connection.cursor()
source = 'Remax'
created = datetime.datetime.now()
created = str(created)


def insert_sql(title, desc, slug, price, location, country, page_link, proximities, pictures):
    try:
        # print(pictures)
        # print(len(pictures))
        pictures = "'%s'" % pictures

        arr = []

        data = (title, desc, slug, price, location, country,
                page_link, proximities, pictures, source, created)
        arr.append(data)

        data_tuple = tuple(arr)
        if len(data_tuple) == 1:
            data_tuple = str(data_tuple).replace('),)', '))')

        query = "INSERT INTO properties("
        query += "title,description,slug,price,location,country,page_link,proximities,pictures,source,created_at) VALUES"
        query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),
                                                                   "NULL").replace('((', '(').replace('))', ')')

        mycursor.execute(query)
        connection.commit()

        print(mycursor.rowcount, "record inserted.")
    except:
        import sys
        print(sys.exc_info())


def check_isexits(page_link):
    try:
        page_link = "'%s'" % page_link
        qry2 = "SELECT count(*) FROM properties WHERE page_link="+page_link+';'

        mycursor.execute(qry2)
        record = mycursor.fetchall()
        data_rec = str(record[0])
        rec = data_rec.split('(')[1]
        data_rec = rec.split(',)')[0]
        data_rec = int(data_rec)
        return data_rec
    except:
        import sys
        print(sys.exc_info())
        return 0


def get_pagelinks():

    total_pages = 28051

    for p in range(total_pages):

        if p == 0:
            continue

        print('Page-->', p)
        page = 'https://global.remax.com/searchresults.aspx?action=list&start=1&page=' + \
            str(p)+'&count=24&currency=USD&min=1&max=none&sb=MostRecent&sort=MostRecent'

        print('--'*70)
        con = driver.get(url=page)
        import time
        time.sleep(3)
        soup = BeautifulSoup(driver.page_source, u'html.parser')
        # print(soup)
        div = soup.findAll('div', attrs={'class': 'image-popup thumb-item'})
        for d in div:
            try:
                link = d['data-listing-url']
                link = 'https://global.remax.com'+str(link)
                driver.get(link)
                soup = BeautifulSoup(driver.page_source, u'html.parser')

                try:
                    page_link = link
                    is_existschec = check_isexits(page_link)
                    print(is_existschec)
                    if is_existschec > 0:
                        print('I am Skipping')
                        continue
                except:
                    page_link = None
                try:
                    title = soup.find(
                        'span', attrs={'class': 'titlebar-listfull__location hidden-xs'}).text.strip()
                except:
                    title = None
                try:
                    description = soup.find(
                        'div', attrs={'class': 'desc-short-googletrans'}).text.strip()
                except:
                    description = None

                try:
                    slug = link.split('/')[-1].strip()
                    print(slug)
                except:
                    slug = None

                try:
                    try:
                        price = soup.find(
                            'a', attrs={'class': 'key-price-alt'}).text.strip()
                        if ' ' in price:
                            price = price.split(' ')[0].strip()

                        price = price.replace('USD', '').strip()
                        price = price.replace(',', '').replace('.', '').strip()

                    except:
                        price = soup.find(
                            'span', attrs={'class': 'price-main'}).text.strip()
                        if ' ' in price:
                            price = price.split(' ')[0].strip()

                        price = price.replace('USD', '').strip()
                        price = price.replace(',', '').replace('.', '').strip()
                except:
                    price = None

                try:
                    full_loc = soup.find(
                        'span', attrs={'class': 'titlebar-listfull__location hidden-xs'}).text.strip()

                    try:
                        location = full_loc.split(',')
                        loc1 = location[0].split('-')[-1]
                        loc2 = location[-1].strip()

                        full_location = loc1+' '+loc2
                    except:
                        full_location = None

                    print('Loc-->', full_location)

                    try:
                        country = full_loc.split('-')[-1].strip()
                        print(country)
                    except:
                        country = None

                except:
                    full_location = None
                try:
                    proximities = soup.find(
                        'span', attrs={'class': 'titlebar-listfull__rooms hidden-xs'}).text.strip()
                except:
                    proximities = None
                try:
                    pictures = soup.findAll(
                        'div', attrs={'class': 'sp-image-container'})
                    # print(pictures)
                    pics_arr = []
                    for p in pictures:
                        try:
                            # print(p)
                            pic = p.find_next('img')['src']
                            pic = 'https://global.remax.com/'+str(pic)

                        except:
                            pic = None

                        pics_arr.append(pic)

                except:
                    pictures = None

                print('--'*40)

                insert_sql(title, description, slug, price, full_location,
                           country, page_link, proximities, pics_arr)

            except:
                continue


get_pagelinks()
