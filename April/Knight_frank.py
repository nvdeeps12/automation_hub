import json
import requests
from bs4 import BeautifulSoup

import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd
import time
from selenium import webdriver
import datetime

options = webdriver.ChromeOptions()
options.add_argument("--log-level=3")
options.add_argument('--disable-gpu')
# options.add_argument('--headless')

driver = webdriver.Chrome(
    executable_path='F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe', options=options)


source = 'Knight_Frank'
created = datetime.datetime.now()
created = str(created)


# link = 'https://www.knightfrank.com/properties/residential/for-sale/4-lewis-cubitt-walk-n1c-king-s-cross-london-n1c/krd012050438'


# cities=['Botswana','Kenya','Malawi','Nigeria','Rwanda','SouthAfrica','Tanzania','Uganda','Zambia','Zimbabwe','Australia','Cambodia','China','HongKong','India','Indonesia','Japan','Malaysia','NewZealand','Philippines','Singapore','SouthKorea','Thailand','Taiwan','Austria','Belgium','CzechRepublic','France','Germany','Hungary','Ireland','Italy','Monaco','Netherlands','Poland','Portugal','Romania','Russia','Spain','Switzerland','UK','united-arab-emirates','Bahrain','Qatar','SaudiArabia','USA','Canada','Caribbean']
cities = ['Botswana']
main_dict = []
row = 0
for city in cities:
    offset = 0
    print('Going city - ' + city)
    city = city.lower()
    driver.get('https://www.knightfrank.com/properties/residential/for-sale/' +
               str(city)+'/all-types/all-beds;offset='+str(offset))
    time.sleep(3)
    soup_l = BeautifulSoup(driver.page_source, u'html.parser')
    total_offset = soup_l.find('div', attrs={'class': 'lead'}).find(
        'span').text.strip().split('of ')[1]
    print(total_offset)
    if offset == 18:
        break
    while(offset < int(total_offset)):
        driver.get('https://www.knightfrank.com/properties/residential/for-sale/' +
                   str(city)+'/all-types/all-beds;offset='+str(offset))
        time.sleep(5)
        soup_l = BeautifulSoup(driver.page_source, u'html.parser')
        # links=driver.find_elements_by_css_selector('div.properties-item> property> div > a')
        links = soup_l.findAll('div', attrs={'class': 'properties-item'})
        # print(links)
        offset += 18
        for l in links:
            try:
                link = 'https://www.knightfrank.com' + \
                    str(l.find('div', attrs={
                        'class': 'grid-details'}).find_previous('a')['href'])
                print(link)
                driver.get(link)
        # link = 'https://www.knightfrank.com/properties/residential/for-sale/4-lewis-cubitt-walk-n1c-king-s-cross-london-n1c/krd012050438'
                time.sleep(3)

                soup = BeautifulSoup(driver.page_source, u'html.parser')

                try:
                    title = soup.find(
                        'h1', attrs={'class': 'address'}).text.strip()
                except:
                    title = None

                # print(title)

                try:
                    description = soup.find(
                        'div', attrs={'class': 'read-more'}).text.strip()
                except:
                    description = None

                try:
                    slug = link.split('/')[-1].strip()
                    # print(slug)
                except:
                    slug = None

                try:
                    price = soup.find(
                        'div', attrs={'class': 'price'}).text.strip()
                    # if ' ' in price:
                    #     price = price.split(' ')[0].strip()

                    # price = price.replace('USD','').strip()
                    # price = price.replace(',','').replace('.','').strip()
                except:
                    price = None

                try:
                    full_loc = title
                    if 'in' in full_loc:
                        full_loc = full_loc.split('in')[1].strip()

                    full_location = full_loc

                    # print('Loc-->',full_location)

                except:
                    full_location = None

                try:
                    country = soup.find(
                        'div', attrs={'class': 'country'}).text.strip()
                    # print(country)
                except:
                    country = None

                prximities_arr = []
                try:
                    proximities = soup.find(
                        'div', attrs={'class': 'amenities'})

                    counts = proximities.findAll('ul')[0].findAll('li')
                    objes = proximities.findAll('ul')[1].findAll('li')
                    # print(counts)
                    count = 0
                    for c in counts:
                        count = c.text.strip()
                        # print(count)
                    for obj in objes:
                        obj = obj.text.strip()
                        proximities = count+' '+str(obj)
                        prximities_arr.append(proximities)

                except:
                    prximities_arr = None

                print(prximities_arr)

                try:
                    pics_arr = []
                    pictures = soup.find(
                        'img', attrs={'class': 'container-item'})['src']
                    pics_arr.append(pictures)
                    # print(pictures
                except:
                    pictures = None

                print(pics_arr)

                # print(title,'Slug->>',slug,'Price--',price,'country-->',country,page_link,'Proximities->',proximities)

                print('--'*40)
                row += 1
                final_dict = [{
                    "id": str(row),
                    "title": title,
                    "description": description,
                    "slug": slug,
                    "price": price,
                    "location": full_location,
                    "country": country,
                    "page_link": link,
                    "proximities": prximities_arr,
                    "pictures": pics_arr,
                    "source": source,
                }]
                main_dict.append(final_dict)
            except:
                import sys
                print(sys.exc_info())
                continue
file_name = 'F:/PycharmProjects/MainKam/fiver/April/' + source+str('.json')
print('File_Name--', file_name)
with open(file_name, 'a+') as f:
    sorted = json.dumps(main_dict, indent=4)
    f.write(sorted)
print('----')
