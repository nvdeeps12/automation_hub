import json
import datetime
import requests
from bs4 import BeautifulSoup


import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd
import time
from selenium import webdriver
options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
options.add_argument("--log-level=3")
options.add_argument('--ignore-certificate-errors')
options.add_argument('--headless')
driver = webdriver.Chrome(
    executable_path='F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe', options=options)
soup = BeautifulSoup(driver.page_source, u'html.parser')
source = 'Remax'
row = 0
main_dict = dict()


def get_pagelinks():

    total_pages = 28051

    for p in range(total_pages):

        if p == 0:
            continue

        print('Page-->', p)
        page = 'https://global.remax.com/searchresults.aspx?action=list&start=1&page=' + \
            str(p)+'&count=24&currency=USD&min=1&max=none&sb=MostRecent&sort=MostRecent'

        print('--'*70)
        con = driver.get(url=page)
        import time
        time.sleep(3)
        soup = BeautifulSoup(driver.page_source, u'html.parser')
        # print(soup)
        div = soup.findAll('div', attrs={'class': 'image-popup thumb-item'})
        for d in div:
            try:
                link = d['data-listing-url']
                link = 'https://global.remax.com'+str(link)
                driver.get(link)
                soup = BeautifulSoup(driver.page_source, u'html.parser')

                try:
                    title = soup.find(
                        'span', attrs={'class': 'titlebar-listfull__location hidden-xs'}).text.strip()
                except:
                    title = None
                try:
                    description = soup.find(
                        'div', attrs={'class': 'desc-short-googletrans'}).text.strip()
                except:
                    description = None

                try:
                    slug = link.split('/')[-1].strip()
                    print(slug)
                except:
                    slug = None

                try:
                    try:
                        price = soup.find(
                            'a', attrs={'class': 'key-price-alt'}).text.strip()
                        if ' ' in price:
                            price = price.split(' ')[0].strip()

                        price = price.replace('USD', '').strip()
                        price = price.replace(',', '').replace('.', '').strip()

                    except:
                        price = soup.find(
                            'span', attrs={'class': 'price-main'}).text.strip()
                        if ' ' in price:
                            price = price.split(' ')[0].strip()

                        price = price.replace('USD', '').strip()
                        price = price.replace(',', '').replace('.', '').strip()
                except:
                    price = None

                try:
                    full_loc = soup.find(
                        'span', attrs={'class': 'titlebar-listfull__location hidden-xs'}).text.strip()

                    try:
                        location = full_loc.split(',')
                        loc1 = location[0].split('-')[-1]
                        loc2 = location[-1].strip()

                        full_location = loc1+' '+loc2
                    except:
                        full_location = None

                    print('Loc-->', full_location)

                    try:
                        country = full_loc.split('-')[-1].strip()
                        print(country)
                    except:
                        country = None

                except:
                    full_location = None
                try:
                    proximities = soup.find(
                        'span', attrs={'class': 'titlebar-listfull__rooms hidden-xs'}).text.strip()
                except:
                    proximities = None
                try:
                    pictures = soup.findAll(
                        'div', attrs={'class': 'sp-image-container'})
                    # print(pictures)
                    pics_arr = []
                    for p in pictures:
                        try:
                            # print(p)
                            pic = p.find_next('img')['src']
                            pic = 'https://global.remax.com/'+str(pic)

                        except:
                            pic = None

                        pics_arr.append(pic)

                except:
                    pictures = None

                print('--'*40)
                row += 1
                final_dict = [{
                    "id": str(row),
                    "title": title,
                    "description": description,
                    "slug": slug,
                    "price": price,
                    "location": location,
                    "country": country,
                    "page_link": link,
                    "proximities": proximities,
                    "pictures": pics_arr,
                    "source": source,
                }]
                main_dict.update(final_dict)
            except:
                continue


get_pagelinks()
file_name = 'F:/PycharmProjects/MainKam/fiver/April/' + source+str('.json')
print('File_Name--', file_name)
with open(file_name, 'a+') as f:
    sorted = json.dumps(main_dict, indent=4)
    f.write(sorted)
print('----')
