import datetime
import requests
from bs4 import BeautifulSoup
import mysql.connector

import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd
import time
import json

connection = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="residential_propdb"
)
mycursor = connection.cursor()
source = 'Remax'
created = datetime.datetime.now()
created = str(created)


def insert_sql(title, desc, slug, price, location, country, page_link, proximities, pictures):
    try:
        # print(pictures)
        # print(len(pictures))
        pictures = "'%s'" % pictures

        arr = []

        data = (title, desc, slug, price, location, country,
                page_link, proximities, pictures, source, created)
        arr.append(data)

        data_tuple = tuple(arr)
        if len(data_tuple) == 1:
            data_tuple = str(data_tuple).replace('),)', '))')

        query = "INSERT INTO properties("
        query += "title,description,slug,price,location,country,page_link,proximities,pictures,source,created_at) VALUES"
        query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),
                                                                   "NULL").replace('((', '(').replace('))', ')')

        mycursor.execute(query)
        connection.commit()

        print(mycursor.rowcount, "record inserted.")
    except:
        import sys
        print(sys.exc_info())


def get_data():
    try:

        qry2 = "SELECT * FROM properties; "

        mycursor.execute(qry2)
        record = mycursor.fetchall()
        # data_rec=str(record[0])
        # rec=data_rec.split('(')[1]
        # data_rec=rec.split(',)')[0]
        # data_rec = int(data_rec)
        return record
    except:
        import sys
        print(sys.exc_info())
        return 0


datavar = get_data()

arr = []
for i in datavar:

    ids = i[0]
    title = i[1]
    des = i[2]
    slug = i[3]
    price = i[4]
    location = i[5]
    country = i[6]
    page_link = i[7]
    proximities = i[8]
    pics = i[9]
    source = i[10]
    created = i[11]

    created = str(created)

    dict_prop = {'id': ids, 'title': title, 'description': des, 'slug': slug,

                 'price': price, 'location': location, 'country': country,

                 'page_link': page_link, 'proximities': proximities, 'pictures': pics, 'source': source, 'created': created
                 }

    file_name = 'F:/PycharmProjects/MainKam/fiver/April/vn_poll' + \
        str('.json')
    print('File_Name--', file_name)
    with open(file_name, 'a+') as f:
        sorted = json.dumps(dict_prop, indent=4)
        f.write(sorted)
    print('----')
    # print(datavar)
