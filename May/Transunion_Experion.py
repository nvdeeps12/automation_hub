import time
import json
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

options = Options()
# options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='/home/sewak/Downloads/geckodriver',firefox_options=options)
# driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver',options=options)
driver.get('https://service.transunion.com/dss/login.page?dest=dispute')


username = 'MAEIS1981'
password = 'M1981AEIS'

acc_arr = []
acc_dict = {}
finlarr = []

try:
    driver.find_element_by_xpath('//*[@id="c1527812999683"]/form/div/div/section[2]/div/div/div/div[1]/input').send_keys(username)
    time.sleep(1)
except:
    pass

try:
    driver.find_element_by_xpath('//*[@id="c1527812999683"]/form/div/div/section[2]/div/div/div/div[2]/input').send_keys(password)
    time.sleep(1)
except:
    pass

try:
    driver.find_element_by_xpath('//*[@id="c1527812999683"]/form/div/div/section[2]/div/div/div/button').click()
    time.sleep(10)
except:
    pass

try:
    driver.find_element_by_xpath('//*[@id="bodyContent"]/div/div/div/div/div/div/button').click()
    time.sleep(15)
except:
    pass

try:
    driver.find_element_by_xpath('//*[@id="startDispute"]/a').click()
    time.sleep(10)
except:
    pass

try:
    driver.find_element_by_xpath('//*[@id="disputeRefresh"]/label').click()
except:
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/form/div[2]/div/section/p[2]/label').click()
time.sleep(1)

try:
    driver.find_element_by_id('confirmRefreshButton-startDispute').click()
except:
    driver.find_element_by_id('Continue').click()

time.sleep(20)

soup = BeautifulSoup(driver.page_source, u'html.parser')

try:
    personal_information = soup.find('section',attrs={'id':'simple-dispute-pii'}).text.replace('\n',' ').replace('Delete',' ').replace('Change',' ').strip()
except:
    personal_information = 'none'

print(personal_information)

first_dict = {'personal_information': personal_information}
finlarr.append(first_dict)

for i in range(50):
    try:
        driver.find_element_by_id('trade-' + str(i)).click()
        time.sleep(2)
        ele = soup.find('td',attrs={'id':'trade-'+str(i)})

        try:
            ac_name = ele.text.replace('\n','')
        except:
            ac_name = 'none'

        try:
            ac_no = ele.find_next('td').text.strip()
        except:
            ac_no = 'none'

        try:
            bal = ele.find_next('td').find_next('td').text
        except:
            bal = 'none'

        try:
            m_pay = ele.find_next('td').find_next('td').find_next('td').text
        except:
            m_pay = 'none'

        try:
            info = ele.find_next('tr').text.replace('\n',' ')
        except:
            info = 'none'

    except:
        continue

    acc_dict.update({'Account Name':ac_name, 'Account Number':ac_no, 'Balance':bal, 'Monthly Payment':m_pay, 'info':info})
    acc_arr.append(acc_dict)
    acc_dict = {}

finlarr.append({'account_information': acc_arr})

try:
    cons_statement = soup.find('section', attrs={'id': 'simple-cons-statement'}).text.replace('\n',' ').strip()
except:
    cons_statement = 'none'

try:
    public_records = soup.find('section', attrs={'id': 'simple-public-records'}).text.replace('\n',' ').strip()
except:
    public_records = 'none'

try:
    inquiries = soup.find('section', attrs={'id': 'simple-inquiries'}).text.replace('\n',' ').strip()
except:
    inquiries = 'none'

other_dict = {'cons_statement':cons_statement ,'public_records': public_records,'inquiries': inquiries}
finlarr.append(other_dict)

print(finlarr)

filename = "Transunion_dispute.json"
print("File_Name--", filename)
with open(filename, "a+") as f:
    sorted = json.dumps(finlarr, indent=4)
    f.write(sorted)

driver.execute_script('window.print();')