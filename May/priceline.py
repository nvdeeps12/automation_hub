import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import re
import time
# import psycopg2 as sql
import datetime
from datetime import date
import sys
import mysql.connector
import schedule


chromepath = '/home/navdeep/Projects/Fiver/Drivers/chromedriver'

connection = mysql.connector.connect(
    host="localhost",
    user="root",
    password="password",
    database="google"
)

    

mycursor = connection.cursor()

# q = "CREATE TABLE IF NOT EXISTS `priceline_flights` (`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `airline_name` TEXT DEFAULT NULL,`departures` TEXT DEFAULT NULL,`duration` TEXT DEFAULT NULL,`arrival` TEXT DEFAULT NULL,`price` TEXT DEFAULT NULL,`origin` TEXT DEFAULT NULL,`Location` TEXT DEFAULT NULL,`images` TEXT DEFAULT NULL,`trip` TEXT DEFAULT NULL,`departure_date` TEXT DEFAULT NULL,`return_date` TEXT DEFAULT NULL,`Source` TEXT DEFAULT NULL,`created_at` TIMESTAMP NOT NULL,`link` TEXT DEFAULT NULL)"
# mycursor.execute(q)
# connection.commit()


# driver.get('https://www.trvl.priceline.com')
# time.sleep(1)


links_arr = []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INSTRUCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Install Libraries ->(requests, bs4, selenium, xlwt, re, time)
# Just Enter desired inputs down there
# try to make all vars in given formate :-)

# Needed Vars
origin = ['AMS','ACY','AUS','BOS','CUN','SJD','CTG','CHS','CHI','DFW','DEN','FLL','HAV','KEF','SJO','EYW','RNO','LAS','LAX','MIA','YUL','MYR','BNA','MSY','EWR','MCO','PTY','SJO','SJU','POP','SAN','SXM','JTR','JMK','JFK','SAV','PHX','BRO','TPA','ATL','PDX','PSP','SDF','ECP','NAS','MDE','GSP','CUN','MKE','PBI']
LocDep = ['DFW','EWR',
'FLL','MYR','MCO','TPA',
'BOS','CHI','MIA','EWR',
'CHI','FLL','EWR',
'CHI','FLL','EWR',
'LAX','MIA','YUL','MCO',
'LAX','MIA','EWR',
'AUS','BOS','CHI','EWR',
'BOS','FLL','EWR',
'BOS','CHI','EWR',
'BOS','LAX','EWR',
'BOS','CHI','EWR',
'BOS','MIA','EWR',
'BOS','MIA','EWR',
'BOS','CHI','EWR',
'CHI','EWR','SAN','PHX',
'CHI','LAX','MIA','EWR',
'BOS','CHI','EWR',
'BOS','FLL','EWR',
'BOS','FLL','LAX','EWR',
'BOS','FLL','EWR',
'BOS','FLL','LAX',
'ACY','BOS','CHI','DFW',
'BOS','FLL','EWR',
'BOS','CHI','FLL',
'CHI','LAX','MIA','EWR',
'BOS','LAX','EWR',
'BOS','CHS','FLL,EWR',
'AUS','BOS','DFW,EWR',
'AUS','DEN','LAX,EWR',
'CHI','FLL','EWR',
'BOS','CHI','MIA',
'BOS','MIA','EWR',
'BOS','MIA','EWR',
'BOS','CHI','FLL',
'LAS','EWR','TPA',
'BOS','CHI','EWR']
departure_date = '20200521'
adults = '1'
Type = 'Economy'

# Date Format is YYYY/MM/DD
return_date = None
breakpoint = 3
sources = 'Priceline'
if return_date != None:
    trip = 'Roundtrip'
    print('Running For Roundtrip')
else:
    trip = 'Oneway'
    print('Running For Oneway')


def scroll(driver, timeout):
    scroll_pause_time = timeout
    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        # Scroll down to bottom
        driver.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(scroll_pause_time)
        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            # If heights are the same it will exit the function
            break
        last_height = new_height


class PriceLine():
        
    def job2(self):
        if date.today().day==1:    
            Pcobj = PriceLine()
            Pcobj.get_links()

    def check_isexits(self, page_link):
        try:
            page_link = "'%s'" % page_link
            qry2 = "SELECT count(*) FROM wp_scrape_priceline_flight WHERE link="+page_link+';'

            mycursor.execute(qry2)
            record = mycursor.fetchall()
            data_rec = str(record[0])
            rec = data_rec.split('(')[1]
            data_rec = rec.split(',)')[0]
            data_rec = int(data_rec)
            return data_rec
        except:
            import sys
            print(sys.exc_info())
            return 0
    def insert_sql(self,city, first_from_city_short, return_from_city_short, first_to_city_short, price,
                                start_date, first_airline_logo, return_airline_logo, first_depart_time, first_arrive_time, return_depart_time, return_arrive_time, first_stop,first_time,return_stop,return_time,first_stop_at,return_stop_at,link,return_to_city_short):
        try:
            arr = []
           

            location_link = link.split('/m/')[1]
            widget = 2
            ads = 2
            from datetime import date

            today = date.today()
            print("Today's date:", today)
            dates = str(today)
            # print(date)
            
            data = (city, first_from_city_short, return_from_city_short, first_to_city_short, price,start_date, first_airline_logo, return_airline_logo, first_depart_time, first_arrive_time, return_depart_time, return_arrive_time, first_stop,first_time,return_stop,return_time,first_stop_at,return_stop_at,link,location_link,widget,ads,dates,return_to_city_short)
            arr.append(data)

            data_tuple = tuple(arr)
            if len(data_tuple) == 1:
                data_tuple = str(data_tuple).replace('),)', '))')

            query = "INSERT INTO wp_scrape_priceline_flight("
            query += "`city`,`first_from_city_short`,`return_from_city_short`,`first_to_city_short`,`price`,`start_date`,`first_airline_logo`,`return_airline_logo`,`first_depart_time`,`first_arrive_time`,`return_depart_time`,`return_arrive_time`,`first_stop`,`first_time`,`return_stop`,`return_time`,`first_stop_at`,`return_stop_at`,`link`,`location_link`,`widget`,`ads`,`date`,`return_to_city_short`) VALUES"
            query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),"NULL").replace('((', '(').replace('))', ')')

            # print(query)
            mycursor.execute(query)
            connection.commit()
            print(mycursor.rowcount, "record inserted.")
        except:
            print('--')
            print(sys.exc_info())

    def log(self, logmsg, logtype):
        """
        method to write message in log file
        logmsg : message that need to write in log file
        logtype : type of log like info, warn, error
        """
        if logtype == 'info':
            self.logger.info(logmsg)
        if logtype == 'warn':
            self.logger.warn(logmsg)
        if logtype == 'error':
            self.logger.error(logmsg)

    def get_links(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--disable-gpu')
        # options.add_argument('--headless')
        # options.add_extension('Setup.crx')
        # chromedriver_path = 'F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe'

        driver = webdriver.Chrome(executable_path=chromepath, options=options)

        
        row = 0
        for ori in origin:
            for loc in LocDep:
                
                if return_date != None:
                    page = 'https://www.priceline.com/m/fly/search/'+ori+'-'+loc + \
                        '-'+departure_date+'/'+loc+'-'+ori+'-'+return_date+'/'+'?cabin-class=ECO&no-date-search=false&search-type=1111&num-adults='+str(adults)+str('&Currency=Usd')

                else:
                    page = 'https://www.priceline.com/m/fly/search/'+ori+'-'+loc + \
                        '-'+departure_date+'/'+'alt-dates=false&cabin-class=ECO&num-adults='+str(adults)+'&num-children=0&num-infants=0&num-youths=0&search-type=11'

                import time
                page = str(page)
                if self.check_isexits(page) > 0:
                    print("Skiping ........")
                    continue

                print('Page--', page)
                print('Breakpoint is -->', breakpoint)
                # page = 'https://www.trvl.priceline.com/flights/results?departureFrom=NYC&departureFromType=0&arrivalTo=LAX&arrivalToType=1&departDate=2020-05-06&returnDate=2020-05-09&searchType=2&cabinType=Economy&adults=1'
                driver.get(page)
                time.sleep(3)
                # Scrolled

                # scroll(driver, 5)

                soup = BeautifulSoup(driver.page_source, u'html.parser')
                flight_cards = soup.findAll(
                    'li', attrs={'class': 'wrapComponentsRenderInView__InViewWrapper-cv93rp-0 cJUWNe'})
                arr = []
                # airline_arr = []
                # dep_arr = []
                # duration_arr = []
                # arrival_arr = []
                # airmg_arr = []

                for fc in flight_cards:
                    try:
                        city = ''
                    except:
                        city = 'Not Avaliable'
                    
                    try:
                        first_from_city_short = soup.find('abbr',attrs={'data-test':'left-airport-code'}).text.strip()
                    except:
                        first_from_city_short = 'Not Avaliable'
                        
                    try:
                        return_from_city_short = soup.findAll('abbr',attrs={'data-test':'left-airport-code'})[1].text.strip()
                    except:
                        return_from_city_short = 'Not Avaliable'
                        
                        
                    try:
                        first_to_city_short = soup.find('abbr',attrs={'data-test':'right-airport-code'}).text.strip()
                    except:
                        first_to_city_short = 'Not Avaliable'
                        
                    try:
                        return_to_city_short = soup.findAll('abbr',attrs={'data-test':'right-airport-code'})[1].text.strip()
                    except:
                        return_to_city_short = 'Not Avaliable'
                        
                    
                    # break
                    try:
                        start_date = soup.findAll('div',attrs={'class':'DayDateBox__DateTextNoWrap-sc-1ln967b-2 cjbvha sc-hmXxxW iEFdWe'})[0].text.strip()
                    except:
                        start_date = 'Not Avaliable'
                        
                    try:
                        first_airline_logo = soup.findAll('img')[0]['src']
                    except:
                        first_airline_logo = 'Not Avaliable'
                        
                    try:
                        return_airline_logo = soup.findAll('img')[1]['src']
                    except:
                        return_airline_logo = 'Not Avaliable'
                        
                    # TODO: 4 Fields to be Added 
                    
                    try:
                        first_depart_time = soup.findAll('div',attrs={'data-test':'time-airport-departure'})[0].find_next('time').text.strip()
                    except:
                        first_depart_time = 'Not Avaliable'
                        
                    try:
                        first_arrive_time = soup.findAll('div',attrs={'data-test':'time-airport-arrival'})[0].find_next('time').text.strip()
                    except:
                        first_arrive_time = 'Not Avaliable'
                        
                    try:
                        return_depart_time = soup.find('div',attrs={'data-test':'time-airport-departure'}).find_next('time').text.strip()
                    except:
                        return_depart_time = 'Not Avaliable'
                        
                    try:
                        return_arrive_time = soup.findAll('div',attrs={'data-test':'time-airport-arrival'})[1].find_next('time').text.strip()
                    except:
                        return_arrive_time = 'Not Avaliable'
                        
                    try:
                        first_stop = soup.findAll('span',attrs={'class':'Slice__StyledStopsText-sc-1mltl6u-12 lwtPK sc-cqCuEk glRZYv'})[0].text.strip()
                    except:
                        first_stop = 'Not Avaliable'
                        
                    try:
                        first_time = soup.findAll('span',attrs={'class':'Slice__StyledStopsText-sc-1mltl6u-12 lwtPK sc-cqCuEk glRZYv'})[0].find_next('time').text.strip()
                    except:
                        first_time = 'Not Avaliable'
                        
                    try:
                        return_stop = soup.findAll('span',attrs={'class':'Slice__StyledStopsText-sc-1mltl6u-12 lwtPK sc-cqCuEk glRZYv'})[1].text.strip()
                    except:
                        return_stop = 'Not Avaliable'
                        
                    try:
                        return_stop_at = soup.findAll('p', attrs={'data-test': 'stop-text'})[1].text.strip()
                    except:
                        return_stop_at = 'Not Avaliable'
                        
                    
                    try:
                        price = soup.find('span', attrs={'data-test': 'rounded-dollars'}).text.strip()
                        # price = price+str(' USD')
                        # print(price)
                    except:
                        price = None
                        
                    try:
                        return_time = soup.findAll('div',attrs={'data-test':'duration-timebox'})[1].find_next('time').text.strip()
                    except:
                        return_time = None
                        
                    try:
                        first_stop_at = soup.findAll('p', attrs={'data-test': 'stop-text'})[0].text.strip()
                    except:
                        first_stop_at = None

                    link = page
                    
                    
                    row = row+1
                    
                    
                    self.insert_sql(city, first_from_city_short, return_from_city_short, first_to_city_short, price,
                                    start_date, first_airline_logo, return_airline_logo, first_depart_time, first_arrive_time, return_depart_time, return_arrive_time, first_stop,first_time,return_stop,return_time,first_stop_at,return_stop_at,link,return_to_city_short)

                    
                    print('Done-->', row)

                    print('--'*50)


# driver.quit()
objs = PriceLine()
objs.get_links()

schedule.every().day.at("08:00").do(objs.job2)
# schedule.every(1).minute.do(job2) 
while True:
    try:
        print(datetime.datetime.now())
        schedule.run_pending()
        time.sleep(1)
    except:
        pass
