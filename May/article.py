import requests
from bs4 import BeautifulSoup
import xlwt,datetime
from logs.logHandler import LogHelper
from datetime import date
import schedule
import time

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('Sheet')
sheet1.write(0, 0, 'Title')
sheet1.write(0, 1, 'Video url')
sheet1.write(0, 2, 'Teaser Text')
sheet1.write(0, 3, 'Img')
sheet1.write(0, 4, 'Date Publish')
sheet1.write(0, 5, 'Topics')
sheet1.write(0, 6, 'Author')
sheet1.write(0, 7, 'Media Type')
sheet1.write(0, 8, 'Duration in min (Article, Video)')
sheet1.write(0, 9, 'Pages (Books, Report)')
sheet1.write(0, 10, 'Publication Link')


# Add Category and type here

category = 'TLPublications'
type = 'Article,Book,Report,Video'

# Output File Name
filename = 'Article_'+str(datetime.datetime.now())+str('.json')
output_file_name = filename
print(output_file_name)

article_arr =[]
class Article():
    
    def __init__(self):
        self.logger = LogHelper().create_rotating_log('logs_article', 'article')

    def log(self, logmsg, logtype):
        """
        method to write message in log file
        logmsg : message that need to write in log file
        logtype : type of log like info, warn, error
        """
        if logtype == 'info':
            self.logger.info(logmsg)
        if logtype == 'warn':
            self.logger.warn(logmsg)
        if logtype == 'error':
            self.logger.error(logmsg)
            
    def job2(self):
        if date.today().day == 1:
            Acobj = Article()
            Acobj.get_articles()
               
    
    def get_articles(self):
        
        try:
            self.logger.info('Process Started')
            link = 'https://www.imd.org/api/search?Term=&Sort=Date&Filter=category:' + \
                str(category)+';type:'+str(type)+';&Page='

            print(link)
            con = requests.get(url=link+str(1))
            page_count = con.json()['PageCount']
            
            # page_count = 1
            row = 0
            error = []
            # rem_p = [23, 24, 28, 33, 37, 44, 45, 49, 50, 54]

            for pn in range(page_count):
                
                MSG = "Running Page->"+str(pn)
                self.logger.info(MSG)
                
                # LOGGING PAGE
                print('Page running -> '+str(pn))
                con1 = requests.get(
                    url=link+str(pn))
                # soup=BeautifulSoup(con.content,u'html.parser')
                print(con1.status_code)
                data = con1.json()['Items']

                if len(data) == 0:
                    error.append(pn)
                for d in data:
                    # pprint.pprint(d)
                    title = d['Title']
                    # print(title)
                    url = d['Link']
                    print(url)
                    try:
                        video_url = str(d['MainMultimedia']).split(
                            'src="')[1].split('"')[0]
                    except:
                        video_url = 'none'
                    print(video_url)
                    Cat = str(d['Type'])
                    author = d['Authors']
                    print(author)
                    page = d['Length']
                    imageurl = "https://www.imd.org"+str(d['ImageLink'])
                    publishing_date = str(d['PublicationDate']).split('T')[0]
                    topics = d['Topics']
                    topic_ar = []
                    try:
                        for t in topics:
                            topic_ar.append(t+", ")
                    except:
                        topic_ar = 'None'

                    con = requests.get(url=url)
                    soup = BeautifulSoup(con.content, u'html.parser')
                    try:
                        duration = soup.find('div', attrs={
                            'class': 'article-meta'}).find('svg').find_previous('div').text.strip()
                    except:
                        duration = ' '
                    if Cat == 'Book':
                        duration = ' '
                    elif Cat == 'Report':
                        duration = ' '
                    try:
                        all_text = soup.find(
                            'div', attrs={'class': 'content-body'}).findAll('p')
                        teaser_text = all_text[1].text.strip().split(all_text[2].text.strip())[0]
                    except:
                        all_text = soup.find(
                            'div', attrs={'class': 'content-body'}).findAll('p')
                        teaser_text = all_text[1].get_text(separator=" ").strip().split(' ')
                        teaser_text = ''
                    # if len(teaser_text)>60:
                        teaser_text = " ".join(teaser_text[:70])
                    print(teaser_text)
                    row += 1
                    sheet1.write(row, 0, title)
                    sheet1.write(row, 1, video_url)
                    sheet1.write(row, 2, teaser_text)
                    sheet1.write(row, 3, imageurl)
                    sheet1.write(row, 4, publishing_date)
                    sheet1.write(row, 5, topic_ar)
                    sheet1.write(row, 6, author)
                    sheet1.write(row, 7, Cat)
                    sheet1.write(row, 8, duration)
                    sheet1.write(row, 9, page)
                    sheet1.write(row, 10, url)
                    # wb.save(output_file_name)
                    print('Done-',row)
                    print('______'*20)
                    
                    dict1 = {
                        'id':row,
                        'title':title,
                        'video_url':video_url,
                        'teaser_text':teaser_text,
                        'images':imageurl,
                        'publish_date':publishing_date,
                        'topic_arr':topic_ar,
                        'author':author,
                        'category':Cat,
                        'duration':duration,
                        'page':page,
                        'url':url
                        
                    }
                    article_arr.append(dict1)
                    # Appending Articles
            import json
            # file_name = 'Article.json'
            print('File_Name--', filename)
            with open(filename, 'a+') as f:
                sorted = json.dumps(article_arr, indent=4)
                f.write(sorted)
                print('Json Generated')
                # Json Genereated Successfully
                    
                    

            self.logger.info('Process Finished Successfully')

        except:
            import sys
            self.logger.error(sys.exc_info())
        

obj = Article()
# obj.get_articles()

schedule.every().day.at("08:00").do(obj.job2)
# schedule.every(1).minute.do(job2) 
while True:
    try:
        schedule.run_pending()
        time.sleep(1)
    except:
        pass