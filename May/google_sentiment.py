from google.cloud import language_v1
from google.cloud.language_v1 import enums
import requests, json
dataarr = []
rows = []
# wbw = xlwt.Workbook()
# sheet1 = wbw.add_sheet('Sentiments')
# row = 0
# sheet1.write(0, 0, 'Url')
# sheet1.write(0, 1, 'Sentiment_Score')
# sheet1.write(0, 2, 'Sentiment_Magnitude')
# # sheet1.write(0, 3, 'Entities_Sentiment')
# sheet1.write(0, 3, 'Entities')
import csv
loc = "updated url list.csv"
file_name = 'google.json'

with open(loc, 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
        if 'http' not in row[0]:
            continue
        if 'https://www.einstein.edu/education/residency/surgery' in row[0]:
            continue
        rows.append(row[0])
        
import mysql.connector
import sys
connection = mysql.connector.connect(
    host="localhost",
    user="root",
    password="password",
    database="google"
    )
mycursor = connection.cursor()


def check_isexits(page_link):
    try:
        page_link = "'%s'"%page_link
        qry2="SELECT count(*) FROM sentiments WHERE url="+page_link+';'
        
        mycursor.execute(qry2)
        record=mycursor.fetchall()
        data_rec=str(record[0])
        rec=data_rec.split('(')[1]
        data_rec=rec.split(',)')[0]
        data_rec = int(data_rec)
        return data_rec
    except:
        import sys
        print(sys.exc_info())
        return 0
    
def check_count():
    try:
        qry2="SELECT count(*) FROM sentiments;"
        
        mycursor.execute(qry2)
        record=mycursor.fetchall()
        data_rec=str(record[0])
        rec=data_rec.split('(')[1]
        data_rec=rec.split(',)')[0]
        data_rec = int(data_rec)
        return data_rec
    except:
        import sys
        print(sys.exc_info())
        return 0
    
def insert_sql(url,score,sentiment_magnitude,entities):
    try:
        
        arr = []
        score = "'%s'"%score
        entities = "'%s'"%entities
        
        data = (url,score,sentiment_magnitude,entities)
        arr.append(data)

        data_tuple = tuple(arr)
        if len(data_tuple) == 1:
                data_tuple = str(data_tuple).replace('),)', '))')
        
        query = "INSERT INTO sentiments("
        query +=  "url,score,sentiment_magnitude,entities) VALUES"
        query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),"NULL").replace('((', '(').replace('))', ')')
        
        print(query)
        mycursor.execute(query)
        connection.commit()
        print(mycursor.rowcount, "record inserted.")
    except:
        print(sys.exc_info())
        



def sentiment_entity_analyze(content_uri):
	"""
	Analyzing Sentiment in text file stored in Cloud Storage

	Args:
	content_uri: URL for which need to get sentiment
	"""
	try:
		client = language_v1.LanguageServiceClient.from_service_account_json('nlp.json') # path of json file
		
		# The text to analyze
		res = requests.get(content_uri)
		text = res.content
		
		# Available types: PLAIN_TEXT, HTML
		type_ = enums.Document.Type.HTML

		# Optional. If not specified, the language is automatically detected.
		# For list of supported languages:
		# https://cloud.google.com/natural-language/docs/languages

		#language = "en" 
		document = {"content": text, "type": type_}#, "language": language}

		# Available values: NONE, UTF8, UTF16, UTF32
		encoding_type = enums.EncodingType.UTF8
		
		# getting sentiment
		response = client.analyze_sentiment(document, encoding_type=encoding_type)
		
		sentiment_score = response.document_sentiment.score
		sentiment_magnitude = response.document_sentiment.magnitude
		response_dict = {
			'sentiment_score': sentiment_score,
			'sentiment_magnitude': sentiment_magnitude
		}
		
		# getting entities
		response = client.analyze_entity_sentiment(document, encoding_type=encoding_type)
		entities = []
		
		for entity in response.entities:
			ent_dict = {
					'entity_name': entity.name,
					'entity_type': enums.Entity.Type(entity.type).name,
					'entity_sentiment':entity.sentiment
				}
			
			entities.append(ent_dict)
		response_dict.update({'entities':entities})

		return response_dict
	except:
		return {}



## url to get sentiment

def inser_data():

    for ur in rows:
        url = ur
        print(url)

        is_exists = check_isexits(url)
        if is_exists>0:
            print('Continue')
            continue
        
        resp = sentiment_entity_analyze(url)
        
        try:
            urls = url
        except:
            urls = 'None'

        try:
            score = resp['sentiment_score']
        except:
            score = 'None'
            
        try:
            sentiment_magnitude = resp['sentiment_magnitude']
        except:
            sentiment_magnitude = 'None'
    
        try:
            entity_sentiment = resp['entity_sentiment']
        except:
            entity_sentiment = 'None'
    
        
        try:
            entities = resp['entities']
        except:
            entities = 'None'
        
        insert_sql(url,score,sentiment_magnitude,entities)
        print('--'*50)
        con = check_count()
        print('Updated Count--',con)
        print('--'*50)
	# print('--'*80)
 
inser_data()
 
