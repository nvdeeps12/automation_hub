import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd, json
import time
from selenium import webdriver

options = webdriver.FirefoxOptions()
options.add_argument("--disable-gpu")


# Appending In Arr
account_arr = []
payments_arr = []

# options.add_argument('--headless')
driver = webdriver.Firefox(
    executable_path="/home/nvdeep/Projects/Fiver/Drivers/geckodriver", options=options
)
driver.get("https://membership.tui.transunion.com/tucm/login.page")

# Use WILLIAM7787 a77BOVYAN!
username_field = driver.find_element_by_xpath(
    '//*[@id="c1354063416773"]/form/div/div/section[3]/div/div[1]/div/div[1]/input'
)
username_field.send_keys("WILLIAM7787")
time.sleep(1)
password_field = driver.find_element_by_xpath(
    '//*[@id="c1354063416773"]/form/div/div/section[3]/div/div[1]/div/div[2]/input'
)
password_field.send_keys("a77BOVYAN!")
time.sleep(1)
driver.find_element_by_xpath(
    '//*[@id="c1354063416773"]/form/div/div/section[3]/div/div[1]/div/button'
).click()
# Waitinnnn
time.sleep(5)

# Clicked The Continue Button
try:
    driver.find_element_by_xpath(
        '//*[@id="bodyContent"]/div/div[2]/div/div/div[2]/div[1]/button'
    ).click()
    time.sleep(10)
except:
    pass


# Hitting Credit Report Page
driver.get("https://membership.tui.transunion.com/tucm/creditReport_TUCM.page?")

# Clicking on Each Report to Expand
# Real Estate
try:
    driver.find_element_by_xpath(
        '//*[@id="CR-Accounts-RealEstate"]/div/div[2]/div[1]/div[1]'
    ).click()
    time.sleep(2)
except:
    pass

# Credit Revolving Reports
for e in range(30):
    try:
        element = (
            '//*[@id="CR-Accounts-CreditRevolving"]/div/div[2]/div['
            + str(e)
            + "]/div[1]/div"
        )
        # print(element)
        driver.find_element_by_xpath(element).click()
        time.sleep(2)
        print("--" * 30)
        
    except:
        pass

# Cr Installments
for e in range(30):
    try:
        element = (
            '//*[@id="CR-Accounts-Installment"]/div/div[2]/div['
            + str(e)
            + "]/div[1]/div"
        )
        # print(element)
        driver.find_element_by_xpath(element).click()
        time.sleep(2)
        print("--" * 30)

        
    except:
        pass

soup = BeautifulSoup(driver.page_source, u"html.parser")

divs = soup.findAll("div", attrs={"class": "report-account-table"})
# print(divs)

for d in divs:
    try:
        report_name = d["data-selenium"]
        report_name = str(report_name).split("-")[-1].strip()
        # a_dict.update({"report_name": report_name})
    except:
        report_name = "None"

    print("Rep-->", report_name)
    cells = d.find_next("div", attrs={"class": "tbody"}).findAll(
        "div", attrs={"class": "frow report-row"}
    )

    for row in cells:
        try:
            keys = row.find("div", attrs={"class": "fhcell"}).text.strip()
        except:
            keys = "None"

        try:
            values = row.find(
                "div", attrs={"class": "tu-cell fcell dispute-cell"}
            ).text.strip()
        except:
            values = "None"

        # print("Key-->", keys)
        # print("Values", values)

        a_dict = {"report": report_name, keys: values}
        account_arr.append(a_dict)

import json

filename = "Transunion_AccountStatus.json"
print("File_Name--", filename)
with open(filename, "a+") as f:
    sorted = json.dumps(account_arr, indent=4)
    f.write(sorted)
    print("Json Generated")



# ---------------------------------------------------------------------
# Clicking For Payment Status
# ---------------------------------------------------------------------

# Loading For Payments
driver.get("https://membership.tui.transunion.com/tucm/creditReport_TUCM.page?")
time.sleep(5)


try:
    driver.find_element_by_xpath(
        '//*[@id="CR-Accounts-RealEstate"]/div/div[2]/div[2]/div/div/div[1]/div[2]'
    ).click()
    time.sleep(2)
    driver.find_element_by_link_text("Expand All").click()
    # driver.find_elements_by_xpath('//*[@id="CR-Accounts-RealEstate"]/div/div[2]/div[2]/div/div/div[3]/div[2]/a').click()
except:
    pass

# Credit Revolving Reports
for e in range(30):
    try:
        # //*[@id="CR-Accounts-CreditRevolving"]/div/div[2]/div[4]/div/div/div[1]/div[2]
        element = (
            '//*[@id="CR-Accounts-CreditRevolving"]/div/div[2]/div['
            + str(e)
            + "]/div/div/div[1]/div[2]"
        )
        # print(element)
        driver.find_element_by_xpath(element).click()
        time.sleep(2)
        # Expand Button
        # ex_button = '//*[@id="CR-Accounts-CreditRevolving"]/div/div[2]/div[4]/div/div/div['+str(e)+']/div[2]/a'
        driver.find_element_by_link_text("Expand All").click()
        print("--" * 30)
    except:
        pass

# Cr Installments
for e in range(30):
    try:
        element = (
            '//*[@id="CR-Accounts-Installment"]/div/div[2]/div['
            + str(e)
            + "]/div/div/div[1]/div[2]"
        )
        # print(element)
        driver.find_element_by_xpath(element).click()
        time.sleep(2)

        # ex_button = '//*[@id="CR-Accounts-Installment"]/div/div[2]/div[4]/div/div/div['+str(e)+']/div[2]/a'
        # driver.find_element_by_xpath(ex_button).click()
        driver.find_element_by_link_text("Expand All").click()
        print("--" * 30)
    except:
        pass


print("--" * 50)
time.sleep(3)
soup = BeautifulSoup(driver.page_source, u"html.parser")

divs = soup.findAll("div", attrs={"class": "report-account-table"})
# print(divs)

for d in divs:
    try:
        report_name = d["data-selenium"]
        report_name = str(report_name).split("-")[-1].strip()
        # a_dict.update({"report_name": report_name})
    except:
        report_name = "None"

    print("Rep-->", report_name)
    cells = d.find_next("div", attrs={"class": "tbody"}).findAll(
        "div", attrs={"class": "frow report-row"}
    )

    for row in cells:
        try:
            keys = row.find("div", attrs={"class": "fhcell"}).text.strip()
        except:
            keys = "None"

        try:
            values = row.find(
                "div", attrs={"class": "tu-cell fcell dispute-cell"}
            ).text.strip()
        except:
            values = "None"

        # print("Key-->", keys)
        # print("Values", values)

        p_dict = {"report": report_name, keys: values}
        
        print(p_dict)
        payments_arr.append(p_dict)

import json

filename = "Transunion_PaymentStatus.json"
print("File_Name--", filename)
with open(filename, "a+") as f:
    sorted = json.dumps(payments_arr, indent=4)
    f.write(sorted)
    print("Json Generated")

 driver.execute_script('window.print();')