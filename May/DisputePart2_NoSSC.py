



email = 'WILLIAM787@YMAIL.COM'
socialsec1 = '618'
socialsec2 = '25'
socialsec3 = '2314'

import requests
from bs4 import BeautifulSoup
import xlwt,json
import xlrd
import time
from selenium import webdriver
finlarr = []
email = input('Enter Email->')
socialsec1 = input('Enter Social Security first part -->')
socialsec2 = input('Enter Social Security second part -->')
socialsec3 = input('Enter Social Security third part -->')
dobday = input('Enter Dob day->')
dobmonth = input('Enter Dob month->')
dobyear = input('Enter Dob year->')
first = input('Enter First Name->')
mid = input('Enter Middle Name->')
lname = input('Enter Last Name->')
city = input('Enter City->')
state = input('Enter State ->')
zipcode = input('Enter Zip Code->')
address = input('Enter Address->')

# email = 'DEBBIE2363@HOTMAIL.COM'
# socialsec1 = '069'
# socialsec2 = '56'
# socialsec3 = '4706'
# dobday = '08'
# dobmonth = '23'
# dobyear = '1963'
# first = 'DEBBIE'
# mid = 'R'
# lname = 'BROWN'
# city = 'HUSTON'
# state = 'TX'
# zipcode = '77048'
# address = '5310 RUSSELVILLE RD'



appState = {
    "recentDestinations": [
        {
            "id": "Save as PDF",
            "origin": "local",
            "account": ""
        }
    ],
    "selectedDestinationId": "Save as PDF",
    "version": 2
}

profile = {
    'printing.print_preview_sticky_settings.appState': json.dumps(appState)}

options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
options.add_experimental_option("useAutomationExtension", False)
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('prefs', profile)
options.add_argument('--kiosk-printing')
# options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver', options=options)

soup=BeautifulSoup(driver.page_source,u'html.parser')


driver.get('https://www.experian.com/ncaconline/dispute')
time.sleep(10)
fname = driver.find_element_by_id('reg-fna-pi-txt-firstName')
fname.click()
fname.send_keys(first)

fname = driver.find_element_by_id('reg-fna-pi-txt-middleName')
fname.click()
fname.send_keys(mid)
fname = driver.find_element_by_id('reg-fna-pi-txt-lastName')
fname.click()
fname.send_keys(lname)
fname = driver.find_element_by_id('reg-fna-pi-txt-address')
fname.click()
fname.send_keys(address)

fname = driver.find_element_by_id('reg-fna-pi-txt-city')
fname.click()
fname.send_keys(city)

fname = driver.find_element_by_id('reg-fna-pi-dd-state')
fname.click()
fname.send_keys(state)

fname = driver.find_element_by_id('reg-fna-pi-txt-zipCode')
fname.click()
fname.send_keys(zipcode)

ssnxpath1 = driver.find_element_by_id('reg-fna-pi-txt-ssn1')
ssnxpath1.send_keys(socialsec1)
time.sleep(1)
ssnxpath2 = driver.find_element_by_id('reg-fna-pi-txt-ssn2')
ssnxpath2.send_keys(socialsec2)
time.sleep(1)
ssnxpath3 = driver.find_element_by_id('reg-fna-pi-txt-ssn3')
ssnxpath3.send_keys(socialsec3)
time.sleep(1)



ssnxpath1 = driver.find_element_by_id('reg-fna-pi-txt-dob1')
ssnxpath1.send_keys(dobday)
time.sleep(1)
ssnxpath2 = driver.find_element_by_id('reg-fna-pi-txt-dob2')
ssnxpath2.send_keys(dobmonth)
time.sleep(1)
ssnxpath3 = driver.find_element_by_id('reg-fna-pi-txt-dob3')
ssnxpath3.send_keys(dobyear)
time.sleep(1)

fname = driver.find_element_by_id('reg-fna-pi-txt-emailAddress')
fname.click()
fname.send_keys(email)

fname = driver.find_element_by_id('reg-fna-pi-txt-emailAddressConfirm')
fname.click()
fname.send_keys(email)

driver.find_element_by_id(
    'reg-fna-pi-chk-agreeTermsConditions').click()
driver.find_element_by_id(
    'reg-fna-pi-btn-submit').click()


# Getting Soup Things
time.sleep(20)
ressoup = BeautifulSoup(driver.page_source,u'html.parser')
try:
    title = ressoup.find('div', attrs={'id': 'section-titles'}).text.strip()
    print(title)
    if 'Your credit report' in title:
        time.sleep(5)
        driver.find_element_by_id('expand-submit').click()
        print('Clicked on Expandall')
        print('---'*40)
        time.sleep(10)

        soup = BeautifulSoup(driver.page_source, u'html.parser')

        try:
            personal_info = soup.find('div', attrs={'id': 'report-box-personal-info'}).text.replace('\n', ' ').replace('\t',' ').strip()
        except:
            personal_info = 'none'

        finlarr.append({'personal_information': personal_info})

        try:
            personal_statement = soup.find('div', attrs={'id': 'report-box-personal-statements'}).text.replace('\n', ' ').replace('\t',' ').strip()
        except:
            personal_statement = 'none'

        finlarr.append({'personal_statements': personal_statement})

        try:
            negative_items = soup.find('div', attrs={'id': 'report-box-potentially-negative'}).text.replace('\n', ' ').replace('\t',' ').strip()
        except:
            negative_items = 'none'

        finlarr.append({'potentially_negative_items': negative_items})

        try:
            accounts = soup.find('div', attrs={'id': 'report-box-good-standing'}).text.replace('\n', ' ').replace('\t',' ').strip()
        except:
            accounts = 'none'

        finlarr.append({'accounts_in_good-standing': accounts})

        try:
            credit_inquiry = soup.find('div', attrs={'id': 'report-box-credit-inquiries'}).text.replace('\n', ' ').replace('\t',' ').strip()
        except:
            credit_inquiry = 'none'

        finlarr.append({'credit_inquiries': credit_inquiry})

        filename = "Transunion_dispute_part2NoSSc.json"
        print("File_Name--", filename)
        with open(filename, "a+") as f:
            sorted = json.dumps(finlarr, indent=4)
            f.write(sorted)

        driver.execute_script('window.print();')