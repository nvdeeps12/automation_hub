import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd

import time
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument("--disable-gpu")
# options.add_argument('--headless')
driver = webdriver.Chrome(
    executable_path="/home/navdeep/Projects/Fiver/Drivers/chromedriver", options=options
)


def get_gender():
    wb2 = xlwt.Workbook()
    sheet1 = wb2.add_sheet("Gender Decoder")

    sheet1.write(0, 0, "Url")
    sheet1.write(0, 1, "Nutralism")
    sheet1.write(0, 2, "Masculine")
    sheet1.write(0, 3, "Feminine")

    loc = "Pd.xlsx"
    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    print(sheet.nrows)
    row = 0
    for i in range(1, sheet.nrows + 1):
        try:
            textoanalize = sheet.cell_value(i, 3)
            url = sheet.cell_value(i, 2)
            # print(url)

            if len(url) < 5:
                continue

            driver.get("http://gender-decoder.katmatfield.com/")
            time.sleep(2)
            area = driver.find_element_by_xpath('//*[@id="texttotest"]')
            area.send_keys(textoanalize)

            driver.find_element_by_xpath("/html/body/div[2]/form/input[2]").click()
            time.sleep(5)

            soup = BeautifulSoup(driver.page_source, u"html.parser")
            # print(soup)
            div_con = soup.find("div", attrs={"class": "main-content"})
            # print(div_con)
            try:
                nutralism = div_con.find_next("h4").text.strip()
            except:
                nutralism = "None"
            try:
                masculine = (
                    soup.find("div", attrs={"id": "masculine"})
                    .find_next("ul")
                    .findAll("li")
                )

                mus_arr = []
                for m in masculine:
                    m = m.text.strip()
                    mus_arr.append(m)
                masculine = "masculine words in this ad are  " + str(mus_arr)
            except:
                masculine = "None"

            print(masculine)
            try:
                feminine = (
                    soup.find("div", attrs={"id": "feminine"})
                    .find_next("ul")
                    .findAll("li")
                )
                fe_arr = []
                for m in feminine:
                    m = m.text.strip()
                    fe_arr.append(m)

                feminine = "feminine words in this ad are  " + str(fe_arr)
            except:
                feminine = "None"

            print(feminine)
            row += 1

            sheet1.write(row, 0, url)
            sheet1.write(row, 1, nutralism)
            sheet1.write(row, 2, masculine)
            sheet1.write(row, 3, feminine)
            wb2.save("Gender_Decoder.xls")
            print("Done->", row)
            print("--" * 40)

            # driver.quit()
        except:
            import sys

            print(sys.exc_info())
            continue


get_gender()
