import requests
from bs4 import BeautifulSoup
import xlwt,json
import xlrd
import random
import time
from selenium import webdriver


appState = {
    "recentDestinations": [
        {
            "id": "Save as PDF",
            "origin": "local",
            "account": ""
        }
    ],
    "selectedDestinationId": "Save as PDF",
    "version": 2
}

profile = {
    'printing.print_preview_sticky_settings.appState': json.dumps(appState)}

options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
options.add_experimental_option("useAutomationExtension", False)
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('prefs', profile)
options.add_argument('--kiosk-printing')
# options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver', options=options)

soup=BeautifulSoup(driver.page_source,u'html.parser')


Reportarr =['1327218015']

email =input('Enter Email->')
socialsec1 =input('Enter Social Security first part -->')
socialsec2 =input('Enter Social Security second part -->')
socialsec3 = input('Enter Social Security third part -->')

# 618-25-2314
finlarr = []

for rn in Reportarr:
    try:
        driver.get('https://www.experian.com/ncaconline/dispute')
        try:
            driver.execute_script("window.onbeforeunload = function() {};")
        except:
            pass
        print('Trying with-',rn)
        time.sleep(10)
        report_numberxpath = driver.find_element_by_id('reg-capid-rn-txt-reportNumber')
        numb = random.choice(Reportarr)
        report_numberxpath.send_keys(numb)
        time.sleep(1)
        ssnxpath1 = driver.find_element_by_id('reg-capid-rn-txt-ssn1')
        ssnxpath1.send_keys(socialsec1)
        time.sleep(1)
        ssnxpath2 = driver.find_element_by_id('reg-capid-rn-txt-ssn2')
        ssnxpath2.send_keys(socialsec2)
        time.sleep(1)
        ssnxpath3 = driver.find_element_by_id('reg-capid-rn-txt-ssn3')
        ssnxpath3.send_keys(socialsec3)
        time.sleep(1)
        
        
        emailxpath = driver.find_element_by_id('reg-capid-rn-txt-emailAddress')
        emailxpath.send_keys(email)
        time.sleep(1)
        conemailxpath = driver.find_element_by_id('reg-capid-rn-txt-emailAddressConfirm')
        conemailxpath.send_keys(email)
        time.sleep(1)
        
        # Submitting 
        driver.find_element_by_id('reg-capid-rn-chk-agreeTermsConditions').click()
        time.sleep(1)
        driver.find_element_by_id('reg-capid-rn-btn-submit').click()
    except:
        pass
       
    # Loading The Soup to see Response
    time.sleep(20)
    ressoup = BeautifulSoup(driver.page_source,u'html.parser')
    try:
        title = ressoup.find('div', attrs={'id': 'section-titles'}).text.strip()
        print(title)
        if 'Your credit report' in title:
            time.sleep(5)
            driver.find_element_by_id('expand-submit').click()
            print('Clicked on Expandall')
            print('---'*40)
            time.sleep(10)

            soup = BeautifulSoup(driver.page_source, u'html.parser')

            try:
                personal_info = soup.find('div', attrs={'id': 'report-box-personal-info'}).text.replace('\n', ' ').replace('\t',' ').strip()
            except:
                personal_info = 'none'

            finlarr.append({'personal_information': personal_info})

            try:
                personal_statement = soup.find('div', attrs={'id': 'report-box-personal-statements'}).text.replace('\n', ' ').replace('\t',' ').strip()
            except:
                personal_statement = 'none'

            finlarr.append({'personal_statements': personal_statement})

            try:
                negative_items = soup.find('div', attrs={'id': 'report-box-potentially-negative'}).text.replace('\n', ' ').replace('\t',' ').strip()
            except:
                negative_items = 'none'

            finlarr.append({'potentially_negative_items': negative_items})

            try:
                accounts = soup.find('div', attrs={'id': 'report-box-good-standing'}).text.replace('\n', ' ').replace('\t',' ').strip()
            except:
                accounts = 'none'

            finlarr.append({'accounts_in_good-standing': accounts})

            try:
                credit_inquiry = soup.find('div', attrs={'id': 'report-box-credit-inquiries'}).text.replace('\n', ' ').replace('\t',' ').strip()
            except:
                credit_inquiry = 'none'

            finlarr.append({'credit_inquiries': credit_inquiry})

            filename = "Transunion_dispute_part2.json"
            print("File_Name--", filename)
            with open(filename, "a+") as f:
                sorted = json.dumps(finlarr, indent=4)
                f.write(sorted)

            driver.execute_script('window.print();')
            break
    except:
        continue
