import requests
import sys
import time
import re
from datetime import datetime
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import xlrd
# from urllib.parse import urlparse
session = requests.Session()
options = webdriver.ChromeOptions()
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Chrome(
    executable_path='F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe', options=options)
# c = session.get('https://www.nenaandco.com/collections/')
# pprint.pprint(session.cookies.get_dict())
# cookies = session.cookies.get_dict()

loc = ("path of file")

wb = xlrd.open_workbook(
    'F:/PycharmProjects/MainKam/fiver/May/product_links.xlsx')
# User Details
# card = wb.sheet_by_index(1)

# # For row 0 and column 0
# card.cell_value(0, 0)

card = wb.sheet_by_index(1)

# # For row 0 and column 0
card.cell_value(0, 0)
# Address info
firstname = card.cell_value(1, 0)
lastname = card.cell_value(1, 1)
address = card.cell_value(1, 2)
city = card.cell_value(1, 3)
country = card.cell_value(1, 4)
state = str(card.cell_value(1, 5))
pincode = int(card.cell_value(1, 6))
phone = int(card.cell_value(1, 7))
email = str(card.cell_value(1, 15))
print(firstname, lastname, city, country, state, pincode)

# card details

card_firstdigits = int(card.cell_value(1, 8))
card_lastdigits = int(card.cell_value(1, 9))
brand = card.cell_value(1, 10)
ex_month = int(card.cell_value(1, 11))
ex_year = int(card.cell_value(1, 12))
card_firstname = card.cell_value(1, 13)
card_lastname = card.cell_value(1, 14)


def UTCtoEST():
    current = datetime.now()
    return str(current) + ' EST'


links = wb.sheet_by_index(0)

# For row 0 and column 0
links.cell_value(0, 0)


for i in range(links.nrows):
    driver.get('https://www.nenaandco.com/cart')

    s = requests.session()
    home = 'nenaandco'
    ###Get Session Id###
    session = s.get('https://www.'+home+'.com/cart.js').json()
    sessionID = session['token']
    print('SessionID:', sessionID)
    link = links.cell_value(i, 0)
    print('Link ->'+link)
    ###ATC###

    con2 = s.get(url=link)
    pro_soup = BeautifulSoup(con2.content, u'html.parser')
    idd = pro_soup.find('input', attrs={
        'id': 'productselect'})['value']
    print(idd)
    print(UTCtoEST(), 'Adding item....')
    atcdata = {
        'action': 'add',
        'id': idd,
        'quantity': '1'
    }
    for atcurlRetry in range(1):
        atcURL = s.post('https://www.'+home+'.com/cart/add.js',
                        data=atcdata, allow_redirects=True)
        match = re.findall('"quantity":1', atcURL.text)
        if match:
            print(UTCtoEST(), 'ATC successful....')
            break
        print(UTCtoEST(), 'Trying to ATC....')
        time.sleep(0)
    else:
        print(UTCtoEST(), 'Could not ATC after ' +
              ' retries, therefore exiting the bot.')
        sys.exit(1)

    ###Going to Checkout Page###
    for cartRetry in range(1):
        cartdata = {
            'updates[]': 1,
            'note': '',
            'checkout': 'Check Out'
        }
        atc = s.post('https://www.'+home+'.com/cart',
                     data=cartdata, allow_redirects=True)
    ###Parsing URL###
        parse = urlparse(atc.url)
        storeID = parse.path.split('/')[1]
        checkoutID = parse.path.split('checkouts/')[1]
        print('Checkout Session Id:', checkoutID)
    ###Get Token###
        soup = BeautifulSoup(atc.text, 'lxml')
        input = soup.find_all('input')[2]
        auth_token = soup.find(
            'input', attrs={'name': 'authenticity_token'})['value']
        previous_step = soup.find(
            'input', attrs={'name': 'previous_step'})['value']
        step = soup.find(
            'input', attrs={'name': 'step'})['value']
        print('Auth_token:', auth_token)
    ###Get Contact info###
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8',
            'Host': 'checkout.shopify.com',
            'Referer': 'https: //checkout.shopify.com/'+storeID+'/checkouts/'+checkoutID+'?step=contact_information',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'
        }

        qs = [
            ('_method', 'patch'),
            ('authenticity_token',
             '_IGYKwPcUCJrOrS7r6_0UaLD6LEdDlaXPJd7-DgBcPEyZdH8kBINQEwPtrox4Z91CtkdVPbHXOfLlz5AWRxLFg'),
            ('previous_step', 'contact_information'),
            ('step', 'shipping_method'),
            ('checkout[email]', email),
            ('checkout[buyer_accepts_marketing]', '0'),
            ('checkout[buyer_accepts_marketing]', '1'),
            ('checkout[shipping_address][first_name]', ''),
            ('checkout[shipping_address][first_name]', firstname),
            ('checkout[shipping_address][last_name]', ''),
            ('checkout[shipping_address][last_name]', lastname),
            ('checkout[shipping_address][address1]', ''),
            ('checkout[shipping_address][address1]', address),
            ('checkout[shipping_address][address2]', ''),
            ('checkout[shipping_address][address2]', ''),
            ('checkout[shipping_address][city]', ''),
            ('checkout[shipping_address][city]', city),
            ('checkout[shipping_address][country]', ''),
            ('checkout[shipping_address][country]', country),
            ('checkout[shipping_address][province]', ''),
            ('checkout[shipping_address][province]', state),
            ('checkout[shipping_address][zip]', ''),
            ('checkout[shipping_address][zip]', pincode),
            ('checkout[shipping_address][phone]', ''),
            ('checkout[shipping_address][phone]', phone),
            ('checkout[client_details][browser_width]', '1519'),
            ('checkout[client_details][browser_height]', '482'),
            ('checkout[client_details][javascript_enabled]', '1'),
            ('checkout[client_details][color_depth]', '24'),
            ('checkout[client_details][java_enabled]', 'false'),
            ('checkout[client_details][browser_tz]', '-330'),
        ]
        GETcontact = s.get(atc.url, data=qs, headers=headers,
                           allow_redirects=True)
        print(GETcontact.status_code)
    ###Post Contact Info###
        headers1 = {
            'Origin': 'https://checkout.shopify.com',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8',
            'Referer': 'https://checkout.shopify.com/'+storeID+'/checkouts/'+checkoutID,
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'
        }
        formData = [
            ('_method', 'patch'),
            ('authenticity_token',
             '_IGYKwPcUCJrOrS7r6_0UaLD6LEdDlaXPJd7-DgBcPEyZdH8kBINQEwPtrox4Z91CtkdVPbHXOfLlz5AWRxLFg'),
            ('previous_step', 'contact_information'),
            ('step', 'shipping_method'),
            ('checkout[email]', email),
            ('checkout[buyer_accepts_marketing]', '0'),
            ('checkout[buyer_accepts_marketing]', '1'),
            ('checkout[shipping_address][first_name]', ''),
            ('checkout[shipping_address][first_name]', firstname),
            ('checkout[shipping_address][last_name]', ''),
            ('checkout[shipping_address][last_name]', lastname),
            ('checkout[shipping_address][address1]', ''),
            ('checkout[shipping_address][address1]', address),
            ('checkout[shipping_address][address2]', ''),
            ('checkout[shipping_address][address2]', ''),
            ('checkout[shipping_address][city]', ''),
            ('checkout[shipping_address][city]', city),
            ('checkout[shipping_address][country]', ''),
            ('checkout[shipping_address][country]', country),
            ('checkout[shipping_address][province]', ''),
            ('checkout[shipping_address][province]', state),
            ('checkout[shipping_address][zip]', ''),
            ('checkout[shipping_address][zip]', pincode),
            ('checkout[shipping_address][phone]', ''),
            ('checkout[shipping_address][phone]', phone),
            ('checkout[client_details][browser_width]', '1519'),
            ('checkout[client_details][browser_height]', '482'),
            ('checkout[client_details][javascript_enabled]', '1'),
            ('checkout[client_details][color_depth]', '24'),
            ('checkout[client_details][java_enabled]', 'false'),
            ('checkout[client_details][browser_tz]', '-330'),
        ]
        POSTcontact = s.post(atc.url, data=formData,
                             headers=headers1, allow_redirects=True)
        ###Parsing Shipping Method###
        soup = BeautifulSoup(POSTcontact.text, u'html.parser')
        # print(soup.find('h2').text.strip())
        shipping = soup.find('input', attrs={'class': 'input-radio'})
        # print(shipping)
        shipping_method = shipping['value']

    ###Submitting Shipping Data###
        headers2 = {
            'Origin': 'https://checkout.shopify.com',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8',
            'Referer': 'https://checkout.shopify.com/'+storeID+'/checkouts/'+checkoutID,
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'
        }
        ShipformData = {
            'utf8': '✓',
            '_method': 'patch',
            'authenticity_token': auth_token,
            'previous_step': 'shipping_method',
            'step': 'payment_method',
            'checkout[shipping_rate][id]': shipping_method,
            'button': '',
            'checkout[client_details][browser_width]': '1280',
            'checkout[client_details][browser_height]': '368',
            'checkout[client_details][javascript_enabled]': '1'
        }
        shippingmethod = s.post(atc.url, data=ShipformData,
                                headers=headers2, allow_redirects=True)
        ###Parsing payment_gateaway###
        soup = BeautifulSoup(shippingmethod.text, 'html.parser')
        ul = soup.find(attrs={'class': 'radio-wrapper'})
        auth_token = soup.find(
            'input', attrs={'name': 'authenticity_token'})['value']
        # print(ul)
        payment_gateaway = '91953795'
        auth_token = soup.find(
            'input', attrs={'name': 'authenticity_token'})['value']
        # previous_step = soup.find(
        #     'input', attrs={'name': 'previous_step'})['value']
        # step = soup.find(
        #     'input', attrs={'name': 'step'})['value']
    ###submitting payment info###
        CCheaders = {
            'accept': 'application/json',
            'Origin': 'https://checkout.shopifycs.com',
            'Accept-Language': 'en-US,en;q=0.8',
            'Host': 'elb.deposit.shopifycs.com',
            'content-type': 'application/json',
            'Referer': 'https://checkout.shopifycs.com/number?identifier='+checkoutID+'&location=3A%2F%2Fcheckout.shopify.com%2F'+storeID+'%2Fcheckouts%2F'+checkoutID+'%3Fpreviousstep%3Dshipping_method%26step%3Dpayment_method',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'
        }
        ccinfo = {
            "credit_card": {
                "first_name": card_firstname,
                "last_name": card_lastname,
                "first_digits": card_firstdigits,
                "last_digits": card_lastdigits,
                "brand": brand,
                "expiry_month": ex_month,
                "expiry_year": ex_year
            }
        }
        creditcard = s.post('https://elb.deposit.shopifycs.com/sessions',
                            json=ccinfo, headers=CCheaders, allow_redirects=True)
        # print(creditcard.status_code)
        cc_verify = creditcard.json()
        # import pprint
        # pprint.pprint(cc_verify)
        cc_verify_id = cc_verify['id']
        # print(cc_verify_id)
    ###submitting credit card info##
        paymentheaders = {
            'Origin': 'https://checkout.shopify.com',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8',
            'Referer': 'https://checkout.shopify.com/'+storeID+'/checkouts/'+checkoutID,
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'
        }
        paymentdata = {
            '_method': 'patch',
            'authenticity_token': auth_token,
            'previous_step': 'payment_method',
            'step': '',
            's': cc_verify_id,
            'checkout[payment_gateway]': '91953795',
            'checkout[credit_card][vault]': 'false',
            'checkout[different_billing_address]': 'false',
            'checkout[remember_me]': '',
            'checkout[remember_me]': '0',
            'checkout[vault_phone]': '',
            'checkout[total_price]': '76195',
            'complete': '1',
            'checkout[client_details][browser_width]': '1677',
            'checkout[client_details][browser_height]': '415',
            'checkout[client_details][javascript_enabled]': '1',
            'checkout[client_details][color_depth]': '24',
            'checkout[client_details][java_enabled]': 'false',
            'checkout[client_details][browser_tz]': '-330',

        }
        submitpayment = s.post(atc.url, data=paymentdata,
                               headers=paymentheaders, allow_redirects=True)
        print(UTCtoEST(), submitpayment.status_code, submitpayment.url)

        for cookie in driver.get_cookies():
            c = {cookie['name']: cookie['value']}
            s.cookies.update(c)
        for cookie in s.cookies:  # session cookies
            # Setting domain to None automatically instructs most webdrivers to use the domain of the current window
            # handle
            # print(cookie.domain)
            cookie_dict = {'domain': cookie.domain, 'name': cookie.name,
                           'value': cookie.value, 'secure': cookie.secure}
            if cookie.expires:
                cookie_dict['expiry'] = cookie.expires
            if cookie.path_specified:
                cookie_dict['path'] = cookie.path

            driver.add_cookie(cookie_dict)
        driver.get(submitpayment.url)
        # iframe = driver.find_elements_by_tag_name('iframe')[3]
        # driver.switch_to.frame(iframe)
        driver.find_element_by_xpath(
            '/html/body/div/div/div/main/div[1]/div/form/div[4]/div[1]/button').click()
        time.sleep(5)
