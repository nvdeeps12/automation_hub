import json
from selenium import webdriver
import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd
import time
from selenium.webdriver.common.keys import Keys

appState = {
    "recentDestinations": [
        {
            "id": "Save as PDF",
            "origin": "local",
            "account": ""
        }
    ],
    "selectedDestinationId": "Save as PDF",
    "version": 2
}

profile = {
    'printing.print_preview_sticky_settings.appState': json.dumps(appState)}

try:
    options = webdriver.ChromeOptions()
    # options.add_argument('--disable-gpu')
    # options.add_argument("disable-infobars")
    options.add_experimental_option("useAutomationExtension", False)
    options.add_experimental_option(
        "excludeSwitches", ["enable-automation"])
    options.add_experimental_option('prefs', profile)
    options.add_argument('--kiosk-printing')
    # options.add_argument('--headless')
    driver = webdriver.Chrome(
        executable_path='/home/nvdeep/Projects/Fiver/Drivers/chromedriver', options=options)

    # soup=BeautifulSoup(driver.page_source,u'html.parser')
except:
    options = webdriver.FirefoxOptions()
    options.add_argument('--disable-gpu')
    # options.add_argument('print.always_print_silent')
    fp = webdriver.FirefoxProfile()
# 0 means to download to the desktop, 1 means to download to the default "Downloads" directory, 2 means to use the directory
    fp.set_preference("browser.download.folderList", 1)
    fp.set_preference("browser.helperApps.alwaysAsk.force", False)
    fp.set_preference("print.always_print_silent", True)
    fp.set_preference("Save as PDF", True)
    fp.set_preference("print save as pdf", True)
    fp.set_preference("browser.download.manager.showWhenStarting", False)

    # driver = webdriver.Firefox(
    #     capabilities=firefox_capabilities, firefox_binary=binary, firefox_profile=fp)
    driver = webdriver.Firefox(
        executable_path='geckodriver', options=options, firefox_profile=fp)

username = input('Enter User Name -> ')
userpassword = input('Enter User Password -> ')

driver.get('https://usa.experian.com/login/index')
time.sleep(10)
uname = driver.find_element_by_xpath(
    '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-signin/ecs-card/section[2]/div/ecs-form/form/ecs-input/div/div[2]/input')
uname.click()

uname.send_keys(username)

upass = driver.find_element_by_xpath(
    '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-signin/ecs-card/section[2]/div/ecs-form/form/div[2]/ecs-input/div/div[2]/input')
upass.click()
upass.send_keys(userpassword)


driver.find_element_by_xpath(
    '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-signin/ecs-card/section[2]/div/ecs-form/form/div[4]/button').click()
time.sleep(3)
try:
    driver.get('https://usa.experian.com/login/ato/question')

    teacher = driver.find_element_by_xpath(
        '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-question/ecs-card/section[2]/div/app-security-question-page/ecs-form/form/ecs-input[1]/div/div[2]/input')
    teach = input('What was the name of your favorite teacher? -> ')
    pinn = input('Enter 4 digit Pin -> ')

    teacher.click()

    teacher.send_keys(teach)

    pin = driver.find_element_by_xpath(
        '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-question/ecs-card/section[2]/div/app-security-question-page/ecs-form/form/ecs-input[2]/div/div[2]/input')

    pin.click()
    pin.send_keys(pinn)

    driver.find_element_by_xpath(
        '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-question/ecs-card/section[2]/div/app-security-question-page/ecs-form/form/button').click()
    time.sleep(10)

    DOB = driver.find_element_by_xpath(
        '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-email/ecs-card/section[2]/div/app-dob-ssn-page/ecs-form/form/ecs-dob-input/div/ecs-input/div/div[2]/input')
    date_OB = input('Enter Date Of birth -> ')
    sec_pin = input('Enter Social Security Number -> ')
    DOB.click()

    DOB.send_keys(date_OB)

    ssn = driver.find_element_by_xpath(
        '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-email/ecs-card/section[2]/div/app-dob-ssn-page/ecs-form/form/ecs-ssn-input/label/div[2]/input')
    ssn.click()
    ssn.send_keys(sec_pin)

    driver.find_element_by_xpath(
        '/html/body/app-root/app-public/ecs-public-template/div/div/div/div/div/app-email/ecs-card/section[2]/div/app-dob-ssn-page/ecs-form/form/div/button').click()
except:
    pass

time.sleep(3)

driver.get('https://usa.experian.com/member/disputeCenter/reportSummary')
time.sleep(5)
try:
    error = driver.find_element_by_class_name('dispute-header').text
    if "Dispute Can't Be Processed Online" in error:
        driver.get(
            'https://usa.experian.com/member/reports/experian/now?scroll=false')
    time.sleep(10)
    driver.find_element_by_xpath(
        '/html/body/app-root/div[2]/div/div[1]/main/div/div[2]/div/div/div/div[2]/div[1]/button').click()
    time.sleep(5)
    # else:
    #     driver.find_element_by_xpath('/html/body/app-root/div[2]/div/div[1]/main/div/div/div/div/div/div[2]/div[2]/a/span').click()
except:
    time.sleep(10)
    driver.find_element_by_xpath(
        '/html/body/app-root/div[2]/div/div[1]/main/div/div/div/div/div/div[2]/div[2]/a/span').click()


time.sleep(5)
# driver.switch_to.active_element
driver.switch_to_window(driver.window_handles[1])
driver.switch_to.default_content
driver.switch_to.window
# driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
# driver.find_element_by_tag_name('button').click()

# time.sleep(5)

# print(driver.current_url)
time.sleep(3)
# print(driver.current_window_handle)
driver.execute_script('window.print();')

soup = BeautifulSoup(driver.page_source, u'html.parser')

p_main = []


personal = soup.find('div', attrs={'class': 'personal-info-section'})

pps = personal.find_all('div', attrs={'class': 'report-item--container'})
for p in pps:
    pp = {
        p.find('h3').text.strip(): p.text.strip().split(p.find('h3').text.strip())[1]
    }
    p_main.append(pp)

filename = "C:/Users/My Buddy/Downloads/Microsoft.SkypeApp_kzf8qxf38zg5c!App/All/scripts/Personal_info.json"
print("File_Name--", filename)
with open(filename, "a+") as f:
    sorted = json.dumps(p_main, indent=4)
    f.write(sorted)
    print("Json Generated")

a_main1 = []
a_main = []
try:
    account = soup.find('div', attrs={'class': 'accounts-section'})
    accounts = account.find_all(
        'div', attrs={"class": "report-item--container"})

    for ac in accounts:
        ac = {
            ac.find('h3').text.strip(): ac.text.strip()
        }
        a_main1.append(ac)
    aa = {
        "Account": a_main1
    }
except:
    pass
a_main.append(aa)


filename = "Accounts.json"
print("File_Name--", filename)
with open(filename, "a+") as f:
    sorted = json.dumps(a_main, indent=4)
    f.write(sorted)
    print("Json Generated")