import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import re
import time
# import psycopg2 as sql
import datetime
from datetime import date,timedelta
import datetime
import sys
import mysql.connector
import schedule


chromepath = '/home/ubuntu/ec2/geckodriver'
print(chromepath)
connection = mysql.connector.connect(
    host="localhost",
    user="test",
    password="test123",
    database="priceline"
)

    

mycursor = connection.cursor()

# q = "CREATE TABLE IF NOT EXISTS `priceline_flights` (`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `airline_name` TEXT DEFAULT NULL,`departures` TEXT DEFAULT NULL,`duration` TEXT DEFAULT NULL,`arrival` TEXT DEFAULT NULL,`price` TEXT DEFAULT NULL,`origin` TEXT DEFAULT NULL,`Location` TEXT DEFAULT NULL,`images` TEXT DEFAULT NULL,`trip` TEXT DEFAULT NULL,`departure_date` TEXT DEFAULT NULL,`return_date` TEXT DEFAULT NULL,`Source` TEXT DEFAULT NULL,`created_at` TIMESTAMP NOT NULL,`link` TEXT DEFAULT NULL)"
# mycursor.execute(q)
# connection.commit()


# driver.get('https://www.trvl.priceline.com')
# time.sleep(1)


links_arr = []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INSTRUCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Install Libraries ->(requests, bs4, selenium, xlwt, re, time)
# Just Enter desired inputs down there
# try to make all vars in given formate :-)

# Needed Vars
origin = ['AMS','ACY','AUS','BOS','CUN','SJD','CTG','CHS','CHI','DFW','DEN','FLL','HAV','KEF','SJO','EYW','RNO','LAS','LAX','MIA','YUL','MYR','BNA','MSY','EWR','MCO','PTY','SJO','SJU','POP','SAN','SXM','JTR','JMK','JFK','SAV','PHX','BRO','TPA','ATL','PDX','PSP','SDF','ECP','NAS','MDE','GSP','CUN','MKE','PBI']
LocDep = ['DFW','EWR',
'FLL','MYR','MCO','TPA',
'BOS','CHI','MIA','EWR',
'CHI','FLL','EWR',
'CHI','FLL','EWR',
'LAX','MIA','YUL','MCO',
'LAX','MIA','EWR',
'AUS','BOS','CHI','EWR',
'BOS','FLL','EWR',
'BOS','CHI','EWR',
'BOS','LAX','EWR',
'BOS','CHI','EWR',
'BOS','MIA','EWR',
'BOS','MIA','EWR',
'BOS','CHI','EWR',
'CHI','EWR','SAN','PHX',
'CHI','LAX','MIA','EWR',
'BOS','CHI','EWR',
'BOS','FLL','EWR',
'BOS','FLL','LAX','EWR',
'BOS','FLL','EWR',
'BOS','FLL','LAX',
'ACY','BOS','CHI','DFW',
'BOS','FLL','EWR',
'BOS','CHI','FLL',
'CHI','LAX','MIA','EWR',
'BOS','LAX','EWR',
'BOS','CHS','FLL,EWR',
'AUS','BOS','DFW,EWR',
'AUS','DEN','LAX,EWR',
'CHI','FLL','EWR',
'BOS','CHI','MIA',
'BOS','MIA','EWR',
'BOS','MIA','EWR',
'BOS','CHI','FLL',
'LAS','EWR','TPA',
'BOS','CHI','EWR']


departure_date = str(date.today())
adults = '1'
Type = 'Economy'

# Date Format is YYYY/MM/DD
return_date = datetime.datetime.now() + timedelta(days=7)
return_date = str(return_date).split(' ')[0].strip()

print('Return_date-->',return_date)
breakpoint = 3
sources = 'Priceline'
if return_date != None:
    trip = 'Roundtrip'
    print('Running For Roundtrip')
else:
    trip = 'Oneway'
    print('Running For Oneway')


def scroll(driver, timeout):
    scroll_pause_time = timeout
    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        # Scroll down to bottom
        driver.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(scroll_pause_time)
        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            # If heights are the same it will exit the function
            break
        last_height = new_height


class PriceLine():
    
    def click_showmore(self,driver):
        try:
            for i in range(5):
                driver.find_element_by_id('c2ZXk-loadMore').click()
                time.sleep(5)
                print('Clicked on loadmore')
        except:
            pass
        
    def job2(self):
        if date.today().day==1:    
            Pcobj = PriceLine()
            Pcobj.get_links()

    def check_isexits(self, page_link):
        try:
            page_link = "'%s'" % page_link
            qry2 = "SELECT count(*) FROM wp_scrape_priceline_flight WHERE link="+page_link+';'

            mycursor.execute(qry2)
            record = mycursor.fetchall()
            data_rec = str(record[0])
            rec = data_rec.split('(')[1]
            data_rec = rec.split(',)')[0]
            data_rec = int(data_rec)
            return data_rec
        except:
            import sys
            print(sys.exc_info())
            return 0
    def insert_sql(self,city, first_from_city_short, return_from_city_short, first_to_city_short, price,
                                start_date, first_airline_logo, return_airline_logo, first_depart_time, first_arrive_time, return_depart_time, return_arrive_time, first_stop,first_time,return_stop,return_time,first_stop_at,return_stop_at,link,return_to_city_short):
        try:
            arr = []
           

            location_link = link
            widget = 2
            ads = 2
            from datetime import date

            today = date.today()
            print("Today's date:", today)
            dates = str(today)
            # print(date)
            
            data = (city, first_from_city_short, return_from_city_short, first_to_city_short, price,start_date, first_airline_logo, return_airline_logo, first_depart_time, first_arrive_time, return_depart_time, return_arrive_time, first_stop,first_time,return_stop,return_time,first_stop_at,return_stop_at,link,location_link,widget,ads,dates,return_to_city_short)
            arr.append(data)

            data_tuple = tuple(arr)
            if len(data_tuple) == 1:
                data_tuple = str(data_tuple).replace('),)', '))')

            query = "INSERT INTO wp_scrape_priceline_flight("
            query += "`city`,`first_from_city_short`,`return_from_city_short`,`first_to_city_short`,`price`,`start_date`,`first_airline_logo`,`return_airline_logo`,`first_depart_time`,`first_arrive_time`,`return_depart_time`,`return_arrive_time`,`first_stop`,`first_time`,`return_stop`,`return_time`,`first_stop_at`,`return_stop_at`,`link`,`location_link`,`widget`,`ads`,`date`,`return_to_city_short`) VALUES"
            query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),"NULL").replace('((', '(').replace('))', ')')

            # print(query)
            mycursor.execute(query)
            connection.commit()
            print(mycursor.rowcount, "record inserted.")
        except:
            print('--')
            print(sys.exc_info())

    def log(self, logmsg, logtype):
        """
        method to write message in log file
        logmsg : message that need to write in log file
        logtype : type of log like info, warn, error
        """
        if logtype == 'info':
            self.logger.info(logmsg)
        if logtype == 'warn':
            self.logger.warn(logmsg)
        if logtype == 'error':
            self.logger.error(logmsg)

    def get_links(self):
        try:
            # options = webdriver.FirefoxOptions()
            # options.add_argument('--disable-gpu')
            # options.add_argument('--no-sandbox')
            # options.add_argument("--log-level=3")
            # options.add_argument('--disable-dev-shm-usage')
            # options.add_argument('--headless')
            # chromedriver_path = 'F:/PycharmProjects/MainKam/fiver/Drivers/chromedriver.exe'

            driver = webdriver.Firefox(executable_path=chromepath)

            
            row = 0
            for ori in origin:
                for loc in LocDep:
                    
                    if return_date != None:
                        page = 'https://www.kayak.com/flights/'+ori+'-'+loc + \
                            '/'+departure_date+'/'+return_date+'?sort=bestflight'+str('&Currency=Usd')
                            
                        page= 'https://www.kayak.com/flights/AMS-BOS/2020-06-21/2020-06-28'

                    else:
                        page = 'https://www.kayak.com/flights/FLL-NRT/2020-06-21/2020-06-28?sort=bestflight_a&attempt=1&lastms=1590220835960'
                        

                    import time
                    page = str(page)
                    
                    print('Page is -->',page)
                    if self.check_isexits(page) > 0:
                        print("Skiping ........")
                        continue
                        # TODO: Commented continue
                        
                    print('Breakpoint is -->', breakpoint)
                    
                    driver.get(page)
                    time.sleep(3)
                    # Show More Button
                    # self.click_showmore(driver)

                    soup = BeautifulSoup(driver.page_source, u'html.parser')
                    # print(soup)
                    flight_cards = soup.findAll('div', attrs={'class': 'resultInner'})
                    arr = []
                    if len(flight_cards)==0:
                        print('Conttt')
                        continue
                
                    for fc in flight_cards:
                        
                        try:
                            city = ''
                        except:
                            city = 'Not Avaliable'
                        
                        try:
                            first_from_city_short = fc.findAll('div',attrs={'class':'bottom-airport js-airport'})[0].text.strip()
                            print(first_from_city_short)
                        
                        except:
                            import sys
                            print('I Am in Cont')
                            continue
                            print(sys.exc_info())
                            first_from_city_short = 'Not Avaliable'
                            
                        try:
                            return_from_city_short = fc.findAll('div',attrs={'class':'bottom-airport js-airport'})[2].text.strip()
                        except:
                            return_from_city_short = 'Not Avaliable'
                            
                            
                        try:
                            first_to_city_short = fc.findAll('div',attrs={'class':'bottom-airport js-airport'})[1].text.strip()
                        except:
                            first_to_city_short = 'Not Avaliable'
                            
                        try:
                            return_to_city_short = fc.findAll('div',attrs={'class':'bottom-airport js-airport'})[4].text.strip()
                        except:
                            return_to_city_short = 'Not Avaliable'
                            
                        
                        # break
                        try:
                            start_date = departure_date
                        except:
                            start_date = 'Not Avaliable'
                            
                        try:
                            first_airline_logo = fc.findAll('img')[0]['src']
                        except:
                            first_airline_logo = 'Not Avaliable'
                            
                        try:
                            return_airline_logo = fc.findAll('img')[1]['src']
                        except:
                            return_airline_logo = 'Not Avaliable'
                            
                        
                        try:
                            first_depart_time = fc.findAll('span',attrs={'class':'time-pair'})[0].text.strip()
                        except:
                            first_depart_time = 'Not Avaliable'
                            
                        print('first depa time',first_depart_time)
                        try:
                            first_arrive_time = fc.findAll('span',attrs={'class':'time-pair'})[1].text.strip()
                        except:
                            first_arrive_time = 'Not Avaliable'
                            
                        print('first arrival time',first_arrive_time)
                        try:
                            return_depart_time = fc.findAll('span',attrs={'class':'time-pair'})[2].text.strip()
                        except:
                            return_depart_time = 'Not Avaliable'
                            
                        print('Return dep time',return_depart_time)
                        try:
                            return_arrive_time = fc.findAll('span',attrs={'class':'time-pair'})[3].text.strip()
                        except:
                            return_arrive_time = 'Not Avaliable'
                            
                        print('Return Arrival',return_arrive_time)
                        
                        # TODO: These field not valid for nonstop
                        try:
                            first_stop = fc.findAll('span',attrs={'class':'leg-warning'})[0].find_previous('div').find_previous('div').text.strip()
                            first_stop = first_stop.split('stop')[0]+str(' stop').strip()
                        except:
                            first_stop = 'Not Avaliable'
                            
                        print('First-->',first_stop)
                        try:
                            first_stop_at = fc.findAll('span',attrs={'class':'leg-warning'})[0].find_previous('div').find_previous('div').text.strip()
                            first_stop_at = first_stop_at.split('stop')[1].strip()
                        
                        except:
                            first_stop_at = 'Not Avaliable'
                            
                        print('First Stop At',first_stop_at)
                        try:
                            first_time = fc.findAll('div',attrs={'class':'section duration allow-multi-modal-icons'})[0].text.strip()
                            first_time = first_time.split('m')[0]+str('m').strip()
                        except:
                            first_time = 'Not Avaliable'
                            
                        print('Fisrt Time',first_time)
                        try:
                            return_stop = fc.findAll('span',attrs={'class':'leg-warning'})[1].find_previous('div').find_previous('div').text.strip()
                            return_stop = return_stop.split('stop')[0]+str(' stop').strip()
                        
                        except:
                            return_stop = 'Not Avaliable'
                            
                        print('Return Stop',return_stop)
                            
                        try:
                            return_stop_at = fc.findAll('span',attrs={'class':'leg-warning'})[1].find_previous('div').find_previous('div').text.strip()
                            return_stop_at = return_stop_at.split('stop')[1].strip()
                        except:
                            return_stop_at = 'Not Avaliable'
                        print('Return Stop At',return_stop_at)
                        
                        try:
                            price = fc.find('span', attrs={'class': 'price-text'}).text.strip()
                            price = str(price).split('$')[1]+str('.0').strip()
                            print(price)
                        except:
                            price = 'Not Avaliable'
                            
                        try:
                            return_time = fc.findAll('div',attrs={'class':'section duration allow-multi-modal-icons'})[1].text.strip()
                            return_time = return_time.split('m')[0]+str('m').strip()
                            print('Return Time',return_time)
                        except:
                            return_time = 'Not Avaliable'
                            
                        print('--'*50)

                        link = page
                        
                        
                        row = row+1
                        
                        
                        self.insert_sql(city, first_from_city_short, return_from_city_short, first_to_city_short, price,
                                        start_date, first_airline_logo, return_airline_logo, first_depart_time, first_arrive_time, return_depart_time, return_arrive_time, first_stop,first_time,return_stop,return_time,first_stop_at,return_stop_at,link,return_to_city_short)

                        
                        print('Done-->', row)

                        print('--'*50)
                        
        except:
            import sys
            print(sys.exc_info())
            


# driver.quit()
objs = PriceLine()
objs.get_links()

schedule.every().day.at("08:00").do(objs.job2)
# schedule.every(1).minute.do(job2) 
while True:
    try:
        print(datetime.datetime.now())
        schedule.run_pending()
        time.sleep(1)
    except:
        pass
