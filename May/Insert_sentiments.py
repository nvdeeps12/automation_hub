import mysql.connector
import sys
connection = mysql.connector.connect(
    host="localhost",
    user="root",
    password="password",
    database="google"
    )
mycursor = connection.cursor()


def get_sentiments():
    try:
        # page_link = "'%s'"%page_link
        qry2="SELECT * FROM tones;"
        
        mycursor.execute(qry2)
        record=mycursor.fetchall()
        # data_rec=str(record[0])
        data_arr = []
        for gc in record:
            # print(gc)
            id = gc[0]
            url = gc[1]
            tones = gc[2]
            # magnitude = gc[3]
            # entities = gc[4]
            print(id)
            
            dic1 = {
                'id':id,
                'url':url,
                'tones':tones,
                # 'magnitude':magnitude,
                # 'entities':entities
            }
            data_arr.append(dic1)
        # print(record)
        
        # rec=data_rec.split('(')[1]
        # data_rec=rec.split(',)')[0]
        # data_rec = int(data_rec)
        return data_arr
    except:
        import sys
        print(sys.exc_info())
        return 0
    
    
data = get_sentiments()
print(data)
import json
filename = 'Tone_Analysis_ibm.json'
print('File_Name--', filename)
with open(filename, 'a+') as f:
    sorted = json.dumps(data, indent=4)
    f.write(sorted)
    print('Json Generated')
# print(data)