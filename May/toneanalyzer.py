import json,xlrd,xlwt
from ibm_watson import ToneAnalyzerV3
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

authenticator = IAMAuthenticator('Qi75-3zGA_gyvCcffNap6zXeK0KHu57iMnz7pQCs2uZB')
tone_analyzer = ToneAnalyzerV3(
    version='2019-07-12',
    authenticator=authenticator
)

import mysql.connector
import sys
connection = mysql.connector.connect(
    host="localhost",
    user="root",
    password="password",
    database="google"
    )
mycursor = connection.cursor()


def check_isexits(page_link):
    try:
        page_link = "'%s'"%page_link
        qry2="SELECT count(*) FROM tones WHERE url="+page_link+';'
        
        mycursor.execute(qry2)
        record=mycursor.fetchall()
        data_rec=str(record[0])
        rec=data_rec.split('(')[1]
        data_rec=rec.split(',)')[0]
        data_rec = int(data_rec)
        return data_rec
    except:
        import sys
        print(sys.exc_info())
        return 0
    
    
def check_count():
    try:
        qry2="SELECT count(*) FROM tones;"
        
        mycursor.execute(qry2)
        record=mycursor.fetchall()
        data_rec=str(record[0])
        rec=data_rec.split('(')[1]
        data_rec=rec.split(',)')[0]
        data_rec = int(data_rec)
        return data_rec
    except:
        import sys
        print(sys.exc_info())
        return 0
    
    
def insert_sql(url,tones):
    try:
        
        arr = []
        # score = "'%s'"%score
        tones = "'%s'"%tones
        
        data = (url,tones)
        arr.append(data)

        data_tuple = tuple(arr)
        if len(data_tuple) == 1:
                data_tuple = str(data_tuple).replace('),)', '))')
        
        query = "INSERT INTO tones("
        query +=  "url,tone) VALUES"
        query += str(data_tuple).replace("'NULL'", "NULL").replace(str(None),"NULL").replace('((', '(').replace('))', ')')
        
        # print(query)
        mycursor.execute(query)
        connection.commit()
        print(mycursor.rowcount, "record inserted.")
    except:
        print(sys.exc_info())

def analyzetone(textoanalize):
    try:
        tone_analyzer.set_service_url('https://api.us-south.tone-analyzer.watson.cloud.ibm.com')

        text = textoanalize

        tone_analysis = tone_analyzer.tone(
            {'text': text},
            content_type='application/json'
        ).get_result()
        tone_data = (json.dumps(tone_analysis, indent=2))
        return tone_data
    except:
        return {}

def get_Tones():
        loc = "Pd.xlsx"
        wb = xlrd.open_workbook(loc)
        sheet = wb.sheet_by_index(0)
        print(sheet.nrows)

        for i in range(1, sheet.nrows+1):
            textoanalize = sheet.cell_value(i, 3)
            url = sheet.cell_value(i, 2)
            if len(url)<5:
                continue
            exists = check_isexits(url)
            if exists>0:
                print('Exists Continuing')
                continue
            
            print('Url',url)
            print('---'*100)
            content = analyzetone(textoanalize)

            insert_sql(url,content)
            print('Udated Count is -->',check_count())



get_Tones()