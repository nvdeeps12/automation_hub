import requests
from bs4 import BeautifulSoup
import csv

fields = ['Product_link','Scrum Title','Features','Color','Model','Dimension','Weight','Straps','Hardware', 'Images','Price', 'Description', 'Shipping', 'Returns', 'Reviews']
out_file = open('rock_wells.csv','w')
csvwriter = csv.DictWriter(out_file, delimiter=',', fieldnames=fields)
csvwriter.writeheader()


class rockwell():

    def getproduct_links(self):
        con  = requests.get('https://www.rockcowleatherstudio.com/collections/')
        soup = BeautifulSoup(con.content, u"html.parser")
        lst_links = soup.findAll('li',attrs={'class':'dropdown-column__list-item'})
        for ls in lst_links:
            ls_href = ls.find('a')['href']
            full_lnk = 'https://www.rockcowleatherstudio.com'+str(ls_href)
            self.get_product_lnks(full_lnk)


    def get_product_lnks(self,prod_lnk):
        con = requests.get(prod_lnk)
        soup = BeautifulSoup(con.content, u"html.parser")
        try:
            page = soup.find('span',attrs={'class':'pagination__current'}).text.strip()
            page = page.split('of')[1].strip()
            page = int(page)
        except:
            page = 0

        for p in range(page):
            if p == 0:
                continue
            page_link = prod_lnk+str('?page=')+str(p)
            print('Running Pgae-->',page_link)
            con = requests.get(page_link)
            soup = BeautifulSoup(con.content, u"html.parser")
            lst_links = soup.findAll('a', attrs={'class': 'collection__product-link'})
            for ls in lst_links:
                ls_href = ls['href']
                full_lnk = 'https://www.rockcowleatherstudio.com' + str(ls_href)
                self.get_prodvars(full_lnk)


    def get_prodvars(self,lnk):
        try:
            dict_products = {}
            con = requests.get(lnk)
            soup = BeautifulSoup(con.content, u"html.parser")

            try:
                bread_title = soup.findAll('span',attrs={'class':'breadcrumb__title'})[2].text.strip()
            except:
                bread_title = 'None'

            try:
                full_title = soup.find('h2',attrs={'class':'product__title'}).text.strip()
            except:
                full_title = 'None'


            try:
                price = soup.find('span',attrs={'class':'money'}).text.strip()
            except:
                price = 'None'

            try:
                desc = soup.find('div',attrs={'itemprop':'description'}).text.strip()
                # print(desc)

                # print(model, diim, w8)
            except:
                desc = 'None'
            try:
                model = str(desc).split('Model Number:')[1].split('Dimensions:')[0]
            except:
                model = 'None'
            try:
                diim = str(desc).split('Dimensions:')[1].split('Weight:')[0]
            except:
                diim = 'None'
            try:
                w8 = str(desc).split('Weight:')[1].split('Hardware:')[0]
            except:
                w8 = 'None'
            try:
                hw = str(desc).split('Hardware:')[1].split('Shoulder Strap:')[0]
            except:
                hw = 'None'
            try:
                strp = str(desc).split('Shoulder Strap: ')[1].split('Color:')[0]

            except:
                strp = 'None'
            try:
                clr = str(desc).split('Color:')[1].split('Features:')[0]

            except:
                clr = 'None'
            try:
                fe = str(desc).split('Features:')[1]
            except:
                fe = 'none'


            dict_products['Features'] = fe
            dict_products['Color'] = clr
            dict_products['Weight'] = w8
            dict_products['Hardware'] = hw
            dict_products['Dimension'] = diim
            dict_products['Model'] = model
            dict_products['Straps'] = strp



            print(model,diim,w8,hw,strp,clr,fe)
            try:
                shiping = soup.findAll('div',attrs={'class':'container container--shrink'})[1].text.strip()
            except:
                shiping = 'None'


            try:
                return_exchanges = soup.findAll('div',attrs={'class':'container container--shrink'})[2].text.strip()
            except:
                return_exchanges = 'None'


            try:
                reviews = soup.find('span',attrs={'class':'spr-badge'})
            except:
                reviews = 'No Reviews'

            thumbarr = []
            try:
                photos = soup.findAll('li',attrs={'class':'product__thumbnail'})
                for p in photos:
                    imgsrc = p.find('img')['src']
                    imgsrc = 'https:'+str(imgsrc)
                    thumbarr.append(imgsrc)
            except:
                thumbarr = []



            dict_products['Product_link'] = lnk
            dict_products['Scrum Title'] = bread_title
            dict_products['Images'] = thumbarr
            dict_products['Price'] = price
            dict_products['Description'] = desc
            dict_products['Shipping'] = shiping
            dict_products['Returns'] = return_exchanges
            dict_products['Reviews'] = reviews
            print(dict_products)
            print('--'*100)
            csvwriter.writerow(dict_products)


        except:
            pass



obj = rockwell()
obj.getproduct_links()