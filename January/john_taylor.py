# coding=utf-8
import time
import json
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import csv

print('John Taylor Script')
start = int(input('Enter Start Point->'))
end = int(input('Enter End Point->'))

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
options.add_argument('--lang=fr-FR')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver')#, chrome_options=options)


links_arr = ['https://www.john-taylor.com/','https://www.john-taylor.fr/?_ga=2.163538940.1108062898.1583252473-1537043426.1583070131']


user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
main = []
class Johnt():
	def lang(self,link):
		lan = str(link).split('https://www.john-taylor.')[1].split('/')[0]
		# print(lan)
		lang = ''
		if lan == 'fr':
			lang = 'France'
		elif lan == 'com':
			lang = 'English'
		elif lan == 'it':
			lang = 'Italian'
		elif lan == 'es':
			lang = 'Spanish'
		elif lan == 'de':
			lang = 'German'
		elif lan == 'ru.com':
			lang = 'Russian'
		elif lan == 'cz':
			lang = 'Czech'
		else:
			lang = 'NONE'
		return lang


	# Contry Function

	def country(self,linkk):
		cc = linkk.split('/sale/')[0].split('/')[-1]
		return cc


	def gethrefs(self):
		self.Ids = 0
		for ln in links_arr:
			cur_link = ln+str('?currency=EUR')
			page = driver.get(cur_link)
			soup = BeautifulSoup(driver.page_source, 'html.parser')
			divs = soup.find('div',attrs={'id':'menu-button'}).find_next('ul')
			lis = divs.findAll('li')
			for ls in lis:
				## This is for testing

				if len(main) > end:
					break

				hrefs = ls.find('a')['href']
				if '#' in hrefs:
					continue

				self.gethotel_href(hrefs)

	def gethotel_href(self,href):
		page = driver.get(href)
		soup = BeautifulSoup(driver.page_source, 'html.parser')
		lis = soup.findAll('a',attrs={'class':'link_property_view'})[start:end]

		for ls in lis:
			hrefs = ls['href']
			hrefs = hrefs+str('?currency=EUR')
			print(hrefs)
			print('--'*100)
			print('Appended Records',len(main))
			# for testing
			if len(main)>end:
				break
			print('Arr-',main)
			self.get_propertydata(hrefs)

	def get_propertydata(self, urls):
		try:
			beds = None
			rooms = None
			bath = None

			pictures_arr = []
			self.Ids += 1
			url = urls
			user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
			page = requests.get(url, headers={'User-Agent': user_agent})
			soup = BeautifulSoup(page.text, 'html.parser')
			if (page.status_code == 200):
				try:
					price = soup.find('span', {'itemprop': 'price'}).text.strip()
					if 'Price' not  in price:
						price = price.replace(' ', '')
				except:
					price = 'Price upon request'

				try:
					title = soup.find('h2', {'class': 'comment_title'}).text.strip()
				except:
					title = None
				# print(title)

				try:
					description = soup.find('h2', {'class': 'comment_title'}).find_next('p').text.strip()
				except:
					description = None
				# print(description)
				try:
					size = soup.find('span', {'class': 'list_icons'}).get_text()
				except:
					size = None
				# print(size)

				try:
					rooms_query = soup.find('div', {'class': 'icons_detail'}).get_text().split()
					rooms = ""
					beds = ""
					bath = "No Info"
					for i in range(0, len(rooms_query)):
						# print(rooms_query[i])
						if (rooms_query[i][0] == 'R'):
							rooms = rooms_query[i - 1][len(rooms_query[i - 1]) - 1]
						if (rooms_query[i][0] == 'B'):
							beds = rooms_query[i - 1][len(rooms_query[i - 1]) - 1]
						if (rooms_query[i] == 'Swimming'):
							bath = 'Swimming'

				except:
					rooms_query = None

				# print(rooms_query.split())

				# print(bath)
				# print(rooms)
				# print(beds)
				try:
					country = self.country(url)
				except:
					country = None

				try:
					language = self.lang(url)
				except:
					language = None




				features_arr = []
				try:
					features = soup.find('div', {'class': 'prod-essentials'}).find('ul').findAll('li')
					# print(features)
					featues_var = None
					for fr in features:
						try:
							fr = fr.text.strip()
							features_arr.append(fr)
						except:
							pass

					# print('Faettt', features_arr)
					# print('----' * 50)

				except:
					features = None
				# print(features)
				try:
					type_ = soup.find('ul', {'class': 'prod-bullets prod-bullets2 row'}).get_text().split()
					typeof = type_[len(type_) - 1]
				except:
					typeof = None

				# print(typeof)
				try:
					location = soup.find('span', {'class': 'city'}).get_text()
					try:
						location = location.split('sale')[1]
					except:
						try:
							location = location.split('Ème')[1]
						except:
							location = soup.find('span', {'class': 'city'}).get_text()
				except:
					location = None

				try:
					pictures = soup.findAll('div', attrs={'class': 'clic-picture'})
					for pic in pictures:
						imgs = pic.attrs['style'].split("background-image:url('")[1].split("');")[0]
						pictures_arr.append(imgs)
				except:
					pass

				# print('Title->', title)
				# print('Location->', location)

				slug = url.split('/')[-2]
				# print(slug)
				import json
				self.a = dict()
				property_dict = {"position": self.Ids, "title": title, "description": str(description),
								 # "size": size, "rooms": rooms,
								 # "beds": beds,
								 "price": price, "proximities": features_arr,
								 # "type": typeof,
								 "slug": slug,
								 "language":language,
								 "country":country,
								 "location": location, "page-link": url,
								 "pictures": pictures_arr
								 }

				main.append(property_dict)


			# property_dict = self.main
			# property_dict.update(self.main)

			# self.main.append(property_dict)
			else:
				print("Not available link")
		except:
			pass



obj = Johnt()
obj.gethrefs()

property_dict = main
file_name = 'john_taylor' + str('.json')
print('File_Name--', file_name)
with open(file_name, 'a+') as f:
	sorted = json.dumps(property_dict, indent=4)
	f.write(sorted)
print('--' * 100)