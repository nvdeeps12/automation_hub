import requests
from bs4 import BeautifulSoup
import xlwt
import time
from selenium import webdriver
from January.create_xls import write_links,sub_links
options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver', options=options)
wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Merk')
sheet1.write(0,1,'Model')
sheet1.write(0,2,'Type')
sheet1.write(0,3,'Motor')
sheet1.write(0,4,'Origineel vermogen(Eco)')
sheet1.write(0,5,'Origineel Koppel(Eco)')
sheet1.write(0,6,'Vermogen na tunning(Eco)')
sheet1.write(0, 7, 'Koppel na tunning(Eco)')
sheet1.write(0,8,'Resultaat vermogen(Eco)')
sheet1.write(0, 9, 'Resultaat Koppel(Eco)')
sheet1.write(0,10,'Origineel vermogen(Stage 1)')
sheet1.write(0,11, 'Vermogen na tunning(Stage 1)')
sheet1.write(0,12,'Koppel na tunning(Stage 1)')
sheet1.write(0,13, 'Resltaat vermogen(Stage 1)')
sheet1.write(0,14,'Resltaat koppel(Stage 1)')
sheet1.write(0,15, 'Extra opties')
arr = []
class ecu():
    row=0
    def get_category(self):
        con =requests.get('https://www.ecu-soft.be/chiptuning')
        soup = BeautifulSoup(con.content, u'html.parser')
        categories = soup.find('select',attrs={'class':'js-filter'}).findAll('option')[1:]
        for c in categories:
            try:
                category = c.text.strip()
                category=str(category).replace(' ','_')

            except:
                category = 'None'

            self.get_models(category)

    def get_models(self,cats):

        link = 'https://www.ecu-soft.be/chiptuning/'+str(cats)+str('/')
        arr.append(link)
        print(arr)
        print('--'*70)
        con = driver.get(link)

        # driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[2]/div/select').click()
        # driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div[2]/div/select/option[2]").click()
        # time.sleep(5)
        # driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[3]/div/select').click()
        # time.sleep(5)
        # driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div[3]/div/select/option[2]").click()
        # time.sleep(7)
        # driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[3]/div/select').click()
        #
        # driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div[4]/div/select/option[2]").click()
        # time.sleep(5)
        # soup = BeautifulSoup(driver.page_source, u'html.parser')
        #
        # print('--'*100)
        # prod = soup.find('select',attrs={'name':'motor'}).findAll('option')[1:]
        #
        # for pd in prod:
        #     try:
        #         model = pd['value']
        #         model=str(model).replace(' ','_')
        #     except:
        #         model = 'None'
        #
        #     link = 'https://www.ecu-soft.be'+str(model)
        #     self.get_cars(link)

    def get_cars(self,link):
        link = link
        con = requests.get(link)
        soup = BeautifulSoup(con.content, u'html.parser')
        motors = soup.find('select', attrs={'name': 'product'}).findAll('option')[1:]
        for car in motors:
            try:
                car_model = car.text.strip()
                car_model=str(car_model).replace(' ','_')
            except:
                car_model = 'None'

            car_link = 'https://www.ecu-soft.be/chiptuning/alpina/'+str(car_model)+str('/')

            # self.get_years(soup,car_link)

    def get_years(self,soup,made_link):
        motors = soup.find('select', attrs={'name': 'year'}).findAll('option')[1:]
        for yer in motors:
            try:
                year = yer.text.strip()
                year = [int(i) for i in year.split() if i.isdigit()]
            except:
                year = 'None'

            made_link = made_link+str(year[0])+"/"
            # made_link = made_link.replace('... ->','').strip()
            # made_link = made_link.replace('-> ...','').strip()
            print(made_link)
            # self.get_carmotors(soup,made_link)

    def get_carmotors(self,soup,mad_link):
        motors = soup.find('select', attrs={'name': 'motor'}).findAll('option')[1:]

        for yer in motors:
            try:
                motr = yer.text.strip()

                motr_l=str(motr).split('pk)')[0].split('(')[1]
                motr_f=motr.split(motr_l)[0].split('(')[0]
                motr_m=motr_f+motr_l
                motrr=motr_m.replace(' ','_')
                motrr=str(motrr).replace(')','').replace('.','_').replace('__','_')
            except:
                motrr = 'None'
            print(motrr)
            mad_link = mad_link+str(motrr)+str('/').strip()
            print('Full-->',mad_link)
            # self.get_data(mad_link)

        print('--'*50)


    def get_data(self,end_link):
        req = requests.get(end_link)
        soup=BeautifulSoup(req.content,u'html.parser')
        try:
            Merk=soup.find('div',attrs={'class':'title'}).text.strip()
        except:
            Merk='None'

        print(Merk)

        try:
            Model=soup.find('div',attrs={'class':'subtitle'}).text.strip()
        except:
            Model='None'

        print(Model)
        try:
            type=soup.find('select',attrs={'name':'year'})
            type_data=type.find('option',attrs={'selected':'selected'}).text.strip()
        except:
            type_data='None'
        print(type_data)

        try:
            motor = soup.find('select', attrs={'name': 'motor'})
            motor_data = motor.find('option', attrs={'selected': 'selected'}).text.strip()
        except:
            motor_data='None'
        print(motor_data)

#         ~~~~~~~~~~specs~~~~~~~~~~~~~
#         ~~~~~~~ECO~~~~~~~~~~
        try:
            specstb=soup.find('div',attrs={'data-target':'eco'})
            specs_row=specstb.findAll('div',attrs={'class':'row'})

            sepcs_col_1=specs_row[2].findAll('div')

            eco_Koppel_Origineel=sepcs_col_1[1].text.strip()
            eco_Koppel_Na_tuning=sepcs_col_1[2].text.strip()
            eco_Koppel_Resultaat=sepcs_col_1[3].text.strip()

            sepcs_col_1 = specs_row[1].findAll('div')

            eco_ver_Origineel = sepcs_col_1[1].text.strip()
            eco_ver_Na_tuning = sepcs_col_1[2].text.strip()
            eco_ver_Resultaat = sepcs_col_1[3].text.strip()

        except:
            eco_Koppel_Resultaat = 'None'
            eco_Koppel_Na_tuning = "None"
            eco_Koppel_Origineel = 'None'

            eco_ver_Origineel = 'None'
            eco_ver_Na_tuning = 'None'
            eco_ver_Resultaat = 'None'

        print("eco_Koppel_Origineel : " +eco_Koppel_Origineel)
        print("eco_Koppel_Na_tunning : " +eco_Koppel_Na_tuning)
        print("eco_Koppel_Resultaat : " +eco_Koppel_Resultaat)
        print('~~~~~'*20)

#       ~~~~~~~~STAGE 1~~~~~~~~~~
        try:
            stage_specstb = soup.find('div', attrs={'data-title': 'Stage 1'})
            stage_specs_row = stage_specstb.findAll('div', attrs={'class': 'row'})
            stage_sepcs_col_2=stage_specs_row[2].findAll('div')

            Stage_Vermogen_Origineel = stage_sepcs_col_2[1].text.strip()
            Stage_Vermogen_Na_tuning = stage_sepcs_col_2[2].text.strip()
            Stage_Vermogen_Resultaat = stage_sepcs_col_2[3].text.strip()

            stage_sepcs_col_3 = stage_specs_row[3].findAll('div')

            Stage_Koppel_Origineel = stage_sepcs_col_3[1].text.strip()
            Stage_Koppel_Na_tuning = stage_sepcs_col_3[2].text.strip()
            Stage_Koppel_Resultaat = stage_sepcs_col_3[3].text.strip()
        except:
            Stage_Vermogen_Na_tuning='None'
            Stage_Vermogen_Origineel='None'
            Stage_Vermogen_Resultaat='None'
            Stage_Koppel_Na_tuning='None'
            Stage_Koppel_Resultaat='None'
            Stage_Koppel_Origineel='None'

        print("Stage_Koppel_Origineel : " + Stage_Koppel_Origineel)
        print("Stage_Koppel_Na_tunning :  " + Stage_Koppel_Na_tuning)
        print("Stage_Koppel_Resultaat : " + Stage_Koppel_Resultaat)
        print('~~~~~'*20)
        print("Stage_Vermogen_Origineel : " + Stage_Vermogen_Origineel)
        print("Stage_Vermogen_Na_tunning :  " + Stage_Vermogen_Na_tuning)
        print("Stage_Vermogen_Resultaat : " + Stage_Vermogen_Resultaat)

#       ~~~~~~~~Extra~~~~~~~~~
        try:
            extra=soup.find('div',attrs={'data-target':'options'})
            ex_row= extra.find('div',attrs={'class':'specs'}).findAll('div',attrs={'class':'row'})
            ex_data=ex_row[0].text.strip()
        except:
            ex_data='None'
            pass
        print(ex_data)
        self.row = self.row + 1

        sheet1.write(self.row, 0, Merk)
        sheet1.write(self.row, 1, Model)
        sheet1.write(self.row, 2, type_data)
        sheet1.write(self.row, 3, motor_data)
        sheet1.write(self.row, 4, eco_ver_Origineel)
        sheet1.write(self.row, 5, eco_Koppel_Origineel)
        sheet1.write(self.row, 6, eco_ver_Na_tuning)
        sheet1.write(self.row, 7, eco_Koppel_Na_tuning)
        sheet1.write(self.row, 8, eco_ver_Resultaat)
        sheet1.write(self.row, 9, eco_Koppel_Resultaat)
        sheet1.write(self.row, 10, Stage_Vermogen_Origineel)
        sheet1.write(self.row, 11, Stage_Vermogen_Na_tuning)
        sheet1.write(self.row, 12, Stage_Koppel_Na_tuning)
        sheet1.write(self.row, 13, Stage_Vermogen_Resultaat)
        sheet1.write(self.row, 14, Stage_Koppel_Resultaat)
        sheet1.write(self.row, 15, ex_data)

        wb.save('../Files/ECU.xls')
        print('Done ~ ' + str(self.row))
        import datetime
        print(datetime.datetime.now())


obj=ecu()
obj.get_category()