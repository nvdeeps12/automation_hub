import requests
from bs4 import BeautifulSoup
import xlrd,xlwt
rd = xlrd.open_workbook('../Files/Gmuyt.xlsx')
sheet = rd.sheet_by_index(0)
from ..January.return_true import match_titles
####
wb = xlwt.Workbook()
sheet1= wb.add_sheet('College Locations')
sheet1.write(0,0,"Collage Name")
sheet1.write(0,1,"Youtube Link")
sheet1.write(0,2,"Web Title")
sheet1.write(0,3,"Status")
from selenium import webdriver
options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver',options=options)
class yt():
    def get_youtubedata(self):
        self.row = 0
        for u in range(sheet.nrows):
            try:
                if u<150:
                    continue
                if u==401:
                    break
                self.value = sheet.cell_value(u, 1)
                if len(self.value) == 0:
                    continue
                links = 'https://www.youtube.com/results?search_query=' + str(self.value)+''
                links = str(links)
                # print('Link-->', links)
                req = driver.get(links)
                soup = BeautifulSoup(driver.page_source, u"html.parser")
                watch_link = soup.find('a',attrs={'id':'video-title'})

                try:
                    href = watch_link['href']
                    self.row+=1
                    college_title = watch_link.text.strip()
                    # print('Web Title->',college_title,'Value->',self.value)
                    title_status = match_titles(college_title,self.value)
                    if title_status==True:
                        status_ ='True'
                    else:
                        status_ = 'False'

                    self.getwatchdata(href,college_title,status_)
                except:
                    import sys
                    print(sys.exc_info())
                    continue
            except:
                continue

    def getwatchdata(self,ytlink,web_title,tit_status):
        import time
        you_link = 'https://www.youtube.com'+str(ytlink)
        emb=you_link.split('=')[1]
        # print('You--->',you_link)
        emb_link='<iframe width="560" height="315" src="https://www.youtube.com/embed/'+emb+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
        # print('embded link - '+emb_link)
        print('--'*100)

        sheet1.write(self.row, 0, self.value)
        sheet1.write(self.row, 1, emb_link)
        sheet1.write(self.row, 2, web_title)
        sheet1.write(self.row, 3, tit_status)
        # sheet1.write(u, 2, '')
        wb.save('College_Youtubes.xls')
        # driver.get(you_link)

        # soup = BeautifulSoup(driver.page_source, u"html.parser")
        # driver.find_element_by_xpath('/html/body/ytd-app/div/ytd-page-manager/ytd-watch-flexy/div[4]/div[1]/div/div[5]/div[2]/ytd-video-primary-info-renderer/div/div/div[3]/div/ytd-menu-renderer/div/ytd-button-renderer[1]/a').click()
        # time.sleep(5)
        # driver.find_element_by_xpath('/html/body/ytd-app/ytd-popup-container/paper-dialog[1]/ytd-unified-share-panel-renderer/div[3]/yt-third-party-network-section-renderer/div[1]/yt-third-party-share-target-section-renderer/div/div/yt-share-target-renderer[1]/button/yt-icon').click()
        # time.sleep(2)
        # # box=soup.find('div',attrs={'id':'labelAndInputContainer'})
        # # print(box)
        # try:
        #     textarea=soup.find('textarea',attrs={'id':'textarea'})
        #     print(textarea)
        # except:
        #     import sys
        #     print(sys.exc_info())





cc = yt()
cc.get_youtubedata()
