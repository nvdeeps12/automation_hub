import  requests
from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver',options=options)

wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Name')
sheet1.write(0,1,'Kies uw model')
sheet1.write(0,2,'Kies de generatie')
sheet1.write(0,3,'Kies het motortype')
sheet1.write(0,4,'Stage1 (Motordiagnose + datalogs)')
sheet1.write(0,5,'Satge1 (Stage 1 software)')
sheet1.write(0,6,'Satge1 (Testrit op de weg)')
sheet1.write(0,7,'Stage1 (Tuning certificaat)')
sheet1.write(0,8,'Stage1 (Vermogensrun voor en na)')
sheet1.write(0,9,'Stage1+ (Motordiagnose + datalogs)')
sheet1.write(0,10,'Satge1+ (Stage 1 software)')
sheet1.write(0,11,'Satge1+ (Testrit op de weg)')
sheet1.write(0,12,'Stage1+ (Tuning certificaat)')
sheet1.write(0,13,'Stage1+ (Vermogensrun voor en na)')
sheet1.write(0,14,'Vermogen (Standard)')
sheet1.write(0,15,'Vermogen (Na tuning)')
sheet1.write(0,16,'Vermogen (Verschil)')
sheet1.write(0,17,'Koppel (Standard)')
sheet1.write(0,18,'Koppel (Na tuning)')
sheet1.write(0,19,'Koppel (Verschil)')
sheet1.write(0,20,'Brandstof')
sheet1.write(0,21,'Methode')
sheet1.write(0,22,'Aanvullende opties')
sheet1.write(0,23,'Cilinderinhoud')
sheet1.write(0,24,'Compressieverhouding')
sheet1.write(0,25,'Type ecu')
sheet1.write(0,26,'Boring X slag')
sheet1.write(0,27,'Motornummer')


class atc():

    def get_brand_links(self):
        self.row = 0
        arr = ['Cadillac', 'Chevrolet', 'Chrysler', 'Citroën', 'Cupra', 'Dacia', 'Dodge', 'DS', 'Ferrari', 'Fiat', 'Ford', 'GMC', 'Holden', 'Honda', 'Hummer', 'Hyundai', 'Infiniti', 'Isuzu', 'Iveco', 'Jaguar', 'Jeep', 'Kia', 'Lamborghini', 'Lancia', 'Land Rover', 'Lexus', 'Lincoln', 'Lotus', 'MAN', 'Maserati', 'Mazda', 'McLaren', 'Mercedes-Benz', 'Mercury', 'Mini', 'Mitsubishi', 'Nissan', 'Oldsmobile', 'Opel', 'Peugeot', 'Pontiac', 'Porsche', 'Renault', 'Rolls Royce', 'Rover', 'Saab', 'Saturn', 'Seat', 'Skoda', 'Smart', 'SsangYong', 'Subaru', 'Suzuki', 'Tesla', 'Toyota', 'Vauxhall', 'Volkswagen', 'Volvo', 'Kies uw model', 'Kies de generatie', 'Kies het motortype']
        for b in arr:
            try:
                self.brand = b
            except:
                brand = 'None'

            brand_link = 'https://www.atm-chiptuning.com/chiptuning/'+str(self.brand)+str('/').strip()
            brand_link = brand_link.replace(' ','-')
            print('Brand Link-->',brand_link)
            con = requests.get(brand_link)
            soup = BeautifulSoup(con.content,u'html.parser')
            self.get_models(soup)


    def get_models(self,modelsoup):
        try:
            links= modelsoup.findAll('a',attrs={'class':'btn btn--ghost Chiptuning-options__item'})
            for l in links:
                try:
                    md_link = l['href']
                    model = l.text.strip()

                    self.getmodelyear(md_link,model)
                except:
                    continue
        except:
            links = ''



    def getmodelyear(self,y_link,model):
        y_con = requests.get(y_link)
        soup = BeautifulSoup(y_con.content,u'html.parser')
        anchors = soup.findAll('a',attrs={'class':'btn btn--ghost Chiptuning-options__item'})
        for a in anchors:
            try:
                type_link = a['href']
                types = a.text.strip()
                self.getmotor_types(type_link,model,types)
            except:
                continue

    def getmotor_types(self, m_link,model,types):

        g_con = requests.get(m_link)
        soup = BeautifulSoup(g_con.content, u'html.parser')
        type_links = soup.findAll('a',attrs={'class':'btn btn--ghost Chiptuning-options__item'})
        for tp in type_links:
            try:
                stage_link = tp['href']
                motor_type = tp.text.strip()
                ## Interating The Rows
                self.row=self.row+1
                self.get_stages(stage_link,model,types,motor_type)
            except:
                continue


    def get_stages(self,stage_link,model,types,motor_type):
        print(stage_link)
        req=requests.get(stage_link)

        soup=BeautifulSoup(req.content,u"html.parser")
        try:
            content=soup.find('div',attrs={'class':'Chiptuning-details__info'})
            # Name=content.find('h2').text.strip()
            # print(Name)
            Name = self.brand
            # stage=content.find('a',attrs={'class':'Chiptuning-stages__stage'}).text.strip()

            info1=content.findAll('div',attrs={'class':'Chiptuning-stages__stage-info'})

# ~~~~~~~~~~~~~~~~~~~~~~~ Stage 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            # print('~~' * 10 + 'Stage 1' + '~~' * 10)
            info_li=info1[0].findAll('li')

            md_cls=info_li[0].get('class')
            md_cls=md_cls[0]
            if md_cls=='yes':
                md='yes'
            else:
                md='no'

            soft_cls = info_li[1].get('class')
            soft_cls = soft_cls[0]
            if soft_cls == 'yes':
                soft = 'yes'
            else:
                soft = 'no'

            tod_cls = info_li[2].get('class')
            tod_cls = tod_cls[0]
            if tod_cls == 'yes':
                tod = 'yes'
            else:
                tod = 'no'

            tc_cls = info_li[3].get('class')
            tc_cls = tc_cls[0]
            if tc_cls == 'yes':
                tc = 'yes'
            else:
                tc = 'no'

            vvn_cls = info_li[4].get('class')
            vvn_cls = vvn_cls[0]
            if vvn_cls == 'yes':
                vvn = 'yes'
            else:
                vvn = 'no'

            # print("Motordiagnose + datalogs - " + md)
            # print("Stage 1 software - " + soft)
            # print("Testrit op de weg - " + tod)
            # print("Tuning certificaat - " + tc)
            # print("Vermogensrun voor en na - " + vvn)

# ~~~~~~~~~~~~~~~~~~~~~~~ Stage 1+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#             print('~~' * 10 + 'Stage 1+' + '~~' * 10)
            info_li = info1[1].findAll('li')

            md_cls = info_li[0].get('class')
            md_cls = md_cls[0]
            if md_cls == 'yes':
                md1 = 'yes'
            else:
                md1 = 'no'

            soft_cls = info_li[1].get('class')
            soft_cls = soft_cls[0]
            if soft_cls == 'yes':
                soft1 = 'yes'
            else:
                soft1 = 'no'

            tod_cls = info_li[2].get('class')
            tod_cls = tod_cls[0]
            if tod_cls == 'yes':
                tod1 = 'yes'
            else:
                tod1 = 'no'

            tc_cls = info_li[3].get('class')
            tc_cls = tc_cls[0]
            if tc_cls == 'yes':
                tc1 = 'yes'
            else:
                tc1 = 'no'

            vvn_cls = info_li[4].get('class')
            vvn_cls = vvn_cls[0]
            if vvn_cls == 'yes':
                vvn1 = 'yes'
            else:
                vvn1 = 'no'



            # print("Motordiagnose + datalogs - "+md1)
            # print("Stage 1 software - "+soft1)
            # print("Testrit op de weg - "+tod1)
            # print("Tuning certificaat - "+tc1)
            # print("Vermogensrun voor en na - "+vvn1)
 # ~~~~~~~~~~~~~~~~~~~~~~~ info2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            # print('~~'*10+'Info 2'+'~~'*10)
            info2=content.find('div',attrs={'class':'Chiptuning-comparison'})
            all_row=info2.findAll('div',attrs={'class':'Chiptuning-comparison__col'})
            # verm=all_row[1]
            # all_verm=all_row[1].find('div',attrs={'class':'Chiptuning-comparison__number'})
            # print(all_verm)
            try:
                verm_standard=all_row[5].text.strip()
            except:
                verm_standard='none'
            try:

                verm_natunning=all_row[6].text.strip()
            except:
                verm_natunning='none'
            try:

                verm_verschil=all_row[7].text.strip()
            except:
                verm_verschil='none'
            # print("Vermogen Standard - "+verm_standard)
            # print("Vermogen NA-Tunning - "+verm_natunning)
            # print("Vermogen Verschil - "+verm_verschil)
            try:

                kop_standard = all_row[9].text.strip()
            except:
                kop_standard='none'
            try:
                kop_natunning = all_row[10].text.strip()
            except:
                kop_natunning='none'
            try:
                kop_verschil = all_row[11].text.strip()
            except:
                kop_verschil='none'
            # print("Koppel Standard - " + kop_standard)
            # print("Koppel NA-Tunning - " + kop_natunning)
            # print("Koppel Verschil - " + kop_verschil)

            # print('~~' * 10 + 'Info 3' + '~~' * 10)
            info3=content.find('table',attrs={'class':'Chiptuning-extra'})

            all_td=info3.findAll('td')
            try:

                Brandstof=all_td[2].text.strip()
            except:
                Brandstof='none'
            try:

                Methode=all_td[5].text.strip()
            except:
                Methode="none"
            try:
                Aanvullende_opties=all_td[8].text.strip()
            except:
                Aanvullende_opties='none'
            # print("Brandstof - "+ Brandstof)
            # print("Methode - "+ Methode)
            # print("Aanvullende_opties - "+ Aanvullende_opties)
            #
            # print('~~' * 10 + 'Info 4' + '~~' * 10)

            info4=content.find('div',attrs={'class':'Chiptuning-specs'})
            # moto_table=info4.findAll('table')
            moto_all_td=info4.findAll('td')
            try:
                Cilinderinhoud=moto_all_td[1].text.strip()
            except:
                Cilinderinhoud='none'
            try:
                Compressieverhouding=moto_all_td[3].text.strip()
            except:
                Compressieverhouding='none'
            import re
            try:
                Type_ecu=moto_all_td[5].text.strip()

                Type_ecu = soup.find('td', text=re.compile('Type ecu')).find_next('td').text.strip()
            except:
                Type_ecu='none'
            try:
                Boring_X_slag=moto_all_td[7].text.strip()
                Boring_X_slag = soup.find('td', text=re.compile('Boring X slag')).find_next('td').text.strip()
            except:
                Boring_X_slag='None'
            try:
                Motornummer=moto_all_td[9].text.strip()
                Motornummer = soup.find('td', text=re.compile('Motornummer')).find_next('td').text.strip()
            except:
                Motornummer='none'
            # print('Cilinderinhoud - ' + Cilinderinhoud)
            # print('Compressieverhouding - ' + Compressieverhouding)
            print('Type_ecu - ' + Type_ecu)
            print('Boring_X_slag - ' + Boring_X_slag)
            print('Motornummer - ' + Motornummer)

            sheet1.write(self.row, 0,Name)
            sheet1.write(self.row, 1, model)
            sheet1.write(self.row, 2, types)
            sheet1.write(self.row, 3, motor_type)
            sheet1.write(self.row, 4,md)
            sheet1.write(self.row, 5, soft)
            sheet1.write(self.row, 6, tod)
            sheet1.write(self.row, 7, tc)
            sheet1.write(self.row, 8, vvn)
            sheet1.write(self.row, 9, md1)
            sheet1.write(self.row, 10, soft1)
            sheet1.write(self.row, 11, tod1)
            sheet1.write(self.row, 12, tc1)
            sheet1.write(self.row, 13, vvn1)
            sheet1.write(self.row, 14, verm_standard)
            sheet1.write(self.row, 15, verm_natunning)
            sheet1.write(self.row, 16, verm_verschil)
            sheet1.write(self.row, 17, kop_standard)
            sheet1.write(self.row, 18, kop_natunning)
            sheet1.write(self.row, 19, kop_verschil)
            sheet1.write(self.row, 20, Brandstof)
            sheet1.write(self.row, 21, Methode)
            sheet1.write(self.row, 22, Aanvullende_opties)
            sheet1.write(self.row, 23, Cilinderinhoud)
            sheet1.write(self.row, 24, Compressieverhouding)
            sheet1.write(self.row, 25, Type_ecu)
            sheet1.write(self.row, 26, Boring_X_slag)
            sheet1.write(self.row, 27, Motornummer)
            wb.save('Atm-Chip_cadilag.xls')
            print('Done Records-->',self.row)
        except:
            import sys
            print(sys.exc_info())








objc = atc()
# objc.get_stages('https://www.atm-chiptuning.com/chiptuning/audi-a2-14-tdi-75pk/')
objc.get_brand_links()




