import requests
from bs4 import BeautifulSoup
import xlwt, xlrd
import csv
from selenium import webdriver
# from scraper_api import ScraperAPIClient
# client = ScraperAPIClient('64a1e7c80a691e18f448658e3dca39e8')


options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Chrome(
    executable_path='/home/nvdeep/Projects/Fiver/Drivers/chromedriver', options=options)



fields = ['Name', 'Sheet Address', 'Web Address', 'Landline', 'Wireless', 'Web_name','Mails','People Link']
out_file = open('Thornton-Absentee--2020-1.csv', 'w')
csvwriter = csv.DictWriter(out_file, delimiter=',', fieldnames=fields)
csvwriter.writeheader()


wb1 = xlwt.Workbook()
sheet1 = wb1.add_sheet('Data')

sheet1.write(0, 0, "Name")
sheet1.write(0, 1, "Sheet Address")
sheet1.write(0, 2, "Web Address")
sheet1.write(0, 3, "Landline")
sheet1.write(0, 4, "Wireless")
sheet1.write(0, 5, "Web_name")
sheet1.write(0, 6, "Mails")
sheet1.write(0, 7, "Status")
sheet1.write(0, 8, "People Link")

loc = ("/home/nvdeep/Downloads/address.xlsx")
#
# To open Workbook
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)


class jj():
    def name_checkfnc(self,sheet_fname, sheet_lname, web_fname, web_lname):
        sheet_fname = str(sheet_fname).lower()
        sheet_lname = str(sheet_lname).lower()
        web_fname = str(web_fname).lower()
        web_lname = str(web_lname).lower()
        print('Matching-->', sheet_fname, '-', sheet_lname, ' With', web_fname, '-', web_lname)
        
        return True

        if sheet_fname == web_fname and sheet_lname == web_lname:
            print('Status Is True')
            return True
        else:
            print('Status Is False')
            return False

    def address_check(self,sheet_address, full_address):
        # sheet_city = str(sheet_city).lower()
        # sheet_state = str(sheet_state).lower()
        sheet_address = str(sheet_address).lower().strip()
        full_address = str(full_address).lower().strip()

        full_address = full_address.replace(' ', '-')
        full_address = full_address.replace(',', '')
        full_address = full_address.replace('--', '-')

        sheet_address = sheet_address.replace(',','')
        sheet_address = sheet_address.replace(' ', '-')
        sheet_address = sheet_address.replace('---', '-')
        sheet_address = sheet_address.replace('--', '-')


        print('Sheet Address-->', sheet_address)
        print('Full_Address-->', full_address)

        return True

        if sheet_address == full_address:
            print('Status Is True')
            print('Sheet Address-->', sheet_address)
            print('Web Address--->', full_address)
            print('--' * 100)
            return True
        else:
            print('Status Is False')
            # print('City-->',sheet_city)
            # print('State-->',sheet_state)
            print('--' * 100)
            return False

    def get_links(self):
        self.row = 0
        for p in range(sheet.nrows):
            
            import time
            print('Ruuning-->',p)
            page = sheet.cell_value(p, 17)
            page = str(page).replace(' ','-')
            page = page.replace('--','-')
            self.people_page = page
            self.first = sheet.cell_value(p,13)
            self.last = sheet.cell_value(p, 14)
            self.name = self.first+' '+self.last
            self.full_address = sheet.cell_value(p, 12)

            # self.state = sheet.cell_value(p, 9)

            self.agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
            try:
                result = driver.get(page)
                print('Page--',page)
                
                import time
                
                # print(con)
                soup = BeautifulSoup(driver.page_source, u'html.parser')
                title = soup.find('title').text.strip()
                print('Title -->',title)
                if 'Access' in title:
                    print('Capcha Waiting To Solve')
                    
                    time.sleep(180)
                name_check = soup.find('h1',attrs={'class':'name-list-header'}).text.strip()
                # print(name_check)
                total_pages = soup.find('h1',attrs={'class':'name-list-header'}).find_next('b').text.strip()

                total_pages = int(total_pages)/6
                total_pages = int(total_pages)+1

                for pg in range(total_pages):
                    # if pg == 0:
                    #     continue
                    pages_ = str(page)+'/'+str('page-')+str(pg)
                    
                    print('--' * 100)
                    con = driver.get(page)
                    soup = BeautifulSoup(driver.page_source, u'html.parser')
                    # print(soup)
                    divs = soup.findAll('div', attrs={'class': 'lh-20px'})
                    h3_arr = []
                    for ds in divs:
                        phones = ds.findAll('a',attrs={'class':'link-underline'})
                        phones_arr = []
                        # for phone in phones:
                        #     ph = phone['title']
                            
                        #     # if '(' not in ph:
                        #     #     continue
                        #     # ph = ph.text.split('(')[1].strip()
                        #     phones_arr.append(ph)
                        # print(phones_arr)
                            
                        # print('Phones-->>',phones)
                        
                        profile_link=ds.find('h2').find('a').get('href')
                        print('Prof-->',profile_link)
                        self.name_1 = ds.find('h2').find('a').text.strip()
                        self.fname_web=str(self.name_1).split(' ')[0].lower()
                        self.lname_web=str(self.name_1).split(' ')[-1].lower()
                        self.name_web=self.fname_web+" "+self.lname_web
                        anchors = ds.findAll('h3')
                        h3_arr.append(anchors)
                        self.get_addresses(h3_arr,profile_link)

            except:
                import sys
                # print(sys.exc_info())
                continue

    def get_addresses(self, add_arr,profile_link):
    
        for ad in add_arr:
            h3 = ad
            self.get_add_links(h3,profile_link)

    def get_add_links(self, h3_links,profile_link):

        for ads in h3_links:
            try:
                address_href = ads.find('a')['href']
                add_name = ads.find('a').text.strip()
                add_name=str(add_name).replace(',',' ')
                # print(len(add_name))

                self.web_address = add_name
                if '(' in add_name or len(add_name)<10:
                    continue
                name_status = self.name_checkfnc(self.first,self.last,self.fname_web,self.lname_web)
                # print(name_status)
                add_check = self.address_check(self.full_address,add_name)

                if add_check==True:
                    if name_status==True:
                        Status = 'True'
                        print('Getting Phones Now')
                        self.get_phones(profile_link,Status)
            except:
                import sys
                continue


    def get_phones(self, anc,Status):
        # print('I am In Phone'*100)
        print(anc)
        mails_arr = []
        wiree = []
        landd = []
        prof_dict = {}

        self.row = self.row + 1
        con = driver.get(anc)
        import time
        time.sleep(1)
        soup = BeautifulSoup(driver.page_source, u'html.parser')

        try:
            p = soup.findAll('div', attrs={'class': 'py-3'})
            # print('HH',p)
            print('**'*100)

            phone = p[0].findAll('a')
            # print(phone)
            
            w = 2
            l = 0

            for ph in phone:
                wire = ph
                smallp = ph.find_next('small').text.strip()
                print('Smalll-->>',smallp)
                
                w = w + 1
                wi = ph.text.strip()
                wi = wi + " , "
                
                if 'Wireless' in smallp:
                    wiree.append(wi)
                else:
                    landd.append(wi)
                
        except:
            pass
        
        print(wiree)
        print(landd)


        try:
            emails = soup.findAll('h3',attrs={'class':'my-card-block'})
            for e in emails:
                try:
                    mails = e.text.strip()
                    if '@' in mails:
                        mails_arr.append(mails)
                except:
                    pass
        except:
            pass
        
        sheet1.write(self.row,0,self.name)
        sheet1.write(self.row,1,self.full_address)
        sheet1.write(self.row, 2, self.web_address)
        sheet1.write(self.row,3,landd)
        sheet1.write(self.row,4,wiree)
        sheet1.write(self.row, 5, self.name_web)
        sheet1.write(self.row, 6, mails_arr)
        sheet1.write(self.row, 7, Status)
        sheet1.write(self.row, 8, self.people_page)
        wb1.save('smartback.xls')
        print('Done-->',self.row)





obj = jj()
obj.get_links()
