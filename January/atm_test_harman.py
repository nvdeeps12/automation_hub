import requests
from bs4 import BeautifulSoup

import xlwt, xlrd

import xlwt,xlrd


wb1 = xlwt.Workbook()
sheet1 = wb1.add_sheet('Data')

sheet1.write(0, 0, "Name")
sheet1.write(0, 1, "Address")
sheet1.write(0, 2, "Landline")
sheet1.write(0, 3, "Wireless")

loc = ("address.xlsx")
#
# To open Workbook
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)


loc = ("F:\Projects\Fiver\Files\Address.xlsx")
#
# To open Workbook
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)


class jj():

    def get_links(self):
        self.row = 0

        for p in range(sheet.nrows):
            page = sheet.cell_value(p, 16)
            # print(page)
            if len(page) == 0 or 'address' in page:

                self.first = sheet.cell_value(p,13)
                # if 'First' in first:
                self.last = sheet.cell_value(p, 14)
                # if 'Last' in last:
                self.name = self.first+' '+self.last
                # print(self.name)
            self.address = sheet.cell_value(p, 12)
            # print(self.address)

        # print(self.name)
        # print(self.address)
        #     print(page)
            agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
            try:
                con = requests.get(page, headers={'user-agent': agent})
                # print(con)
                soup = BeautifulSoup(con.content, u'html.parser')
                name_check = soup.find('h1',attrs={'class':'name-list-header'}).text.strip()
                total_pages = soup.find('h1',attrs={'class':'name-list-header'}).find_next('b').text.strip()

                total_pages = int(total_pages)/6

                total_pages = int(total_pages)+1

                for pg in range(total_pages):
                    if pg == 0:
                        continue
                    pages_ = str(page)+'/'+str('page-')+str(pg)
                    print('Paging-->',pages_)
                    con = requests.get(pages_, headers={'user-agent': agent})
                    # print(con)
                    soup = BeautifulSoup(con.content, u'html.parser')

                    divs = soup.findAll('div', attrs={'class': 'p-1 mw-100'})
                    h3_arr = []
                    for ds in divs:

                        profile_link=ds.find('h2').find('a').get('href')

                        name_1 = ds.find('h2').find('strong').text.strip()
                        self.fname_web=str(name_1).split(' ')[0]
                        self.lname_web=str(name_1).split(' ')[-1]
                        self.name_web=self.fname_web+" "+self.lname_web
                        # print('H22',self.name_1)
                        anchors = ds.findAll('h3')
                        h3_arr.append(anchors)
                        # print('Anc-->',anchors)

                        self.get_addresses(h3_arr,profile_link)
                        # self.get_phones(anchors)
            except:
                import sys
                print(sys.exc_info())
                continue
    def get_addresses(self, add_arr,profile_link):

        # pages_arr = ['https://www.smartbackgroundchecks.com/people/Raymond-Dejarnette/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Myron-Bigler/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Melicendro-Abeyta/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/William-Kalthoff/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Lillian-Favors/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Barry-Goltz/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Jackie-Sarchett/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Robert-Pace/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/James-Smutka/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Alexzandra-Taylor/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Teddy-Swander/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Michael-Coon/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Sara-Purviance/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Charles-Oliver/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Ben-Mobley/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Richard-Elliott/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Elliott-Barton/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Susan-Dorle/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Ronald-Bell/Colorado Springs/CO', 'https://www.smartbackgroundchecks.com/people/Laurie-Bolster/Colorado Springs/CO']

        for p in range(sheet.nrows):
            page = sheet.cell_value(p, 16)
            if len(page)==0 or 'address' in page:
                continue

            first = sheet.cell_value(p, 13)
            print(first)
            if 'First' in first:
                continue
            print(first)
            last = sheet.cell_value(p, 14)
            if 'Last' in last:
                continue
            self.name = first+' '+last

            self.address = sheet.cell_value(p, 12)

            if 'Full' in self.address:
                continue
            print(self.name)
            print(self.address)
            # print(page)
            agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
            con = requests.get(page,headers={'user-agent': agent})
            soup = BeautifulSoup(con.content,u'html.parser')
            # name_check = soup.find('h1',attrs={'class':'name-list-header'}).text.strip()
            divs = soup.findAll('div',attrs={'class':'p-1 mw-100'})
            h3_arr = []
            for ds in divs:
                self.row = self.row + 1
                # self.name = ds.find('h2').find('strong').text.strip()
                # print('H22',self.name)
                anchors = ds.findAll('h3')
                h3_arr.append(anchors)
                # print('Anc-->',anchors)
                print('--'*50)
                # self.get_addresses(h3_arr)
                # self.get_phones(anchors)


    def get_addresses(self,add_arr):


        for ad in add_arr:
            h3 = ad

            self.get_add_links(h3,profile_link)
            # anc= ad.find('a')['href']
            # print(anc)

    def get_add_links(self, h3_links,profile_link):

            self.get_add_links(h3)
            # anc= ad.find('a')['href']
            # print(anc)


    def get_add_links(self,h3_links):


        for ads in h3_links:
            try:
                address_href = ads.find('a')['href']
                add_name = ads.find('a').text.strip()

                add_name=str(add_name).replace(',',' ')
                # print(self.name+"===="+self.name_web)
                # print(add_name+'==='+self.address)

                # if str(add_name.lower()) ==str(self.address.lower()):
                #     print('I am In Address')
                if add_name==self.address and self.name==self.name_web:
                    # print('Fname Web-->', self.fname_web)
                    # print('Lname Web-->', self.lname_web)
                    # print(add_name)
                    # print(address_href)
                    self.row = self.row + 1
                    self.get_phones(profile_link)
            except:
                import sys
                print(sys.exc_info())
                add_name = 'None'
                address_href = 'None'
            # print('Name-->', self.name)
            # print(add_name)
            # print(address_href)

    def get_phones(self, anc):
        print("Hello "+str(anc))
        print(' i AM in Get Phone')
            except:
                add_name = 'None'
                address_href = 'None'
            print('Name-->',self.name)
            print(add_name)
            print(address_href)






    def get_phones(self,anc):

>>>>>>> 5a6c8585f7889eef6141d19f2f83aae1ae2ac99e
        con = requests.get(anc)
        soup = BeautifulSoup(con.content, u'html.parser')
        p = soup.findAll('div', attrs={'class': 'py-3'})

        # print("####"*10)
        try:
            phone = p[1].findAll('a')
            w = 2
            l = 0

            wiree = []
            landd = []
            for ph in phone:
                wire = ph.find_next('small')

                if "Wireless" in wire:
                    w = w + 1
                    wi = ph.text.strip()
                    wi = wi + " , "
                    wiree.append(wi)
                    # print("Wireless = "+ph.text.strip())
                elif 'LandLine/Services' in wire:
                    l = l + 1
                    land = ph.text.strip()
                    land = land + ' , '
                    landd.append(land)
                    # print("LandLine/Services = "+ph.text.strip())
                else:
                    l = 0
                    w = 0

<<<<<<< HEAD
                # print(wiree)
                # print(landd)
            sheet1.write(self.row,0,self.name)
            sheet1.write(self.row,1,self.address)
            sheet1.write(self.row,2,landd)
            sheet1.write(self.row,3,wiree)
            wb1.save('output1.xls')
=======
                print(wiree)
                print(landd)
            # sheet1.write(self.row,0,self.name)
            # sheet1.write(self.row,1,landd)
            # sheet1.write(self.row,2,wiree)
            # wb.save('out.xls')
>>>>>>> 5a6c8585f7889eef6141d19f2f83aae1ae2ac99e
            print(self.row)
            print('~~~~~~~~~' * 100)
        except:
            import sys
            print(sys.exc_info())
            phone = 'none'
        # print(phone)
        # try:
        #     p1 = p[0]
        # except:
        #     p1 = 'None'
        #
        # try:
        #     p2 = p[1]
        # except:
        #     p2 = "None"
        #
        # print(p1,p2)


obj = jj()
obj.get_links()