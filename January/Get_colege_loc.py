import requests
from selenium import webdriver
from bs4 import BeautifulSoup
import xlrd,xlwt
import time

options = webdriver.FirefoxOptions()
        # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='..\Drivers\geckodriver',options=options)
rd = xlrd.open_workbook('../Files/Gmm_data.xls')
sheet = rd.sheet_by_index(0)
wb = xlwt.Workbook()
sheet1= wb.add_sheet('College Locations')
sheet1.write(0,0,"Collage Name")
sheet1.write(0,1,"Location Link")
# sheet1.write(0,2,"Images Link")

for u in range(sheet.nrows):
    try:
        print('Starting->',u)
        value = sheet.cell_value(u, 1)
        if len(value)==0:
            continue
        links = 'https://www.google.com/maps/place/'+str(value)
        links = str(links)
        print('Link-->',links)
        driver.get(links)
        import time

        time.sleep(3)
        driver.find_element_by_id('searchbox-searchbutton').click() ## Clicked On Search

        time.sleep(5)
        ## Click On share
        driver.find_element_by_xpath('/html/body/jsl/div[3]/div[9]/div[8]/div/div[1]/div/div/div[5]/div[5]/div/button').click()
        time.sleep(2)
        ## Click For Link
        driver.find_element_by_xpath('//*[@id="modal-dialog-widget"]/div[2]/div/div[3]/div/div/div/div[2]/button[2]').click()
        soup = BeautifulSoup(driver.page_source, u"html.parser")
        # print(soup)

        iframe = soup.find('input',attrs={'class':'section-embed-map-input'})['value']
        src = str(iframe).split('width')[0].split('src="')[1].split('"')[0]
        location_link = src
        print('Location Link-->',location_link)
        sheet1.write(u, 0, value)
        sheet1.write(u, 1, location_link)
        # sheet1.write(u, 2, '')
        wb.save('College_Locations1.xls')

        print('--'*100)
    except:
        continue