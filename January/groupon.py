import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
from scraper_api import ScraperAPIClient
client = ScraperAPIClient('64a1e7c80a691e18f448658e3dca39e8')

# options = webdriver.ChromeOptions()
        # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
# options.add_argument('--disable-gpu')
# options.add_argument('--headless')
# driver = webdriver.PhantomJS(executable_path='..\Drivers\phantomjs')
links_arr = []




wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Bisnuess_Name')
sheet1.write(0,1,'Street')
sheet1.write(0,2,'City')
sheet1.write(0,3,'Pin')
sheet1.write(0,4,'State')
sheet1.write(0,5,'Phone')
sheet1.write(0,6,'Website')
sheet1.write(0,7,'Total_Sold')

class groupon():

    def get_links(self):
        self.row = 0
        self.agent = 'Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19'

        result = requests.get('https://www.groupon.com/browse/chicago?category=automotive',headers ={'User-Agent':self.agent})
        # print('Page--', page)
        # print(con)
        soup = BeautifulSoup(result.content, u'html.parser')
        print(soup)
        total_pages = soup.find('p',attrs={'class':'results'}).text.strip()
        print(total_pages)
        total_pages = total_pages.split('of')[1].split('results')[0]
        total_pages = int(total_pages)
        remainder = total_pages%36
        total_pages = total_pages/36
        total_pages = int(total_pages)
        if remainder!=0:
            total_pages+=2

        for p in range(total_pages):
            pages = 'https://www.groupon.com/browse/chicago?category=automotive&page='+str(p)
            cons = driver.get(pages)
            page_soup = BeautifulSoup(driver.page_source,u'html.parser')

            divs = page_soup.findAll('div',attrs={'class':'cui-content'})

            for d in divs:
                try:
                    link = d.find('a')['href']
                    self.row+=1
                    self.get_groupondata(link)
                except:
                    link = 'None'


    def get_groupondata(self,grplink):
        print(grplink)
        # cons = requests.get(grplink, headers={'user-agent': self.agent})
        # page_soup = BeautifulSoup(cons.content, u'html.parser')

        driver.get(grplink)
        import time
        time.sleep(5)
        page_soup=BeautifulSoup(driver.page_source,u"html.parser")
        try:
            bisnuess_name = page_soup.find('h1',attrs={'id':'deal-title'}).text.strip()
        except:
            bisnuess_name = "None"

        try:
            full_address=page_soup.find('div',attrs={'class':'address-bottom'}).text.strip()

            street=full_address.split(',')[0]

            city=full_address.split(street)[1].split(',')[1].split(',')[0]

            stateNDpin=full_address.split(city)[1].split(',')[1]

            pin=[int(i) for i in stateNDpin.split() if i.isdigit()]
            pin=pin[0]

            state=stateNDpin.split(str(pin))[0]

        except:
            street='none'
            city='none'
            state='none'
            pin='none'

        try:
            Phone=page_soup.find('li',attrs={'class':'phone-number contact-row'}).text.strip()
        except:
            Phone='none'

        try:
            website=page_soup.find('a',attrs={'class':'merchant-website'}).get('href')
        except:

            website='none'

        try:
            g_sold='None'
            all_solds=page_soup.findAll('div',attrs={'class':'breakout-messages'})
            self.grp_arr = []

            for gp in all_solds:
                gp=gp.text.strip()
                if 'purchase' in gp:
                    gp = gp.split('purchase')[1]
                self.grp_arr.append(gp)

            g_sold = self.grp_arr
        except:
            g_sold='None'

        if g_sold!='None':
            if len(g_sold)==1:
                g_sold = g_sold[0]
            else:
                g_sold = g_sold

        # print(bisnuess_name)
        # print(street)
        # print(city)
        # print(pin)
        # print(state)
        # print(Phone)
        # print(website)
        print('GSOld',g_sold)
        print('--'*50)
        sheet1.write(self.row, 0, bisnuess_name)
        sheet1.write(self.row, 1, street)
        sheet1.write(self.row, 2, city)
        sheet1.write(self.row, 3, pin)
        sheet1.write(self.row, 4, state)
        sheet1.write(self.row, 5, Phone)
        sheet1.write(self.row, 6, website)
        sheet1.write(self.row, 7, g_sold)
        wb.save('Groupon.xls')
objs = groupon()
objs.get_links()