import requests
from bs4 import BeautifulSoup
import xlwt
import time
from selenium import webdriver

options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver', options=options)
wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Merk')
sheet1.write(0,1,'Model')
sheet1.write(0,2,'Type')
sheet1.write(0,3,'Motor')
sheet1.write(0,4,'Origineel vermogen(Eco)')
sheet1.write(0,5,'Origineel Koppel(Eco)')
sheet1.write(0,6,'Vermogen na tunning(Eco)')
sheet1.write(0, 7, 'Koppel na tunning(Eco)')
sheet1.write(0,8,'Resultaat vermogen(Eco)')
sheet1.write(0, 9, 'Resultaat Koppel(Eco)')
sheet1.write(0,10,'Origineel vermogen(Stage 1)')
sheet1.write(0,11, 'Vermogen na tunning(Stage 1)')
sheet1.write(0,12,'Koppel na tunning(Stage 1)')
sheet1.write(0,13, 'Resltaat vermogen(Stage 1)')
sheet1.write(0,14,'Resltaat koppel(Stage 1)')
sheet1.write(0,15, 'Extra opties')


def pass_dfs():
    arr = ['https://www.ecu-soft.be/chiptuning/alpina/b4/all/b4_biturbo_410','https://www.ecu-soft.be/chiptuning/alpina/b5/all/4_4_v8_supercharged_507','https://www.ecu-soft.be/chiptuning/alpina/b6/all/4_4_v8_bi_turbo_500']
    for d in arr:
        links = d
        get_data(links)


def get_data(end_link):
    row = 1
    req = requests.get(end_link)
    soup=BeautifulSoup(req.content,u'html.parser')
    try:
        Merk=soup.find('div',attrs={'class':'title'}).text.strip()
    except:
        Merk='None'

    print(Merk)

    try:
        Model=soup.find('div',attrs={'class':'subtitle'}).text.strip()
    except:
        Model='None'

    print(Model)
    try:
        type=soup.find('select',attrs={'name':'year'})
        type_data=type.find('option',attrs={'selected':'selected'}).text.strip()
    except:
        type_data='None'
    print(type_data)

    try:
        motor = soup.find('select', attrs={'name': 'motor'})
        motor_data = motor.find('option', attrs={'selected': 'selected'}).text.strip()
    except:
        motor_data='None'
    print(motor_data)

#         ~~~~~~~~~~specs~~~~~~~~~~~~~
#         ~~~~~~~ECO~~~~~~~~~~
    try:
        specstb=soup.find('div',attrs={'data-target':'eco'})
        specs_row=specstb.findAll('div',attrs={'class':'row'})

        sepcs_col_1=specs_row[2].findAll('div')

        eco_Koppel_Origineel=sepcs_col_1[1].text.strip()
        eco_Koppel_Na_tuning=sepcs_col_1[2].text.strip()
        eco_Koppel_Resultaat=sepcs_col_1[3].text.strip()

        sepcs_col_1 = specs_row[1].findAll('div')

        eco_ver_Origineel = sepcs_col_1[1].text.strip()
        eco_ver_Na_tuning = sepcs_col_1[2].text.strip()
        eco_ver_Resultaat = sepcs_col_1[3].text.strip()

    except:
        eco_Koppel_Resultaat = 'None'
        eco_Koppel_Na_tuning = "None"
        eco_Koppel_Origineel = 'None'

        eco_ver_Origineel = 'None'
        eco_ver_Na_tuning = 'None'
        eco_ver_Resultaat = 'None'

    print("eco_Koppel_Origineel : " +eco_Koppel_Origineel)
    print("eco_Koppel_Na_tunning : " +eco_Koppel_Na_tuning)
    print("eco_Koppel_Resultaat : " +eco_Koppel_Resultaat)
    print('~~~~~'*20)

#       ~~~~~~~~STAGE 1~~~~~~~~~~
    try:
        stage_specstb = soup.find('div', attrs={'data-title': 'Stage 1'})
        stage_specs_row = stage_specstb.findAll('div', attrs={'class': 'row'})
        stage_sepcs_col_2=stage_specs_row[2].findAll('div')

        Stage_Vermogen_Origineel = stage_sepcs_col_2[1].text.strip()
        Stage_Vermogen_Na_tuning = stage_sepcs_col_2[2].text.strip()
        Stage_Vermogen_Resultaat = stage_sepcs_col_2[3].text.strip()

        stage_sepcs_col_3 = stage_specs_row[3].findAll('div')

        Stage_Koppel_Origineel = stage_sepcs_col_3[1].text.strip()
        Stage_Koppel_Na_tuning = stage_sepcs_col_3[2].text.strip()
        Stage_Koppel_Resultaat = stage_sepcs_col_3[3].text.strip()
    except:
        Stage_Vermogen_Na_tuning='None'
        Stage_Vermogen_Origineel='None'
        Stage_Vermogen_Resultaat='None'
        Stage_Koppel_Na_tuning='None'
        Stage_Koppel_Resultaat='None'
        Stage_Koppel_Origineel='None'

    print("Stage_Koppel_Origineel : " + Stage_Koppel_Origineel)
    print("Stage_Koppel_Na_tunning :  " + Stage_Koppel_Na_tuning)
    print("Stage_Koppel_Resultaat : " + Stage_Koppel_Resultaat)
    print('~~~~~'*20)
    print("Stage_Vermogen_Origineel : " + Stage_Vermogen_Origineel)
    print("Stage_Vermogen_Na_tunning :  " + Stage_Vermogen_Na_tuning)
    print("Stage_Vermogen_Resultaat : " + Stage_Vermogen_Resultaat)

#       ~~~~~~~~Extra~~~~~~~~~
    try:
        extra=soup.find('div',attrs={'data-target':'options'})
        ex_row= extra.find('div',attrs={'class':'specs'}).findAll('div',attrs={'class':'row'})
        ex_data=ex_row[0].text.strip()
    except:
        ex_data='None'
        pass
    print(ex_data)


    sheet1.write(row, 0, Merk)
    sheet1.write(row, 1, Model)
    sheet1.write(row, 2, type_data)
    sheet1.write(row, 3, motor_data)
    sheet1.write(row, 4, eco_ver_Origineel)
    sheet1.write(row, 5, eco_Koppel_Origineel)
    sheet1.write(row, 6, eco_ver_Na_tuning)
    sheet1.write(row, 7, eco_Koppel_Na_tuning)
    sheet1.write(row, 8, eco_ver_Resultaat)
    sheet1.write(row, 9, eco_Koppel_Resultaat)
    sheet1.write(row, 10, Stage_Vermogen_Origineel)
    sheet1.write(row, 11, Stage_Vermogen_Na_tuning)
    sheet1.write(row, 12, Stage_Koppel_Na_tuning)
    sheet1.write(row, 13, Stage_Vermogen_Resultaat)
    sheet1.write(row, 14, Stage_Koppel_Resultaat)
    sheet1.write(row, 15, ex_data)
    row = row + 1
    wb.save('../Files/ECU.xls')
    print('Done ~ ' + str(row))
    import datetime
    print(datetime.datetime.now())

pass_dfs()