from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('Sheet1')
sheet1.write(0, 0, 'Name')
sheet1.write(0, 1, 'Website')
sheet1.write(0, 2, 'Phone')
class yepp():
    def get_yeppdata(self):
        options = webdriver.FirefoxOptions()
        # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
        options.add_argument('--disable-gpu')
        options.add_argument('--headless')
        driver = webdriver.Firefox(executable_path='..\Drivers\geckodriver',options=options)
        count = 0
        for p in range(11):
            ## Skiping 0th page
            if p==0:
                p=1
            pages = 'https://www.yell.com/ucs/UcsSearchAction.do?scrambleSeed=650179331&keywords=restaurants&location=london&pageNum='+str(p)
            print('Running Page-->',pages)
            driver.get(pages)
            soup=BeautifulSoup(driver.page_source,u"html.parser")
            ## Getting Name,Phone and Website
            data_div = soup.findAll('div',attrs={'class':'row businessCapsule--mainRow'})

            for d in data_div:
                try:
                    bisnuess_name = d.find('h2',attrs={'class':'text-h2'}).find('span').text.strip()
                except:
                    bisnuess_name = 'None'

                try:
                    phone = d.find('span', attrs={'class': 'business--telephoneNumber'}).text.strip()
                except:
                    phone = 'None'

                try:
                    website = d.find('a', attrs={'data-tracking': 'WL:CLOSED'})['href']
                except:
                    website = 'None'

                count+=1

                print('Name-->',bisnuess_name)
                print('Phone',phone)
                print('Website',website)
                print('Done-->',count)
                sheet1.write(count, 0, bisnuess_name)
                sheet1.write(count, 1, website)
                sheet1.write(count, 2, phone)
                wb.save('yell.xls')
                print('--'*60)


obj = yepp()
obj.get_yeppdata()