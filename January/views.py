from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from v1.ezcrm.users.models import Permissions,UsersPermissions,UserRole
from v1.ezcrm.users.serializers import RolePermissionsSerializer,PermissionsSerializer,RoleSerializer,UserRoleSerializer
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token
from v1.ezcrm.ezadmin.serializers import ActiveUsersSerializer
from v1.talent.models import TalentSkill,Profile
from v1.ezcrm.users.models import Role,RolesPermissions
from v1.ezcrm.ezadmin.serializers import UserPermissionsSerializer
import re
import random
from binascii import hexlify
import re, random, string
from ezapi import settings
from django.db.models import Count
import re

# Function checks if the string
# contains any special character
def special_char_check(string):
    # Make own character set and pass
    # this as argument in compile method
    regex = re.compile('[@!#$%^&*()<>?/\|}{~:]')

    # Pass the string in search
    # method of regex object.
    if (regex.search(string) == None):
        return False
    else:
        return True


def add_user_permissions(role_id,permission_id):

    role = Role.objects.get(id=role_id)
    role_name = role.name.lower()
    if role_name == 'super admin':
        perm_status = True
    else:
        perm_status = False


    user_permissions_dict = {}
    user_roles = UsersPermissions.objects.filter(role_id=role_id).distinct('user_id').values_list('user_id', flat=True)
    for usr in user_roles:
        user_ids = usr
        is_exists = UsersPermissions.objects.filter(role_id=role_id,permission_id = permission_id,user_id= user_ids).count()
        if is_exists==0:
            user_permissions_dict.update({'permission_id': permission_id,"status": perm_status, "role_id": role_id,'user_id':user_ids})
            User_permissions_serializer = UserPermissionsSerializer()
            User_permissions_serializer.create(user_permissions_dict)


def craeteusername(email):
    username = email.split("@")[0]
    username = re.sub('[^A-Za-z0-9\.]+', '', username)
    username = username.lower()
    orignam_uname = username
    exists = True
    count = 1
    while exists:
        exists = User.objects.filter(username=username).exists()
        if exists:
            if count < 10:
                username = orignam_uname + "0" + str(count)

            else:
                username = orignam_uname + str(count)
            count += 1
    return username.lower()


def generatepassword(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_letters + '0123456789_!@'
    return ''.join(random.choice(letters) for i in range(stringLength))

def sendEmail(to_email, first_name, email_content, mailsubject=None):
    connection = sendgrid.SendGridAPIClient(api_key=settings.SENDGRID_API)
    mail = Mail()
    mail.from_email = Email(settings.FROM_EMAIL, settings.FROM_NAME)
    personalization = Personalization()
    personalization.add_to(Email(to_email))
    mail.add_personalization(personalization)
    if first_name == None and mailsubject != None:
        mail.subject = mailsubject
    else:
        mail.subject = "Welcome to EmployZone, " + str(first_name)
    mail.add_content(Content("text/html", email_content))
    response = connection.send(mail)


def createEmailContent(objUser, password):
    filepath = settings.BASE_DIR + '/email_templates/welcome.html'
    filecnt = open(filepath, 'r')
    emailcontent = filecnt.read()
    emailcontent = emailcontent.replace('{{first_name}}', objUser.first_name)
    emailcontent = emailcontent.replace('{{fullname}}', objUser.first_name + ' ' + objUser.last_name)
    emailcontent = emailcontent.replace('{{email}}', objUser.email)
    emailcontent = emailcontent.replace('{{password}}', password)
    emailcontent = emailcontent.replace('{{link_change_pwd}}', settings.EMP_RESET_PASSWORD)
    emailcontent = emailcontent.replace('{{search_jobs}}', settings.WEBAPP_DOMAIN + '/search/')
    emailcontent = emailcontent.replace('{{profile_url}}', settings.WEBAPP_DOMAIN + '/profile')
    emailcontent = emailcontent.replace('{{skill_url}}', settings.WEBAPP_DOMAIN + '/skills')
    return emailcontent


def saveKeyPass(objU):
    key = hexlify(os.urandom(16))
    key = str(key)
    key = key.replace("b'", "").replace("'", "")
    secret = hexlify(os.urandom(16))
    secret = str(secret)
    secret = secret.replace("b'", "").replace("'", "")
    tal_obj = TalentApi()
    tal_obj.api_key = key
    tal_obj.pass_key = secret
    tal_obj.user = objU
    tal_obj.save()


def checkAuthenticatedUser(request):
    groups = request.user.groups.values_list('name', flat=True)
    if 'ezcrm' not in groups:
        response = {
            'status': 'error',
            'code': status.HTTP_401_UNAUTHORIZED,
            'message': 'Unauthorized',
        }
        return response

    else:
        return True


def checkpermission(permission_keys, user_id):
    try:
        if len(permission_keys)==1:
            key = permission_keys[0]

            permisson_obj = Permissions.objects.get(key__iexact = key)
            permission_id = permisson_obj.id
            permission_count = UsersPermissions.objects.filter(permission_id = permission_id,user_id = user_id ,status=True).count()

            if permission_count >0:
                return True
            else:
                response = {
                    'status': 'error',
                    'code': status.HTTP_401_UNAUTHORIZED,
                    'message': 'You do not have access to this section.',
                }
                return response

        if len(permission_keys)==2:
            key1 = permission_keys[0]
            key2 = permission_keys[1]


            permisson_obj = Permissions.objects.get(key__iexact=key1)
            permission_id = permisson_obj.id
            permission_count = UsersPermissions.objects.filter(permission_id=permission_id, user_id=user_id,status=True).count()

            permisson_obj2 = Permissions.objects.get(key__iexact=key2)
            permission_id = permisson_obj2.id
            permission2_count = UsersPermissions.objects.filter(permission_id=permission_id, user_id=user_id,status=True).count()

            if permission_count >0:
                return True
            elif permission2_count >0:
                return True
            else:
                response = {
                    'status': 'error',
                    'code': status.HTTP_401_UNAUTHORIZED,
                    'message': 'You do not have access to this section.',
                }
                return response
    except:
        response = {
        'status': 'error',
        'code': status.HTTP_401_UNAUTHORIZED,
        'message': 'You do not have access to this section.',
        }
        return response


class AddUser(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'add_user': {}})
            return Response(resp)
        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'add_user': {}})
            return Response(resp)

        try:
            jsondata = dict(request.data)
            datakeys = jsondata.keys()

            required_field = ''

            if 'email' not in datakeys:
                required_field = 'email'

            if 'first_name' not in datakeys:
                required_field = 'first_name'

            if 'role_id' not in datakeys:
                required_field = 'role_id'


            if required_field != '':
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': str(required_field) + ' is required.',
                    'add_user': {}
                }
                return Response(response)

            if str(jsondata['first_name']).strip()=='' or str(jsondata['email']).strip()=='':
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'bad request.',
                    'add_user': {}
                }
                return Response(response)

            if not re.match("[^@]+@[^@]+\.[^@]+", jsondata['email']):
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': "email not valid",
                    'add_user': {}
                }
                return Response(response)

            role_exists = Role.objects.filter(id=jsondata['role_id']).count()

            if role_exists == 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'Role not found',
                    'user_role': {}
                }
                return Response(response)

            # try:
            #     lastname = jsondata['last_name']
            # except:
            #     lastname = ''

            user = User.objects.get(email = jsondata['email'])
            user_id = user.id

            is_exists = UserRole.objects.filter(user_id=user_id, role_id=jsondata['role_id']).count()

            if is_exists > 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_409_CONFLICT,
                    'message': 'role already assigned to this user',
                    'user_role': {}
                }
                return Response(response)

            # usr_password = generatepassword()
            # objU = User()
            # objU.first_name = jsondata['first_name']
            # objU.last_name = lastname
            # username = craeteusername(jsondata['email'])  ## create username from email
            # objU.username = username
            # objU.email = jsondata['email']
            # objU.password = usr_password
            # objU.set_password(objU.password)
            # objU.is_active = True
            # objU.is_superuser = False
            # objU.is_staff = False
            # objU.save()
            #

            ## assign group to user
            EzGroup = Group.objects.get(name='ezcrm')
            EzGroup.user_set.add(user)
            ### welcome email code
            # emailcontent = createEmailContent(objU, usr_password)
            # sendEmail(objU.email, objU.first_name, emailcontent)####

            # from v1.accounts.tasks import send_async_registermail
            # send_async_registermail.delay(objU.email, objU.first_name, emailcontent)
            ## save user key and secret
            # saveKeyPass(objU)
            ## create user token
            # token = Token.objects.create(user=objU)
            # usergroups = objU.groups.values_list('name', flat=True)


            # from v1.accounts.tasks import celery_Update_login_datetime
            # user_id_ = objU.id
            # celery_Update_login_datetime.delay(user_id_)

            role_dict = {'role_id':jsondata['role_id'],'user_id':jsondata['user_id']}
            USR = UserRoleSerializer()
            USR.create(role_dict)

            permissions = RolesPermissions.objects.filter(role_id=jsondata['role_id'])

            if len(permissions) > 0:
                role_permissions_serializer = RolePermissionsSerializer(permissions, many=True)

                user_permissions_dict = {}
                for p in role_permissions_serializer.data:
                    permission_id = p['permission']
                    role = p['role']
                    User_permissions_serializer = UserPermissionsSerializer()
                    user_permissions_dict.update(
                        {'permission_id': permission_id, "user_id": user_id, "status": True,
                         "role_id": role})
                    User_permissions_serializer.create(user_permissions_dict)


            response = {
                'status': 'success',
                'code': status.HTTP_201_CREATED,
                'message': 'user created',
                'token': {},
                'add_user': {}
                }
            return Response(response)

        except:

            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'add_user': {}
            }
            return Response(response)




class GetActiveUsers(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'active_users': []})
            return Response(resp)

        user = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user)
        if resp != True:
            resp.update({'active_users': {}})
            return Response(resp)

        try:
            sortby = request.GET.get('sortby', 'id')
            sort_order = request.GET.get('sortorder', 'asc')
            page = request.GET.get('page', 1)
            count = request.GET.get('count', 100)
            id_filter = request.GET.get('id', None)
            f_name = request.GET.get('fname',None)
            l_name = request.GET.get('lname', None)
            email = request.GET.get('email', None)
            mobile = request.GET.get('mobile', None)
            skills = request.GET.get('skills', None)

            page = int(page)
            count = int(count)
            if count > 100:
                count = 100

            if page == 0 or page == 1:
                offset = 0
            else:
                offset = (page - 1) * (count)
            upto = offset + count

            if sort_order == 'desc':
                sortby = '-' + str(sortby)

            active_users = []
            total = 0
            filter_msg = ''

            ## Id Filter On====================
            if id_filter != None and f_name==None and l_name==None and email==None and mobile==None and skills==None:
                filter_msg = 'ID On'
                active_users = User.objects.filter(is_active=True, groups__name='talent', id=id_filter).order_by(sortby)[offset:upto]
                total = User.objects.filter(is_active=True, groups__name='talent', id=id_filter).count()


            ## First_name ON =====================
            elif f_name!=None and l_name==None and email == None and mobile==None and skills==None:
                ## First Name on only
                filter_msg = 'First Name on only'

                if id_filter!=None:
                    filter_msg = 'ID On'
                    active_users = User.objects.filter(is_active = True,groups__name = 'talent',id = id_filter,first_name__istartswith = f_name).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active = True,groups__name = 'talent',id= id_filter,first_name__istartswith = f_name).count()
                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name).count()

            elif f_name!=None and l_name!=None and email==None and mobile==None and skills==None:
                ## First Name and Last Name both On
                filter_msg = 'First Name and Last Name both On'
                if id_filter!=None:
                    active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith = f_name,last_name__istartswith = l_name).order_by(sortby)[offset:upto]
                    total = User.objects.filter(id=id_filter,is_active=True, groups__name='talent',first_name__istartswith = f_name,last_name__istartswith = l_name).count()
                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name).count()

            elif f_name != None and l_name != None and email != None and mobile == None and skills == None:
                ## First Name,Last Email On
                filter_msg = 'First Name,Last Email On'
                if id_filter!=None:
                    active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith = email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith = email).count()
                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith=email).count()


            elif f_name != None and l_name == None and email != None and mobile == None and skills == None:
                ## First Name,Last Email On
                filter_msg = 'First Name Email On'
                if id_filter!=None:
                    active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith = email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,email__istartswith = email).count()

                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name,email__istartswith=email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith=email).count()

            elif f_name != None and l_name == None and email != None and mobile == None and skills != None:
                ## First Name,Last Email On
                filter_msg = 'First Name Email,SKills On'
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id', flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)
                if skills=='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith = email).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,email__istartswith = email).exclude(id__in=skilled_true_lte_users).count()
                    else:
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith=email).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith=email).exclude( id__in=skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, email__istartswith=email).exclude(
                                id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith=email).exclude( id__in=skilled_true_lte_users).count()
                else:
                    if id_filter!=None: ## ID Filter
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, email__istartswith=email,id__in = skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,email__istartswith=email,id__in = skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, email__istartswith=email, id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, email__istartswith=email, id__in=skilled_users).count()

            elif f_name != None and l_name != None and email != None and mobile != None and skills == None:
                ## First Name,Last Email,Mobile On
                filter_msg = 'First Name,Last Email,Mobile On'

                if mobile == 'yes':
                    if id_filter!=None:
                        active_users = User.objects.filter(id= id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith = email,id__in = Profile.objects.filter(phone__isnull = False).values_list('user_id',flat = True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith = email,id__in = Profile.objects.filter(phone__isnull = False).values_list('user_id',flat = True)).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name,  last_name__istartswith=l_name, email__istartswith=email,id__in=Profile.objects.filter( phone__isnull=False).values_list('user_id',flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).count()

                elif mobile == 'no':
                    if id_filter!=None:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith=email,id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email,id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()
                    else:
                        if id_filter!=None:
                            active_users = User.objects.filter(id= id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name, email__istartswith=email, id__in=Profile.objects.filter( phone__isnull=True).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id= id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name,email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email, id__in=Profile.objects.filter( phone__isnull=True).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()




            ## Last Name On

            elif l_name!=None and f_name==None and email==None and mobile==None and skills==None:
                ## Last Name on Only
                filter_msg = 'Last Name on Only'
                if id_filter!=None:
                    active_users = User.objects.filter(id= id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name).order_by(sortby)[offset:upto]
                    total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name).count()
                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name).count()

            elif l_name!=None and f_name==None and email!=None and mobile==None and skills==None:
                ## Last,Email On
                filter_msg = 'Last,Email On'
                if id_filter!=None:
                    active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,email__istartswith =email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(id= id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,email__istartswith =email).count()
                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name, email__istartswith=email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name,email__istartswith=email).count()

            elif l_name != None and f_name==None  and email == None and mobile != None and skills == None:
                ## Last, Mobile On
                filter_msg = 'Last, Mobile On'
                if mobile == 'yes':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',last_name__istartswith=l_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id',  flat=True)).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name, id__in=Profile.objects.filter( phone__isnull=False).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name,id__in=Profile.objects.filter(phone__isnull=False).values_list( 'user_id', flat=True)).count()

                elif mobile == 'no':
                    if id_filter!=None:

                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name).exclude(id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',last_name__istartswith=l_name).exclude(id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name).exclude(id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id',flat=True)).order_by(sortby)[ offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name).exclude( id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()


            elif l_name != None and f_name==None and email == None and mobile == None and skills != None:
                ## Last Name and SKills On
                filter_msg = 'Last Name and SKills On'

                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id',flat=True)

                if skills =='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name).exclude(id__in =skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name ).exclude(id__in =skilled_true_lte_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name).exclude( id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name).exclude( id__in=skilled_true_lte_users).count()


                else:
                    if id_filter!=None:
                        active_users = User.objects.filter(id= id_filter,is_active=True, groups__name='talent',last_name__istartswith=l_name, id__in=skilled_users).order_by( sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,id__in=skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name, id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name, id__in=skilled_users).count()


            elif l_name != None and f_name==None and email == None and mobile != None and skills != None:
                ## Last Name and SKills On
                filter_msg = 'Last Name,mobile and SKills On'

                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id',flat=True)

                if skills =='0':
                    mobile_true_users = Profile.objects.filter(phone__isnull=False)
                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,id__in = mobile_true_users).exclude(id__in =skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,id__in = mobile_true_users ).exclude(id__in =skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name, id__in=mobile_true_users).exclude(
                                id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name,id__in=mobile_true_users).exclude( id__in=skilled_true_lte_users).count()


                    elif mobile=='no':
                        if id_filter!=None:

                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name).exclude( id__in=skilled_true_lte_users).exclude(id__in = mobile_true_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',last_name__istartswith=l_name).exclude(id__in=skilled_true_lte_users).exclude(id__in = mobile_true_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name).exclude(id__in=skilled_true_lte_users).exclude(id__in=mobile_true_users).order_by(sortby)[ offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name).exclude(id__in=skilled_true_lte_users).exclude(id__in=mobile_true_users).count()

                else:
                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',last_name__istartswith=l_name, id__in=skilled_users).order_by( sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name,id__in=skilled_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name,id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name, id__in=skilled_users).count()

                    elif mobile=='no':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',last_name__istartswith=l_name,id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', last_name__istartswith=l_name, id__in=skilled_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',last_name__istartswith=l_name,id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', last_name__istartswith=l_name, id__in=skilled_users).count()



            ## Email on ====================

            elif l_name == None and f_name==None and email != None and mobile == None and skills == None:
            ## Email On Only
                filter_msg = 'Email On Only'

                if id_filter!=None:
                    active_users = User.objects.filter(id= id_filter,is_active=True, groups__name='talent',email__istartswith=email).order_by( sortby)[offset:upto]
                    total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',  email__istartswith=email).count()
                else:
                    active_users = User.objects.filter(is_active=True, groups__name='talent',email__istartswith=email).order_by(sortby)[offset:upto]
                    total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email).count()

            elif l_name == None and f_name == None and email != None and mobile != None and skills == None:
                ## Email ,Mobile On
                filter_msg= 'Email ,Mobile On'

                if mobile == 'yes':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',email__istartswith=email,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id',  flat=True)).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).count()


                elif mobile == 'no':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',email__istartswith=email, id__in=Profile.objects.filter( phone__isnull=True).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).count()


            elif l_name == None and f_name == None and email != None and mobile == None and skills != None:
            ## Email and SKills On
                filter_msg = 'Email and SKills On'
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id', flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)
                if skills=='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email).exclude(id__in=skilled_true_lte_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email).exclude( id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email).exclude( id__in=skilled_true_lte_users).count()

                else:
                    if id_filter!=None:

                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email,id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email,id__in=skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email,id__in=skilled_users).count()



            elif l_name == None and f_name==None and email != None and mobile != None and skills != None:
                ## Last Name and SKills On
                filter_msg = 'Email,mobile and SKills On'

                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)
                if skills =='0':
                    mobile_true_users = Profile.objects.filter(phone__isnull=False).values_list('user_id',flat = True)

                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email,id__in = mobile_true_users).exclude(id__in =skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email,id__in = mobile_true_users).exclude(id__in =skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=mobile_true_users).exclude( id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=mobile_true_users).exclude(id__in=skilled_true_lte_users).count()


                    elif mobile=='no':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email).exclude( id__in=skilled_true_lte_users).exclude(id__in = mobile_true_users).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',email__istartswith=email).exclude(id__in=skilled_true_lte_users).exclude(id__in = mobile_true_users).exclude(id__in = skilled_true_lte_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',email__istartswith=email).exclude( id__in=skilled_true_lte_users).exclude(id__in=mobile_true_users).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email).exclude( id__in=skilled_true_lte_users).exclude(id__in=mobile_true_users).exclude( id__in=skilled_true_lte_users).count()


                else:
                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',email__istartswith=email, id__in=skilled_users).order_by( sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email,id__in=skilled_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',email__istartswith=email, id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=skilled_users).count()



                    elif mobile=='no':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',email__istartswith=email,id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', email__istartswith=email, id__in=skilled_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email, id__in=skilled_users).order_by( sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', email__istartswith=email,id__in=skilled_users).count()




            ## Mobile On Only ===================
            elif l_name == None and f_name == None and email == None and mobile != None and skills == None:
                filter_msg = 'Mobile On Only'

                if mobile == 'yes':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id',  flat=True)).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', id__in=Profile.objects.filter( phone__isnull=False).values_list('user_id',flat=True)).order_by( sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).count()

                elif mobile == 'no':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id',flat=True)).order_by( sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()


            elif f_name != None and l_name == None and email == None and mobile != None and skills == None:
                ## Mobile first_name On
                filter_msg = 'Mobile first_name On'
                if mobile == 'yes':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id',  flat=True)).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name,id__in=Profile.objects.filter( phone__isnull=False).values_list('user_id',flat=True)).order_by( sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',    first_name__istartswith=f_name, id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).count()



                elif mobile == 'no':

                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id= id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).order_by( sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).count()




            elif f_name != None and l_name == None and email == None and mobile != None and skills!= None:
                ## Mobile first_name On

                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id', flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)
                filter_msg = 'Mobile first_name,SKills On'
                if mobile == 'yes':
                    if skills=='0':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id',  flat=True)).exclude(id__in = skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=False).values_list( 'user_id', flat=True)).exclude( id__in=skilled_true_lte_users).count()



                    else:
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter( phone__isnull=False).values_list('user_id', flat=True)).filter(id__in = skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).filter(id__in = skilled_users).count()


                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter( phone__isnull=False).values_list('user_id',flat=True)).filter( id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, id__in=Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)).filter(id__in=skilled_users).count()





                elif mobile == 'no':
                    if skills=='0':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)).exclude(id__in = skilled_true_lte_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name, id__in=Profile.objects.filter( phone__isnull=True).values_list('user_id', flat=True)).exclude( id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).exclude( id__in=skilled_true_lte_users).count()



                    else:
                        if id_filter!=None:

                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter( phone__isnull=True).values_list('user_id', flat=True)).filter(id__in = skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).filter(id__in = skilled_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=Profile.objects.filter( phone__isnull=True).values_list('user_id', flat=True)).filter(
                                id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',first_name__istartswith=f_name,id__in=Profile.objects.filter(phone__isnull=True).values_list( 'user_id', flat=True)).filter(id__in=skilled_users).count()





            elif f_name == None and l_name == None and email == None and mobile != None and skills != None:
                ## Mobile Skills On
                filter_msg = 'Mobile ,Skills On'
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id', flat=True)

                mobile_true_users = Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)
                mobile_false_users = Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)

                from itertools import chain

                order = False
                if sort_order == 'asc':
                    order = False
                if sort_order == 'desc':
                    order = True

                # skilledMtrueobjs = sorted(chain(skilled_users, mobile_true_users), reverse=order)
                skilled_Mfalse_users = sorted(chain(skilled_users, mobile_false_users), reverse=order)
                ## Getting True Skilled Users Statically


                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)

                if mobile == 'yes':
                    if skills=='0':

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=mobile_true_users).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=mobile_true_users).exclude(id__in = skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',id__in=mobile_true_users).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', id__in=mobile_true_users).exclude(id__in=skilled_true_lte_users).count()




                    else:

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', id__in=mobile_true_users).filter(id__in = skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', id__in=mobile_true_users).filter(id__in = skilled_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', id__in=mobile_true_users).filter( id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', id__in=mobile_true_users).filter(id__in=skilled_users).count()

                elif mobile == 'no':
                    if skills=='0':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=mobile_false_users).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=mobile_false_users).exclude(id__in = skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', id__in=mobile_false_users).exclude( id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent', id__in=mobile_false_users).exclude( id__in=skilled_true_lte_users).count()



                    else:

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=skilled_Mfalse_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',id__in=skilled_Mfalse_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent', id__in=skilled_Mfalse_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',id__in=skilled_Mfalse_users).count()

            ## Skills On Only=======================

            elif f_name == None and l_name == None and email == None and mobile == None and skills != None:
                ## SKills on Only
                filter_msg = 'SKills on Only'

                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id',flat=True)
                if skills=='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent').exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent').exclude(id__in = skilled_true_lte_users).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent').exclude(
                            id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent').exclude(
                            id__in=skilled_true_lte_users).count()

                else:
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', id__in=skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent', id__in=skilled_users).count()

            elif f_name!=None and l_name==None and email==None and mobile==None and skills!=None:
                ## Skills Fname On
                filter_msg = 'Skills Fname On'
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)
                if skills=='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name).exclude(id__in = skilled_true_lte_users).count()

                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           first_name__istartswith=f_name).exclude(
                            id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',
                                                    first_name__istartswith=f_name).exclude(
                            id__in=skilled_true_lte_users).count()


                else:
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name, id__in=skilled_users).order_by( sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,id__in=skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           first_name__istartswith=f_name,
                                                           id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',
                                                    first_name__istartswith=f_name, id__in=skilled_users).count()


            elif f_name!=None and l_name!=None and email==None and mobile==None and skills!=None:
                ## Skills Fname On
                filter_msg = 'Skills Fname,Lname On'
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter( user_id__count__lte=5000).values_list('user_id', flat=True)

                if skills=='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name).exclude(id__in = skilled_true_lte_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           first_name__istartswith=f_name,
                                                           last_name__istartswith=l_name).exclude(
                            id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',
                                                    first_name__istartswith=f_name,
                                                    last_name__istartswith=l_name).exclude(
                            id__in=skilled_true_lte_users).count()

                else:
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name, id__in=skilled_users).order_by( sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name,id__in=skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           first_name__istartswith=f_name,
                                                           last_name__istartswith=l_name,
                                                           id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',
                                                    first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                    id__in=skilled_users).count()


            elif f_name!=None and l_name!=None and email!=None and mobile==None and skills!=None:
                ## Skills Fname On
                filter_msg = 'Skills Fname,Lname,email On'
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)
                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(
                    user_id__count__lte=5000).values_list('user_id', flat=True)

                if skills=='0':
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email).exclude(id__in = skilled_true_lte_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           first_name__istartswith=f_name,
                                                           last_name__istartswith=l_name,
                                                           email__istartswith=email).exclude(
                            id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',
                                                    first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                    email__istartswith=email).exclude(
                            id__in=skilled_true_lte_users).count()

                else:
                    if id_filter!=None:
                        active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email, id__in=skilled_users).order_by( sortby)[offset:upto]
                        total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email,id__in=skilled_users).count()
                    else:
                        active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                           first_name__istartswith=f_name,
                                                           last_name__istartswith=l_name, email__istartswith=email,
                                                           id__in=skilled_users).order_by(sortby)[offset:upto]
                        total = User.objects.filter(is_active=True, groups__name='talent',
                                                    first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                    email__istartswith=email, id__in=skilled_users).count()


            elif f_name!=None and l_name!=None and email!=None and mobile!=None and skills!=None:
                ## Skills Fname On
                filter_msg = 'Skills Fname,Lname,email,Mobile On'

                mobile_true_users = Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)
                mobile_false_users = Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)

                from itertools import chain

                order = False
                if sort_order == 'asc':
                    order = False
                if sort_order == 'desc':
                    order = True

                # skilledMtrueobjs = sorted(chain(skilled_users, mobile_true_users), reverse=order)
                # skilled_Mfalse_users = sorted(chain(skilled_users, mobile_false_users), reverse=order)

                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)

                if skills=='0':
                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email,id__in = mobile_true_users).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email,id__in = mobile_true_users).exclude(id__in = skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name, email__istartswith=email,
                                                               id__in=mobile_true_users).exclude(
                                id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        email__istartswith=email, id__in=mobile_true_users).exclude(
                                id__in=skilled_true_lte_users).count()


                    elif mobile == 'no':

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email,id__in=mobile_false_users).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email, id__in=mobile_false_users).exclude(id__in=skilled_true_lte_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name, email__istartswith=email,
                                                               id__in=mobile_false_users).exclude(
                                id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        email__istartswith=email, id__in=mobile_false_users).exclude(
                                id__in=skilled_true_lte_users).count()

                else:
                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email, id__in=mobile_true_users).filter(id__in = skilled_users).order_by( sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name,email__istartswith=email,id__in=mobile_true_users).filter(id__in = skilled_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name, email__istartswith=email,
                                                               id__in=mobile_true_users).filter(
                                id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        email__istartswith=email, id__in=mobile_true_users).filter(
                                id__in=skilled_users).count()


                    elif mobile=='no':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name, email__istartswith=email,id__in=skilled_users).exclude(id__in = mobile_true_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, email__istartswith=email, id__in=skilled_users).exclude(id__in = mobile_true_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name, email__istartswith=email,
                                                               id__in=skilled_users).exclude(
                                id__in=mobile_true_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        email__istartswith=email, id__in=skilled_users).exclude(
                                id__in=mobile_true_users).count()




            elif f_name!=None and l_name!=None and email==None and mobile!=None and skills!=None:
                ## Skills Fname On
                filter_msg = 'Skills Fname,Lname,Mobile On'

                mobile_true_users = Profile.objects.filter(phone__isnull=False).values_list('user_id', flat=True)
                mobile_false_users = Profile.objects.filter(phone__isnull=True).values_list('user_id', flat=True)
                skilled_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count=skills).values_list('user_id',flat=True)


                from itertools import chain

                order = False
                if sort_order == 'asc':
                    order = False
                if sort_order == 'desc':
                    order = True

                # skilledMtrueobjs = sorted(chain(skilled_users, mobile_true_users), reverse=order)
                # skilled_Mfalse_users = sorted(chain(skilled_users, mobile_false_users), reverse=order)

                skilled_true_lte_users = TalentSkill.objects.values('user_id').annotate(Count('user_id')).filter(user_id__count__lte=5000).values_list('user_id', flat=True)

                if skills=='0':
                    if mobile=='yes':
                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name,id__in = mobile_true_users).exclude(id__in = skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name,id__in = mobile_true_users).exclude(id__in = skilled_true_lte_users).count()

                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name,
                                                               id__in=mobile_true_users).exclude(
                                id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        id__in=mobile_true_users).exclude(
                                id__in=skilled_true_lte_users).count()


                    elif mobile == 'no':

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name,id__in=mobile_false_users).exclude(id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, id__in=mobile_false_users).exclude(id__in=skilled_true_lte_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name,
                                                               id__in=mobile_false_users).exclude(
                                id__in=skilled_true_lte_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        id__in=mobile_false_users).exclude(
                                id__in=skilled_true_lte_users).count()

                else:
                    if mobile=='yes':

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name, id__in=mobile_true_users).filter(id__in = skilled_users).order_by( sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent', first_name__istartswith=f_name,last_name__istartswith=l_name,id__in=mobile_true_users).filter(id__in = skilled_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name,
                                                               id__in=mobile_true_users).filter(
                                id__in=skilled_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        id__in=mobile_true_users).filter(id__in=skilled_users).count()

                    elif mobile=='no':

                        if id_filter!=None:
                            active_users = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name,last_name__istartswith=l_name, id__in=skilled_users).exclude(id__in = mobile_true_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(id = id_filter,is_active=True, groups__name='talent',first_name__istartswith=f_name, last_name__istartswith=l_name, id__in=skilled_users).exclude(id__in = mobile_true_users).count()
                        else:
                            active_users = User.objects.filter(is_active=True, groups__name='talent',
                                                               first_name__istartswith=f_name,
                                                               last_name__istartswith=l_name,
                                                               id__in=skilled_users).exclude(
                                id__in=mobile_true_users).order_by(sortby)[offset:upto]
                            total = User.objects.filter(is_active=True, groups__name='talent',
                                                        first_name__istartswith=f_name, last_name__istartswith=l_name,
                                                        id__in=skilled_users).exclude(id__in=mobile_true_users).count()



            else:
                ## All Filters Off
                filter_msg = 'Off'
                active_users = User.objects.filter(is_active=True, groups__name='talent').order_by(sortby)[offset:upto]
                total = User.objects.filter(is_active=True, groups__name='talent').count()


            active_users_serializer = ActiveUsersSerializer(active_users, many=True)

            for u in active_users_serializer.data:
                ## Check if Resume and Mobile Exist
                resume_obj = Profile.objects.filter(user_id = u['id']).values_list('resume',flat=True)
                phone_obj = Profile.objects.filter(user_id = u['id']).values_list('phone',flat=True)

                try:
                    resume_status = 'no'
                    for i in resume_obj:
                        try:
                            resume = i
                            if 'http' in resume:
                                resume_status = 'yes'
                            else:
                                resume_status = 'no'
                        except:
                            resume_status = 'no'

                    phone = 'no'
                    for rs in phone_obj:
                        try:
                            phone  = rs
                            if len(phone)>0:
                                phone = phone

                            else:
                                phone = 'no'
                        except:
                            phone = 'no'



                except:
                    resume_status = 'no'
                    phone = 'no'

                u.update({'resume':resume_status,'phone':phone})

                ##  Updating Skills count of that particular user

                skills_obj_count = TalentSkill.objects.filter(user_id = u['id']).count()
                u.update({'skills': skills_obj_count})


            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'active users list filters '+str(filter_msg),
                'total_users': total,
                'active_users': active_users_serializer.data,
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'total_users': 0,
                'active_users': []
            }
            return Response(response)



class GetDeletedTalentUsers(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'deleted_users': []})
            return Response(resp)

        user = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user)
        if resp != True:
            resp.update({'deleted_users': {}})
            return Response(resp)


        try:
            sortby = request.GET.get('sortby', 'id')
            sort_order = request.GET.get('sortorder', 'asc')
            page = request.GET.get('page', 1)
            count = request.GET.get('count', 100)

            page = int(page)
            count = int(count)
            if count > 100:
                count = 100

            if page == 0 or page == 1:
                offset = 0
            else:
                offset = (page - 1) * (count)
            upto = offset + count

            if sort_order == 'desc':
                sortby = '-' + str(sortby)


            total_deleted_users = User.objects.filter(is_active = False,groups__name = 'talent').count()
            deleted_users = User.objects.filter(is_active = False,groups__name = 'talent').order_by(sortby)[offset:upto]
            deleted_users_serializer = ActiveUsersSerializer(deleted_users, many=True)


            for u in deleted_users_serializer.data:
                ## Check if Resume and Mobile Exist
                resume_obj = Profile.objects.filter(user_id = u['id']).values_list('resume','phone')

                try:
                    resume_status = resume_obj[0][0]
                    if len(resume_status)>0:
                        resume_status ='yes'
                    else:
                        resume_status = 'no'

                    phone = resume_obj[0][1]
                    if len(phone)>0:
                        phone = phone
                    else:
                        phone = 'no'

                except:
                    resume_status = 'no'
                    phone = 'no'

                u.update({'resume':resume_status,'phone':phone})

                ##  Updating Skills count of that particular user

                skills_obj_count = TalentSkill.objects.filter(user_id = u['id']).count()
                u.update({'skills': skills_obj_count})


            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'deleted users list',
                'total_users': total_deleted_users,
                'deleted_users': deleted_users_serializer.data,
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'total_users': 0,
                'deleted_users': []
            }
            return Response(response)


class ChangePermissionsStatusView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request):
        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'roles_permissions': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp != True:
            resp.update({'roles_permissions': {}})
            return Response(resp)


        try:
            jsondata = dict(request.data)
            required_field = ''
            if 'role_id' not in jsondata.keys():
                required_field = 'role_id'
            if 'permission_id' not in jsondata.keys():
                required_field = 'permission_id'
            if 'status' not in jsondata.keys():
                required_field= 'status'

            if required_field != '':
                response = {
                    'status':'error',
                    'code':status.HTTP_400_BAD_REQUEST,
                    'message':required_field + ' is required',
                    'roles_permissions':{}
                }
                return Response(response)


            roles = Role.objects.filter(id=jsondata['role_id'])
            if len(roles) == 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'Role not found',
                    'roles_permissions': {}
                }
                return Response(response)


            permission = Permissions.objects.filter(id=jsondata['permission_id'])
            if len(permission)==0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'Permission not found',
                    'roles_permissions': {}
                }
                return Response(response)


            user_permission_objs = UsersPermissions.objects.filter(role_id = jsondata['role_id'],permission_id = jsondata['permission_id'])

            for u in user_permission_objs:
                u_obj = u
                UP = UserPermissionsSerializer(u_obj)
                UP.update(u_obj, jsondata)

            user_status = jsondata['status']

            if user_status == False:
                UsersPermissions.objects.filter(permission_id=jsondata['permission_id'],role_id=jsondata['role_id']).delete()
                RolesPermissions.objects.filter(permission_id = jsondata['permission_id'],role_id = jsondata['role_id']).delete()
            if user_status == True:
            ## Creating a New Object for roles permissions if status is true
                is_exists_count = RolesPermissions.objects.filter(permission_id = jsondata['permission_id'],role_id = jsondata['role_id']).count()
                if is_exists_count==0:
                    add_role_per_dict = {'role_id':jsondata['role_id'],'permission_id':jsondata['permission_id']}
                    RPS = RolePermissionsSerializer()
                    RPS.create(add_role_per_dict)

                ## Adding Permission Again For Users Using Celery
                from v1.accounts.tasks import celery_Add_User_Permissions
                celery_Add_User_Permissions.delay(jsondata['role_id'],jsondata['permission_id'])

            roles = roles[0]
            role_dict =({'role_id': roles.id, 'name': roles.name, 'key': roles.key})

            permission_obj = permission[0]

            perm_status = None
            if user_status==True:
                status_count = UsersPermissions.objects.filter(role_id = jsondata['role_id'],permission_id = jsondata['permission_id'],status = False).count()
                if status_count==0:
                    perm_status = True
                else:
                    perm_status = False

            if user_status== False:
                status_count = UsersPermissions.objects.filter(role_id=jsondata['role_id'], permission_id=jsondata['permission_id'],status=True).count()

                if status_count==0:
                    perm_status = False
                else:
                    perm_status = True


            permission_dict = {'id':permission_obj.id,'permission':permission_obj.permission,'key':permission_obj.key,'description':permission_obj.description,'status':perm_status}
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Status Changed Successfully',
                'role':role_dict,
                'roles_permissions': permission_dict
            }
            return Response(response)
        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'role':{},
                'roles_permissions': {}
            }
            return Response(response)


class GetRolePermissions(APIView):

    def get(self, request, role_id):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'roles_permissions': []})
            return Response(resp)

        # Manage User Management Permission

        user = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user)
        if resp != True:
            resp.update({'roles_permissions': []})
            return Response(resp)

        try:
            RS_obj = Role.objects.filter(id = role_id)
            if len(RS_obj)==0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'role not found',
                    'roles_permissions': []
                }

                return Response(response)
            else:
                role_dict = {}
                role_obj = Role.objects.get(id=role_id)
                role_id = role_obj.id
                role_name = role_obj.name
                key = role_obj.key
                role_dict.update({'role_id':role_id,'name':role_name,'key':key})
                permissions = Permissions.objects.all()
                Roles = PermissionsSerializer(permissions,many=True)
                for r in Roles.data:
                    permission_id = r['id']

                    is_exists = RolesPermissions.objects.filter(role_id= role_id,permission_id= permission_id).count()

                    if is_exists>0:
                        per_status = True
                    else:
                        per_status = False

                    r.update({'status':per_status})

            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'roles and permissions',
                'role':role_dict,
                'roles_permissions': Roles.data
            }
            return Response(response)
        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'roles_permissions': []
            }
            return Response(response)



class GetPermissionByID(APIView):
    permission_classes = (IsAuthenticated,)
    ## This Endpoint is For Admin Section to Edit Permissions

    def get_object(self, pk):
        try:
            P_obj = Permissions.objects.get(id=pk)
            return P_obj
        except:
            return None

    def get(self, request, pk):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'permissions': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'permissions': {}})
            return Response(resp)

        try:
            P_obj = self.get_object(pk)

            if P_obj == None:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'permission not found',
                    'permissions': {}
                }
                return Response(response)
            else:
                Permssion_obj = PermissionsSerializer(P_obj)
                response = {
                    'status': 'success',
                    'code': status.HTTP_200_OK,
                    'message': 'permission detail',
                    'permissions': Permssion_obj.data
                }
                return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'permissions': {}
            }
            return Response(response)


    def put(self, request, pk):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'permissions': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'permissions': {}})
            return Response(resp)

        try:
            jsondata = dict(request.data)
            if len(jsondata)==0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'bad request',
                    'permissions': {}
                }
                return Response(response)

            P_obj = self.get_object(pk)
            if P_obj == None:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'permission not found',
                    'permissions': {}
                }
                return Response(response)

            if 'key' in jsondata.keys():
                del jsondata['key']

            if 'id' in jsondata.keys():
                del jsondata['id']


            if 'permission' in jsondata.keys():
                if str(jsondata['permission']).strip() == '' or jsondata['permission'] == None or jsondata['permission'] == 'None':
                    response = {
                        'status': 'error',
                        'code': status.HTTP_400_BAD_REQUEST,
                        'message':'permission is required',
                        'permissions': {}
                    }
                    return Response(response)

            if 'description' in jsondata.keys():
                if str(jsondata['description']).strip() == '' or jsondata['description'] == None or jsondata['description'] == 'None':
                    response = {
                        'status': 'error',
                        'code': status.HTTP_400_BAD_REQUEST,
                        'message':'description is required',
                        'permissions': {}
                    }
                    return Response(response)


            PS = PermissionsSerializer()
            objD = PS.update(P_obj, jsondata)
            serCS = PermissionsSerializer(objD)
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'permission updated successfully',
                'permissions': serCS.data
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'permissions': {}
            }
            return Response(response)


class PostGetPermissions(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'permissions': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'permissions': {}})
            return Response(resp)
        try:
            jsondata = dict(request.data)
            datakeys = jsondata.keys()

            required_field = ''

            if 'permission' not in datakeys:
                required_field = 'permission'

            if 'key' not in datakeys:
                required_field = 'key'

            if 'description' not in datakeys:
                required_field = 'description'

            if required_field != '':
                response = {
                    'status': 'error',
                    'code': status.HTTP_422_UNPROCESSABLE_ENTITY,
                    'message': str(required_field) + ' is required.',
                    'permissions': {}
                }
                return Response(response)

            if str(jsondata['permission']).strip() == '' or jsondata['permission'] == None or jsondata[
                'permission'] == 'None':
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'permission is required',
                    'permissions': {}
                }
                return Response(response)

            if str(jsondata['description']).strip() == '' or jsondata['description'] == None or jsondata['description'] == 'None':
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'description is required',
                    'permissions': {}
                }
                return Response(response)


            check_numonly = jsondata['key'].isdigit()

            if check_numonly ==True:
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'permission_key should not be an integer',
                    'permissions': {}
                }
                return Response(response)


            special_check = special_char_check(jsondata['key'])

            if special_check==True:
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'permission_key should not contain special characters',
                    'permissions': {}
                }
                return Response(response)

            jsondata['key'] = jsondata['key'].replace(' ','_')

            is_exists = Permissions.objects.filter(key=jsondata['key']).count()

            if is_exists > 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_409_CONFLICT,
                    'message': 'permission_key already exists',
                    'permissions': {}
                }
                return Response(response)

            PS = PermissionsSerializer()
            PS_obj = PS.create(jsondata)
            posted_data = PermissionsSerializer(PS_obj).data

            role_id = None
            try:
                role_obj = Role.objects.get(name='super admin')
                role_id = role_obj.id
                permission_status = True
            except:
                permission_status = False

            get_all_users = UsersPermissions.objects.filter(role_id=role_id).distinct('user_id')
            UPS_data = UserPermissionsSerializer(get_all_users, many=True).data
            UPS_Ser = UserPermissionsSerializer()

            if len(UPS_data)==1:
                user_id = UPS_data[0]['user']
                user_permissions_dict ={"user_id": user_id, "role_id": role_id, "permission_id": PS_obj.id,"status": permission_status}
                UPS_Ser.create(user_permissions_dict)

            else:
                for u in UPS_data:
                    user_id = u['user']
                    role_id = u['role']
                    permission_id = PS_obj.id
                    user_permissions_dict = {"user_id": user_id, "role_id": role_id, "permission_id": permission_id, "status": permission_status}
                    UPS_Ser.create(user_permissions_dict)


            role_permission_dict = {"permission_id": PS_obj.id, "role_id": role_id}
            RPS = RolePermissionsSerializer()
            RPS.create(role_permission_dict)


            response = {
                'status': 'success',
                'code': status.HTTP_201_CREATED,
                'message': 'permission saved',
                'permissions': posted_data
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'permissions': {}
            }
            return Response(response)



    def get(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'permissions': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'permissions': {}})
            return Response(resp)

        ## Admin Pannel Endpoint
        try:
            sortby = request.GET.get('sortby', 'id')
            sort_order = request.GET.get('sortorder', 'asc')
            page = request.GET.get('page', 1)
            count = request.GET.get('count', 100)

            page = int(page)
            count = int(count)
            if count > 100:
                count = 100

            if page == 0 or page == 1:
                offset = 0
            else:
                offset = (page - 1) * (count)
            upto = offset + count

            if sort_order == 'desc':
                sortby = '-' + str(sortby)


            total = Permissions.objects.count()
            obsjs = Permissions.objects.all().order_by(sortby)[offset:upto]
            permission_serializer = PermissionsSerializer(obsjs,many=True)

            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'permissions list',
                'total':total,
                'permissions': permission_serializer.data
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'permissions': {}
            }
            return Response(response)



class PostRoles(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'role': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'role': {}})
            return Response(resp)

        try:
            jsondata = dict(request.data)
            datakeys = jsondata.keys()

            required_field = ''

            if 'name' not in datakeys:
                required_field = 'name'

            if 'key' not in datakeys:
                required_field = 'key'


            if required_field != '':
                response = {
                    'status': 'error',
                    'code':status.HTTP_400_BAD_REQUEST,
                    'message': str(required_field) + ' is required.',
                    'role': {}
                }
                return Response(response)

            is_exists = Role.objects.filter(name = jsondata['name']).count()

            if is_exists>0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_409_CONFLICT,
                    'message': 'role already exists with this name',
                    'role': {}
                }
                return Response(response)

            RS = RoleSerializer()
            obj_RS = RS.create(jsondata)
            role_data = RoleSerializer(obj_RS).data

            response = {
                'status': 'success',
                'code': status.HTTP_201_CREATED,
                'message': 'role saved',
                'role': role_data
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'role': {}
            }
            return Response(response)



    def get(self, request):

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'role': {}})
            return Response(resp)


        try:
            sortby = request.GET.get('sortby', 'id')
            sort_order = request.GET.get('sortorder', 'asc')
            page = request.GET.get('page', 1)
            count = request.GET.get('count', 100)

            page = int(page)
            count = int(count)
            if count > 100:
                count = 100

            if page == 0 or page == 1:
                offset = 0
            else:
                offset = (page - 1) * (count)
            upto = offset + count

            if sort_order == 'desc':
                sortby = '-' + str(sortby)

            total = Role.objects.count()
            obsjs = Role.objects.all().order_by(sortby)[offset:upto]
            role_serializer = RoleSerializer(obsjs,many=True)

            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'roles list',
                'total':total,
                'role': role_serializer.data
            }
            return Response(response)

        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'role': []
            }
            return Response(response)


class AddNewPermission(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):

        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'roles_permissions': {}})
            return Response(resp)


        # Manage User Management Permission

        user = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user)
        if resp != True:
            resp.update({'roles_permissions': {}})
            return Response(resp)

        try:
            jsondata = dict(request.data)
            datakeys = jsondata.keys()

            required_field = ''

            if 'permission_id' not in datakeys:
                required_field = 'permission_id'

            if 'role_id' not in datakeys:
                required_field = 'role_id'


            if required_field != '':
                response = {
                    'status': 'error',
                    'code':status.HTTP_400_BAD_REQUEST,
                    'message': str(required_field) + ' is required.',
                    'roles_permissions': {}
                }
                return Response(response)


            is_exists_role = Role.objects.filter(id=jsondata['role_id']).count()

            if is_exists_role == 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_409_CONFLICT,
                    'message': 'role not found',
                    'roles_permissions': {}
                }
                return Response(response)

            is_exists = Permissions.objects.filter(id=jsondata['permission_id']).count()
            if is_exists == 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_409_CONFLICT,
                    'message': 'permission not found',
                    'roles_permissions': {}
                }
                return Response(response)

            is_exists_permission = UsersPermissions.objects.filter(role_id=jsondata['role_id'],permission_id = jsondata['permission_id']).count()
            if is_exists_permission > 0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_409_CONFLICT,
                    'message': 'user_permission already exists',
                    'roles_permissions': {}
                }
                return Response(response)

            permission_status = False
            try:
                role_obj = Role.objects.get(id =jsondata['role_id'])
                name = role_obj.name
                if name == 'super admin':
                    permission_status = True
            except:
                permission_status = False

            get_all_users = UsersPermissions.objects.filter(role_id=jsondata['role_id']).distinct('user_id')
            UPS_data = UserPermissionsSerializer(get_all_users,many=True).data
            UPS_Ser = UserPermissionsSerializer()
            for u in UPS_data:
                user_id = u['user']
                role_id = u['role']
                permission_id = jsondata['permission_id']

                UPS_Ser.create({"user_id": user_id, "role_id": role_id,"permission_id": permission_id,"status": permission_status})

            is_exists_role_permission = RolesPermissions.objects.filter(role_id=jsondata['role_id'],permission_id=jsondata['permission_id']).count()
            if is_exists_role_permission == 0:
                jsondata.update({"permission_id":jsondata['permission_id'],"role_id":jsondata['role_id']})
                RPS = RolePermissionsSerializer()
                RPS.create(jsondata)

            response = {
                'status': 'success',
                'code': status.HTTP_201_CREATED,
                'message': 'permission added successfully',
                'roles_permissions': {}
            }
            return Response(response)
        except:
            response = {
                'status': 'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'roles_permissions': {}
            }
            return Response(response)



class DeletePermissions(APIView):
    permission_classes = (IsAuthenticated,)

    ## This Endpoint Is Used to Delete Permissions For Admins

    def get_object(self, permission_id):
        try:
            UR_obj = Permissions.objects.get(id=permission_id)
        except:
            UR_obj = None
        return UR_obj

    def delete(self, request, permission_id):
        resp = checkAuthenticatedUser(request)
        if resp != True:
            resp.update({'permissions': {}})
            return Response(resp)

        # Manage User Management Permission

        user_id = request.user.id
        permission_keys = ['Manage_UserManagement']
        resp = checkpermission(permission_keys, user_id)
        if resp is not True:
            resp.update({'permissions': {}})
            return Response(resp)

        try:
            UR_obj = self.get_object(permission_id)
            if UR_obj is None:
                response = {
                    'status': 'error',
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'Permission not found',
                    'permissions': {}
                }
                return Response(response)


            exists_for_user = UsersPermissions.objects.filter(permission_id=permission_id,status = True)

            if len(exists_for_user)>0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'Permission is assigned to Users',
                    'permissions': {}
                }
                return Response(response)

            exists_for_role = RolesPermissions.objects.filter(permission_id=permission_id)

            if len(exists_for_role)>0:
                response = {
                    'status': 'error',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'Permission is assigned to Role',
                    'permissions': {}
                }
                return Response(response)

            UsersPermissions.objects.filter(permission_id=permission_id,status = False).delete()
            RolesPermissions.objects.filter(permission_id=permission_id).delete()
            UR_obj.delete()

            response = {
                'status':'success',
                'code': status.HTTP_200_OK,
                'message': 'Permission Deleted Successfully',
                'permissions': {}
            }
            return Response(response)

        except:
            response = {
                'status':'error',
                'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                'message': 'Internal Server Error',
                'permissions': {}
            }
            return Response(response)
