import requests
from selenium import webdriver
from bs4 import BeautifulSoup
import xlrd,xlwt
import time

options = webdriver.FirefoxOptions()
        # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Firefox(executable_path='..\Drivers\geckodriver',options=options)
rd = xlrd.open_workbook('../Files/Gmm_data.xls')
sheet = rd.sheet_by_index(0)
wb = xlwt.Workbook()
sheet1= wb.add_sheet('College Locations')
sheet1.write(0,0,"Collage Name")
# sheet1.write(0,1,"Location Link")
sheet1.write(0,1,"Images Link")

for u in range(sheet.nrows):
    try:
        value = sheet.cell_value(u, 1)
        if len(value)==0:
            continue
        links = 'https://www.google.com/maps/place/'+str(value)
        links = str(links)
        print('Link-->',links)
        driver.get(links)
        import time

        time.sleep(3)
        driver.find_element_by_id('searchbox-searchbutton').click() ## Clicked On Search

        time.sleep(5)
        driver.find_element_by_class_name('section-carouselphoto-photo-container-photo').click()
        sheet1.write(u, 0, value)
        # sheet1.write(u, 1, location_link)
        sheet1.write(u, 1, '')
        wb.save('College_picc.xls')
        print('--'*100)
    except:
        import sys
        print(sys.exc_info())
        print('ERRr')

driver.close()