from bs4 import BeautifulSoup
import requests
import csv
from selenium import webdriver
import time
import xlwt
import datetime
row = 0

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('Justdial_Jewelers')

sheet1.write(0,0,"Name")
sheet1.write(0,1,"Phone")
sheet1.write(0,2,"Rating")
sheet1.write(0,3,"Rating Count")
sheet1.write(0,4,"Address")
sheet1.write(0,5,"Location")
sheet1.write(0,6,"Website")



def innerHTML(element):
    return element.decode_contents(formatter="html")


def get_name(body):
    try:
        return body.find('span', {'class': 'jcn'}).a.string
    except:
        return 'None'


def which_digit(html):
    try:
        mappingDict = {'icon-ji': 9,
                       'icon-dc': '+',
                       'icon-fe': '(',
                       'icon-hg': ')',
                       'icon-ba': '-',
                       'icon-lk': 8,
                       'icon-nm': 7,
                       'icon-po': 6,
                       'icon-rq': 5,
                       'icon-ts': 4,
                       'icon-vu': 3,
                       'icon-wx': 2,
                       'icon-yz': 1,
                       'icon-acb': 0,
                       }
        return mappingDict.get(html, '')
    except:
        return 'None'


def get_website(web_soup):
    website_link = web_soup['data-href']
    link = requests.get(website_link, headers={'User-Agent': "Mozilla/5.0 (Windows NT 6.1; Win64; x64)"})
    soup = BeautifulSoup(link.content, "html.parser")

    try:
        website = soup.find('i', attrs={'class': 'web_ic sprite_wb comp-icon'}).find_previous('li').find(
            'a').text.strip()
    except:
        website = 'None'

    return website


def get_phone_number(body):
    i = 0
    phoneNo = "No Number!"
    try:

        for item in body.find('p', {'class': 'contact-info'}):
            i += 1
            if (i == 2):
                phoneNo = ''
                try:
                    for element in item.find_all(class_=True):
                        classes = []
                        classes.extend(element["class"])
                        phoneNo += str((which_digit(classes[1])))
                    print('Phone-->',phoneNo)
                    return phoneNo
                except:
                    pass
    except:
        pass
    body = body['data-href']
    soup = BeautifulSoup(body, 'html.parser')
    for a in soup.find_all('a', {"id": "whatsapptriggeer"}):
        try:
            phoneNo = str(a['href'][-10:])
            return phoneNo
        except:
            return 'None'


def get_rating(body):
    try:
        rating = 0.0
        text = body.find('span', {'class': 'star_m'})
        if text is not None:
            for item in text:
                rating += float(item['class'][0][1:]) / 10
        return rating
    except:
        return 'None'


def get_rating_count(body):
    try:
        text = body.find('span', {'class': 'rt_count'}).string

        # Get only digits
        rating_count = ''.join(i for i in text if i.isdigit())
        return rating_count
    except:
        return 'None'


def get_address(body):
    try:
        return body.find('span', {'class': 'mrehover'}).text.strip()
    except:
        return 'None'


def get_location(body):
    try:
        text = body.find('a', {'class': 'rsmap'})
        if text == None:
            return 'None'
        text_list = text['onclick'].split(",")

        latitutde = text_list[3].strip().replace("'", "")
        longitude = text_list[4].strip().replace("'", "")

        return latitutde + ", " + longitude
    except:
        return 'None'

page_number = 1
service_count = 1


arr=  [ 'https://www.justdial.com/Pathanamthitta/Jewelers/page-', 'https://www.justdial.com/Thrissur/Jewelers/page-', 'https://www.justdial.com/Thiruvananthapuram/Jewelers/page-', 'https://www.justdial.com/Wayanad/Jewelers/page-', 'https://www.justdial.com/Alirajpur/Jewelers/page-', 'https://www.justdial.com/Anuppur/Jewelers/page-', 'https://www.justdial.com/Ashok Nagar/Jewelers/page-', 'https://www.justdial.com/Balaghat/Jewelers/page-', 'https://www.justdial.com/Barwani/Jewelers/page-', 'https://www.justdial.com/Betul/Jewelers/page-', 'https://www.justdial.com/Bhind/Jewelers/page-', 'https://www.justdial.com/Bhopal/Jewelers/page-', 'https://www.justdial.com/Burhanpur/Jewelers/page-', 'https://www.justdial.com/Chhatarpur/Jewelers/page-', 'https://www.justdial.com/Chhindwara/Jewelers/page-', 'https://www.justdial.com/Damoh/Jewelers/page-', 'https://www.justdial.com/Datia/Jewelers/page-', 'https://www.justdial.com/Dewas/Jewelers/page-', 'https://www.justdial.com/Dhar/Jewelers/page-', 'https://www.justdial.com/Dindori/Jewelers/page-', 'https://www.justdial.com/Guna/Jewelers/page-', 'https://www.justdial.com/Gwalior/Jewelers/page-', 'https://www.justdial.com/Harda/Jewelers/page-', 'https://www.justdial.com/Hoshangabad/Jewelers/page-', 'https://www.justdial.com/Indore/Jewelers/page-', 'https://www.justdial.com/Jabalpur/Jewelers/page-', 'https://www.justdial.com/Jhabua/Jewelers/page-', 'https://www.justdial.com/Katni/Jewelers/page-', 'https://www.justdial.com/Khandwa (East Nimar)/Jewelers/page-', 'https://www.justdial.com/Khargone (West Nimar)/Jewelers/page-', 'https://www.justdial.com/Mandla/Jewelers/page-', 'https://www.justdial.com/Mandsaur/Jewelers/page-', 'https://www.justdial.com/Morena/Jewelers/page-', 'https://www.justdial.com/Narsinghpur/Jewelers/page-', 'https://www.justdial.com/Neemuch/Jewelers/page-', 'https://www.justdial.com/Panna/Jewelers/page-', 'https://www.justdial.com/Rewa/Jewelers/page-', 'https://www.justdial.com/Rajgarh/Jewelers/page-', 'https://www.justdial.com/Ratlam/Jewelers/page-', 'https://www.justdial.com/Raisen/Jewelers/page-', 'https://www.justdial.com/Sagar/Jewelers/page-', 'https://www.justdial.com/Satna/Jewelers/page-', 'https://www.justdial.com/Sehore/Jewelers/page-', 'https://www.justdial.com/Seoni/Jewelers/page-', 'https://www.justdial.com/Shahdol/Jewelers/page-', 'https://www.justdial.com/Shajapur/Jewelers/page-', 'https://www.justdial.com/Sheopur/Jewelers/page-', 'https://www.justdial.com/Shivpuri/Jewelers/page-', 'https://www.justdial.com/Sidhi/Jewelers/page-', 'https://www.justdial.com/Singrauli/Jewelers/page-', 'https://www.justdial.com/Tikamgarh/Jewelers/page-', 'https://www.justdial.com/Ujjain/Jewelers/page-', 'https://www.justdial.com/Umaria/Jewelers/page-', 'https://www.justdial.com/Vidisha/Jewelers/page-', 'https://www.justdial.com/Ahmednagar/Jewelers/page-', 'https://www.justdial.com/Akola/Jewelers/page-', 'https://www.justdial.com/Amravati/Jewelers/page-', 'https://www.justdial.com/Aurangabad/Jewelers/page-', 'https://www.justdial.com/Bhandara/Jewelers/page-', 'https://www.justdial.com/Beed/Jewelers/page-', 'https://www.justdial.com/Buldhana/Jewelers/page-', 'https://www.justdial.com/Chandrapur/Jewelers/page-', 'https://www.justdial.com/Dhule/Jewelers/page-', 'https://www.justdial.com/Gadchiroli/Jewelers/page-', 'https://www.justdial.com/Gondia/Jewelers/page-', 'https://www.justdial.com/Hingoli/Jewelers/page-', 'https://www.justdial.com/Jalgaon/Jewelers/page-', 'https://www.justdial.com/Jalna/Jewelers/page-', 'https://www.justdial.com/Kolhapur/Jewelers/page-', 'https://www.justdial.com/Latur/Jewelers/page-', 'https://www.justdial.com/Mumbai City/Jewelers/page-', 'https://www.justdial.com/Mumbai suburban/Jewelers/page-', 'https://www.justdial.com/Nandurbar/Jewelers/page-', 'https://www.justdial.com/Nanded/Jewelers/page-', 'https://www.justdial.com/Nagpur/Jewelers/page-', 'https://www.justdial.com/Nashik/Jewelers/page-', 'https://www.justdial.com/Osmanabad/Jewelers/page-', 'https://www.justdial.com/Parbhani/Jewelers/page-', 'https://www.justdial.com/Pune/Jewelers/page-', 'https://www.justdial.com/Raigad/Jewelers/page-', 'https://www.justdial.com/Ratnagiri/Jewelers/page-', 'https://www.justdial.com/Sindhudurg/Jewelers/page-', 'https://www.justdial.com/Sangli/Jewelers/page-', 'https://www.justdial.com/Solapur/Jewelers/page-', 'https://www.justdial.com/Satara/Jewelers/page-', 'https://www.justdial.com/Thane/Jewelers/page-', 'https://www.justdial.com/Wardha/Jewelers/page-', 'https://www.justdial.com/Washim/Jewelers/page-', 'https://www.justdial.com/Yavatmal/Jewelers/page-']

# Write fields first0
#csvwriter.writerow(dict((fn,fn) for fn in fields))

for cities in arr:
    page_number = 1
    while True:
        # options = webdriver.ChromeOptions()
        # # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
        # options.add_argument('--disable-gpu')
        # # options.add_argument('--headless')
        # driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver', options=options)
        # Check if reached end of result
        if page_number > 50:
            break

        url=cities +str(page_number)
        print('Page Url-->',url)
        # req = requests.get(url, headers={'User-Agent' : "Mozilla/5.0 (Windows NT 6.1; Win64; x64)"})

        # page=urllib2.urlopen(url)
        # soup = BeautifulSoup(req.content, "html.parser")
        con = requests.get(url,headers={'User-Agent': "Mozilla/5.0 (Windows NT 6.1; Win64; x64)"} )
        # time.sleep(5)
        # pre_scroll_height = driver.execute_script('return document.body.scrollHeight;')
        # run_time, max_run_time = 0, 1
        # while True:
        #     iteration_start = time.time()
        #     # Scroll webpage, the 100 allows for a more 'aggressive' scroll
        #     driver.execute_script('window.scrollTo(0, 100*document.body.scrollHeight);')
        #
        #     post_scroll_height = driver.execute_script('return document.body.scrollHeight;')
        #
        #     scrolled = post_scroll_height != pre_scroll_height
        #     timed_out = run_time >= max_run_time
        #
        #     if scrolled:
        #         run_time = 0
        #         pre_scroll_height = post_scroll_height
        #     elif not scrolled and not timed_out:
        #         run_time += time.time() - iteration_start
        #     elif not scrolled and timed_out:
        #         break
        #
        # time.sleep(20)

        soup=BeautifulSoup(con.content,u'html.parser')
        services = soup.find_all('li', {'class': 'cntanr'})
        for service_html in services:
            # Parse HTML to fetch data
            dict_service = {}
            website = get_website(service_html)
            name = get_name(service_html)
            print('Name-->',name)
            phone = get_phone_number(service_html)
            rating = get_rating(service_html)
            count = get_rating_count(service_html)
            address = get_address(service_html)
            location = get_location(service_html)

            ## Writing Into Excel
            row += 1
            print('Rows Done--',row)
            print('Time-->',datetime.datetime.now())
            print('--'*60)
            sheet1.write(row, 0, name)
            sheet1.write(row, 1, phone)
            sheet1.write(row, 2, rating)
            sheet1.write(row, 3, count)
            sheet1.write(row, 4, address)
            sheet1.write(row, 5, location)
            sheet1.write(row, 6, website)
            wb.save('F:\Projects\Fiver\Files\justdial_jewelers_palakar.xls')


        page_number += 1
    #     driver.close()
    # driver.close()
