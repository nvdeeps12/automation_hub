from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
import requests
import sys
import xlrd

wb = xlwt.Workbook()
sheet1 = wb.add_sheet('College')
sheet2 = wb.add_sheet('Courses')
sheet1.write(0,0,"Collage_id")
sheet1.write(0,1,"College_name")
sheet1.write(0,2,"Short_name")
sheet1.write(0,3,"Est year")
sheet1.write(0,4,"Ownership")
sheet1.write(0,5,"Website")
sheet1.write(0,6,"Campus_size")
sheet1.write(0,7,"Youtube link")
sheet1.write(0,8,"Address")
sheet1.write(0,9,"Country")
sheet1.write(0,10,"City")
sheet1.write(0,11,"State")
sheet1.write(0,12,'Pin')
sheet1.write(0,13,'Gender')
sheet1.write(0,14,'Rank')
sheet1.write(0,15,'Approval')
sheet1.write(0,16,'Faculty')
sheet1.write(0,17,'Enrolments')
sheet1.write(0,18,'Exams')
sheet1.write(0,19,'Link')

sheet2.write(0,0,'S.no')
sheet2.write(0,1,'collage_id')
sheet2.write(0,2,'course_id')
sheet2.write(0,3,'fee')
sheet2.write(0,4,'Duration')
sheet2.write(0,5,'Seats')
sheet2.write(0,6,'College Name')

loc = ("F:\Projects\Fiver\Files\gmu_missing_courses.xlsx")
#
# To open Workbook
wb2 = xlrd.open_workbook(loc)
sheet = wb2.sheet_by_index(0)


missiin_arr = [(26, 'shree-guru-gobind-singh-tricentenary-university-gurgaon'), (40, 'apollo-institute-of-medical-sciences-and-research-hyderabad'), (41, 'srm-institute-of-science-and-technology-chennai'), (45, 'king-georges-medical-university-lucknow'), (53, 'gitam-institute-of-medical-sciences-and-research-visakhapatnam'), (54, 'guru-gobind-singh-indraprastha-university-Delhi'), (80, 'army-college-of-medical-sciences-Delhi'), (81, 'shri-ramkrishna-institute-of-medical-sciences-durgapur'), (91, 'indian-institute-of-medical-science-and-research-jalna'), (94, 'shri-shankaracharya-institute-of-medical-science-bhilai'), (97, 'esic-medical-college-faridabad'), (105, 'sri-padmavathi-medical-college-for-women-tirupati'), (108, 'hbt-medical-college-and-dr-rn-cooper-municipal-medical-college-and-general-hospital-mumbai'), (109, 'esic-medical-college-hyderabad'), (112, 'adesh-medical-college-and-hospital-kurukshetra'), (129, 'esic-medical-college-gulbarga'), (131, 'ananta-institute-of-medical-science-and-research-centre-rajsamand'), (136, 'heritage-institute-of-medical-sciences-varanasi'), (153, 'smbt-institute-of-medical-sciences-and-research-center-nashik'), (154, 'hind-institute-of-medical-sciences-sitapur'), (156, 'pacific-medical-college-and-hospital-udaipur'), (157, 'government-medical-college-and-esic-hospital-coimbatore'), (160, 'dr-baba-saheb-ambedkar-medical-college-and-hospital-new-Delhi'), (168, 'rkdf-medical-college-hospital-and-research-centre-bhopal'), (171, 'north-Delhi-municipal-corporation-medical-college-new-Delhi'), (173, 'doon-medical-college-dehradun'), (181, 'chamarajanagar-institute-of-medical-sciences-chamarajanagar'), (186, 'velammal-medical-college-hospital-and-research-institute-madurai'), (202, 'tagore-medical-college-and-hospital-chennai'), (203, 'karwar-institute-of-medical-sciences-karwar'), (206, 'major-sd-singh-medical-college-and-hospital-farrukhabad'), (229, 'jaipur-national-university-jaipur'), (231, 'kodagu-institute-of-medical-sciences-madikeri'), (236, 'shridev-suman-subharti-medical-college-dehradun'), (237, 'kalpana-chawla-government-medical-college-karnal'), (238, 'hi-tech-medical-college-and-hospital-rourkela'), (240, 'chhatrapati-shahu-ji-maharaj-university-kanpur'), (243, 'government-medical-college-and-super-facility-hospital-chakrapanpur'), (248, 'akash-institute-of-medical-science-and-research-centre-devanahalli'), (253, 'government-medical-college-and-hospital-jalgaon'), (255, 'subbaiah-institute-of-medical-sciences-shimoga'), (257, 'east-point-college-of-medical-sciences-and-research-centre-bangalore'), (261, 'acsr-government-medical-college-nellore'), (262, 'andaman-and-nicobar-islands-institute-of-medical-sciences-port-blair'), (265, 'lord-buddha-koshi-medical-college-and-hospital-saharsa'), (271, 'government-medical-college-banda'), (272, 'gouri-devi-institute-of-medical-sciences-and-hospital-durgapur'), (275, 'irt-perundurai-medical-college-perundurai'), (276, 'acs-medical-college-and-hospital-chennai'), (277, 'gs-medical-college-and-hospital-hapur'), (279, 'kanachur-institute-of-medical-sciences-natekal'), (286, 'banas-medical-college-and-research-institute-palanpur'), (288, 'datta-meghe-institute-of-medical-sciences-wardha'), (289, 'kd-medical-college-hospital-and-research-center-mathura'), (295, 'college-of-medicine-and-jnm-hospital-kalyani'), (302, 'rajarshee-chhatrapati-shahu-maharaj-government-medical-college-and-cpr-hospital-kolhapur'), (307, 'jaipur-national-university-institute-for-medical-sciences-and-research-centre-jaipur'), (308, 'kerala-university-of-health-sciences-thrissur'), (310, 'shridevi-institute-of-medical-sciences-and-research-hospital-tumkur'), (312, 'bkl-walawalkar-rural-medical-college-ratnagiri'), (313, 'believers-church-medical-college-hospital-thiruvalla'), (320, 'government-medical-college-bettiah'), (321, 'government-medical-college-gondia'), (322, 'vardhman-institute-of-medical-sciences-pavapuri'), (323, 'koppal-institute-of-medical-sciences-koppal'), (326, 'madha-medical-college-and-research-institute-chennai'), (327, 'amaltas-institute-of-medical-sciences-dewas'), (330, 'mizoram-institute-of-medical-education-and-research-falkawn'), (332, 'sln-medical-college-and-hospital-koraput'), (336, 'fh-medical-college-firozabad'), (351, 'ayaan-institute-of-medical-sciences-moinabad'), (357, 'prakash-institute-of-medical-sciences-and-research-sangli'), (361, 'nimra-institute-of-medical-sciences-vijayawada'), (363, 'government-medical-college-kannauj'), (365, 'zydus-medical-college-and-hospital-dahod'), (367, 'rajshree-medical-research-institute-bareilly'), (370, 'government-medical-college-omandurar'), (372, 'apollo-institute-of-medical-sciences-and-research-chittoor'), (376, 'government-medical-college-and-hospital-pudukkottai'), (377, 'ras-bihari-bose-subharti-university-dehradun'), (378, 'gulbarga-institute-of-medical-sciences-kalaburagi'), (380, 'chintpurni-medical-college-and-hospital-pathankot'), (381, 'maharishi-markandeshwar-deemed-be-university-mullana'), (384, 'gadag-institute-of-medical-sciences-gadag'), (385, 'mahamaya-rajkiya-allopathic-medical-college-ambedkarnagar'), (386, 'gmers-medical-college-and-hospital-gandhinagar'), (388, 'vedantaa-institute-of-medical-science-vedantaa-hospital-and-research-center-palghar'), (391, 'fathima-institute-of-medical-sciences-kadapa'), (394, 'malda-medical-college-and-hospital-malda'), (400, 'shri-lal-bahadur-shastri-government-medical-college-mandi'), (401, 'dr-ntr-university-of-health-sciences-vijayawada'), (403, 'patliputra-medical-college-dhanbad'), (404, 'government-medical-college-jalaun'), (407, 'government-medical-college-chandrapur'), (408, 'shaikh-ul-hind-maulana-mahmood-hasan-medical-college-saharanpur'), (415, 'gmers-medical-college-junagadh'), (417, 'rajiv-gandhi-institute-of-medical-sciences-adilabad'), (418, 'sri-muthukumaran-medical-college-hospital-and-research-institute-chennai'), (419, 'institute-of-medical-sciences-and-research-satara'), (422, 'pt-deendayal-upadhyay-memorial-health-sciences-and-ayush-university-of-chhattisgarh-raipur'), (424, 'parul-institute-of-medical-sciences-and-research-vadodara'), (425, 'pandit-raghunath-murmu-medical-college-and-hospital-baripada'), (428, 'government-medical-college-rajnandgaon'), (430, 'gmers-medical-college-vadnagar'), (431, 'government-medical-college-pali'), (434, 'malabar-medical-college-hospital-and-research-centre-modakkallur'), (436, 'singhania-university-jhunjhunu'), (437, 'dm-wims-medical-college-and-hospital-meppadi'), (438, 'all-india-institute-of-medical-sciences-mangalagiri'), (441, 'mayo-institute-of-medical-sciences-barabanki'), (442, 'dr-mk-shah-medical-college-and-research-centre-ahmedabad'), (443, 'mulayam-singh-yadav-medical-college-and-hospital-meerut'), (444, 'government-medical-college-and-hospital-balasore'), (445, 'shri-bhausaheb-hire-government-medical-college-and-hospital-chakkarbardi'), (446, 'rama-medical-college-hospital-and-research-centre-hapur'), (447, 'rvm-institute-of-medical-sciences-and-research-center-siddipet'), (450, 'baba-farid-university-of-health-sciences-faridkot'), (452, 'government-tiruvannamalai-medical-college-and-hospital-tiruvannamalai'), (455, 'sambhram-institute-of-medical-sciences-and-research-beml-nagar'), (456, 'government-medical-college-kollam'), (459, 'sarvepalli-radhakrishnan-university-bhopal'), (460, 'modern-institute-of-medical-sciences-indore'), (461, 'gayatri-vidya-parishad-institute-of-health-care-and-medical-technology-visakhapatnam'), (463, 'late-bali-ram-kashyap-memorial-government-medical-college-jagdalpur'), (465, 'government-medical-college-and-hospital-balangir'), (467, 'al-azhar-medical-college-and-super-speciality-hospital-thodupuzha'), (472, 'pacific-institute-of-medical-sciences-udaipur'), (473, 'dr-radhakrishnan-government-medical-college-hamirpur'), (474, 'gmers-medical-college-nanakwada'), (475, 'pandit-deendayal-upadhyaya-medical-college-churu'), (476, 'government-medical-college-ambikapur'), (478, 'government-medical-college-khandwa'), (480, 'tomo-riba-institute-health-and-medical-sciences-naharlagun'), (481, 'dr-mgr-educational-and-research-institute-chennai'), (483, 'late-shri-l-m-government-medical-college-raigarh'), (484, 'government-medical-college-palakkad'), (485, 'pt-jawahar-lal-nehru-government-medical-college-and-hospital-chamba'), (486, 'government-medical-college-ratlam'), (487, 'gmers-medical-college-and-hospital-himmatnagar'), (488, 'rajasthan-university-of-health-sciences-jaipur'), (490, 'sree-uthradom-thirunal-academy-of-medical-sciences-vencode'), (491, 'ponnaiyah-ramajayam-institute-of-medical-sciences-chennai'), (493, 'pacific-medical-university-udaipur'), (495, 'government-medical-college-bharatpur'), (496, 'annapoorana-medical-college-and-hospital-salem'), (497, 'hemwati-nandan-bahuguna-uttarakhand-medical-education-university-dehradun'), (498, 'government-medical-college-manjeri'), (499, 'viswabharathi-medical-college-and-general-hospital-kurnool'), (500, 'rajmata-vijaya-raje-scindia-medical-college-bhilwara'), (501, 'annaii-medical-college-and-hospital-sriperumbudur'), (502, 'dr-yashwant-singh-parmar-government-medical-college-nahan'), (505, 'dr-vrk-womens-medical-college-teaching-hospital-and-research-centre-aziznagar'), (506, 'atal-bihari-vajpayee-government-medical-college-vidisha'), (507, 'institute-of-medical-sciences-and-research-vidyagiri'), (508, 'pk-das-institute-of-medical-sciences-ottapalam'), (509, 'government-sivagangai-medical-college-and-hospital-sivaganga'), (510, 'maheshwara-medical-college-and-hospital-patancheru'), (511, 'all-india-institute-of-medical-sciences-nagpur'), (512, 'krantiguru-shyamji-krishna-verma-kachchh-university-bhuj'), (513, 'mount-zion-medical-college-hospital-ezhamkulam'), (514, 'government-medical-college-datia'), (516, 'aryabhatta-knowledge-university-patna'), (518, 'government-medical-college-idukki'), (520, 'parul-university-vadodara'), (521, 'lnct-university-bhopal'), (522, 'gujarat-medical-education-and-research-society-medical-college-and-hospital-dharpur'), (523, 'government-medical-college-dungarpur'), (524, 'maharaja-sayajirao-university-of-baroda-vadodara'), (525, 'sri-balaji-vidyapeeth-pondicherry'), (526, 'sri-devaraj-urs-academy-of-higher-education-and-research-tamaka'), (527, 'manipur-university-imphal'), (528, 'madhya-pradesh-medical-science-university-jabalpur'), (529, 'rama-university-kanpur'), (530, 'kolhan-university-chaibasa'), (531, 'government-medical-college-nmdc-jagdalpur'), (532, 'santosh-university-ghaziabad'), (534, 'geetanjali-university-udaipur'), (535, 'malla-reddy-medical-college-for-women-hyderabad'), (536, 'sambalpur-university-sambalpur'), (537, 'ranchi-university-ranchi'), (540, 'government-sivagangai-medical-college-sivaganga'), (541, 'hemchandracharya-north-gujarat-university-patan'), (542, 'ipgmer-and-sskm-hospital-kolkatta'), (543, 'krishna-mohan-medical-college-mathura'), (544, 'gmers-medical-college-ahemdabad'), (545, 'government-medical-college-suryapet'), (546, 'bk-shah-medical-institute-&-research-centre-varodara'), (547, 'smt.-kashibai-navale-medical-college-and-general-hospital-pune'), (548, 'government-medical-college-badaun'), (549, 'government-medical-college-bahraich'), (550, 'government-medical-college-nalgonda'), (551, 'college-of-medicine-&-jnm-hospital-kolkatta'), (552, 'american-international-institute-of-medical-sciences---'), (553, 'shri-bm-patil-medical-college-and-research-centre'), (554, 'gmers-medical-college-patan'), (555, 'gmers-medical-college-vadodra'), (556, 'gmers-medical-college-valsad'), (557, 'nc-medical-college-&-hospital-panipat'), (558, 'government-medical-college-barmer'), (559, 'government-medical-college-faizabad'), (560, 'government-medical-college-rajouri'), (561, 'surabhi-institute-of-medical-sciences-siddipet'), (562, 'government-medical-college-baramati'), (563, 'goverment-medical-college-shahdol'), (564, 'government-medical-college-baramulla-baramulla'), (565, 'government-medical-college-basti'), (566, 'raiganj-government-medical-college-and-hospital-raiganj'), (567, 'raipur-institute-of-medical-sciences-raipur'), (568, 'raja-dashrath-medical-college-ayodhya'), (569, 'bowring-&-lady-curzon-hospital-bangalore'), (570, 'shri-vinobha-bhave-institute-medical-sciences-silvasa'), (571, 'madhubani-medical-college-madhubani'), (572, 'chhindwara-institute-of-medical-sciences-chhindwara'), (573, 'government-medical-college-karur'), (574, 'government-medical-college-kathua'), (575, 'government-medical-college-khaleelwadi'), (576, 'diamond-harbour-government-medical-college'), (577, 'government-medical-college-anantnag-anantnag'), (578, 'government-medical-college-firozabad'), (579, 'coochbehar-government-medical-college-and-hospital'), (580, 'all-india-institute-of-medical-sciences-amalapuram'), (581, 'chamarajanagar-institute-of-medical-sciences-chamarajanagar'), (582, 'dr.-patnam-mahender-reddy-institute-of-medical-sciences-chevella'), (583, 'vikhe-patil-institute-of-medical-sciences'), (584, 'rampurhat-government-medical-college-and-hospital-rampurhat'), (585, 'al-falah-school-of-medical-sciences'), (586, 'bahiram-jeejiji-bhai-medical-college'), (587, 'king-edward-memorial-hospital-and-seth-gs-medical-college-mumbai'), (588, 'prasad-medical-college-lucknow'), (589, 'government-medical-college-shahjahanpur'), (590, 'government-medical-college-shivpuri'), (591, 'mahatma-gandhi-institute-of-medical-sciences-wardha'), (592, 'mahavir-institute-of-medical-sciences-vikarabad'), (593, 'government-institute-of-medical-sciences\greater-noida')]


class Gmu():
    def get_colleges(self):
        self.row=0
        self.collage_id=1
        # options = webdriver.FirefoxOptions()
        # # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
        # options.add_argument('--disable-gpu')
        # # options.add_argument('--headless')
        # driver = webdriver.Firefox(executable_path='..\Drivers\geckodriver',options=options)
        for p in missiin_arr:
            self.ids = p[0]
            if self.ids<541:
                continue
            slug = p[1]
            print('Ruuning--College_id>', self.ids)
            slug = str(slug).lower()
            ## This is Record Id
            self.collage_id+=1

            self.getcollege_data(slug)

    def getcollege_data(self,content):

        page = 'https://www.careers360.com/colleges/'+str(content)

        con = requests.get(page)

        status_code = con.status_code
        if status_code!=200:
            page = 'https://www.careers360.com/university/'+str(content)
            con = requests.get(page)

        print('Page-->', page)
        soup = BeautifulSoup(con.content, u"html.parser")

        try:
            college_name = soup.find('h1').text.strip()
        except:
            college_name = 'None'


        try:
            short_name = soup.find('span', attrs={'class': 'shortName'}).text.strip().split('(Also known as:')[1].split(')')[0]
        except:
            short_name = 'None'


        try:

            lcation = soup.find('span', attrs={'class':'location'}).text.strip()

            lcation = soup.find('span', attrs={'class': 'location'}).text.strip()

        except:
            lcation = 'None'

        import re
        try:
            rank = soup.find(text=re.compile('Ranked in Medical')).find_next('div').text.strip()
        except:
            rank = 'None'


        try:
            ownership = soup.find(text=re.compile('Ownership')).find_next('div').text.strip()
        except:
            ownership = 'None'

        try:
            approval = soup.find(text=re.compile('Approval')).find_next('div').text.strip()
        except:
            approval = 'None'

        try:
            genders_ac = soup.find(text=re.compile('Genders Accepted')).find_next('div').text.strip()
        except:
            genders_ac = 'None'

        ## One Blck Ends Here

        try:
            es_year = soup.find(text=re.compile('Estd. Year:')).find_next('strong').text.strip()
        except:
            es_year = 'None'



        try:
            campus_size = soup.find(text=re.compile('Campus Size:')).find_next('strong').text.strip()
        except:
            campus_size = 'None'

        try:
            total_faculty = soup.find(text=re.compile('Total Faculty:')).find_next('strong').text.strip()
        except:
            total_faculty = 'None'

        try:
            total_enrolments = soup.find(text=re.compile('Total Student Enrollments:')).find_next('strong').text.strip()
        except:
            total_enrolments = 'None'


        ## Address From Last Block

        try:
            add_block = soup.find('div',attrs={'class':'adrsDetail'}).text.strip()
        except:
            add_block = 'None'

        try:
            website=soup.find('div',attrs={'class':'adrsDetail'}).find('a').get('href')
        except:

            website = 'None'
        try:
            address= add_block.strip().split('Address:')[1].split('-')[0]
        except:
            address = 'none'

        try:
            pin= add_block.split('-')[1].split(',')[0]
            res = [int(i) for i in pin.split() if i.isdigit()]

            pin=str(res[0])
        except:
            pin = 'none'

        try:
            state=lcation.split(',')[1]
        except:
            state = 'none'

        # try:
        #     country=add_block.split(state)[1].split(',')[1]
        #
        # except:
        #     country = 'none'

        try:
            city = lcation.split(',')[0]
        except:
            city = 'None'
        try:
            country='India'

        except:
            country = 'none'

        country="INDIA"

        try:
            Ylink=soup.find('div',attrs={'id':'gallery'}).find('a',attrs={'class':'owl-video'}).get('href')
            Ylink=Ylink.split('src=')[1].split('frameborder')[0]
        except:
            Ylink='none'

        try:
            exams=[]
            examms=soup.find('div',attrs={'id':'examsCutoff'}).findAll('div',attrs={'class':'ecBlk vtop'})

            for exam in examms:
                exam=exam.find('strong').text.strip()+' , '
                exams.append(exam)
        except:
            exams="none"
        print(exams)
        print("College Name- " + college_name)
        print("Address - " + address+" State "+state+' pin'+pin+' Country'+ country)
        print("College ID- " + str(self.collage_id))
        print("Campus Size "+ campus_size)
        print("Est Year" + es_year)
        print("Ownership -"+ ownership)
        print("Short Name -" +short_name)
        print("lcation- " + lcation)
        print('Video ' + Ylink)
        print("website " + website)


        sheet1.write(self.collage_id,0,self.ids)
        sheet1.write(self.collage_id,1,college_name)
        sheet1.write(self.collage_id,2,short_name)
        sheet1.write(self.collage_id,3,es_year)
        sheet1.write(self.collage_id,4,ownership)
        sheet1.write(self.collage_id,5,website)
        sheet1.write(self.collage_id,6,campus_size)
        sheet1.write(self.collage_id,7,Ylink)
        sheet1.write(self.collage_id,8,address)
        sheet1.write(self.collage_id,9,country)
        sheet1.write(self.collage_id, 10, city)
        sheet1.write(self.collage_id,11,state)
        sheet1.write(self.collage_id,12,pin)
        sheet1.write(self.collage_id, 13, genders_ac)
        sheet1.write(self.collage_id, 14, rank)
        sheet1.write(self.collage_id, 15, approval)
        sheet1.write(self.collage_id, 16, total_faculty)
        sheet1.write(self.collage_id, 17, total_enrolments)
        sheet1.write(self.collage_id, 18, exams)
        sheet1.write(self.collage_id, 19, str(content))

        ## Gettng Courses Data
        try:
            allcourse_link = soup.findAll('a',attrs={'class':'viewAllBtn nxtpageLink'})[0]['href']
            if 'courses' not in allcourse_link:
                allcourse_link = soup.findAll('a',attrs={'class':'viewAllBtn nxtpageLink'})[1]['href']
            allcourse_link = 'https://www.careers360.com'+str(allcourse_link)
            print(allcourse_link)
            page_con = requests.get(allcourse_link)
            course_sop1 = BeautifulSoup(page_con.content, u"html.parser")
            try:
                tc=int(course_sop1.find('span',attrs={'class':'showResults'}).text.strip().split('Showing')[1].split('Courses')[0])
            except:
                tc = 1
            try:
                cp = 1
                print(tc)
                pg=(tc/30)
                pg=int(pg)
                rem=(tc%30)
                if rem>=1:
                    pg=pg+1

                else:
                    pg=pg


                while cp<=pg:

                    print('--' * 100)
                    print("page-" + str(cp))

                    page_con2 = requests.get(allcourse_link + "?page="+str(cp))
                    print('Courses Link-->',allcourse_link + "?page="+str(cp))
                    cp = cp + 1
                    print('--' * 100)
                    course_sop=BeautifulSoup(page_con2.content,u"html.parser")

                    cards = course_sop.findAll('div',attrs={'class':'cardBlk'})
                    for c in cards:
                        # print(c)
                        try:
                            course_name = c.find('h2',attrs={'class':'heading4'}).text.strip()
                        except:
                            course_name = 'None'

                        try:
                            duration = c.find('h2',attrs={'class':'headInData'}).text.strip().split(':')[1]
                        except:
                            duration = 'None'

                        try:
                            fee = c.find(text=re.compile('Total Fees:')).find_previous('li').text.strip().split(":")[1]
                        except:
                            fee = 'None'
                        try:
                            seats = c.find(text=re.compile('Seats:')).find_previous('li').text.strip().split(':')[1]
                        except:
                            seats = 'None'

                        print("Course name- "+ course_name)
                        print("Duration- "+ duration)
                        print("Fee -"+ fee)
                        print("Seats- "+seats)
                        print('**' * 50)
                        self.row=self.row+1
                        sheet2.write(self.row,0,self.row)
                        sheet2.write(self.row,1,self.ids)
                        sheet2.write(self.row,2,course_name)
                        sheet2.write(self.row,3,duration)
                        sheet2.write(self.row,4,fee)
                        sheet2.write(self.row,5,seats)
                        sheet2.write(self.row,6,college_name)
                        wb.save('F:\Projects\Fiver\Files\Missed_courses_541.xls')

            except:
                print(sys.exc_info())
                cards = 'None'

        except:
            allcourse_link = 'None'


        print(allcourse_link)


        print('--'*50)




obj = Gmu()
obj.get_colleges()