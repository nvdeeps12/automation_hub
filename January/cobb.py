from bs4 import BeautifulSoup
from selenium import webdriver
import xlwt
import requests
import sys
wb = xlwt.Workbook()
sheet1 = wb.add_sheet('College')
sheet2 = wb.add_sheet('Courses')
sheet1.write(0,0,"Collage_id")
sheet1.write(0,1,"College_name")
sheet1.write(0,2,"Short_name")
sheet1.write(0,3,"Est year")
sheet1.write(0,4,"Ownership")
sheet1.write(0,5,"Website")
sheet1.write(0,6,"Campus_size")
sheet1.write(0,7,"Youtube link")
sheet1.write(0,8,"Address")
sheet1.write(0,9,"Country")
sheet1.write(0,10,"City")
sheet1.write(0,11,"State")
sheet1.write(0,12,'Pin')
sheet1.write(0,13,'Gender')
sheet1.write(0,14,'Rank')
sheet1.write(0,15,'Approval')
sheet1.write(0,16,'Faculty')
sheet1.write(0,17,'Enrolments')
sheet1.write(0,18,'Exams')
sheet1.write(0,19,'Link')

sheet2.write(0,0,'S.no')
sheet2.write(0,1,'collage_id')
sheet2.write(0,2,'course_id')
sheet2.write(0,3,'fee')
sheet2.write(0,4,'Duration')
sheet2.write(0,5,'Seats')
sheet2.write(0,6,'College Name')

class Cobb():
    def get_cobbdata(self):
        self.row=0
        self.collage_id=0
        # options = webdriver.FirefoxOptions()
        # # options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
        # options.add_argument('--disable-gpu')
        # # options.add_argument('--headless')
        # driver = webdriver.Firefox(executable_path='..\Drivers\geckodriver',options=options)
        count = 0
        links_arr = ['https://cobbca.cobbcounty.org/CitizenAccess/?culture=en-US']
        for link in links_arr:
            page_link  = link
            for p in range(20):
                ## Skiping 0th page
                if p==0:
                    p=1
                pages = page_link
                print('Running Page-->',pages)
                con = requests.get(pages)
                print(con.status_code)
                soup=BeautifulSoup(con.content,u"html.parser")

                ## Getting Name,Phone and Website
                data_div = soup.findAll('div',attrs={'class':'content'})

                for d in data_div:
                    try:
                        self.collage_id=self.collage_id+1
                        college_lnks = d.find('h4').find('a')['href']
                        self.getcollege_data(college_lnks)
                    except:
                        continue


    def getcollege_data(self,content):

        con = requests.get(content)
        soup = BeautifulSoup(con.content, u"html.parser")

        try:
            college_name = soup.find('h1').text.strip()
        except:
            college_name = 'None'


        try:
            short_name = soup.find('span', attrs={'class': 'shortName'}).text.strip().split('(Also known as:')[1].split(')')[0]
        except:
            short_name = 'None'


        try:

            lcation = soup.find('span', attrs={'class':'location'}).text.strip()

            lcation = soup.find('span', attrs={'class': 'location'}).text.strip()

        except:
            lcation = 'None'

        import re
        try:
            rank = soup.find(text=re.compile('Ranked in Medical')).find_next('div').text.strip()
        except:
            rank = 'None'


        try:
            ownership = soup.find(text=re.compile('Ownership')).find_next('div').text.strip()
        except:
            ownership = 'None'

        try:
            approval = soup.find(text=re.compile('Approval')).find_next('div').text.strip()
        except:
            approval = 'None'

        try:
            genders_ac = soup.find(text=re.compile('Genders Accepted')).find_next('div').text.strip()
        except:
            genders_ac = 'None'

        ## One Blck Ends Here

        try:
            es_year = soup.find(text=re.compile('Estd. Year:')).find_next('strong').text.strip()
        except:
            es_year = 'None'



        try:
            campus_size = soup.find(text=re.compile('Campus Size:')).find_next('strong').text.strip()
        except:
            campus_size = 'None'

        try:
            total_faculty = soup.find(text=re.compile('Total Faculty:')).find_next('strong').text.strip()
        except:
            total_faculty = 'None'

        try:
            total_enrolments = soup.find(text=re.compile('Total Student Enrollments:')).find_next('strong').text.strip()
        except:
            total_enrolments = 'None'


        ## Address From Last Block


        add_block = soup.find('address').text.strip()

        try:
            website=soup.find('div',attrs={'class':'adrsDetail'}).find('a').get('href')
        except:

            website = 'None'
        try:
            address= add_block.strip().split('Address:')[1].split('-')[0]

        except:
            address = 'none'

        try:
            pin= add_block.split('-')[1].split(',')[0]
            res = [int(i) for i in pin.split() if i.isdigit()]

            pin=str(res[0])
        except:
            pin = 'none'

        try:
            state=lcation.split(',')[1]
        except:
            state = 'none'

        # try:
        #     country=add_block.split(state)[1].split(',')[1]
        #
        # except:
        #     country = 'none'

        try:
            city = lcation.split(',')[0]
        except:
            city = 'None'
        try:
            country='India'

        except:
            country = 'none'

        country="INDIA"

        try:
            Ylink=soup.find('div',attrs={'id':'gallery'}).find('a',attrs={'class':'owl-video'}).get('href')
            Ylink=Ylink.split('src=')[1].split('frameborder')[0]
        except:
            Ylink='none'

        try:
            exams=[]
            examms=soup.find('div',attrs={'id':'examsCutoff'}).findAll('div',attrs={'class':'ecBlk vtop'})

            for exam in examms:
                exam=exam.find('strong').text.strip()+' , '
                exams.append(exam)
        except:
            exams="none"
        print(exams)
        print("College Name- " + college_name)
        print("Address - " + address+" State "+state+' pin'+pin+' Country'+ country)
        print("College ID- " + str(self.collage_id))
        print("Campus Size "+ campus_size)
        print("Est Year" + es_year)
        print("Ownership -"+ ownership)
        print("Short Name -" +short_name)
        print("lcation- " + lcation)
        print('Video ' + Ylink)
        print("website " + website)


        sheet1.write(self.collage_id,0,self.collage_id)
        sheet1.write(self.collage_id,1,college_name)
        sheet1.write(self.collage_id,2,short_name)
        sheet1.write(self.collage_id,3,es_year)
        sheet1.write(self.collage_id,4,ownership)
        sheet1.write(self.collage_id,5,website)
        sheet1.write(self.collage_id,6,campus_size)
        sheet1.write(self.collage_id,7,Ylink)
        sheet1.write(self.collage_id,8,address)
        sheet1.write(self.collage_id,9,country)
        sheet1.write(self.collage_id, 10, city)
        sheet1.write(self.collage_id,11,state)
        sheet1.write(self.collage_id,12,pin)
        sheet1.write(self.collage_id, 13, genders_ac)
        sheet1.write(self.collage_id, 14, rank)
        sheet1.write(self.collage_id, 15, approval)
        sheet1.write(self.collage_id, 16, total_faculty)
        sheet1.write(self.collage_id, 17, total_enrolments)
        sheet1.write(self.collage_id, 18, exams)
        sheet1.write(self.collage_id, 19, str(content))

        ## Gettng Courses Data
        try:
            allcourse_link = soup.find('a',attrs={'class':'viewAllBtn nxtpageLink'})['href']
            allcourse_link = 'https://www.careers360.com'+str(allcourse_link)


            page_con = requests.get(allcourse_link)
            course_sop1 = BeautifulSoup(page_con.content, u"html.parser")

            tc=int(course_sop1.find('span',attrs={'class':'showResults'}).text.strip().split('Showing')[1].split('Courses')[0])
            try:
                cp = 1
                print(tc)
                pg=(tc/30)
                pg=int(pg)
                rem=(tc%30)
                if rem>=1:
                    pg=pg+1

                else:
                    pg=pg


                while cp<=pg:

                    print('--' * 100)
                    print("page-" + str(cp))

                    page_con2 = requests.get(allcourse_link + "?page="+str(cp))
                    print(allcourse_link + "?page="+str(cp))
                    cp = cp + 1
                    print('--' * 100)
                    course_sop=BeautifulSoup(page_con2.content,u"html.parser")

                    cards = course_sop.findAll('div',attrs={'class':'cardBlk'})
                    for c in cards:
                        # print(c)
                        try:
                            course_name = c.find('h2',attrs={'class':'heading4'}).text.strip()
                        except:
                            course_name = 'None'

                        try:
                            duration = c.find('h2',attrs={'class':'headInData'}).text.strip().split(':')[1]
                        except:
                            duration = 'None'

                        try:
                            fee = c.find(text=re.compile('Total Fees:')).find_previous('li').text.strip().split(":")[1]
                        except:
                            fee = 'None'
                        try:
                            seats = c.find(text=re.compile('Seats:')).find_previous('li').text.strip().split(':')[1]
                        except:
                            seats = 'None'

                        print("Course name- "+ course_name)
                        print("Duration- "+ duration)
                        print("Fee -"+ fee)
                        print("Seats- "+seats)
                        print('**' * 50)
                        self.row=self.row+1
                        sheet2.write(self.row,0,self.row)
                        sheet2.write(self.row,1,self.collage_id)
                        sheet2.write(self.row,2,course_name)
                        sheet2.write(self.row,3,duration)
                        sheet2.write(self.row,4,fee)
                        sheet2.write(self.row,5,seats)
                        sheet2.write(self.row,6,college_name)
                        wb.save('gmm3.xls')

            except:
                print(sys.exc_info())
                cards = 'None'

        except:
            allcourse_link = 'None'


        print(allcourse_link)


        print('--'*50)




obj = Cobb()
obj.get_cobbdata()