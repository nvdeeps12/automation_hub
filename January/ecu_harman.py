import requests
from bs4 import BeautifulSoup
import xlwt
import xlrd
# rd = xlrd.open_workbook('../Files/leftover.xlsx')
# sheet = rd.sheet_by_index(0)
import time
from selenium import webdriver
# from ..January.create_xls import sub_links
options = webdriver.ChromeOptions()
# options.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
options.add_argument('--disable-gpu')
# options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='..\Drivers\chromedriver', options=options)
wb=xlwt.Workbook()
sheet1=wb.add_sheet('Sheet')
sheet1.write(0,0,'Merk')
sheet1.write(0,1,'Model')
sheet1.write(0,2,'Type')
sheet1.write(0,3,'Motor')
sheet1.write(0,4,'Origineel koppel(Eco)')
sheet1.write(0,5,'Na tunning Koppel(Eco)')
sheet1.write(0,6,'Resultaat Koppel(Eco)')
sheet1.write(0,7,'Origineel vermogen(Stage 1)')
sheet1.write(0,8,'Origineel Koppel(Stage 1)')
sheet1.write(0,9,'Vermogen na tunning(Stage 1)')
sheet1.write(0,10,'Koppel na tunning(Stage 1)')
sheet1.write(0,11,'Resltaat vermogen(Stage 1)')
sheet1.write(0,12,'Resltaat koppel(Stage 1)')
sheet1.write(0,13,'Origineel vermogen(Stage 2)')
sheet1.write(0,14,'Origineel Koppel(Stage 2)')
sheet1.write(0,15,'Vermogen na tunning(Stage 2)')
sheet1.write(0,16,'Koppel na tunning(Stage 2)')
sheet1.write(0,17,'Resltaat vermogen(Stage 2)')
sheet1.write(0,18,'Resltaat koppel(Stage 2)')
sheet1.write(0,19,'Extra opties')
sheet1.write(0,20,'Link')
# arr = ['https://www.ecu-soft.be/chiptuning/Jaguar/', 'https://www.ecu-soft.be/chiptuning/Jeep/', 'https://www.ecu-soft.be/chiptuning/Kia/', 'https://www.ecu-soft.be/chiptuning/KTM/', 'https://www.ecu-soft.be/chiptuning/Lamborghini/', 'https://www.ecu-soft.be/chiptuning/Lancia/', 'https://www.ecu-soft.be/chiptuning/Landrover/', 'https://www.ecu-soft.be/chiptuning/Lexus/', 'https://www.ecu-soft.be/chiptuning/Lotus/', 'https://www.ecu-soft.be/chiptuning/Mahindra/', 'https://www.ecu-soft.be/chiptuning/MAN/', 'https://www.ecu-soft.be/chiptuning/Maserati/', 'https://www.ecu-soft.be/chiptuning/Mazda/', 'https://www.ecu-soft.be/chiptuning/McLaren/', 'https://www.ecu-soft.be/chiptuning/Mercedes/', 'https://www.ecu-soft.be/chiptuning/Mini/', 'https://www.ecu-soft.be/chiptuning/Mitsubishi/', 'https://www.ecu-soft.be/chiptuning/Nissan/', 'https://www.ecu-soft.be/chiptuning/Opel/', 'https://www.ecu-soft.be/chiptuning/Pagani/', 'https://www.ecu-soft.be/chiptuning/Peugeot/', 'https://www.ecu-soft.be/chiptuning/PGO/', 'https://www.ecu-soft.be/chiptuning/Piaggio/', 'https://www.ecu-soft.be/chiptuning/Porsche/', 'https://www.ecu-soft.be/chiptuning/Renault/', 'https://www.ecu-soft.be/chiptuning/Rolls_Royce/', 'https://www.ecu-soft.be/chiptuning/Saab/', 'https://www.ecu-soft.be/chiptuning/Samsung/', 'https://www.ecu-soft.be/chiptuning/Scion/', 'https://www.ecu-soft.be/chiptuning/Seat/', 'https://www.ecu-soft.be/chiptuning/Skoda/', 'https://www.ecu-soft.be/chiptuning/Smart/', 'https://www.ecu-soft.be/chiptuning/SsangYong/', 'https://www.ecu-soft.be/chiptuning/Subaru/', 'https://www.ecu-soft.be/chiptuning/Suzuki/', 'https://www.ecu-soft.be/chiptuning/Tata/', 'https://www.ecu-soft.be/chiptuning/Toyota/', 'https://www.ecu-soft.be/chiptuning/Volkswagen/', 'https://www.ecu-soft.be/chiptuning/Volvo/', 'https://www.ecu-soft.be/chiptuning/Westfield/', 'https://www.ecu-soft.be/chiptuning/Wiesmann/', 'https://www.ecu-soft.be/chiptuning/Ariel_Motors/', 'https://www.ecu-soft.be/chiptuning/Cupra/', 'https://www.ecu-soft.be/chiptuning/MG/', 'https://www.ecu-soft.be/chiptuning/Borgward/', 'https://www.ecu-soft.be/chiptuning/Lincoln/', 'https://www.ecu-soft.be/chiptuning/Geely/', 'https://www.ecu-soft.be/chiptuning/JAC/']
class ecu():
    row=0
    def get_link(self):

        # brand=['toyota']
        brand = [' https://www.ecu-soft.be/chiptuning/opel/'
                 ]
        for b in brand:
            link=b
            driver.get(link)
            s1=BeautifulSoup(driver.page_source,u'html.parser')

            models=s1.find('select',attrs={'name':'product'}).findAll('option')
            m_count=6
            for model in models[1:]:
                driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[2]/div/select').click()
                driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[2]/div/select/option['+str(m_count)+']').click()
                time.sleep(3)
                m_count=m_count+1
                s2 = BeautifulSoup(driver.page_source, u'html.parser')

                years = s2.find('select', attrs={'name': 'year'}).findAll('option')
                y_count = 2
                for year in years[1:]:
                    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[3]/div/select').click()
                    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[3]/div/select/option[' + str(y_count) + ']').click()
                    time.sleep(3)
                    y_count=y_count+1
                    s3 = BeautifulSoup(driver.page_source, u'html.parser')
                    motors=s3.find('select',attrs={'name':'motor'}).findAll('option')
                    mo_count=1
                    for motor in motors[1:]:

                            mo_count = mo_count + 1

                            driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[4]/div/select').click()
                            driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/form/div[4]/div/select/option[' + str(mo_count) + ']').click()
                            time.sleep(2)
                            end_link=driver.current_url
                            s4=BeautifulSoup(driver.page_source,u'html.parser')
                            self.get_data(end_link,s4)

    def get_data(self,endlink,soup):

            link=endlink
            print('Going -> '+str(link))
            # req = requests.get(link)
            # soup=BeautifulSoup(req.content,u'html.parser')
            # soup=s4
            try:
                Merk=soup.find('div',attrs={'class':'title'}).text.strip()
            except:
                Merk='None'

            print(Merk)

            try:
                Model=soup.find('div',attrs={'class':'subtitle'}).text.strip()
            except:
                Model='None'

            print(Model)
            try:
                type=soup.find('select',attrs={'name':'year'})
                type_data=type.find('option',attrs={'selected':'selected'}).text.strip()
            except:
                type_data='None'
            print(type_data)

            try:
                motor = soup.find('select', attrs={'name': 'motor'})
                motor_data = motor.find('option', attrs={'selected': 'selected'}).text.strip()
            except:
                motor_data='None'
            print(motor_data)

    #         ~~~~~~~~~~specs~~~~~~~~~~~~~

    #         ~~~~~~~ECO~~~~~~~~~~
            try:
                specstb=soup.find('div',attrs={'data-title':'ECO'})
                specs_row=specstb.findAll('div',attrs={'class':'row'})

                sepcs_col_1=specs_row[2].findAll('div')

                eco_Koppel_Origineel=sepcs_col_1[1].text.strip()
                eco_Koppel_Na_tuning=sepcs_col_1[2].text.strip()
                eco_Koppel_Resultaat=sepcs_col_1[3].text.strip()


            except:
                eco_Koppel_Resultaat = 'None'
                eco_Koppel_Na_tuning = "None"
                eco_Koppel_Origineel = 'None'


            print("eco_Koppel_Origineel : " +eco_Koppel_Origineel)
            print("eco_Koppel_Na_tunning : " +eco_Koppel_Na_tuning)
            print("eco_Koppel_Resultaat : " +eco_Koppel_Resultaat)
            print('~~~~~~'*20)

    #         ~~~~~~~Stage 2~~~~~~~~~~

            try:
                specstb=soup.find('div',attrs={'data-title':'Stage 2'})
                specs_row=specstb.findAll('div',attrs={'class':'row'})

                sepcs_col_1=specs_row[3].findAll('div')

                stage2_Koppel_Origineel=sepcs_col_1[1].text.strip()
                stage2_Koppel_Na_tuning=sepcs_col_1[2].text.strip()
                stage2_Koppel_Resultaat=sepcs_col_1[3].text.strip()

                sepcs_col_1 = specs_row[2].findAll('div')

                stage2_ver_Origineel = sepcs_col_1[1].text.strip()
                stage2_ver_Na_tuning = sepcs_col_1[2].text.strip()
                stage2_ver_Resultaat = sepcs_col_1[3].text.strip()

            except:
                stage2_Koppel_Resultaat = 'None'
                stage2_Koppel_Na_tuning = "None"
                stage2_Koppel_Origineel = 'None'

                stage2_ver_Origineel = 'None'
                stage2_ver_Na_tuning = 'None'
                stage2_ver_Resultaat = 'None'

            print("stage2_Koppel_Origineel : " +stage2_Koppel_Origineel)
            print("stage2_Koppel_Na_tunning : " +stage2_Koppel_Na_tuning)
            print("stage2_Koppel_Resultaat : " +stage2_Koppel_Resultaat)
            print('~~~~~' * 20)
            print("stage2_ver_Origineel : " + stage2_ver_Origineel)
            print("stage2_ver_Na_tunning : " + stage2_ver_Na_tuning)
            print("stage2_Resultaat : " + stage2_ver_Resultaat)
            print('~~~~~'*20)

    #       ~~~~~~~~STAGE 1~~~~~~~~~~
            try:
                stage_specstb = soup.find('div', attrs={'data-title': 'Stage 1'})
                stage_specs_row = stage_specstb.findAll('div', attrs={'class': 'row'})
                stage_sepcs_col_2=stage_specs_row[2].findAll('div')

                Stage_Vermogen_Origineel = stage_sepcs_col_2[1].text.strip()
                Stage_Vermogen_Na_tuning = stage_sepcs_col_2[2].text.strip()
                Stage_Vermogen_Resultaat = stage_sepcs_col_2[3].text.strip()

                stage_sepcs_col_3 = stage_specs_row[3].findAll('div')

                Stage_Koppel_Origineel = stage_sepcs_col_3[1].text.strip()
                Stage_Koppel_Na_tuning = stage_sepcs_col_3[2].text.strip()
                Stage_Koppel_Resultaat = stage_sepcs_col_3[3].text.strip()
            except:
                Stage_Vermogen_Na_tuning='None'
                Stage_Vermogen_Origineel='None'
                Stage_Vermogen_Resultaat='None'
                Stage_Koppel_Na_tuning='None'
                Stage_Koppel_Resultaat='None'
                Stage_Koppel_Origineel='None'

            print("Stage_Koppel_Origineel : " + Stage_Koppel_Origineel)
            print("Stage_Koppel_Na_tunning :  " + Stage_Koppel_Na_tuning)
            print("Stage_Koppel_Resultaat : " + Stage_Koppel_Resultaat)
            print('~~~~~'*20)
            print("Stage_Vermogen_Origineel : " + Stage_Vermogen_Origineel)
            print("Stage_Vermogen_Na_tunning :  " + Stage_Vermogen_Na_tuning)
            print("Stage_Vermogen_Resultaat : " + Stage_Vermogen_Resultaat)

    #       ~~~~~~~~Extra~~~~~~~~~
            try:
                extra=soup.find('div',attrs={'data-target':'options'})
                ex_row= extra.find('div',attrs={'class':'specs'}).findAll('div',attrs={'class':'row'})[0].findAll('li')
                # print(ex_row)
                ex_data=[]
                for ex in ex_row:
                    ex_data.append(str(ex.text.strip())+', ')
            except:
                ex_data='None'
                pass
            print(ex_data)

            self.row = self.row + 1

            sheet1.write(self.row, 0,Merk)
            sheet1.write(self.row, 1,Model)
            sheet1.write(self.row, 2,type_data)
            sheet1.write(self.row, 3,motor_data)
            sheet1.write(self.row, 4,eco_Koppel_Origineel)
            sheet1.write(self.row, 5,eco_Koppel_Na_tuning)
            sheet1.write(self.row, 6,eco_Koppel_Resultaat)
            sheet1.write(self.row, 7,Stage_Vermogen_Origineel)
            sheet1.write(self.row, 8,Stage_Koppel_Origineel)
            sheet1.write(self.row, 9,Stage_Vermogen_Na_tuning)
            sheet1.write(self.row, 10,Stage_Koppel_Na_tuning)
            sheet1.write(self.row, 11,Stage_Vermogen_Resultaat)
            sheet1.write(self.row, 12,Stage_Koppel_Resultaat)
            sheet1.write(self.row, 13,stage2_ver_Origineel)
            sheet1.write(self.row, 14,stage2_Koppel_Origineel)
            sheet1.write(self.row, 15,stage2_ver_Na_tuning)
            sheet1.write(self.row, 16,stage2_Koppel_Na_tuning)
            sheet1.write(self.row, 17,stage2_ver_Resultaat)
            sheet1.write(self.row, 18,stage2_Koppel_Resultaat)
            sheet1.write(self.row, 19,ex_data)
            sheet1.write(self.row, 20, link)
            wb.save('../Files/ECU_opel.xls')
            print('Done ~ ' + str(self.row))
            import datetime

            print(datetime.datetime.now())
            print('~~~~~~'*100)

obj=ecu()
obj.get_link()